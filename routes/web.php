<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Guest Routes
Route::get('/', 'GuestController@index')->middleware('guest');
Route::get('/terms', 'GuestController@terms')->middleware('guest');

// Authentication Routes
Auth::routes();

// Authenticated Routes
Route::middleware(['auth', 'complete', 'disabled'])->group(function(){

    // Complete Registration
    Route::prefix('register')->name('register.')->group(function(){
        Route::get('profile', 'ProfileController@showCompleteProfile')->name('profile');
        Route::post('profile', 'ProfileController@updateCompleteProfile')->name('profile');
        Route::get('photo', 'ProfileController@showCompletePhoto')->name('photo');
        Route::post('photo', 'ProfileController@updateCompletePhoto')->name('photo');
    });

    // User
    Route::prefix('account')->group(function (){
        Route::get('active', 'UserController@active');
        Route::get('settings', 'UserController@edit')->name('user.settings');
        Route::post('settings', 'UserController@update')->name('user.update');
        Route::post('favorite/add', 'UserController@favorite');
        Route::get('favorite/add', function(){ abort(404); });
        Route::post('favorite/remove', 'UserController@unfavorite');
        Route::get('favorite/remove', function(){ abort(404); });
        Route::get('password', 'UserController@password')->name('user.password');
        Route::post('password/update', 'UserController@update_password')->name('user.password.update');
        Route::get('notifications', 'UserController@notifications')->name('user.notifications');
    });

    // Activity
    Route::get('activity', 'ActivityController@index')->name('activity');
    
    // Profile
    Route::get('browse', 'ProfileController@show')->name('browse');
    Route::post('browse', 'ProfileController@get')->name('browse');
    Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');
    Route::post('profile/edit', 'ProfileController@update')->name('profile.update');
    Route::get('waves', 'ProfileController@waves')->name('waves');
    Route::get('{username}', 'ProfileController@index')->where('username', '[A-Za-z0-9.]+')->name('profile');
    Route::post('{username}/wave', 'ProfileController@wave')->where('username', '[A-Za-z0-9.]+')->name('profile.wave');
    Route::post('block/add', 'ProfileController@add_block')->name('profile.block.add');
    Route::get('block/add', function(){ abort(404); });

    // Messages
    Route::prefix('messages')->group(function (){
        Route::get('inbox', 'MessageController@inbox')->name('messages.inbox');
        Route::post('inbox', 'MessageController@update')->name('messages.update');
        Route::get('sent', 'MessageController@sent')->name('messages.sent');
        Route::post('send', 'MessageController@send')->name('messages.send');
        Route::get('send', function(){ abort(404); });
        Route::get('trash', 'MessageController@trashed')->name('messages.trash');
    });

});

// API Routes
Route::middleware('auth')->group(function (){

    // Notifications
    Route::prefix('notifications')->group(function (){
        Route::post('markall', 'NotificationController@markAllAsRead')->name('notifications.markall');
        Route::get('markall', function(){ abort(404); });
    });

    // Waves
    Route::post('send', 'WaveController@send')->name('wave.send')->prefix('wave');

    // Giphy
    Route::prefix('giphy')->group(function (){
        Route::get('stickers/search/{query}', 'GiphyController@searchStickers');
        Route::get('search/{query}', 'GiphyController@searchGifs');
        Route::get('trending', 'GiphyController@trending');
        Route::get('stickers', 'GiphyController@stickers');
    });

    // Geo
    Route::prefix('geo')->group(function (){
        Route::get('search/{name}/{parent_id?}', '\Igaster\LaravelCities\GeoController@search');
        Route::get('item/{id}', '\Igaster\LaravelCities\GeoController@item');
        Route::get('items/{ids}', '\Igaster\LaravelCities\GeoController@items');
        Route::get('children/{id}', '\Igaster\LaravelCities\GeoController@children');
        Route::get('parent/{id}', '\Igaster\LaravelCities\GeoController@parent');
        Route::get('country/{code}', '\Igaster\LaravelCities\GeoController@country');
        Route::get('countries', '\Igaster\LaravelCities\GeoController@countries');
        Route::get('regions/{country}', '\Igaster\LaravelCities\GeoController@regions');
    });

    // Disable User
    Route::prefix('account')->group(function (){
        Route::get('disabled', 'UserController@disable')->name('user.disable');
        Route::post('disabled', 'UserController@enable');
    });

});
