let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 mix.webpackConfig({
    resolve: {
      alias: {
        jquery: "jquery/src/jquery"
      }
    },
  })
  .autoload({
    jquery: ['$', 'window.jQuery', 'jQuery'],
    'popper.js/dist/umd/popper.min.js': ['Popper']
  })
  .js('resources/assets/js/app.js', 'public/js')
  .extract([
    'jquery',
    'bootstrap',
    'fontfaceobserver'
  ])
  .js('resources/assets/js/utilities.js', 'public/js/utilities.js')
  .js('resources/assets/js/profile.js', 'public/js/profile.js')
  .sass('resources/assets/sass/style.scss', 'public/css')
  .sourceMaps()
  .version();