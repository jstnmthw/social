$(function () {
    window.Echo.private('notifications.' + user)
        .notification((notification) => {
            console.log(notification);
            if (url === notification.data.sender) {
                if (notification.type == 'App\\Notifications\\MessageIncoming') {
                    // Create message
                    var div = $('<div>', { class: 'message' });
                    if (notification.data.type === 0) {
                        var p = $('<p>', { class: 'bubble', text: notification.data.body });
                    }
                    if (notification.data.type === 1) {
                        var p = $('<p>', { class: 'bubble photo' });
                    }
                    if (notification.data.type === 2) {
                        var p = $('<p>', { class: 'bubble sticker' });
                    }
                    if (notification.data.type === 3) {
                        var p = $('<p>', { class: 'bubble gif' });
                    }
                    if (notification.data.type > 0) {
                        $('<img>').attr('src', notification.data.body).appendTo(p);
                    }
                    p.appendTo(div);
                    var avatar = $('<div>', { class: 'avatar' });
                    $('<img>', { src: notification.data.photo + '.jpg', alt: 'Profile Photo' }).appendTo(avatar);
                    avatar.appendTo(div);
                    $('<div>', { class: 'date', text: notification.data.date }).appendTo(div);
                    if ($('.messsages').length > 0) {
                        $('.messages').prepend(div);
                    } else {
                        $('#message-form').after().html(div);
                    }

                    // Notification Sound
                    var notifySound = document.createElement('audio');
                    notifySound.setAttribute('src', 'sounds/notification.mp3');
                    notifySound.setAttribute('autoplay', 'autoplay');
                    notifySound.addEventListener("load", function () {
                        notifySound.play();
                    }, true);

                }
                if (notification.type == 'App\\Notifications\\ProfileViewed') {
                    // viewed..
                }
            }
        });

    // Message Function
    function message(data) {
        $.ajax({
            url: '/messages/send',
            method: 'POST',
            data: data,
            dataType: 'JSON',
            beforeSend: function () {
                $('#message-submit').prop('diabled', true).addClass('disabled');
            }
        }).done(function (data) {
            $('.messages .empty').remove();
            var div = $('<div>', { class: 'message sender' });
            if (data.type == 0) {
                var p = $('<p>', { class: 'bubble', text: data.body });
            }
            if (data.type == 2) {
                var p = $('<p>', { class: 'bubble sticker' });
                $('<img>').attr('src', data.body).appendTo(p);
            }
            if (data.type == 3) {
                var p = $('<p>', { class: 'bubble gif' });
                $('<img>').attr('src', data.body).appendTo(p);
            }
            p.appendTo(div);
            $('<div>', { class: 'date', text: data.date }).appendTo(div);
            if ($('.messages').hasClass('d-none')) { $('.messages').removeClass('d-none'); }
            $('.messages').prepend(div);

        }).always(function () {
            $('#message-submit').prop('diabled', false).removeClass('disabled');
            $('#body').val('');
        });
    }

    // Photo Message
    function photo(data) {
        $.ajax({
            url: '/messages/send',
            method: 'POST',
            data: data,
            dataType: 'JSON',
            mimeType: "multipart/form-data",
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#message-submit').prop('diabled', true).addClass('disabled');
            }
        }).done(function (data) {
            $('.messages .empty').remove();
            var div = $('<div>', { class: 'message sender' }),
                p = $('<p>', { class: 'bubble photo' });
            $('<img>').attr('src', data.body).appendTo(p);
            p.appendTo(div);
            $('<div>', { class: 'date', text: data.date }).appendTo(div);
            if ($('.messages').hasClass('d-none')) { $('.messages').removeClass('d-none'); }
            $('.messages').prepend(div);

        }).always(function () {
            $('#message-submit').prop('diabled', false).removeClass('disabled');
        });
    }

    // Stickers & Gifs by Giphy
    function gifs(path, el, type, query = '') {
        $.ajax({
            url: '/giphy/' + path + '/' + query,
            method: 'GET',
            data: 'JSON',
            beforeSend: function () {
                el.html('<i class="fas fa-circle-notch fa-spin"></i><span class="sr-only">Loading...</span>');
            }
        }).done(function (giphy) {
            el.html('');
            $.each(giphy.data, function (i, value) {
                if (type == 2) {
                    el.append($('<img>', { class: 'sticker-submit', data: { 'type': type }, src: value.images.fixed_width_small.url }));
                }
                if (type == 3) {
                    el.append($('<img>', { class: 'gif-submit', data: { 'type': type }, src: value.images.downsized.url }));
                }
            })
        });
    }

    // Pull stickers on click
    var stickersGallery = $('#stickers-dd .gallery');
    $('#stickers-btn').click(function () {
        gifs('stickers', stickersGallery, 2);
    });

    // Pull gifs on click
    $('#gifs-btn').click(function () {
        var gifGallery = $('#gifs-dd .gallery');
        gifs('trending', gifGallery, 3);
    });

    // Clear search
    $('.gifs, .stickers').on('hidden.bs.dropdown', function () {
        $(this).find('.search-input').val('');
    });

    // Search stickers
    var sThread = null;
    $('#stickers-dd .search-input').keyup(function () {
        clearTimeout(sThread);
        var that = $(this);
        sThread = setTimeout(function () {
            if (that.val() != '') {
                gifs('stickers/search', $('#stickers-dd .gallery'), 2, that.val());
            }
        }, 500);
    });

    // Search gifs
    var gThread = null;
    $('#gifs-dd .search-input').keyup(function () {
        clearTimeout(gThread);
        var that = $(this);
        gThread = setTimeout(function () {
            if (that.val() != '') {
                gifs('search', $('#gifs-dd .gallery'), 3, that.val());
            }
        }, 500);
    });

    // Photo upload
    $('#photo').change(function () {
        if (document.getElementById('photo').files.length > 0) {
            var params = new FormData();
            params.append('_token', $('#csrf-token').val());
            params.append('receiver_id', $('#receiver_id').val());
            params.append('photo', $('#photo')[0].files[0]);
            params.append('type', 1);
            photo(params);
        }
    });

    // Image, sticker or gif submit
    $(document).on('click', '.image-submit, .sticker-submit, .gif-submit', function () {
        var params = {
            _token: $('#csrf-token').val(),
            receiver_id: $('#receiver_id').val(),
            body: $(this).attr('src'),
            type: $(this).data('type')
        };
        message(params);
    });

    // Message submit
    $('#message-submit').click(function (e) {
        var body = $('#body').val();
        if (body == '') {
            return false;
        }
        var params = {
            _token: $('#csrf-token').val(),
            receiver_id: $('#receiver_id').val(),
            body: body,
            type: 0
        };
        message(params);
        e.preventDefault();
    });

    // Wave 
    $('#wave-btn').click(function () {
        var btn = $(this);
        var params = {
            _token: $('#csrf-token').val(),
            receiver_id: $('#receiver_id').val(),
        };
        $.ajax({
            url: '/wave/send',
            method: 'POST',
            dataType: 'JSON',
            data: params,
            beforeSend: function () {
                btn.prop('disabled', true).addClass('disabled');
            }
        }).done(function (data) {
            if (data.success == true) {
                $('#wave-btn').prop('disabled', true).addClass('waved').html('Waved <i class="icon-check"></i>');
            }
        }).fail(function (data) {
            // failed..
        }).always(function (data) {
            // always..
            btn.prop('disabled', false).removeClass('disabled');
        });
    });

    // Add Favorite
    $('.actions').on('click', '#add-favorite', function () {
        var btn = $(this);
        var params = {
            _token: $('#csrf-token').val(),
            username: $('#username').val(),
        };
        $.ajax({
            url: '/account/favorite/add',
            method: 'POST',
            dataType: 'JSON',
            data: params,
            beforeSend: function () {
                btn.prop('disabled', true).addClass('disabled').text('').append('<i class="fas fa-sync fa-spin"></i>');
            }
        }).done(function (data) {
            if (data.success == true) {
                btn.remove();
                var remove_btn = $('<button>', { id: 'remove-favorite', class: 'btn btn-block btn-link', type: 'button', text: 'Remove Favorite' });
                $('.actions').append(remove_btn);
            }
        }).always(function (data) {
            btn.prop('disabled', false).removeClass('disabled');
        });
    });

    // Remove Favorite
    $('.actions').on('click', '#remove-favorite', function () {
        var btn = $(this);
        var params = {
            _token: $('#csrf-token').val(),
            username: $('#username').val(),
        };
        $.ajax({
            url: '/account/favorite/remove',
            method: 'POST',
            dataType: 'JSON',
            data: params,
            beforeSend: function () {
                btn.prop('disabled', true).addClass('disabled').text('').append('<i class="fas fa-sync fa-spin"></i>');
            }
        }).done(function (data) {
            if (data.success == true) {
                btn.remove();
                var add_btn = $('<button>', { id: 'add-favorite', class: 'btn btn-block btn-link', type: 'button', text: 'Add To Favorites' });
                $('.actions').append(add_btn);
            }
        }).always(function (data) {
            btn.prop('disabled', false).removeClass('disabled');
        });
    });

    // Adding photos
    $('#user-photos-form').submit(function (e) {
        var photo = $('#image-editor').cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: true,
            exportZoom: true
        });
        $('#photo').val(photo);
    });

    // Favorites Profile Photos
    $('.fav-profile-photo').each(function (i) {
        var src = $(this).data('src');
        $(this).css('background-image', 'url(' + src + ')');
    });

});