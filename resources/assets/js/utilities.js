/* 
 * Utilities for ThaiLovely.co
 */

// Font Observer
var FontFaceObserver = require('fontfaceobserver');

// Import Scripts
require('cropit');
require('bootstrap-growl');
require('blueimp-gallery/js/jquery.blueimp-gallery.js');

// Set cookie
function setCookie(cname, cvalue, expire) {
    var d = new Date();
    d.setTime(d.getTime() + (expire));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
window.setCookie = setCookie;

// Get cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
window.getCookie = getCookie;

// Font Observer
var fontA = new FontFaceObserver('Fira Sans');
var fontB = new FontFaceObserver('Roboto Slab');

Promise.all([fontA.load(), fontB.load()]).then(function () {
  document.documentElement.classList.add('fonts-loaded');
  setCookie('fonts_loaded');
});

// Animate.css Extension
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
        return this;
    }
});

// Process Submit
$(function(){
    $('.process-submit').on('click', function(e){
        e.preventDefault();
        var btn = $(this),
            f   = btn.closest('form'),
            n   = f.find('input'),
            w   = btn.outerWidth(),
            h   = btn.outerHeight(),
            v   = true,
            i   = $('<i>', { class: 'fas fa-circle-notch fa-spin'});

        n.each(function(i, e){
            if(e.checkValidity() == false) {
                v = false;
                console.log('Invalid');
            }
        });

        if(v !== false){
            btn.html(i).css({'width': w + 'px', 'height': h + 'px'}).attr('disabled',true);
            f.submit();
        }
    });
});