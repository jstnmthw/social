$(function(){
    // Zoom Options for Photo Edtior
    var up = true,
        down = false,
        value = 0,
        increment = 1,
        ceiling = 4;

    var zoomPoints = [0,0.25,0.50,0.75,1];

    function zoom(el, action){
        if(action == 'in') {
            if (up == true && value <= ceiling) {
                value += increment;
                down = true;
                el.cropit('zoom', zoomPoints[value]);
                if (value == ceiling) {
                    up = false;
                }
            }
        }
        if(action == 'out') {
            if (down == true && value >= 0) {
                value -= increment;
                up = true;
                el.cropit('zoom', zoomPoints[value]);
                if (value == 0) {
                    down = false;
                }
            }
        }
    }

    // Cropit
    var imageEditor = $('#image-editor');
        imageEditor.cropit();

    // When a photo is selected
    $('#upload-photo-input').change(function(){

        // If a file is selected
        if(document.getElementById('upload-photo-input').files.length > 0) {

            // Hide modal & disabled file input label
            $('#settings-photo-modal .options').hide();
            $('#settings-photo-modal .photo-editor').show();

            // Image editor
            $('.cropit-preview img').show();
            $('.add-photo-label').hide().find('span').removeClass('animated bounceIn').addClass('animated-hidden');
            $('.image-zoom-in, .image-zoom-out, .delete-photo').addClass('animated bounceIn');

            // Allow the upload
            $('#upload-btn').prop('disabled', false);

        };

    });
});