/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

// Laravel's Echo - Client side broadcasting
import Echo from "laravel-echo";
window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

/*******************************************************
 *  The following is used for global Javascript only.  *    
 *******************************************************/

// Region
window.region = function region() {
    var country = $('#country').val();
    $.ajax({
        type:'GET',
        url:'/geo/regions/'+country,
        beforeSend: function(){
            $('#region').prop('disabled', true).html('<option class="region-load">Loading...</option>');
        }
    }).done(function(data){
        $('#region').html('<option value="">-</option>');
        $.each(data, function(index, value) {
            $('#region').append('<option value="'+value.id+'">'+value.name+'</option>');
        });
    }).always(function(data){
        $('.region-load').remove();
        $('#region').prop('disabled', false);
    });
}
$('#country').on('change', function(){ region(); });

// Online Now Function
function checkActive(){
    if(!getCookie('active')){
        setCookie('active', true, 100000);
        $.get('/account/active');
    }
}
checkActive();
setInterval(function(){ checkActive(); }, 15000);

// Notifications
var uri = window.location.pathname.split( '/' );
window.Echo.private('notifications.'+user)
    .notification((notification) => {
        var msg;
        // Disable notification if already on profile
        if(uri[1] != notification.data.sender) {
            if(notification.type === 'App\\Notifications\\ProfileViewed') {
                msg = 'has viewed your profile.';
                growl(msg, notification);
            }
            if(notification.type === 'App\\Notifications\\MessageSent') {
                msg = 'sent you a message.';
                    growl(msg, notification);
            }
        }
        if(notification.type === 'App\\Notifications\\WaveSent') {
            msg = 'has waved at you. <i class="icon-wave"></i>';
            growl(msg, notification);
        }
        if(notification.type !== 'App\\Notifications\\MessageIncoming') {
            var $badge = $('#notifications-badge');
            var count = Number($badge.text()) + 1;
            $badge.text(count).removeClass('hidden');
            $('#notification-list .empty').remove();
            addNotification(msg, notification);
        }
    });

// Growl notification
function growl(msg, notification){
    // Create el object to prevent XSS
    var a   = $('<a>', { href: '/'+notification.data.sender}),
        img = $('<img>', { src: notification.data.photo+'.jpg', alt: "Profile Photo" }),
        st  = $('<strong>', { text: notification.data.sender, class: 'd-block' }),
        loc = $('<span>', { text: notification.data.location, class: 'd-block' }),
        m   = $('<span>', { text: msg, class: 'd-block' }),
        p   = $('<p>');

    // Combine everything
    p.prepend(m).prepend(loc);
    p.prepend(st);
    a.prepend(p);
    a.prepend(img);

    // Growl Settings
    $.bootstrapGrowl(a, { 
        class: 'notification', 
        offset: { from: 'bottom', amount: 20 },
        align: 'left',
        allow_dismiss: false,
        delay: 50000,
        width: 'auto'
    });
}

// Add new notification
function addNotification(msg, notification){
    if(uri[1] != notification.data.sender) { // Don't add notification when you're on the page.
        var el      = $('#notification-list'),
            li      = $('<li>', { class: 'unread' }),
            a       = $('<a>', { href: notification.data.sender, text: ' '+msg }),
            strong  = $('<strong>', { text: notification.data.sender }),
            span    = $('<span>', { class: 'date', text: notification.data.date });

        if(notification.data.photo == '/images/default_profile-40x40'){
            var img = $('<div>', { class: 'default-photo x36' }),
                i   = $('<i>', { class: 'icon-user' });
            img.append(i);
        }else {
            var img = $('<img>', { 
                class: 'notifications-user-photo', 
                src: notification.data.photo+'.jpg'
            });
        }
        a.prepend([img,strong]).append(span);
        li.prepend(a);
        el.prepend(li);
    }
}

// Mark all notifications as read
$('#markall').click(function(e){
    var params = {
        _token: token,
    };
    $.ajax({
        url: '/notifications/markall',
        method: 'POST',
        data: params,
        dataType: 'JSON'
    }).done(function(data){
        if(data.success == true){
            $('#notifications-badge').addClass('hidden');
            $('.notifications .dropdown-menu .unread').removeClass('unread');
        }
    }).fail(function(data){
        console.log('There was a problem marking all as read.');
    });
    e.stopPropagation();
});

// Alerts
$('.alert-success, .alert-danger').animateCss('bounceIn').removeClass('animated-hidden');