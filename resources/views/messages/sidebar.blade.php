<div class="sidebar">
    <div class="list-group">
        <a href="{{ route('messages.inbox') }}" class="list-group-item list-group-item-action inbox-item">
        	Inbox
        	@if ($msg_unread_count > 0)
        	<span class="badge badge-pill badge-info float-right">{{ $msg_unread_count }}</span>
        	@endif
        </a>
        <a href="{{ route('messages.sent') }}" class="list-group-item list-group-item-action sent-item">Sent</a>
        <a href="{{ route('messages.trash') }}" class="list-group-item list-group-item-action trash-item">Trash</a>
    </div>
</div>