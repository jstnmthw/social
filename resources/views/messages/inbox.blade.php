@extends('layouts.master')
@section('page_title', 'Inbox')
@section('body_class', 'inbox-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3 d-none d-md-block mb-md-3 mb-md-0">
                @include('messages.sidebar')
            </div>
            <div class="col-12 col-md-9">
                <div class="messages">
                @forelse ($msgs as $msg)
                    <a class="message @if(!$msg->read) unread @endif" href="/{{ $msg->sender->username }}">
                        <div class="photo mr-3">
                            @if ($msg->sender->profile->profile_photo)
                            <img src="{{ asset('uploads/'.$msg->sender->username.'/'.$msg->sender->profile->profile_photo.'_avatar.jpg') }}">
                            @else
                            <div class="default-photo x48">
                                <i class="icon-user"></i>
                            </div>
                            @endif
                            @if ((time() - 1200) < strtotime($msg->sender->last_active))
                            <i class="status-icon online"></i>
                            @endif
                        </div>
                        <div class="mr-3">
                            {{ $msg->sender->username }}
                        </div>
                        <div class="d-flex">
                            <div class="message-text text-ellipsis mr-auto">
                                @if($msg->replied)<i title="Replied" class="fas fa-reply ml-1"></i>@endif
                                @switch($msg->type)
                                    @case(0)
                                    {{ str_limit($msg->body, 48) }}
                                    @break
                                    @case(1)
                                    Send you a photo.
                                    @break
                                    @case(2)
                                    Sent you a sticker.
                                    @break
                                    @case(3)
                                    Sent you a gif.
                                    @break
                                @endswitch
                            </div>
                            <div class="date ml-3">
                                {{ $msg->created_at->diffForHumans() }}
                            </div>
                        </div>
                    </a>
                @empty
                @endforelse
                </div>
            </div>
        </div>
    </div>
</main>
@include('includes.footer')
@endsection

@section('js')
<script>
    $(function(){
        $("#check-all").click(function(){
            $('.messages-table tbody input:checkbox').prop('checked', this.checked);
        });
    });
</script>
@endsection