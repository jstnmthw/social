@extends('layouts.master')
@section('page_title', 'Trash')
@section('body_class', 'trash-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3 mb-3 mb-md-0">
                @include('messages.sidebar')
            </div>
            <div class="col-md-9">
                <div class="messages">
                    <form action="{{ route('messages.update') }}" method="POST">
                        {{ csrf_field() }}
                        <table class="table messages-table">
                            <thead class="thead-default">
                                <tr>
                                    <th colspan="5">
                                        <div class="custom-control float-left custom-checkbox m-0">
                                            <input type="checkbox" class="custom-control-input" id="check-all">
                                            <label class="custom-control-label" for="check-all"><span class="sr-only">Check All</span></label>
                                        </div>
                                        <button class="btn btn-outline-primary btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Actions
                                        </button>
                                        <div class="dropdown-menu">
                                            <button type="submit" name="untrash" class="dropdown-item" value="true">Move to Inbox</button>
                                            <button type="submit" name="delete" class="dropdown-item" value="true">Delete Permanently</button>
                                        </div>
                                    </th>
                                    <!-- <th>
                                        <nav aria-label="Page navigation example">
                                            <ul class="pagination pagination-sm">
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Previous">
                                                        <span aria-hidden="true"><i class="icon-arrow-left"></i></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                </li>
                                                <li class="page-item">
                                                    <a class="page-link" href="#" aria-label="Next">
                                                        <span aria-hidden="true"><i class="icon-arrow-right"></i></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($msgs as $msg)
                                <tr class="sender @if(!$msg->read) unread @endif">
                                    <td class="checkbox">
                                        <div class="custom-control custom-checkbox m-0">
                                            <input type="checkbox" class="custom-control-input" id="msg_{{ $msg->id }}" name="msg_ids[]" value="{{ $msg->id }}">
                                            <label class="custom-control-label" for="msg_{{ $msg->id }}"></label>
                                        </div>
                                    </td>
                                    <td class="photo">
                                        <a href="/{{ $msg->sender->username }}">
                                            @if ($msg->sender->profile->profile_photo)
                                            <img class="sender-photo" src="{{ asset('uploads/'.$msg->sender->username.'/'.$msg->sender->profile->profile_photo.'_avatar.jpg') }}">
                                            @else
                                            <div class="default-photo x48">
                                                <i class="icon-user"></i>
                                            </div>
                                            @endif
                                        </a>
                                    </td>
                                    <td class="username">
                                        <a href="/{{ $msg->sender->username }}">
                                            {{ $msg->sender->username }}
                                        </a>
                                    </td>
                                    <td class="message">
                                        <a href="/{{ $msg->sender->username }}">
                                        @switch($msg->type)
                                            @case(0)
                                            {{ str_limit($msg->body, 48) }}
                                            @break
                                            @case(1)
                                            Send you a photo.
                                            @break
                                            @case(2)
                                            Sent you a sticker.
                                            @break
                                            @case(3)
                                            Sent you a gif.
                                            @break
                                        @endswitch
                                        </a>
                                    </td>
                                    <td class="date">
                                        {{ $msg->created_at->diffForHumans() }} @if($msg->replied) <i title="Replied" class="fa fa-reply fa-flip-horizontal"></i>@endif
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5" class="empty">No Trash</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@include('includes.footer')
@endsection

@section('js')
<script>
    $(function(){
        $("#check-all").click(function(){
            $('.messages-table tbody input:checkbox').prop('checked', this.checked);
        });
    });
</script>
@endsection