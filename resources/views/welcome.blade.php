@extends('layouts.master')
@section('page_title', 'Sign Up Free')
@section('body_class', 'welcome-page')

@section('content')
<div id="main">
    <div class="hero-wrap">
        <div class="container">
            <header class="welcome-nav row justify-content-between">
                <h1 class="welcome-brand col-auto mr-auto">
                    <a href="/">
                        <svg id="logo-white" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32" xml:space="preserve">
                            <path fill="#ffffff" d="M24,3.3l-8,4.6L8,3.3L0,7.9l0,11.5l16,9.2l16-9.2V7.9L24,3.3z M16,24.1L4,17.2v-6.9l4-2.3l8,4.6l8-4.6l4,2.3 v6.9L16,24.1z" /></path>
                        </svg>
                        ThaiLovely.co
                    </a>
                </h1>
                <div class="col-auto account-signin">
                    <span class="d-none d-sm-inline">Have an account?</span> 
                    <a class="btn btn-outline btn-white" href="/login">Sign In</a>
                </div>
            </header>
            <section class="jumpotron">
                <h2 class="font-slab">Love in the land of smiles.</h2>
                <p class="lead">Sign up <strong>free</strong> today and browse Thailand’s largest online dating site.</p>
                <a class="signup-btn btn btn-lg btn-primary" href="/register">Sign Up Now</a>
                <p class="mt-3 no-cc">* No credit card required.</p>
            </section>
        </div>
    </div>
    <div class="app-wrap">
        <div class="container">
            <img src="{{ asset('images/apple-store-logo.svg') }}" alt="Apple Store Logo">
            <img src="{{ asset('images/google-store-logo.svg') }}" alt="Google Play Logo">
        </div>
    </div>
    <div class="description-wrap">
        <h3 class="sr-only">Site description.</h3>
        <div class="container">
            <div class="row">
                <div class="desc-item col-md-4">
                    <i class="icon-heart"></i>
                    <h4>Browse</h4>
                    <p>One of the largest online dating sites in South East Asia.</p>
                </div>
                <div class="desc-item col-md-4">
                    <i class="icon-chemistry"></i>
                    <h4>Chemistry</h4>
                    <p>Monthly meet-up events for a chance to let sparks fly face-to-face.</p>
                </div>
                <div class="desc-item col-md-4">
                    <i class="icon-bubbles"></i>
                    <h4>Chat Free</h4>
                    <p>Don't worry about being limited and losing a chance at love.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <p><?php echo date('Y'); ?> &copy; <a href="/" title="ThaiLovely.co">ThaiLovely.co</a>. All Rights Reserved.</p>
    <p><a href="/terms" title="Terms of Service">Terms</a> &bullet; <a href="/privacy" title="Privacy Policy">Privacy</a></p>
</footer>
@endsection