    <nav class="navbar navbar-expand-md fixed-top">
        <a class="navbar-brand font-slab" @auth href="/browse" @else href="/" @endauth>
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="20" viewBox="0 0 22 18"><path fill="#E56E6E" d="M17 0l-5.5 3.3L6 0 .5 3.3v8.2l11 6.5 11-6.5V3.3L17 0zm2.7 9.8l-8.2 4.9-8.3-4.9V4.9L6 3.3l5.5 3.3L17 3.3l2.7 1.6v4.9z"/><path fill="#BA4F4F" d="M.5 3.3l2.7 1.6v4.9L.5 11.5z"/><path fill="#F27E7E" d="M.5 3.3L6 0v3.3L3.2 4.9zM6 0l5.5 3.3v3.2L6 3.3z"/><path fill="#F98080" d="M11.5 3.3L17 0v3.3l-5.5 3.2zm11 0l-2.8 1.6L17 3.3V0z"/><path fill="#BA4F4F" d="M22.5 11.5l-2.8-1.7V4.9l2.8-1.6z"/><path fill="#E56E6E" d="M11.5 14.7l8.2-4.9 2.8 1.7-11 6.5zm-11-3.2l2.7-1.7 8.3 4.9V18z"/><path fill="#BA4F4F" d="M.5 3.3l2.7 1.6v4.9L.5 11.5z"/><path fill="#F27E7E" d="M.5 3.3L6 0v3.3L3.2 4.9zM6 0l5.5 3.3v3.2L6 3.3z"/><path fill="#F98080" d="M11.5 3.3L17 0v3.3l-5.5 3.2zm11 0l-2.8 1.6L17 3.3V0z"/><path fill="#BA4F4F" d="M22.5 11.5l-2.8-1.7V4.9l2.8-1.6z"/><path fill="#E56E6E" d="M11.5 14.7l8.2-4.9 2.8 1.7-11 6.5zm-11-3.2l2.7-1.7 8.3 4.9V18z"/></svg>
            ThaiLovely
        </a>

        @auth
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <i class="icon-menu"></i>
        </button> -->
        <!-- <div class="collapse navbar-collapse" id="navbarCollapse"> -->

            <!-- NAV LINKS -->
            <ul class="navbar-nav mr-auto justify-content-center justify-content-start">
                <li class="nav-item active">
                    <a class="nav-link nav-messages font-slab" href="{{ route('messages.inbox') }}">
                        <i class="far fa-comments"></i>
                        <span class="d-none d-md-inline">Messages</span>
                        @if ($msg_unread_count > 0)
                        <span class="badge badge-pill badge-info">{{ $msg_unread_count }}</span>
                        @endif
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-browse font-slab" href="{{ route('browse') }}">
                        <i class="far fa-stream"></i>
                        <span class="d-none d-md-inline">Browse</span>
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link font-slab" href="{{ route('waves') }}">Waves</a>
                </li> -->
                <!-- <li class="nav-item">
                    <a class="nav-link nav-activity font-slab" href="{{ route('activity') }}">
                        <i class="far fa-bolt"></i>
                        <span class="d-none d-md-inline">Activity</span>
                    </a>
                </li> -->
                <li class="nav-item d-md-none">
                    <a class="nav-link nav-activity font-slab" href="{{ route('activity') }}">
                        <i class="far fa-bell"></i>
                        <span class="d-none d-md-inline">Notifications</span>
                        <span id="notifications-badge" class="badge badge-pill badge-info @if ($notifications_unread_count == 0) hidden @endif">{{ $notifications_unread_count }}</span>
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link nav-settings font-slab" href="{{ route('profile.edit') }}">
                        <i class="far fa-cog"></i>
                        <span class="d-none d-md-inline">Settings</span>
                    </a>
                </li>
            </ul>

            <div class="d-flex align-items-center">

                <!-- USER PROFILE (PICTURE) -->
                <a class="user-btn btn nav-name" href="{{ route('profile', Auth::user()->username) }}" role="button">
                    @if (Auth::user()->profile->profile_photo)
                    <img src="/uploads/{{ Auth::user()->username }}/{{ Auth::user()->profile->profile_photo }}_avatar.jpg" alt="Profile Photo" class="nav-profile-pic">
                    @else
                    <span class="nav-profile-pic default-photo x32">
                        <i class="icon-user"></i>
                    </span>
                    @endif
                    {{ Auth::user()->username }}
                </a>

                <!-- NOTIFICATIONS (BELL) -->
                <div class="notifications dropdown d-none d-md-inline-block">
                    <a class="btn nav-btn" href="#" role="button" id="userNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Notifications</span>
                        <i class="far fa-bell"></i>
                        <span id="notifications-badge" class="badge badge-pill badge-info @if ($notifications_unread_count == 0) hidden @endif">{{ $notifications_unread_count }}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userNotification">
                        <div class="notifications-header">
                            <span class="dropdown-header ">Notifications</span>
                            <button id="markall" class="btn btn-link mark-read">Mark all as read</button>
                        </div>
                        <ul id="notification-list">
                            @forelse ($notifications as $notification)
                                <li class="@if (is_null($notification->read_at)) unread @endif" data-sender="{{ $notification->sender->username }}" data-notification-type="{{ $notification->sender->type }}">
                                    <a href="/{{ $notification->data['sender'] }}">
                                        @if ($notification->sender->profile_photo)
                                        <img src="/uploads/{{ $notification->sender->username .'/'. $notification->sender->profile_photo }}_avatar.jpg" class="notifications-user-photo" title="{{ $notification->sender->username }}'s profile photo.">
                                        @else
                                        <div class="default-photo x36" title="{{ $notification->sender->username }}'s profile photo."><i class="icon-user"></i></div> 
                                        @endif
                                        <strong>{{ $notification->data['sender'] }}</strong> {{ $notification->data['action'] }} 
                                        <span class="date">{{ $notification->created_at->diffForHumans() }}</span>
                                    </a>
                                </li>
                            @empty
                            <li class="empty">
                                No notifications
                            </li>
                            @endforelse
                        </ul>
                    </div>
                </div>

                <!-- SETTINGS (COG) -->
                <a class="btn nav-btn d-none d-md-inline-block" href="{{ route('profile.edit') }}"><i class="far fa-cog"></i></a>

                <!-- USERS OPTIONS (ARROW) -->
                <div class="user-actions dropdown">
                    <a id="ua-toggle" class="btn nav-btn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="icon-arrow-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="ua-toggle">
                        <h6 class="dropdown-header">Options</h6>
                        <a class="dropdown-item" href="{{ route('profile', Auth::user()->username) }}"><i class="icon-user"></i> View Profile</a>
                        <a class="dropdown-item" href="{{ route('profile.edit') }}"><i class="icon-pencil"></i> Edit Profile</a>
                        <a class="dropdown-item" href="{{ route('user.settings') }}"><i class="icon-settings"></i> Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="icon-logout"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>

            </div>

        <!-- </div> -->

        <!-- <div class="mobile-nav d-sm-block d-md-none">
            <ul class="nav justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('messages.inbox') }}">
                        <i class="far fa-comments"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('browse') }}">
                        <i class="far fa-stream"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
            </ul>
        </div> -->

        @endauth
    </nav>