<!-- Confirm delete profile modal -->
<div class="modal modal-no-borders" id="delete_profile-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header pb-3 pl-4 pr-4">
        <h5 class="modal-title mb-0 font-sans font-weight-sbold">Delete Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-0 pb-0 pl-4 pr-4">
        <!-- <h5 class="modal-title mb-2 font-sans font-weight-sbold">Delete Profile</h5> -->
        <p class="mb-0">Are you sure you want to permanetly delete your profile? <br> You will <strong>not</strong> be able to log back in with this account.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" name="disable" class="btn btn-danger">Delete Forever</button>
      </div>
    </div>
  </div>
</div>