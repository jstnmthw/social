<div class="modal profile-photo-modal" id="profile-photo-modal" tabindex="-1" role="dialog" aria-labelledby="photo-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="edit profile photo">
        <div class="modal-content photo-editor">
            <div class="modal-body">
                <div id="image-editor" class="photo-wrap">
                    <span class="default-photo x150">
                        <i class="icon-user"></i>
                    </span>
                    <div class="cropit-preview"></div>
                    <label class="add-photo-label" for="upload-photo-input">
                        <span class="icon-plus-circle icon-plus-circle-blue icon-plus-circle-lg"></span>
                    </label>
                    <button type="button" class="animated-hidden image-zoom-in">
                        <span class="icon-plus-circle icon-plus-circle-green icon-plus-circle-lg"></span>
                    </button>
                    <button type="button" class="animated-hidden image-zoom-out">
                        <span class="icon-minus-circle icon-minus-circle-green icon-plus-circle-lg"></span>
                    </button>
                    <button type="button" class="animated-hidden delete-photo">
                        <span class="icon-minus-circle icon-minus-circle-red"></span>
                    </button>
                    <input type="file" id="upload-photo-input" class="cropit-image-input" name="profile_photo" accept="image/jpeg">
                </div>
                <div class="form-group text-center mt-4">
                    <form id="add-photo-form" method="POST" enctype="multipart/form-data" action="{{ route('profile.update') }}">
                        {{ csrf_field() }}
                        <span class="upload-header mb-3 d-block font-slab">Upload Profile Photo</span>
                        <input id="photo" name="photo" type="hidden">
                        <input class="avatar" id="avatar" name="avatar" type="hidden">
                        <input id="upload-btn" class="btn btn-primary" name="upload_profile_photo" value="Upload Photo" type="submit" disabled>
                        <button class="btn btn-link" type="button" name="cancel" data-dismiss="modal" aria-label="Close">Cancel</button>
                        <p class="footnote">Nudity or overtly sexual photos are not allowed.</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>