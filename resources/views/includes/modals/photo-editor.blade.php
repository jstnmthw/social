<div class="modal" id="photo-editor-modal" tabindex="-1" role="dialog" aria-labelledby="photo-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="edit profile photo">
        <div class="modal-content photo-editor">
            <div class="modal-body">
                <div id="image-editor" class="photo-wrap">
                    <span class="default-photo x150">
                        <i class="icon-picture"></i>
                    </span>
                    <div class="cropit-preview"></div>
                    <label class="add-photo-label" for="upload-photo-input">
                        <span class="icon-plus-circle icon-plus-circle-blue icon-plus-circle-lg"></span>
                    </label>
                    <button type="button" class="animated-hidden image-zoom-in">
                        <span class="icon-plus-circle icon-plus-circle-green icon-plus-circle-lg"></span>
                    </button>
                    <button type="button" class="animated-hidden image-zoom-out">
                        <span class="icon-minus-circle icon-minus-circle-green icon-plus-circle-lg"></span>
                    </button>
                    <button type="button" class="animated-hidden delete-photo">
                        <span class="icon-minus-circle icon-minus-circle-red"></span>
                    </button>
                    <input type="file" id="upload-photo-input" class="cropit-image-input" name="profile_photo" accept="image/jpeg">
                </div>
                <div class="form-group text-center mt-4">
                    <form id="user-photos-form" method="POST" action="{{ route('profile.edit') }}">
                        {{ csrf_field() }}
                        <h2 class="upload-header mb-3">Upload A Photo</h2>
                        <input id="photo" name="photo" type="hidden">
                        <input id="upload-btn" class="btn btn-primary" name="upload_user_photo" value="Upload Photo" type="submit" disabled>
                        <button class="btn btn-link" type="button" name="cancel" data-dismiss="modal" aria-label="Close">Cancel</button>
                        <p class="footnote">Nudity or overtly sexual photos are not allowed.</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>