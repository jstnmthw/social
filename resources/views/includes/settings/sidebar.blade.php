			<div class="settings-sidebar">
                <a href="{{ route('profile.edit') }}" class="list-group-item list-group-item-action{{ Route::is('profile.edit') ? ' active' : ''}}">Edit Profile</a>
				<a href="{{ route('user.settings') }}" class="list-group-item list-group-item-action{{ Route::is('user.settings') ? ' active' : ''}}">Account Settings</a>
                <!-- <a href="#" class="list-group-item list-group-item-action{{ Route::is('account.premium') ? ' active' : ''}}">Premium Settings</a> -->
                <a href="{{ route('user.password') }}" class="list-group-item list-group-item-action{{ Route::is('user.password') ? ' active' : ''}}">Change Password</a>
                <a href="{{ route('user.notifications') }}" class="list-group-item list-group-item-action{{ Route::is('user.notifications') ? ' active' : ''}}">Notifications</a>
                <!-- <a href="#" class="list-group-item list-group-item-action{{ Route::is('accout.support') ? ' active' : ''}}">Support</a> -->
            </div>