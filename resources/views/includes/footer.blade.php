<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-auto">
                <a href="/" title="ThaiLovely" class="footer-logo font-slab">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="22" viewBox="0 0 22 18">
                        <path fill="#E56E6E" d="M17 0l-5.5 3.3L6 0 .5 3.3v8.2l11 6.5 11-6.5V3.3L17 0zm2.7 9.8l-8.2 4.9-8.3-4.9V4.9L6 3.3l5.5 3.3L17 3.3l2.7 1.6v4.9z"/>
                        <path fill="#BA4F4F" d="M.5 3.3l2.7 1.6v4.9L.5 11.5z"/>
                        <path fill="#F27E7E" d="M.5 3.3L6 0v3.3L3.2 4.9zM6 0l5.5 3.3v3.2L6 3.3z"/>
                        <path fill="#F98080" d="M11.5 3.3L17 0v3.3l-5.5 3.2zm11 0l-2.8 1.6L17 3.3V0z"/>
                        <path fill="#BA4F4F" d="M22.5 11.5l-2.8-1.7V4.9l2.8-1.6z"/>
                        <path fill="#E56E6E" d="M11.5 14.7l8.2-4.9 2.8 1.7-11 6.5zm-11-3.2l2.7-1.7 8.3 4.9V18z"/>
                        <path fill="#BA4F4F" d="M.5 3.3l2.7 1.6v4.9L.5 11.5z"/>
                        <path fill="#F27E7E" d="M.5 3.3L6 0v3.3L3.2 4.9zM6 0l5.5 3.3v3.2L6 3.3z"/>
                        <path fill="#F98080" d="M11.5 3.3L17 0v3.3l-5.5 3.2zm11 0l-2.8 1.6L17 3.3V0z"/>
                        <path fill="#BA4F4F" d="M22.5 11.5l-2.8-1.7V4.9l2.8-1.6z"/>
                        <path fill="#E56E6E" d="M11.5 14.7l8.2-4.9 2.8 1.7-11 6.5zm-11-3.2l2.7-1.7 8.3 4.9V18z"/>
                    </svg>
                    ThaiLovely.co
                </a>
                <nav class="footer-nav">
                    <ul>
                        <li><a href="about" title="About">About</a></li>
                        <li><a href="faq" title="FAQ">FAQ</a></li>
                        <li><a href="support" title="Support">Support</a></li>
                        <li><a href="abuse" title="Report Abuse">Report Abuse</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-12 col-lg-auto ml-lg-auto">
                <p class="copyright">&copy; Copyright {{ date('Y') }}. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>