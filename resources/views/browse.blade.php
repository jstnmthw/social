@extends('layouts.master')
@section('page_title', 'Browse')
@section('body_class', 'browse-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
    @if (session('status'))
        <div class="alert alert-success text-lg-center">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('status') }}
        </div>
        @endif
        <div class="row mb-4">
            <div class="col">
                <div class="card filter">
                    <div class="card-body">
                        <form class="search-form" method="POST" action="{{ route('browse') }}">
                            {{ csrf_field() }}
                            <div class="form-row">
                                <div class="form-group col">
                                    <label class="col-form-label">Country:</label>
                                    <select class="form-control custom-select" id="country" name="country">
                                    @foreach ($countries as $country)
                                        @if (session('search.country'))
                                            @if (session('search.country') == $country->id)
                                            <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                            @else
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                            @endif
                                        @elseif (Auth::user()->profile->country == $country->id)
                                        <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                        @else
                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group col">
                                    <label class="col-form-label">Province/State:</label>
                                    <select class="form-control custom-select" id="region" name="region">
                                        <option value="">-</option>
                                        @foreach ($regions as $region)
                                            @if (session('search.region'))
                                                @if (session('search.region') == $region->id)
                                                <option value="{{ $region->id }}" selected>{{ $region->name }}</option>
                                                @else
                                                <option value="{{ $region->id }}">{{ $region->name }}</option>
                                                @endif
                                            @elseif (Auth::user()->profile->region == $region->id)
                                            <option value="{{ $region->id }}" selected>{{ $region->name }}</option>
                                            @else
                                            <option value="{{ $region->id }}">{{ $region->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label class="col-form-label">Ages:</label>
                                    <div class="form-row">
                                        <div class="col-5">
                                            <select class="form-control custom-select" id="age_from" name="age_from">
                                                <option value="">Any</option>
                                                @for ($age = 18; $age <= 70; $age++)
                                                @if(session('search.age_from') == $age)
                                                <option value="{{ $age }}" selected>{{ $age }}</option>
                                                @else
                                                <option value="{{ $age }}">{{ $age }}</option>
                                                @endif
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-2 text-center age">
                                            <span>To</span>
                                        </div>
                                        <div class="col-5">
                                            <select class="form-control custom-select" id="age_to" name="age_to">
                                                <option value="">Any</option>
                                                @for ($age = 18; $age <= 70; $age++)
                                                    @if(session('search.age_to') == $age)
                                                    <option value="{{ $age }}" selected>{{ $age }}</option>
                                                    @else
                                                    <option value="{{ $age }}">{{ $age }}</option>
                                                    @endif
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-2">
                                    <label class="col-form-label">Distance:</label>
                                    <select class="d-block custom-select" id="distance" name="distance">
                                        @foreach ($distances as $i => $distance)
                                            @if(session('search.distance') == $i)
                                            <option value="{{ $i }}" selected>{{ $distance }}km</option>
                                            @else
                                            <option value="{{ $i }}">{{ $distance }}km</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col align-self-end text-right">
                                    <!-- <input type="submit" class="btn btn-primary btn-block process-submit" value="Search"> -->
                                    <button type="submit" class="search-btn btn btn-primary btn-block process-submit">
                                        Search 
                                        <i class="fas fa-search fa-sm" data-fa-transform="flip-h"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if (count($profiles))
        <div class="row browse-deck">
            @foreach ($profiles as $profile)
            <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
                <div class="card">
                    @if ($profile->profile_photo)
                    <a class="card-img-top" href="/{{ $profile->username }}" title="{{ $profile->username }}'s Profile">
                        <img src="/uploads/{{ $profile->username.'/'.$profile->profile_photo.'_browse.jpg' }}" alt="Card image cap">
                    </a>
                    @else
                    <a class="card-img-top" href="/{{ $profile->username }}" title="{{ $profile->username }}'s Profile">
                        <div class="card-img-top default-photo x150">
                            <i class="icon-user"></i>
                        </div>
                    </a>
                    @endif
                    <div class="card-body">
                        <h4 class="card-title d-inline-block"><a href="{{ url($profile->username) }}" title="{{ $profile->username }}'s profile">{{ $profile->username }}</a></h4>
                        <div class="age">{{ $profile->age }}</div>
                        <div class="d-flex justify-content-between">
                            <small class="text-muted">
                                @if ((time() - 1200) < strtotime($profile->last_active))
                                <i class="status-icon online"></i> Online now
                                @else
                                <i class="status-icon"></i>  {{ $profile->last_active }}
                                @endif
                            </small>
                            <div class="region">
                                @foreach ($regions as $region)
                                    @if ($profile->region == $region->id)
                                    {{ $region->name }}
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- <div class="card-footer">
                        <small class="text-muted">
                        @if ((time() - 1200) < strtotime($profile->last_active))
                        <i class="status-icon online"></i> Online now
                        @else
                        <i class="status-icon"></i>  {{ $profile->last_active }}
                        @endif
                        </small>
                    </div> -->
                </div>
            </div>
            @endforeach
        </div>
        @if ($profiles->perPage() < $profiles->total())
        <div class="row">
            <div class="col-12">
                {{ $profiles->links('vendor.pagination.bootstrap-4') }}
            </div>
        </div>
        @endif
    @else
        <div class="row empty-results">
            <div class="col text-center">
                <h4>Sorry, we couldn't find any matches for you.</h4>
                <p class="text-muted">Tip: Have you tried to broaden your search criteria?</p>
            </div>
        </div>
    @endif
    </div>
</main>
@include('includes.footer')
@endsection

@section('js')
<script>
    $(function () {
        // Alert
        $('.alert-success, .alert-danger').animateCss('bounceIn').removeClass('animated-hidden');

        // TEMP: Generate avatar photos
        // var profileCount = $('.browse-deck > div').length;
        // $.ajax({
        //     url: 'https://randomuser.me/api/?gender=female&results='+profileCount+'&noinfo',
        //     dataType: 'json',
        //     success: function(data) {
        //         if(data.results) {
        //             $('.browse-deck > div').each(function(i){
        //                 $(this).find('.card-img-top').html($('<img>', { src: data.results[i].picture.large }));
        //             });
        //         }else {
        //             return false;
        //         }
        //     }
        // });
    })
</script>
@endsection