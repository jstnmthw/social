@extends('layouts.master')
@section('page_title', 'Activity')
@section('body_class', 'activity-page')

@section('content')
@include('includes.nav')
<main id="main">
	<div class="container">
		@if($activities)
		<ul class="activites-list">
			@foreach($activities as $activity)
			<li>
				<a href="/{{ $activity['sender'] == 'You' ? $activity['receiver'] : $activity['sender'] }}">
					@if($activity['photo'])
					<img class="profile-photo mr-3 float-left" src="/uploads/{{ $activity['sender'] == 'You' ? Auth::user()->username : $activity['sender'] }}{{ '/'. $activity['photo'] .'_avatar.jpg' }}" alt="Photo">
					@else
					<span class="float-left default-photo x48 mr-3">
						<i class="icon-user"></i>
					</span>
					@endif
					<p>
						@if($activity['sender'] != 'You')
						<strong>{{ $activity['sender'] }}</strong>
						@else
						{{ $activity['sender'] }}
						@endif
						{!! $activity['action'] !!}
						<datetime><i class="fas fa-{{ $activity['icon'] }}"></i> <span class="ml-1 mr-1">&middot;</span> {{ $activity['when'] }}</datetime>
					</p>
				</a>
			</li>
			@endforeach
		</ul>
		@else
		No activites
		@endif
	</div>
    {{ $activities->links('vendor.pagination.bootstrap-4') }}
</main>
@include('includes.footer')
@endsection

@section('footer_js')
@endsection