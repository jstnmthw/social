@extends('layouts.master')
@section('page_title', 'Account Settings')
@section('body_class', 'settings-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
        <div class="row no-gutters settings-wrap">
            <div class="col-md-3">
                @include('includes.settings.sidebar')
            </div>
            <div class="col-md-9 settings-content">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif 
                @if (session('status'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('status') }}
                </div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <form id="edit-form" class="settings-form control-form" method="POST" action="{{ route('user.update') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Username</label>
                                <div class="col-md-7">
                                    <input id="username" class="form-control" type="text" value="{{ Auth::user()->username }}" readonly disabled>
                                    <small id="usernameHelp" class="form-text text-muted">Only <a href="{{ url('premium') }}" title="Premium Account">premium</a> accounts can change their username.</small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">E-mail</label>
                                <div class="col-md-7">
                                    <input id="email" name="email" class="form-control" type="text" value="{{ Auth::user()->email }}" readonly disabled>
                                </div>
                            </div>
                            <div class="form-group row settings-block-list">
                                <label class="col-md-3 control-form">Blocked Users</label>
                                <div class="col-md-9">
                                    <ul>
                                        @forelse ($user->blocked as $blocked)
                                        <li>
                                            {{ $blocked->username }} [<button type="submit" class="btn btn-unstyled" value="{{ $blocked->username }}" name="unblock">unblock</button>]
                                        </li>
                                        @empty
                                        <li class="text-muted">
                                            You haven't added anyone to your block list.
                                        </li>
                                        @endforelse
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group row align-items-start">
                                <label class="col-md-3 control-form">Disable Profile</label>
                                <div class="col-md-7">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-unstyled" data-toggle="modal" data-target="#disable_profile-modal">
                                        Temporarily Disable
                                    </button>
                                    <br>
                                    <button type="button" class="btn btn-unstyled text-danger" name="delete" data-toggle="modal" data-target="#delete_profile-modal">
                                        Delete Account
                                    </button>
                                    <!-- <button type="submit" class="btn btn-unstyled" name="disable">Temporarily Disable</button><br> -->
                                    <!-- <button type="submit" class="btn btn-unstyled text-danger" name="delete">Delete Account</button> -->
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto mt-4">
                                    <input class="btn btn-primary submit-edits" type="submit" value="Submit" name="user_settings">
                                </div>
                            </div>
                            <!-- Copy/Paste Form Group
                            <div class="form-group row">
                                <label class="col-md-4 control-form"></label>
                                <div class="col-md-8">
                                </div>
                            </div>
                            -->
                            @include('includes.modals.delete-profile')
                            @include('includes.modals.disable-profile')
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('includes.footer')
@endsection