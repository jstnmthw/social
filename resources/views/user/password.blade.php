@extends('layouts.master')
@section('page_title', 'Change Password')
@section('body_class', 'settings-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
        <div class="row no-gutters settings-wrap">
            <div class="col-md-3">
                @include('includes.settings.sidebar')
            </div>
            <div class="col-md-9 settings-content">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                @endif
                @if (session('status'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('status') }}
                </div>
                @endif

                <div class="row justify-content-center mt-md-4">
                    <div class="col-md-10">
                        <form id="edit-form" class="settings-form control-form" method="POST" action="{{ route('user.password.update') }}">
                            {{ csrf_field() }}
                            <div class="form-group row mb-md-5">
                                <label class="col-md-4 control-form">Current Password</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="password" name="current_password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 control-form">New Password</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="password" name="new_password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 control-form">Confirm Password</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="password" name="new_password_confirmation">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-8 ml-md-auto">
                                    <input class="btn btn-primary submit-edits" type="submit" value="Submit">
                                </div>
                            </div>
                            <!-- Copy/Paste Form Group
                            <div class="form-group row">
                                <label class="col-md-4 control-form"></label>
                                <div class="col-md-8">
                                </div>
                            </div>
                            -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('includes.footer')
@endsection

@section('js')
<script src="/js/image-editor.js"></script>
<script>
    $(function(){
        // Alert
        $('.alert-success').animateCss('bounceIn').removeClass('animated-hidden');
        $('.alert-danger').animateCss('shake').removeClass('animated-hidden');
    });
</script>
@endsection