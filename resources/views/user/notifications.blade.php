@extends('layouts.master')
@section('page_title', 'Notifications')
@section('body_class', 'settings-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
        <div class="row no-gutters settings-wrap">
            <div class="col-md-3">
                @include('includes.settings.sidebar')
            </div>
            <div class="col-md-9 settings-content">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('status'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('status') }}
                </div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <form class="settings-form control-form" method="POST" action="{{ route('user.update') }}">
                            {{ csrf_field() }}
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">New Messages Emails</label>
                                <div class="col-md-8 d-flex align-items-center">
                                    <div class="switch switch-md">
                                        <label>
                                            <input type="checkbox" name="msg_notification" {{ Auth::user()->msg_notification ? 'checked' : '' }}>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-9 ml-md-auto mt-4">
                                    <input class="btn btn-primary submit-edits" type="submit" value="Submit" name="notification_settings">
                                </div>
                            </div>
                            <!-- Copy/Paste Form Group
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"></label>
                                <div class="col-md-8">
                                </div>
                            </div>
                            -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('includes.footer')
@endsection