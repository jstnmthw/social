@extends('layouts.master')
@section('page_title', 'Account Disabled')
@section('body_class', 'auth-page')

@section('content')
<div class="auth-wrap">
    <main>
        <form method="POST" action="{{ route('user.disable') }}" class="auth-form pt-4 pb-4 text-center">
            <h1 class="logo mb-3">
                <a href="/">
                    @php echo file_get_contents('images/logo.svg') @endphp
                    ThaiLovely.co
                </a>
            </h1>
            <p class="mb-3">Your account is currently disabled.</p>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-block btn-primary process-submit">Re-enable Account</button>
            <a class="btn btn-unstyled mt-3" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Go back
            </a>
        </form>
    </main>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
@endsection

@section('footer_js')
@endsection