@extends('layouts.master')
@section('page_title', 'Edit Profile')
@section('body_class', 'settings-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
        <div class="row no-gutters settings-wrap">
            <div class="col-md-3">
                @include('includes.settings.sidebar')
            </div>
            <div class="col-md-9 settings-content">
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible animated-hidden" data-dismiss="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
                @endif
                @if (session('status'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ session('status') }} <a class="alert-link" href="/{{ Auth::user()->username }}" title="View Profile">View Profile</a>
                </div>
                @endif
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <form id="edit-form" class="settings-form" method="POST" enctype="multipart/form-data" action="{{ route('profile.update') }}">
                            {{ csrf_field() }}

                            <div class="form-group row settings-photo">
                                <div class="col-md-4">
                                    <button type="button" class="btn-photo" data-toggle="modal" data-target="#settings-photo-modal">
                                        @if (is_null(Auth::user()->profile->profile_photo))
                                        <span class="default-photo x80">
                                            <i class="icon-user"></i>
                                        </span>
                                        @else
                                        <img class="profile-photo" alt="Your profile photo" src="/uploads/{{ Auth::user()->username .'/'. Auth::user()->profile->profile_photo }}_avatar.jpg">
                                        @endif
                                    </button>
                                </div>
                                <div class="col-md-6">
                                    <h2>{{ Auth::user()->username }}</h2>
                                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#settings-photo-modal">Edit Profile Photo</button>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Gender</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" readonly disabled value="{{ ucfirst($genders[Auth::user()->profile->sex]) }}">
                                    <!-- <select id="sex" name="sex" class="custom-select select-fluid">
                                    @foreach ($genders as $i => $gender)
                                        @if (Auth::user()->profile->sex == $i)
                                        <option value="{{ $i }}" selected>{{ ucfirst($gender) }}</option>
                                        @else
                                        <option value="{{ $i }}">{{ ucfirst($gender) }}</option>
                                        @endif
                                    @endforeach
                                    </select> -->
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Interested In</label>
                                <div class="col-md-6">
                                    <select id="orientation" name="orientation" class="custom-select select-fluid">
                                    @foreach ($genders as $i => $gender)
                                        @if (Auth::user()->profile->orientation == $i)
                                        <option value="{{ $i }}" selected>{{ ucfirst($gender) }}s</option>
                                        @else
                                        <option value="{{ $i }}">{{ ucfirst($gender) }}s</option>
                                        @endif
                                    @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Height</label>
                                <div class="col-md-6">
                                    <select id="height" name="height" class="custom-select select-fluid">
                                        <option value="" selected>-</option>
                                        @for ($i = 124; $i <= 226; $i++)
                                            @php 
                                                $inches = $i/2.54; 
                                                $feet = intval($inches/12); 
                                                $inches = $inches%12; 
                                            @endphp
                                            @if (Auth::user()->profile->height == $i)
                                            <option value="{{ $i }}" selected>{{ $i }}cm / {{ $feet }}'{{ $inches }}"</option>
                                            @else
                                            <option value="{{ $i }}">{{ $i }}cm / {{ $feet }}'{{ $inches }}"</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Weight</label>
                                <div class="col-md-6">
                                    <select id="weight" name="weight" class="custom-select select-fluid">
                                        <option value="">-</option>
                                        @for ($i = 40; $i <= 139; $i++) 
                                            @php
                                                $lbs = floor($i / 0.45359237)
                                            @endphp
                                            @if (Auth::user()->profile->weight == $i)
                                            <option value="{{ $i }}" selected>{{ $i }}kgs / {{ $lbs }}lbs</option>
                                            @else
                                            <option value="{{ $i }}">{{ $i }}kgs / {{ $lbs }}lbs</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('country')  ? ' has-error' : '' }}">
                                <label class="col-md-4 col-form-label">Country</label>
                                <div class="col-md-6">
                                    <select id="country" name="country" class="custom-select select-fluid">
                                        @foreach ($countries as $i => $country)
                                            @if(Auth::user()->profile->country == $country->id)
                                            <option value="{{ $country->id }}" selected>{{ $country->name }}</option>
                                            @else
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row{{ $errors->has('region') ? ' has-error' : '' }}">
                                <label class="col-md-4 col-form-label">Region/State</label>
                                <div class="col-md-6">
                                    <select id="region" name="region" class="custom-select select-fluid">
                                        @foreach ($regions as $region)
                                            @if ($region->id == Auth::user()->profile->region)
                                            <option value="{{ $region->id }}" selected>{{ $region->name }}</option>
                                            @else
                                            <option value="{{ $region->id }}">{{ $region->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Bio</label>
                                <div class="col-md-7">
                                    <textarea class="form-control" id="bio" name="bio" rows="3" placeholder="Tell us a little about yourself." maxlength="120">{{ Auth::user()->profile->bio }}</textarea>
                                    <div class="letter-counter-wrap">
                                        <span class="letter-counter">0/120</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-2 mt-5">
                                <div class="col-md-8 ml-md-auto">
                                    <span class="sub-label">Social Accounts</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Instagram</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="instagram" value="{{ Auth::user()->profile->instagram }}">
                                    <small class="form-text">
                                        instagram.com/<strong>johnsmith</strong>
                                    </small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Facebook</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="facebook" value="{{ Auth::user()->profile->facebook }}">
                                    <small class="form-text">
                                        facebook.com/<strong>johnsmith</strong>
                                    </small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Twitter</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="twitter" value="{{ Auth::user()->profile->twitter }}">
                                    <small class="form-text">
                                        twitter.com/<strong>johnsmith</strong>
                                    </small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-8 ml-md-auto">
                                    <button class="btn btn-primary submit-edits" type="submit" name="profile_settings">Submit</button>
                                </div>
                            </div>

                            <!-- Copy/Paste Form Group
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label"></label>
                                <div class="col-md-6">
                                </div>
                            </div>
                            -->

                            <div class="modal settings-photo-modal" id="settings-photo-modal" tabindex="-1" role="dialog" aria-labelledby="photo-modal-label" aria-hidden="true">
                                <div class="modal-dialog" role="edit profile photo">
                                    <div class="modal-content options">
                                        <div class="modal-header">
                                            <span class="modal-title" id="photo-modal-label">Edit Profile Photo</span>
                                        </div>
                                        <div class="modal-body">
                                            <ul class="list-group">
                                                <li class="list-group-item">
                                                    <button type="submit" name="remove_profile_photo" @if (is_null(Auth::user()->profile->profile_photo)) echo disabled @endif>Remove Current Photo</button>
                                                </li>
                                                <li class="list-group-item">
                                                    <label for="upload-photo-input">Upload Photo</label>
                                                </li>
                                                <li class="list-group-item">
                                                    <button type="button" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="modal-content photo-editor">
                                        <div class="modal-body">
                                            <div id="image-editor" class="photo-wrap">
                                                <span class="default-photo x150">
                                                    <i class="icon-user"></i>
                                                </span>
                                                <div class="cropit-preview"></div>
                                                <label class="add-photo-label" for="upload-photo-input">
                                                    <span class="icon-plus-circle icon-plus-circle-blue icon-plus-circle-lg"></span>
                                                </label>
                                                <button type="button" class="animated-hidden image-zoom-in">
                                                    <span class="icon-plus-circle icon-plus-circle-green icon-plus-circle-lg"></span>
                                                </button>
                                                <button type="button" class="animated-hidden image-zoom-out">
                                                    <span class="icon-minus-circle icon-minus-circle-green icon-plus-circle-lg"></span>
                                                </button>
                                                <button type="button" class="animated-hidden delete-photo">
                                                    <span class="icon-minus-circle icon-minus-circle-red"></span>
                                                </button>
                                                <input type="file" id="upload-photo-input" class="cropit-image-input" name="profile_photo" accept="image/jpeg">
                                            </div>
                                            <div class="form-group text-center mt-4">
                                                <span class="upload-header mb-3 d-block font-slab">Upload Profile Photo</span>
                                                <input id="photo" name="photo" type="hidden">
                                                <input class="avatar" id="avatar" name="avatar" type="hidden">
                                                <input id="upload-btn" class="btn btn-primary" name="upload_profile_photo" value="Upload Photo" type="submit" disabled>
                                                <button class="btn btn-link" type="button" name="cancel" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                <p class="footnote">Nudity or overtly sexual photos are not allowed.</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('includes.footer')
@endsection

@section('js')
<script src="/js/image-editor.js"></script>
<script>
    $(function(){

        // Alert
        $('.alert-success, .alert-danger').animateCss('bounceIn').removeClass('animated-hidden');

        // Letter Counter
        var max = 120;
        $('#bio').on('keyup', function(){
            var count = $(this).val().length;
            if(count <= max){
                $('.letter-counter').text(count + '/'+max);
            }
            if (count == max) {
                $('.letter-counter').addClass('maxed');
            }else {
                $('.letter-counter').removeClass('maxed');
            }
        });

        // Form Submit
        $('#edit-form').submit(function(e){
            // Ready up photos
            var photo = $('#image-editor').cropit('export', {
                type: 'image/jpeg',
                quality: 1,
                originalSize: true,
                exportZoom: false
            });
            var avatar = $('#image-editor').cropit('export', {
                type: 'image/jpeg',
                quality: 1,
                originalSize: false
            });
            $('#photo').val(photo);
            $('#avatar').val(avatar);
        });

        // Reset modal on cancel
        $('#settings-photo-modal').on('hidden.bs.modal', function (e) {
            $('.options', this).show();
            $('.photo-editor', this).hide();
            // $('#photo-editor-modal').modal('show');
        });

    });
</script>
@endsection