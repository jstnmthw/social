@extends('layouts.master')
@section('page_title', 'Your Waves')
@section('body_class', 'waves-page')

@section('content')
    <div class="container">
        <div class="row deck">
            @forelse($profiles as $profile)
            <div class="col-xs-12 col-sm-6 col-md-4 col-xl-3">
                <div class="card">
                    <a class="card-img-top" href="/{{ $profile->username }}" title="{{ $profile->username }}'s Profile">
                        <img src="{{ $profile->photo }}.jpg">
                    </a>
                    <div class="card-body">
                        <h4 class="card-title d-inline-block"><a href="{{ url($profile->username) }}" title="{{ $profile->username }}'s profile">{{ $profile->username }}</a></h4>
                        <div class="age">{{ $profile->age }}</div>
                        <div class="region">
                            {{ $profile->region }}, {{ $profile->country }}
                        </div>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">
                        @if ((time() - 1200) < strtotime($profile->last_active))
                            <i class="status-icon online"></i> Online now
                        @else
                            <i class="status-icon"></i>  {{ $profile->last_active }}
                        @endif
                        </small>
                    </div>
                </div>
            </div>
            @empty
            <div class="col text-center mt-lg-5">
                <h3>Oops, looks like you have no waves.</h3>
                <p class="lead">Why not try waving to some other people?</p>
            </div>
            @endforelse
        </div>
    </div>
@endsection

@section('footer_js')
@endsection