@extends('layouts.master')
@section('page_title', $user->username)
@section('body_class', 'profile-page')

@section('content')
@include('includes.nav')
<main id="main">
    <div class="container">
        <div class="row">
            <div class="col sidebar">
                <div class="profile-details mb-3">
                    @if ($owner == true)
                    <a href="{{ route('profile.edit') }}" class="edit-profile-link">
                        <span class="sr-only">Edit Profile</span><i class="icon-settings"></i>
                    </a>
                    @endif
                    <div class="profile-photo">
                        @if ((time() - 1200) < strtotime($user->last_active))
                        <i title="Online" class="status-icon online"></i>
                        @endif
                        @if ($profile->profile_photo)
                        <a href="{{ route('profile.edit') }}">
                            <img src="/uploads/{{ $user->username }}/{{ $user->profile->profile_photo }}_avatar.jpg" alt="Profile Photo">
                        </a>
                        @else
                        <div class="default-photo x150">
                            <i class="icon-user"></i>
                            @if($owner)
                            <a class="add-photo-btn" href="{{ route('profile.edit') }}" alt="Add Button">
                                <i class="fas fa-plus"></i>
                            </a>
                            <!-- <button class="add-photo-btn" href="{{ route('profile.edit') }}" alt="Add Photo" for="upload_profile_photo" data-toggle="modal" data-target="#profile-photo-modal">
                                <i class="fas fa-plus"></i>
                            </button> -->
                            <!-- <form id="add-photo-form" method="POST" enctype="multipart/form-data" action="{{ route('profile.update') }}">
                                {{ csrf_field() }}
                                <label class="add-photo-btn" href="{{ route('profile.edit') }}" alt="Add Photo" for="upload_profile_photo">
                                    <i class="fas fa-plus"></i>
                                </label>
                                <input type="hidden" value="test" name="upload_profile_photo">
                                <input type="file" name="photo" id="upload_profile_photo" class="d-none">
                            </form> -->
                            @endif
                        </div>
                        @endif
                    </div>
                    <h2 class="username">
                        {{ $user->username }}
                    </h2>
                    <span class="location">
                        <i class="icon-location-pin"></i>{{ $profile->region }}, {{ $profile->country }}
                    </span>
                    @if ($user->profile->bio)
                    <p class="headline">
                        {{ $user->profile->bio }}
                    </p>
                    @endif
                    <ul class="social-media">
                        @if(isset($profile->instagram))
                        <li>
                            <a href="{{ Config::get('constants.social_media_urls.instagram') }}/{{ $profile->instagram }}" target="_blank" title="Instagram">
                                <i class="fab fa-instagram"></i> 
                                <span class="sr-only">Instagram</span>
                            </a>
                        </li>
                        @else
                        <li>
                            <i class="fab fa-instagram" aria-hidden="true" title="Instagram"></i> 
                            <span class="sr-only">Instagram</span>
                        </li>
                        @endif
                        @if(isset($profile->facebook))
                        <li>
                            <a href="{{ Config::get('constants.social_media_urls.facebook') }}/{{ $profile->facebook }}" target="_blank" title="Facebook">
                                <i class="fab fa-facebook-f"></i> 
                                <span class="sr-only">Facebook</span>
                            </a>
                        </li>
                        @else
                        <li>
                            <i class="fab fa-facebook" aria-hidden="true" title="Facebook"></i> 
                            <span class="sr-only">Facebook</span>
                        </li>
                        @endif
                        @if(isset($profile->twitter))
                        <li>
                            <a href="{{ Config::get('constants.social_media_urls.twitter') }}/{{ $profile->twitter }}" target="_blank" title="Twitter">
                                <i class="fab fa-twitter"></i> 
                                <span class="sr-only">Twitter</span>
                            </a>
                        </li>
                        @else
                        <li>
                            <i class="fab fa-twitter" aria-hidden="true"></i> 
                            <span class="sr-only">Twitter</span>
                        </li>
                        @endif
                    </ul>
                    @if($owner == false)
                    <div class="form-row actions">
                        @if($user->profile->waved)
                        <span class="btn btn-block btn-wave waved">
                            Waved <i class="icon-check"></i>
                        </span>
                        @else
                        <button id="wave-btn" class="btn btn-block btn-wave" type="button">
                            Wave <i class="icon-wave"></i>
                        </button>
                        @endif
                        @if($user->favorited)
                        <button id="remove-favorite" class="btn btn-block btn-link">
                            Remove Favorite
                        </span>
                        @else
                        <button id="add-favorite" class="btn btn-block btn-link" type="button">
                            Add To Favorites
                        </button>
                        @endif
                    </div>
                    @endif
                </div>
                <div class="user-info mb-3">
                    <div class="user-info-title border-strike">
                        <span>Details</span>
                    </div>
                    <dl class="inline-flex">
                        <dt>Age</dt>
                        <dd>{{ $profile->age }} years</dd>
                        <dt>Gender</dt>
                        <dd>{{ $profile->sex }}</dd>
                        <dt>Interested In</dt>
                        <dd>{{ $profile->orientation }}</dd>
                        <dt>Height</dt>
                        <dd>{{ $profile->height }}</dd>
                        <dt>Weight</dt>
                        <dd>{{ $profile->weight }}</dd>
                        <dt>Location</dt>
                        <dd>{{ $profile->region }}, {{ $profile->country }}</dd>
                        <dt>Last Active</dt>
                        <dd>
                        @if ((time() - 1200) < strtotime($user->last_active))
                            Online now 
                        @else
                            {{ $user->last_active }}
                        @endif
                        </dd>
                    </dl>
                    @if(!$owner)
                    <form method="POST" action="{{ route('profile.block.add') }}" id="block-form">
                        <p class="mb-0 mt-4 text-center">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-link block-btn">Block User</button>
                            <input type="hidden" name="username" value="{{ $user->username }}">
                            <!-- <span class="bullet">&middot;</span>
                            <button type="button" class="btn btn-link report-btn">Report User</button> -->
                        </p>
                    </form>
                    @endif
                </div>
            </div>
            <div class="col message-area">
                @if ($owner)
                <div class="owner-section">
                    <ul class="owner-tabs nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#photos-tab" data-toggle="tab">Photos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#favorites-tab" data-toggle="tab">Favorites</a>
                        </li>
                    </ul>
                    @if (session('photo_limit'))
                    <div class="alert alert-warning photo-limit">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        {{ session('photo_limit') }} <a class="alert-link" href="#" title="View Profile">Upgrade Account</a>
                    </div>
                    @endif
                    <div class="tab-content owner-content" id="owner-content">
                        <div class="tab-pane show active" id="photos-tab" role="tabpanel" aria-labelledby="photos-tab">
                            @if (json_decode($user->profile->photos))
                            <div class="row gallery" id="photo-gallery">
                                @foreach (json_decode($user->profile->photos) as $photo)
                                <div class="gallery-item">
                                    <a href="/uploads/{{ $user->username.'/'.$photo.'.jpg' }}" data-gallery> 
                                        <img src="/uploads/{{ $user->username.'/'.$photo.'.jpg' }}">
                                    </a>
                                    <form method="POST" action="{{ route('profile.update') }}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="photo_id" value="{{ $photo }}">
                                        <button id="btn-delete-photo" type="submit" name="delete_user_photo" class="btn btn-sm btn-secondary">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    </form>
                                </div>
                                @endforeach
                                <div class="gallery-item">
                                    <button type="button" class="upload-btn" data-toggle="modal" data-target="#photo-editor-modal">
                                        Upload A Photo
                                    </button>
                                </div>
                            </div>
                            @else
                            <div class="no-photos">
                                <h3>You have no photos uploaded.</h3>
                                <p>Don't be shy, show everyone how awesome you are!</p>
                                <button type="button" class="btn-primary btn" data-toggle="modal" data-target="#photo-editor-modal">
                                    Upload A Photo
                                </button>
                            </div>
                            @endif
                        </div>
                        <div class="tab-pane" id="favorites-tab" role="tabpanel" aria-labelledby="favorites-tab">
                            <div class="row">
                            @if($user->favorites)
                                @foreach ($user->favorites as $fav)
                                <div class="col-4">
                                    <div class="card">
                                        <a href="/{{ $fav->username }}" title="{{ $fav->username }}'s profile">
                                            <div class="card-img-top">
                                                @if($fav->profile->profile_photo)
                                                <div data-src="/uploads/{{ $fav->username .'/'. $fav->profile->profile_photo }}_browse.jpg" class="fav-profile-photo"></div>
                                                @else
                                                <div class="default-photo x150">
                                                    <i class="icon-user"></i>
                                                </div>
                                                @endif
                                            </div>
                                        </a>
                                        <div class="card-body">
                                            <h4 class="card-title d-inline-block">
                                                <a href="/{{ $fav->username }}">{{ $fav->username }}</a>
                                            </h4>
                                            <div class="age">{{ $fav->profile->age }}</div>
                                            <div class="region">{{ $fav->profile->region }}</div>
                                        </div>
                                        <div class="card-footer">
                                            <small class="text-muted">
                                                <i class="status-icon"></i>
                                                {{ $fav->last_active }}
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @else
                                <div class="col no-favorites">
                                    <p class="text-muted text-center">You have no favorites added.</p>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <form id="message-form" method="POST" action="/messages/send">
                    <input id="csrf-token" type="hidden" value="{{ Session::token() }}" name="_token">
                    <input id="receiver_id" type="hidden" value="{{ $profile->id }}" name="receiver_id">
                    <input id="username" type="hidden" value="{{ $user->username }}" name="username">
                    <div class="form-row">
                        <div class="form-group col mr-2 ml-2 ml-md-3 mr-md-3 mb-0">
                            @if (Auth::user()->profile->profile_photo)
                            <img src="/uploads/{{ Auth::user()->username }}/{{ Auth::user()->profile->profile_photo }}_avatar.jpg" alt="Profile Photo" class="msg-profile-photo">
                            @else
                            <span class="msg-profile-photo default-photo x42">
                                <i class="icon-user"></i>
                            </span>
                            @endif
                            <div class="message-wrap">
                                <input id="body" class="message-input" name="body" type="text" placeholder="What are you waiting for, {{ Auth::user()->username }}? Say Hi.">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group action-group col mr-2 ml-2 ml-md-3 mr-md-3">
                            <label for="photo">
                                <input id="photo" class="photo" name="photo" type="file" accept="image/*"> 
                                <span class="btn btn-secondary action mr-2">
                                    <i class="far fa-images"></i> Photo
                                </span>
                                <div class="cropit-preview"></div>
                            </label>
                            <div class="dropdown stickers d-inline-block">
                                <button id="stickers-btn" class="btn btn-secondary action mr-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-smile"></i> Stickers</button>
                                <div id="stickers-dd" class="dropdown-menu" aria-labelledby="stickers-btn">
                                    <div class="form-row">
                                        <div class="form-group col search mb-0">
                                            <div class="mr-2 ml-2">
                                                <input type="text" class="form-control form-control-sm search-input" placeholder="Search Stickers...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gallery">
                                        <i class="fas fa-circle-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>
                                    </div>
                                    <p class="powered-by-giphy">Powered By GIPHY</p>
                                </div>
                            </div>
                            <div class="dropdown gifs d-inline-block">
                                <button id="gifs-btn" class="btn btn-secondary action" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-magic"></i>
                                    Gif
                                </button>
                                <div id="gifs-dd" class="dropdown-menu" aria-labelledby="gif-btn">
                                    <div class="form-row">
                                        <div class="form-group col search mb-0">
                                            <div class="mr-2 ml-2">
                                                <input type="text" class="form-control form-control-sm search-input" placeholder="Search Gifs...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gallery">
                                        <i class="fas fa-circle-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>
                                    </div>
                                    <p class="powered-by-giphy">Powered By GIPHY</p>
                                </div>
                            </div>
                            <input id="message-submit" class="btn btn-primary submit" name="submit" type="submit" value="Send Message">
                        </div>
                    </div>
                </form>
                <div class="messages{{ $messages->isEmpty() ? ' d-none' : ''}}">
                @if ($messages->isNotEmpty())
                    @foreach ($messages as $message)
                        @if($message->owner)
                        <div class="message sender">
                        @else
                        <div class="message">
                            <div class="avatar">
                                @if ($profile->profile_photo)
                                <img src="/uploads/{{ $user->username }}/{{ $user->profile->profile_photo }}_avatar.jpg" alt="Profile Photo">
                                @else
                                <div class="default-photo x21">
                                    <i class="icon-user"></i>
                                </div>
                                @endif
                            </div>
                        @endif
                            @if ($message->type == 1)
                            <p class="bubble image">
                                <img src="{{ $message->body }}" alt="Photo">
                            </p>
                            @elseif ($message->type == 2)
                            <p class="bubble sticker">
                                <img src="{{ $message->body }}" alt="Sticker">
                            </p>
                            @elseif ($message->type == 3)
                            <p class="bubble gif">
                                <img src="{{ $message->body }}" alt="Gif">
                            </p>
                            @else
                            <p class="bubble">
                                {{ $message->body }}
                            </p>
                            @endif
                            <div class="date">{{ $message->created_at->diffForHumans() }}</div>
                        </div>
                    @endforeach
                @endif
                </div>
                {{ $messages->links('vendor.pagination.bootstrap-4') }}
                    @if ($user->profile->photos)
                    <div class="gallery" id="photo-gallery">
                        @foreach (json_decode($user->profile->photos) as $photo)
                        <a href="/uploads/{{ $user->username.'/'.$photo.'.jpg' }}" class="gallery-item" data-gallery> 
                            <img src="/uploads/{{ $user->username.'/'.$photo.'.jpg' }}">
                        </a>
                        @endforeach
                    </div>
                    @endif
                @endif
            </div>
        </div>
        <!-- <div class="modal" id="photo-editor-modal" tabindex="-1" role="dialog" aria-labelledby="photo-modal-label" aria-hidden="true">
            <div class="modal-dialog" role="edit profile photo">
                <div class="modal-content photo-editor">
                    <div class="modal-body">
                        <div id="image-editor" class="photo-wrap">
                            <span class="default-photo x150">
                                <i class="icon-picture"></i>
                            </span>
                            <div class="cropit-preview"></div>
                            <label class="add-photo-label" for="upload-photo-input">
                                <span class="icon-plus-circle icon-plus-circle-blue icon-plus-circle-lg"></span>
                            </label>
                            <button type="button" class="animated-hidden image-zoom-in">
                                <span class="icon-plus-circle icon-plus-circle-green icon-plus-circle-lg"></span>
                            </button>
                            <button type="button" class="animated-hidden image-zoom-out">
                                <span class="icon-minus-circle icon-minus-circle-green icon-plus-circle-lg"></span>
                            </button>
                            <button type="button" class="animated-hidden delete-photo">
                                <span class="icon-minus-circle icon-minus-circle-red"></span>
                            </button>
                            <input type="file" id="upload-photo-input" class="cropit-image-input" name="profile_photo" accept="image/jpeg">
                        </div>
                        <div class="form-group text-center mt-4">
                            <form id="user-photos-form" method="POST" action="{{ route('profile.edit') }}">
                                {{ csrf_field() }}
                                <h2 class="upload-header mb-3">Upload A Photo</h2>
                                <input id="photo" name="photo" type="hidden">
                                <input id="upload-btn" class="btn btn-primary" name="upload_user_photo" value="Upload Photo" type="submit" disabled>
                                <button class="btn btn-link" type="button" name="cancel" data-dismiss="modal" aria-label="Close">Cancel</button>
                                <p class="footnote">Nudity or overtly sexual photos are not allowed.</p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        @include('includes.modals.photo-editor')
    </div>

    <!-- Gallery -->
    <div id="blueimp-gallery" 
         class="blueimp-gallery"
         thumbnail-indicators="true"
         indicator-container="ol"
         active-indicator-class="active"
         thumbnail-property="thumbnail"
        >
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a href="javascript: void()" class="prev">‹</a>
        <a href="javascript: void()" class="next">›</a>
        <a href="javascript: void()" class="close">×</a>
        <a href="javascript: void()" class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

</main>
@include('includes.footer')
@endsection

@section('js')
<script src="{{ mix('/js/profile.js') }}"></script>
<script src="/js/image-editor.js"></script>
<script>
    // Form Submit
    $('#add-photo-form').submit(function(e){
        // Ready up photos
        var photo = $('#image-editor').cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: true,
            exportZoom: false
        });
        var avatar = $('#image-editor').cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: false
        });
        $('#photo').val(photo);
        $('#avatar').val(avatar);
    });

    // Upload Profile Modal
    $('#upload_profile_photo').change(function(){
        // If a file is selected
        if(document.getElementById('upload_profile_photo').files.length > 0) {
            $('#add-photo-form').submit();
        };
    });
    
</script>
@endsection