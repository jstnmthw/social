@extends('layouts.master')
@section('page_title', 'Upload Photo')
@section('body_class', 'auth-page')

@section('content')
<div class="auth-wrap">
    <main>
        <form id="image-editor" class="form-horizontal auth-form upload-photo-form" enctype="multipart/form-data" method="POST" action="{{ route('register.photo') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col">
                    <div class="photo-wrap">
                        <span class="default-photo x150">
                            <i class="icon-user"></i>
                        </span>
                        <div class="cropit-preview"></div>
                        <label class="add-photo-label" for="upload-photo-input">
                            <span class="icon-plus-circle icon-plus-circle-blue icon-plus-circle-lg"></span>
                        </label>
                        <button type="button" class="animated-hidden image-zoom-in">
                            <span class="icon-plus-circle icon-plus-circle-green icon-plus-circle-lg"></span>
                        </button>
                        <button type="button" class="animated-hidden image-zoom-out">
                            <span class="icon-minus-circle icon-minus-circle-green icon-plus-circle-lg"></span>
                        </button>
                        <button type="button" class="animated-hidden delete-photo">
                            <span class="icon-minus-circle icon-minus-circle-red"></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group text-center">
                <h2 class="upload-header">Upload Profile Photo</h2>
                <input class="upload-input cropit-image-input" type="file" id="upload-photo-input" name="photo_upload" accept="image/jpeg">
                <input id="photo" name="photo" type="hidden">
                <input class="avatar" type="hidden" id="avatar" name="avatar">
            </div>
            <div class="form-group text-center">
                <button type="submit" id="upload-btn" class="btn btn-primary process-submit" name="save" disabled>Upload Photo</button>
                <!-- <input type="submit" id="upload-btn" class="btn btn-primary" name="save" value="Upload Photo" disabled> -->
                <input type="submit" class="btn btn-link" name="skip" value="Skip This Step">
                <p class="footnote">Nudity or overtly sexual photos are not allowed.</p>
            </div>
        </form>
    </main>
</div>
@endsection

@section('js')
<script src="/js/image-editor.js"></script>
<script>
    // Form Submit
    $('#image-editor').submit(function(e){
        // Ready up photos
        var photo = $('#image-editor').cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: true,
            exportZoom: false
        });
        var avatar = $('#image-editor').cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: false
        });
        $('#photo').val(photo);
        $('#avatar').val(avatar);
    });
</script>
@endsection
