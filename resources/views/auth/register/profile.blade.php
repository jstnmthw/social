@extends('layouts.master')
@section('page_title', 'Edit Profile - Register')
@section('body_class', 'auth-page')

@section('content')
<div class="auth-wrap">
    <main>
        <form class="form-horizontal auth-form" method="POST" action="{{ route('register.profile') }}">
            {{ csrf_field() }}
            <h1 class="logo">
                <a href="/">
                    @php echo file_get_contents('images/logo.svg') @endphp
                    ThaiLovely.co
                </a>
            </h1>
            <div class="form-group row{{ $errors->has('sex') ? ' has-error' : '' }}">
                <label for="sex" class="col-sm-6 col-form-label">I am a</label>
                <div class="col-sm-6">
                    <select class="custom-select select-fluid" id="sex" name="sex" required>
                        @foreach ($genders as $i => $gender)
                        <option value="{{ $i }}">{{ ucfirst($gender) }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('sex'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sex') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row{{ $errors->has('orientation') ? ' has-error' : '' }}">
                <label for="orientation" class="col-sm-6 col-form-label">Interested in</label>
                <div class="col-sm-6">
                    <select class="custom-select select-fluid" id="orientation" name="orientation">
                        @foreach ($genders as $i => $gender)
                        <option value="{{ $i }}" {{ ($i == 1 ? 'selected' : '') }}>{{ ucfirst($gender) }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('orientation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('orientation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row{{ $errors->has('dob_month', 'dob_year', 'dob_year')  ? ' has-error' : '' }}">
                <label class="col-sm-3 col-form-label">Birthdate</label>
                <div class="col-sm-9">
                    <div class="form-row">
                        <div class="col-md-5">
                            <select class="custom-select form-control" id="dob_month" name="dob_month" required>
                                <option value="-" disabled>-</option>
                                <option value="01">January</option>
                                <option value="02">Febuary</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="custom-select form-control" id="dob_day" name="dob_day" required>
                                <option value="-" disabled>-</option>
                                @for ($month = 01; $month <= 31; $month++)
                                <option value="{{ $month }}">{{ $month }}</option>
                                @endfor
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select class="custom-select form-control" id="dob_year" name="dob_year" required>
                                <option value="-" disabled>-</option>
                                @for ($year = date('Y') - 18; $year >= 1950; $year--)
                                <option value="{{ $year }}">{{ $year }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                @if ($errors->has('dob_month', 'dob_year', 'dob_year'))
                    <span class="help-block">
                        <strong>{{ $errors->first('dob_year') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group row{{ $errors->has('country')  ? ' has-error' : '' }}">
                <label class="col-sm-3 col-form-label">Country</label>
                <div class="col-sm-9">
                    <select id="country" class="custom-select select-fluid" name="country">
                        <option value="1605651">Thailand</option>
                        <option>-</option>
                        @foreach ($countries as $i => $country)
                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group mb-4 row{{ $errors->has('region') ? ' has-error' : '' }}">
                <label class="col-sm-3 col-form-label">Region/State</label>
                <div class="col-sm-9">
                    <select id="region" name="region" class="custom-select select-fluid">
                    </select>
                </div>
            </div>
            <div class="form-group auth-btns">
                <button type="submit" class="auth-btn btn btn-primary btn-block process-submit">
                    Next Step <i class="fas fa-angle-right ml-1"></i>
                </button>
            </div>
        </form>
        <p class="copyright">Copyright &copy; @php echo date('Y'); @endphp ThaiLovely Co.</p>
    </main>
</div>
@endsection

@section('js')
<script>
    $(function(){
        region();
    });
</script>
@endsection