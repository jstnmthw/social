@extends('layouts.master')
@section('page_title', 'Login')
@section('body_class', 'login-page')

@section('content')
<div class="login-wrap">
    <main>
        @if (session('status'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('status') }}
        </div>
        @endif
        <form class="form-horizontal login-form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <h1 class="logo">
                <a href="/">
                    @php echo file_get_contents('images/logo.svg') @endphp
                    ThaiLovely.co
                </a>
            </h1>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col control-label">E-Mail Address</label>
                <div class="col">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col control-label">Password</label>
                <div class="col">
                    <input id="password" type="password" class="form-control" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col remember-me">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="remember" id="remember">
                        <label class="custom-control-label" for="remember">Remember Me</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col auth-btns">
                    <button type="submit" class="btn btn-primary btn-block login-btn process-submit mb-4">
                        Login <i class="icon-lock"></i>
                    </button>
                    <a class="btn btn-link btn-block forgot-password" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                    <div class="or"><span>Or</span></div>
                    <a class="btn btn-link btn-block" href="/register">
                        Register Account
                    </a>
                </div>
            </div>
        </form>
        <p class="copyright">Copyright &copy; @php echo date('Y'); @endphp ThaiLovely Co.</p>
    </main>
</div>
@endsection