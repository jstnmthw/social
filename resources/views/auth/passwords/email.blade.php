@extends('layouts.master')
@section('page_title', 'Password Reset')
@section('body_class', 'auth-page')

@section('content')
<div class="auth-wrap">
    <main>
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <form class="form-horizontal auth-form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <h1 class="logo">
                <a href="/">
                    @php echo file_get_contents('images/logo.svg') @endphp
                    ThaiLovely.co
                </a>
            </h1>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col control-label">E-Mail Address</label>
                <div class="col">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class="col">
                    <button type="submit" class="auth-btn btn btn-block btn-primary process-submit">
                        Send Password Reset Link
                    </button>
                    <p class="text-center mt-4">
                        <a href="{{ route('login') }}" class="btn btn-link" title="Back to login page">&larr; Back to login page</a>
                    </p>
                </div>
            </div>
        </form>
        </main>
    </div>
@endsection