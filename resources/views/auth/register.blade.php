@extends('layouts.master')
@section('page_title', 'Register')
@section('body_class', 'auth-page')

@section('content')
<div class="auth-wrap">
    <main>
        <form class="form-horizontal auth-form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <h1 class="logo">
                <a href="/">
                    @php echo file_get_contents('images/logo.svg') @endphp
                    ThaiLovely.co
                </a>
            </h1>
            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="control-label">Username</label>
                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required>
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">E-Mail Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>
                <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('terns') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="terms" required>
                    <label class="custom-control-label" for="terms">I agree to the terms.</label>
                </div>
            </div>
            <div class="form-group auth-btns">
                <button type="submit" class="auth-btn btn btn-primary btn-block process-submit mb-3">
                    Register <i class="icon-lock"></i>
                </button>
                <a class="btn btn-link btn-block has-account mt-0" href="{{ route('login') }}">
                    Have an account?
                </a>
            </div>
        </form>
        <p class="copyright">Copyright &copy; @php echo date('Y'); @endphp ThaiLovely Co.</p>
    </main>
</div>
@endsection