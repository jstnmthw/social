<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}"@php echo (isset($_COOKIE["fonts_loaded"])) ? ' class="fonts-loaded"' : ''; @endphp>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('page_title') - ThaiLovely.co</title>

    <link href="{{ mix('css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Fira+Sans:300,400,500,600,700" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
</head>
<body @hasSection('body_class')class="@yield('body_class')"@endif>
    @yield('content')

    <!-- Scripts -->
    @auth
    <script>var url = '{{ Request::path() }}', user = '{{ Auth::user()->username }}', userRegion = '{{ Auth::user()->profile->country }}', token = '{{ Session::token() }}';</script>
    <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
    @endauth
    <script src="{{ mix('/js/manifest.js') }}"></script>
    <script src="{{ mix('/js/vendor.js') }}"></script>
    <script src="{{ mix('/js/utilities.js') }}"></script>
    @auth
    <script src="{{ mix('/js/app.js') }}"></script>
    @endauth
    @yield('js')
    <script src="/js/vendor/fontawesome/all.min.js" defer="defer"></script>
</body>
</html>