<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // My Account (Justin)
        DB::table('users')->insert([
            'username' => 'justin',
            'email' => 'web@jstn.ly',
            'registration_steps' => '2',
            'last_active' => date('Y-m-d h:i:s'),
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
            'password' => password_hash('gl0w!!', PASSWORD_DEFAULT),
            'remember_token' => null
        ]);
        DB::table('profiles')->insert([
            'user_id' => 1,
            'profile_photo' => null,
            'photos' => null,
            'birthdate' => '1988-06-01',
            'height' => '180',
            'weight' => '73',
            'sex' => 1,
            'orientation' => 0,
            'country' => 1605651,
            'region' => 1609348,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);

        // Fake Users
        factory(App\User::class, 500)->create()->each(function ($u) {
        	$u->profile()->save(factory(App\Profile::class)->make());
        });

    }
}
