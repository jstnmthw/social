<?php

use Faker\Generator as Faker;


$factory->define(App\Profile::class, function (Faker $faker) {
    $regions = array(1609348, 1607530, 1611108, 1153670, 1115098, 1151253, 1150514, 1152646, 1153080, 1153670, 1606720, 1608132, 1609775, 1614295);
    return [
        'profile_photo' => null,
        'photos' => null,
        'birthdate' => rand(1998,1970).'-'.rand(01,12).'-'.rand(01,28),
        'height' => $faker->numberBetween($min = 4, $max = 6).$faker->numberBetween($min = 1, $max = 11),
        'weight' => $faker->numberBetween($min = 90, $max = 300),
        'sex' => $faker->numberBetween($min = 0, $max = 2),
        'orientation' => $faker->numberBetween($min = 0, $max = 2),
        'country' => 1605651,
        'region' => $regions[mt_rand(0, count($regions) - 1)],
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ];
});
