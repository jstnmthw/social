<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {

    return [
        'username' => $faker->unique()->userName($max = 16),
        'email' => $faker->unique()->safeEmail,
        'registration_steps' => 3,
        'last_active' => $faker->dateTimeBetween($startDate = '-1 days', $endDate = 'now', $timezone = date_default_timezone_get()),
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s'),
        'password' => password_hash('gl0w!!', PASSWORD_DEFAULT),
        'remember_token' => str_random(10)
    ];

});
