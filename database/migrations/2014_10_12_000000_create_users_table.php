<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email');
            $table->string('password');
            $table->rememberToken();
            $table->string('favorites')->nullable();
            $table->string('blocked')->nullable();
            $table->string('blocked_me')->nullable();
            $table->boolean('msg_notification')->default(1);
            $table->boolean('wave_notification')->default(1);
            $table->boolean('fav_notification')->default(1);
            $table->integer('registration_steps');
            $table->boolean('disabled')->default(0);
            $table->dateTime('last_active');
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['email', 'username', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
