# Dating App v1.0 - Laravel 5.5
Out-of-box dating application written in [Laravel 5.5](https://laravel.com/docs/5.5), using Socket.io and Redis.io.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### System Requirements
The following are required to function properly.

```
PHP 7.1+
MySQL 3.5+
Laravel 5.7+
Composer
Npm
```

### Installing
A step by step series of examples that tell you have to get a development env running

First install any PHP dependencies
```
composer install
```

PHP FileInfo module is required by Intervention Image. Open your `php.ini` in your PHP folder and uncomment the following line.
```
;extension=php_fileinfo.dll
```

Then install node.js required files
```
npm install
```

Migrate all schemes to your database
```
php artisan migrate
```

Seed your Geo table
```
php artisan geo:json
```
```
geo.json
```

## Required background processes
The application must run `laravel-echo-server`, `redis`, and `Laravel queue worker` as background processes for it to run properly.

## Deployment
Add additional notes about how to deploy this on a live system

## Built With
* [Laravel 5.5](http://www.laravel.com/docs/5.5/) - The PHP Framework For Web Artisans

## Authors
**Justin Rivera** - *Initial work*, [(http://jstn.ly)](https://jstn.ly) 

See also the list of [contributors](https://gitlab.com/jstnmthw/ThaiLovely/contributors) who participated in this project.

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
