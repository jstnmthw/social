webpackJsonp([1],{

/***/ "./node_modules/blueimp-gallery/js/blueimp-gallery.js":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
 * blueimp Gallery JS
 * https://github.com/blueimp/Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Swipe implementation based on
 * https://github.com/bradbirdsall/Swipe
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global define, window, document, DocumentTouch */

;(function (factory) {
  'use strict'
  if (true) {
    // Register as an anonymous AMD module:
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__("./node_modules/blueimp-gallery/js/blueimp-helper.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))
  } else {
    // Browser globals:
    window.blueimp = window.blueimp || {}
    window.blueimp.Gallery = factory(window.blueimp.helper || window.jQuery)
  }
})(function ($) {
  'use strict'

  function Gallery (list, options) {
    if (document.body.style.maxHeight === undefined) {
      // document.body.style.maxHeight is undefined on IE6 and lower
      return null
    }
    if (!this || this.options !== Gallery.prototype.options) {
      // Called as function instead of as constructor,
      // so we simply return a new instance:
      return new Gallery(list, options)
    }
    if (!list || !list.length) {
      this.console.log(
        'blueimp Gallery: No or empty list provided as first argument.',
        list
      )
      return
    }
    this.list = list
    this.num = list.length
    this.initOptions(options)
    this.initialize()
  }

  $.extend(Gallery.prototype, {
    options: {
      // The Id, element or querySelector of the gallery widget:
      container: '#blueimp-gallery',
      // The tag name, Id, element or querySelector of the slides container:
      slidesContainer: 'div',
      // The tag name, Id, element or querySelector of the title element:
      titleElement: 'h3',
      // The class to add when the gallery is visible:
      displayClass: 'blueimp-gallery-display',
      // The class to add when the gallery controls are visible:
      controlsClass: 'blueimp-gallery-controls',
      // The class to add when the gallery only displays one element:
      singleClass: 'blueimp-gallery-single',
      // The class to add when the left edge has been reached:
      leftEdgeClass: 'blueimp-gallery-left',
      // The class to add when the right edge has been reached:
      rightEdgeClass: 'blueimp-gallery-right',
      // The class to add when the automatic slideshow is active:
      playingClass: 'blueimp-gallery-playing',
      // The class for all slides:
      slideClass: 'slide',
      // The slide class for loading elements:
      slideLoadingClass: 'slide-loading',
      // The slide class for elements that failed to load:
      slideErrorClass: 'slide-error',
      // The class for the content element loaded into each slide:
      slideContentClass: 'slide-content',
      // The class for the "toggle" control:
      toggleClass: 'toggle',
      // The class for the "prev" control:
      prevClass: 'prev',
      // The class for the "next" control:
      nextClass: 'next',
      // The class for the "close" control:
      closeClass: 'close',
      // The class for the "play-pause" toggle control:
      playPauseClass: 'play-pause',
      // The list object property (or data attribute) with the object type:
      typeProperty: 'type',
      // The list object property (or data attribute) with the object title:
      titleProperty: 'title',
      // The list object property (or data attribute) with the object alt text:
      altTextProperty: 'alt',
      // The list object property (or data attribute) with the object URL:
      urlProperty: 'href',
      // The list object property (or data attribute) with the object srcset URL(s):
      srcsetProperty: 'urlset',
      // The gallery listens for transitionend events before triggering the
      // opened and closed events, unless the following option is set to false:
      displayTransition: true,
      // Defines if the gallery slides are cleared from the gallery modal,
      // or reused for the next gallery initialization:
      clearSlides: true,
      // Defines if images should be stretched to fill the available space,
      // while maintaining their aspect ratio (will only be enabled for browsers
      // supporting background-size="contain", which excludes IE < 9).
      // Set to "cover", to make images cover all available space (requires
      // support for background-size="cover", which excludes IE < 9):
      stretchImages: false,
      // Toggle the controls on pressing the Return key:
      toggleControlsOnReturn: true,
      // Toggle the controls on slide click:
      toggleControlsOnSlideClick: true,
      // Toggle the automatic slideshow interval on pressing the Space key:
      toggleSlideshowOnSpace: true,
      // Navigate the gallery by pressing left and right on the keyboard:
      enableKeyboardNavigation: true,
      // Close the gallery on pressing the Esc key:
      closeOnEscape: true,
      // Close the gallery when clicking on an empty slide area:
      closeOnSlideClick: true,
      // Close the gallery by swiping up or down:
      closeOnSwipeUpOrDown: true,
      // Emulate touch events on mouse-pointer devices such as desktop browsers:
      emulateTouchEvents: true,
      // Stop touch events from bubbling up to ancestor elements of the Gallery:
      stopTouchEventsPropagation: false,
      // Hide the page scrollbars:
      hidePageScrollbars: true,
      // Stops any touches on the container from scrolling the page:
      disableScroll: true,
      // Carousel mode (shortcut for carousel specific options):
      carousel: false,
      // Allow continuous navigation, moving from last to first
      // and from first to last slide:
      continuous: true,
      // Remove elements outside of the preload range from the DOM:
      unloadElements: true,
      // Start with the automatic slideshow:
      startSlideshow: false,
      // Delay in milliseconds between slides for the automatic slideshow:
      slideshowInterval: 5000,
      // The starting index as integer.
      // Can also be an object of the given list,
      // or an equal object with the same url property:
      index: 0,
      // The number of elements to load around the current index:
      preloadRange: 2,
      // The transition speed between slide changes in milliseconds:
      transitionSpeed: 400,
      // The transition speed for automatic slide changes, set to an integer
      // greater 0 to override the default transition speed:
      slideshowTransitionSpeed: undefined,
      // The event object for which the default action will be canceled
      // on Gallery initialization (e.g. the click event to open the Gallery):
      event: undefined,
      // Callback function executed when the Gallery is initialized.
      // Is called with the gallery instance as "this" object:
      onopen: undefined,
      // Callback function executed when the Gallery has been initialized
      // and the initialization transition has been completed.
      // Is called with the gallery instance as "this" object:
      onopened: undefined,
      // Callback function executed on slide change.
      // Is called with the gallery instance as "this" object and the
      // current index and slide as arguments:
      onslide: undefined,
      // Callback function executed after the slide change transition.
      // Is called with the gallery instance as "this" object and the
      // current index and slide as arguments:
      onslideend: undefined,
      // Callback function executed on slide content load.
      // Is called with the gallery instance as "this" object and the
      // slide index and slide element as arguments:
      onslidecomplete: undefined,
      // Callback function executed when the Gallery is about to be closed.
      // Is called with the gallery instance as "this" object:
      onclose: undefined,
      // Callback function executed when the Gallery has been closed
      // and the closing transition has been completed.
      // Is called with the gallery instance as "this" object:
      onclosed: undefined
    },

    carouselOptions: {
      hidePageScrollbars: false,
      toggleControlsOnReturn: false,
      toggleSlideshowOnSpace: false,
      enableKeyboardNavigation: false,
      closeOnEscape: false,
      closeOnSlideClick: false,
      closeOnSwipeUpOrDown: false,
      disableScroll: false,
      startSlideshow: true
    },

    console:
      window.console && typeof window.console.log === 'function'
        ? window.console
        : { log: function () {} },

    // Detect touch, transition, transform and background-size support:
    support: (function (element) {
      var support = {
        touch:
          window.ontouchstart !== undefined ||
          (window.DocumentTouch && document instanceof DocumentTouch)
      }
      var transitions = {
        webkitTransition: {
          end: 'webkitTransitionEnd',
          prefix: '-webkit-'
        },
        MozTransition: {
          end: 'transitionend',
          prefix: '-moz-'
        },
        OTransition: {
          end: 'otransitionend',
          prefix: '-o-'
        },
        transition: {
          end: 'transitionend',
          prefix: ''
        }
      }
      var prop
      for (prop in transitions) {
        if (
          transitions.hasOwnProperty(prop) &&
          element.style[prop] !== undefined
        ) {
          support.transition = transitions[prop]
          support.transition.name = prop
          break
        }
      }
      function elementTests () {
        var transition = support.transition
        var prop
        var translateZ
        document.body.appendChild(element)
        if (transition) {
          prop = transition.name.slice(0, -9) + 'ransform'
          if (element.style[prop] !== undefined) {
            element.style[prop] = 'translateZ(0)'
            translateZ = window
              .getComputedStyle(element)
              .getPropertyValue(transition.prefix + 'transform')
            support.transform = {
              prefix: transition.prefix,
              name: prop,
              translate: true,
              translateZ: !!translateZ && translateZ !== 'none'
            }
          }
        }
        if (element.style.backgroundSize !== undefined) {
          support.backgroundSize = {}
          element.style.backgroundSize = 'contain'
          support.backgroundSize.contain =
            window
              .getComputedStyle(element)
              .getPropertyValue('background-size') === 'contain'
          element.style.backgroundSize = 'cover'
          support.backgroundSize.cover =
            window
              .getComputedStyle(element)
              .getPropertyValue('background-size') === 'cover'
        }
        document.body.removeChild(element)
      }
      if (document.body) {
        elementTests()
      } else {
        $(document).on('DOMContentLoaded', elementTests)
      }
      return support
      // Test element, has to be standard HTML and must not be hidden
      // for the CSS3 tests using window.getComputedStyle to be applicable:
    })(document.createElement('div')),

    requestAnimationFrame:
      window.requestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame,

    cancelAnimationFrame:
      window.cancelAnimationFrame ||
      window.webkitCancelRequestAnimationFrame ||
      window.webkitCancelAnimationFrame ||
      window.mozCancelAnimationFrame,

    initialize: function () {
      this.initStartIndex()
      if (this.initWidget() === false) {
        return false
      }
      this.initEventListeners()
      // Load the slide at the given index:
      this.onslide(this.index)
      // Manually trigger the slideend event for the initial slide:
      this.ontransitionend()
      // Start the automatic slideshow if applicable:
      if (this.options.startSlideshow) {
        this.play()
      }
    },

    slide: function (to, speed) {
      window.clearTimeout(this.timeout)
      var index = this.index
      var direction
      var naturalDirection
      var diff
      if (index === to || this.num === 1) {
        return
      }
      if (!speed) {
        speed = this.options.transitionSpeed
      }
      if (this.support.transform) {
        if (!this.options.continuous) {
          to = this.circle(to)
        }
        // 1: backward, -1: forward:
        direction = Math.abs(index - to) / (index - to)
        // Get the actual position of the slide:
        if (this.options.continuous) {
          naturalDirection = direction
          direction = -this.positions[this.circle(to)] / this.slideWidth
          // If going forward but to < index, use to = slides.length + to
          // If going backward but to > index, use to = -slides.length + to
          if (direction !== naturalDirection) {
            to = -direction * this.num + to
          }
        }
        diff = Math.abs(index - to) - 1
        // Move all the slides between index and to in the right direction:
        while (diff) {
          diff -= 1
          this.move(
            this.circle((to > index ? to : index) - diff - 1),
            this.slideWidth * direction,
            0
          )
        }
        to = this.circle(to)
        this.move(index, this.slideWidth * direction, speed)
        this.move(to, 0, speed)
        if (this.options.continuous) {
          this.move(
            this.circle(to - direction),
            -(this.slideWidth * direction),
            0
          )
        }
      } else {
        to = this.circle(to)
        this.animate(index * -this.slideWidth, to * -this.slideWidth, speed)
      }
      this.onslide(to)
    },

    getIndex: function () {
      return this.index
    },

    getNumber: function () {
      return this.num
    },

    prev: function () {
      if (this.options.continuous || this.index) {
        this.slide(this.index - 1)
      }
    },

    next: function () {
      if (this.options.continuous || this.index < this.num - 1) {
        this.slide(this.index + 1)
      }
    },

    play: function (time) {
      var that = this
      window.clearTimeout(this.timeout)
      this.interval = time || this.options.slideshowInterval
      if (this.elements[this.index] > 1) {
        this.timeout = this.setTimeout(
          (!this.requestAnimationFrame && this.slide) ||
            function (to, speed) {
              that.animationFrameId = that.requestAnimationFrame.call(
                window,
                function () {
                  that.slide(to, speed)
                }
              )
            },
          [this.index + 1, this.options.slideshowTransitionSpeed],
          this.interval
        )
      }
      this.container.addClass(this.options.playingClass)
    },

    pause: function () {
      window.clearTimeout(this.timeout)
      this.interval = null
      if (this.cancelAnimationFrame) {
        this.cancelAnimationFrame.call(window, this.animationFrameId)
        this.animationFrameId = null
      }
      this.container.removeClass(this.options.playingClass)
    },

    add: function (list) {
      var i
      if (!list.concat) {
        // Make a real array out of the list to add:
        list = Array.prototype.slice.call(list)
      }
      if (!this.list.concat) {
        // Make a real array out of the Gallery list:
        this.list = Array.prototype.slice.call(this.list)
      }
      this.list = this.list.concat(list)
      this.num = this.list.length
      if (this.num > 2 && this.options.continuous === null) {
        this.options.continuous = true
        this.container.removeClass(this.options.leftEdgeClass)
      }
      this.container
        .removeClass(this.options.rightEdgeClass)
        .removeClass(this.options.singleClass)
      for (i = this.num - list.length; i < this.num; i += 1) {
        this.addSlide(i)
        this.positionSlide(i)
      }
      this.positions.length = this.num
      this.initSlides(true)
    },

    resetSlides: function () {
      this.slidesContainer.empty()
      this.unloadAllSlides()
      this.slides = []
    },

    handleClose: function () {
      var options = this.options
      this.destroyEventListeners()
      // Cancel the slideshow:
      this.pause()
      this.container[0].style.display = 'none'
      this.container
        .removeClass(options.displayClass)
        .removeClass(options.singleClass)
        .removeClass(options.leftEdgeClass)
        .removeClass(options.rightEdgeClass)
      if (options.hidePageScrollbars) {
        document.body.style.overflow = this.bodyOverflowStyle
      }
      if (this.options.clearSlides) {
        this.resetSlides()
      }
      if (this.options.onclosed) {
        this.options.onclosed.call(this)
      }
    },

    close: function () {
      var that = this
      function closeHandler (event) {
        if (event.target === that.container[0]) {
          that.container.off(that.support.transition.end, closeHandler)
          that.handleClose()
        }
      }
      if (this.options.onclose) {
        this.options.onclose.call(this)
      }
      if (this.support.transition && this.options.displayTransition) {
        this.container.on(this.support.transition.end, closeHandler)
        this.container.removeClass(this.options.displayClass)
      } else {
        this.handleClose()
      }
    },

    circle: function (index) {
      // Always return a number inside of the slides index range:
      return (this.num + index % this.num) % this.num
    },

    move: function (index, dist, speed) {
      this.translateX(index, dist, speed)
      this.positions[index] = dist
    },

    translate: function (index, x, y, speed) {
      var style = this.slides[index].style
      var transition = this.support.transition
      var transform = this.support.transform
      style[transition.name + 'Duration'] = speed + 'ms'
      style[transform.name] =
        'translate(' +
        x +
        'px, ' +
        y +
        'px)' +
        (transform.translateZ ? ' translateZ(0)' : '')
    },

    translateX: function (index, x, speed) {
      this.translate(index, x, 0, speed)
    },

    translateY: function (index, y, speed) {
      this.translate(index, 0, y, speed)
    },

    animate: function (from, to, speed) {
      if (!speed) {
        this.slidesContainer[0].style.left = to + 'px'
        return
      }
      var that = this
      var start = new Date().getTime()
      var timer = window.setInterval(function () {
        var timeElap = new Date().getTime() - start
        if (timeElap > speed) {
          that.slidesContainer[0].style.left = to + 'px'
          that.ontransitionend()
          window.clearInterval(timer)
          return
        }
        that.slidesContainer[0].style.left =
          (to - from) * (Math.floor(timeElap / speed * 100) / 100) + from + 'px'
      }, 4)
    },

    preventDefault: function (event) {
      if (event.preventDefault) {
        event.preventDefault()
      } else {
        event.returnValue = false
      }
    },

    stopPropagation: function (event) {
      if (event.stopPropagation) {
        event.stopPropagation()
      } else {
        event.cancelBubble = true
      }
    },

    onresize: function () {
      this.initSlides(true)
    },

    onmousedown: function (event) {
      // Trigger on clicks of the left mouse button only
      // and exclude video & audio elements:
      if (
        event.which &&
        event.which === 1 &&
        event.target.nodeName !== 'VIDEO' &&
        event.target.nodeName !== 'AUDIO'
      ) {
        // Preventing the default mousedown action is required
        // to make touch emulation work with Firefox:
        event.preventDefault()
        ;(event.originalEvent || event).touches = [
          {
            pageX: event.pageX,
            pageY: event.pageY
          }
        ]
        this.ontouchstart(event)
      }
    },

    onmousemove: function (event) {
      if (this.touchStart) {
        ;(event.originalEvent || event).touches = [
          {
            pageX: event.pageX,
            pageY: event.pageY
          }
        ]
        this.ontouchmove(event)
      }
    },

    onmouseup: function (event) {
      if (this.touchStart) {
        this.ontouchend(event)
        delete this.touchStart
      }
    },

    onmouseout: function (event) {
      if (this.touchStart) {
        var target = event.target
        var related = event.relatedTarget
        if (!related || (related !== target && !$.contains(target, related))) {
          this.onmouseup(event)
        }
      }
    },

    ontouchstart: function (event) {
      if (this.options.stopTouchEventsPropagation) {
        this.stopPropagation(event)
      }
      // jQuery doesn't copy touch event properties by default,
      // so we have to access the originalEvent object:
      var touches = (event.originalEvent || event).touches[0]
      this.touchStart = {
        // Remember the initial touch coordinates:
        x: touches.pageX,
        y: touches.pageY,
        // Store the time to determine touch duration:
        time: Date.now()
      }
      // Helper variable to detect scroll movement:
      this.isScrolling = undefined
      // Reset delta values:
      this.touchDelta = {}
    },

    ontouchmove: function (event) {
      if (this.options.stopTouchEventsPropagation) {
        this.stopPropagation(event)
      }
      // jQuery doesn't copy touch event properties by default,
      // so we have to access the originalEvent object:
      var touches = (event.originalEvent || event).touches[0]
      var scale = (event.originalEvent || event).scale
      var index = this.index
      var touchDeltaX
      var indices
      // Ensure this is a one touch swipe and not, e.g. a pinch:
      if (touches.length > 1 || (scale && scale !== 1)) {
        return
      }
      if (this.options.disableScroll) {
        event.preventDefault()
      }
      // Measure change in x and y coordinates:
      this.touchDelta = {
        x: touches.pageX - this.touchStart.x,
        y: touches.pageY - this.touchStart.y
      }
      touchDeltaX = this.touchDelta.x
      // Detect if this is a vertical scroll movement (run only once per touch):
      if (this.isScrolling === undefined) {
        this.isScrolling =
          this.isScrolling ||
          Math.abs(touchDeltaX) < Math.abs(this.touchDelta.y)
      }
      if (!this.isScrolling) {
        // Always prevent horizontal scroll:
        event.preventDefault()
        // Stop the slideshow:
        window.clearTimeout(this.timeout)
        if (this.options.continuous) {
          indices = [this.circle(index + 1), index, this.circle(index - 1)]
        } else {
          // Increase resistance if first slide and sliding left
          // or last slide and sliding right:
          this.touchDelta.x = touchDeltaX =
            touchDeltaX /
            ((!index && touchDeltaX > 0) ||
            (index === this.num - 1 && touchDeltaX < 0)
              ? Math.abs(touchDeltaX) / this.slideWidth + 1
              : 1)
          indices = [index]
          if (index) {
            indices.push(index - 1)
          }
          if (index < this.num - 1) {
            indices.unshift(index + 1)
          }
        }
        while (indices.length) {
          index = indices.pop()
          this.translateX(index, touchDeltaX + this.positions[index], 0)
        }
      } else {
        this.translateY(index, this.touchDelta.y + this.positions[index], 0)
      }
    },

    ontouchend: function (event) {
      if (this.options.stopTouchEventsPropagation) {
        this.stopPropagation(event)
      }
      var index = this.index
      var speed = this.options.transitionSpeed
      var slideWidth = this.slideWidth
      var isShortDuration = Number(Date.now() - this.touchStart.time) < 250
      // Determine if slide attempt triggers next/prev slide:
      var isValidSlide =
        (isShortDuration && Math.abs(this.touchDelta.x) > 20) ||
        Math.abs(this.touchDelta.x) > slideWidth / 2
      // Determine if slide attempt is past start or end:
      var isPastBounds =
        (!index && this.touchDelta.x > 0) ||
        (index === this.num - 1 && this.touchDelta.x < 0)
      var isValidClose =
        !isValidSlide &&
        this.options.closeOnSwipeUpOrDown &&
        ((isShortDuration && Math.abs(this.touchDelta.y) > 20) ||
          Math.abs(this.touchDelta.y) > this.slideHeight / 2)
      var direction
      var indexForward
      var indexBackward
      var distanceForward
      var distanceBackward
      if (this.options.continuous) {
        isPastBounds = false
      }
      // Determine direction of swipe (true: right, false: left):
      direction = this.touchDelta.x < 0 ? -1 : 1
      if (!this.isScrolling) {
        if (isValidSlide && !isPastBounds) {
          indexForward = index + direction
          indexBackward = index - direction
          distanceForward = slideWidth * direction
          distanceBackward = -slideWidth * direction
          if (this.options.continuous) {
            this.move(this.circle(indexForward), distanceForward, 0)
            this.move(this.circle(index - 2 * direction), distanceBackward, 0)
          } else if (indexForward >= 0 && indexForward < this.num) {
            this.move(indexForward, distanceForward, 0)
          }
          this.move(index, this.positions[index] + distanceForward, speed)
          this.move(
            this.circle(indexBackward),
            this.positions[this.circle(indexBackward)] + distanceForward,
            speed
          )
          index = this.circle(indexBackward)
          this.onslide(index)
        } else {
          // Move back into position
          if (this.options.continuous) {
            this.move(this.circle(index - 1), -slideWidth, speed)
            this.move(index, 0, speed)
            this.move(this.circle(index + 1), slideWidth, speed)
          } else {
            if (index) {
              this.move(index - 1, -slideWidth, speed)
            }
            this.move(index, 0, speed)
            if (index < this.num - 1) {
              this.move(index + 1, slideWidth, speed)
            }
          }
        }
      } else {
        if (isValidClose) {
          this.close()
        } else {
          // Move back into position
          this.translateY(index, 0, speed)
        }
      }
    },

    ontouchcancel: function (event) {
      if (this.touchStart) {
        this.ontouchend(event)
        delete this.touchStart
      }
    },

    ontransitionend: function (event) {
      var slide = this.slides[this.index]
      if (!event || slide === event.target) {
        if (this.interval) {
          this.play()
        }
        this.setTimeout(this.options.onslideend, [this.index, slide])
      }
    },

    oncomplete: function (event) {
      var target = event.target || event.srcElement
      var parent = target && target.parentNode
      var index
      if (!target || !parent) {
        return
      }
      index = this.getNodeIndex(parent)
      $(parent).removeClass(this.options.slideLoadingClass)
      if (event.type === 'error') {
        $(parent).addClass(this.options.slideErrorClass)
        this.elements[index] = 3 // Fail
      } else {
        this.elements[index] = 2 // Done
      }
      // Fix for IE7's lack of support for percentage max-height:
      if (target.clientHeight > this.container[0].clientHeight) {
        target.style.maxHeight = this.container[0].clientHeight
      }
      if (this.interval && this.slides[this.index] === parent) {
        this.play()
      }
      this.setTimeout(this.options.onslidecomplete, [index, parent])
    },

    onload: function (event) {
      this.oncomplete(event)
    },

    onerror: function (event) {
      this.oncomplete(event)
    },

    onkeydown: function (event) {
      switch (event.which || event.keyCode) {
        case 13: // Return
          if (this.options.toggleControlsOnReturn) {
            this.preventDefault(event)
            this.toggleControls()
          }
          break
        case 27: // Esc
          if (this.options.closeOnEscape) {
            this.close()
            // prevent Esc from closing other things
            event.stopImmediatePropagation()
          }
          break
        case 32: // Space
          if (this.options.toggleSlideshowOnSpace) {
            this.preventDefault(event)
            this.toggleSlideshow()
          }
          break
        case 37: // Left
          if (this.options.enableKeyboardNavigation) {
            this.preventDefault(event)
            this.prev()
          }
          break
        case 39: // Right
          if (this.options.enableKeyboardNavigation) {
            this.preventDefault(event)
            this.next()
          }
          break
      }
    },

    handleClick: function (event) {
      var options = this.options
      var target = event.target || event.srcElement
      var parent = target.parentNode
      function isTarget (className) {
        return $(target).hasClass(className) || $(parent).hasClass(className)
      }
      if (isTarget(options.toggleClass)) {
        // Click on "toggle" control
        this.preventDefault(event)
        this.toggleControls()
      } else if (isTarget(options.prevClass)) {
        // Click on "prev" control
        this.preventDefault(event)
        this.prev()
      } else if (isTarget(options.nextClass)) {
        // Click on "next" control
        this.preventDefault(event)
        this.next()
      } else if (isTarget(options.closeClass)) {
        // Click on "close" control
        this.preventDefault(event)
        this.close()
      } else if (isTarget(options.playPauseClass)) {
        // Click on "play-pause" control
        this.preventDefault(event)
        this.toggleSlideshow()
      } else if (parent === this.slidesContainer[0]) {
        // Click on slide background
        if (options.closeOnSlideClick) {
          this.preventDefault(event)
          this.close()
        } else if (options.toggleControlsOnSlideClick) {
          this.preventDefault(event)
          this.toggleControls()
        }
      } else if (
        parent.parentNode &&
        parent.parentNode === this.slidesContainer[0]
      ) {
        // Click on displayed element
        if (options.toggleControlsOnSlideClick) {
          this.preventDefault(event)
          this.toggleControls()
        }
      }
    },

    onclick: function (event) {
      if (
        this.options.emulateTouchEvents &&
        this.touchDelta &&
        (Math.abs(this.touchDelta.x) > 20 || Math.abs(this.touchDelta.y) > 20)
      ) {
        delete this.touchDelta
        return
      }
      return this.handleClick(event)
    },

    updateEdgeClasses: function (index) {
      if (!index) {
        this.container.addClass(this.options.leftEdgeClass)
      } else {
        this.container.removeClass(this.options.leftEdgeClass)
      }
      if (index === this.num - 1) {
        this.container.addClass(this.options.rightEdgeClass)
      } else {
        this.container.removeClass(this.options.rightEdgeClass)
      }
    },

    handleSlide: function (index) {
      if (!this.options.continuous) {
        this.updateEdgeClasses(index)
      }
      this.loadElements(index)
      if (this.options.unloadElements) {
        this.unloadElements(index)
      }
      this.setTitle(index)
    },

    onslide: function (index) {
      this.index = index
      this.handleSlide(index)
      this.setTimeout(this.options.onslide, [index, this.slides[index]])
    },

    setTitle: function (index) {
      var firstChild = this.slides[index].firstChild
      var text = firstChild.title || firstChild.alt
      var titleElement = this.titleElement
      if (titleElement.length) {
        this.titleElement.empty()
        if (text) {
          titleElement[0].appendChild(document.createTextNode(text))
        }
      }
    },

    setTimeout: function (func, args, wait) {
      var that = this
      return (
        func &&
        window.setTimeout(function () {
          func.apply(that, args || [])
        }, wait || 0)
      )
    },

    imageFactory: function (obj, callback) {
      var that = this
      var img = this.imagePrototype.cloneNode(false)
      var url = obj
      var backgroundSize = this.options.stretchImages
      var called
      var element
      var title
      var altText
      function callbackWrapper (event) {
        if (!called) {
          event = {
            type: event.type,
            target: element
          }
          if (!element.parentNode) {
            // Fix for IE7 firing the load event for
            // cached images before the element could
            // be added to the DOM:
            return that.setTimeout(callbackWrapper, [event])
          }
          called = true
          $(img).off('load error', callbackWrapper)
          if (backgroundSize) {
            if (event.type === 'load') {
              element.style.background = 'url("' + url + '") center no-repeat'
              element.style.backgroundSize = backgroundSize
            }
          }
          callback(event)
        }
      }
      if (typeof url !== 'string') {
        url = this.getItemProperty(obj, this.options.urlProperty)
        title = this.getItemProperty(obj, this.options.titleProperty)
        altText =
          this.getItemProperty(obj, this.options.altTextProperty) || title
      }
      if (backgroundSize === true) {
        backgroundSize = 'contain'
      }
      backgroundSize =
        this.support.backgroundSize &&
        this.support.backgroundSize[backgroundSize] &&
        backgroundSize
      if (backgroundSize) {
        element = this.elementPrototype.cloneNode(false)
      } else {
        element = img
        img.draggable = false
      }
      if (title) {
        element.title = title
      }
      if (altText) {
        element.alt = altText
      }
      $(img).on('load error', callbackWrapper)
      img.src = url
      return element
    },

    createElement: function (obj, callback) {
      var type = obj && this.getItemProperty(obj, this.options.typeProperty)
      var factory =
        (type && this[type.split('/')[0] + 'Factory']) || this.imageFactory
      var element = obj && factory.call(this, obj, callback)
      var srcset = this.getItemProperty(obj, this.options.srcsetProperty)
      if (!element) {
        element = this.elementPrototype.cloneNode(false)
        this.setTimeout(callback, [
          {
            type: 'error',
            target: element
          }
        ])
      }
      if (srcset) {
        element.setAttribute('srcset', srcset)
      }
      $(element).addClass(this.options.slideContentClass)
      return element
    },

    loadElement: function (index) {
      if (!this.elements[index]) {
        if (this.slides[index].firstChild) {
          this.elements[index] = $(this.slides[index]).hasClass(
            this.options.slideErrorClass
          )
            ? 3
            : 2
        } else {
          this.elements[index] = 1 // Loading
          $(this.slides[index]).addClass(this.options.slideLoadingClass)
          this.slides[index].appendChild(
            this.createElement(this.list[index], this.proxyListener)
          )
        }
      }
    },

    loadElements: function (index) {
      var limit = Math.min(this.num, this.options.preloadRange * 2 + 1)
      var j = index
      var i
      for (i = 0; i < limit; i += 1) {
        // First load the current slide element (0),
        // then the next one (+1),
        // then the previous one (-2),
        // then the next after next (+2), etc.:
        j += i * (i % 2 === 0 ? -1 : 1)
        // Connect the ends of the list to load slide elements for
        // continuous navigation:
        j = this.circle(j)
        this.loadElement(j)
      }
    },

    unloadElements: function (index) {
      var i, diff
      for (i in this.elements) {
        if (this.elements.hasOwnProperty(i)) {
          diff = Math.abs(index - i)
          if (
            diff > this.options.preloadRange &&
            diff + this.options.preloadRange < this.num
          ) {
            this.unloadSlide(i)
            delete this.elements[i]
          }
        }
      }
    },

    addSlide: function (index) {
      var slide = this.slidePrototype.cloneNode(false)
      slide.setAttribute('data-index', index)
      this.slidesContainer[0].appendChild(slide)
      this.slides.push(slide)
    },

    positionSlide: function (index) {
      var slide = this.slides[index]
      slide.style.width = this.slideWidth + 'px'
      if (this.support.transform) {
        slide.style.left = index * -this.slideWidth + 'px'
        this.move(
          index,
          this.index > index
            ? -this.slideWidth
            : this.index < index ? this.slideWidth : 0,
          0
        )
      }
    },

    initSlides: function (reload) {
      var clearSlides, i
      if (!reload) {
        this.positions = []
        this.positions.length = this.num
        this.elements = {}
        this.imagePrototype = document.createElement('img')
        this.elementPrototype = document.createElement('div')
        this.slidePrototype = document.createElement('div')
        $(this.slidePrototype).addClass(this.options.slideClass)
        this.slides = this.slidesContainer[0].children
        clearSlides =
          this.options.clearSlides || this.slides.length !== this.num
      }
      this.slideWidth = this.container[0].clientWidth
      this.slideHeight = this.container[0].clientHeight
      this.slidesContainer[0].style.width = this.num * this.slideWidth + 'px'
      if (clearSlides) {
        this.resetSlides()
      }
      for (i = 0; i < this.num; i += 1) {
        if (clearSlides) {
          this.addSlide(i)
        }
        this.positionSlide(i)
      }
      // Reposition the slides before and after the given index:
      if (this.options.continuous && this.support.transform) {
        this.move(this.circle(this.index - 1), -this.slideWidth, 0)
        this.move(this.circle(this.index + 1), this.slideWidth, 0)
      }
      if (!this.support.transform) {
        this.slidesContainer[0].style.left =
          this.index * -this.slideWidth + 'px'
      }
    },

    unloadSlide: function (index) {
      var slide, firstChild
      slide = this.slides[index]
      firstChild = slide.firstChild
      if (firstChild !== null) {
        slide.removeChild(firstChild)
      }
    },

    unloadAllSlides: function () {
      var i, len
      for (i = 0, len = this.slides.length; i < len; i++) {
        this.unloadSlide(i)
      }
    },

    toggleControls: function () {
      var controlsClass = this.options.controlsClass
      if (this.container.hasClass(controlsClass)) {
        this.container.removeClass(controlsClass)
      } else {
        this.container.addClass(controlsClass)
      }
    },

    toggleSlideshow: function () {
      if (!this.interval) {
        this.play()
      } else {
        this.pause()
      }
    },

    getNodeIndex: function (element) {
      return parseInt(element.getAttribute('data-index'), 10)
    },

    getNestedProperty: function (obj, property) {
      property.replace(
        // Matches native JavaScript notation in a String,
        // e.g. '["doubleQuoteProp"].dotProp[2]'
        // eslint-disable-next-line no-useless-escape
        /\[(?:'([^']+)'|"([^"]+)"|(\d+))\]|(?:(?:^|\.)([^\.\[]+))/g,
        function (str, singleQuoteProp, doubleQuoteProp, arrayIndex, dotProp) {
          var prop =
            dotProp ||
            singleQuoteProp ||
            doubleQuoteProp ||
            (arrayIndex && parseInt(arrayIndex, 10))
          if (str && obj) {
            obj = obj[prop]
          }
        }
      )
      return obj
    },

    getDataProperty: function (obj, property) {
      var key
      var prop
      if (obj.dataset) {
        key = property.replace(/-([a-z])/g, function (_, b) {
          return b.toUpperCase()
        })
        prop = obj.dataset[key]
      } else if (obj.getAttribute) {
        prop = obj.getAttribute(
          'data-' + property.replace(/([A-Z])/g, '-$1').toLowerCase()
        )
      }
      if (typeof prop === 'string') {
        // eslint-disable-next-line no-useless-escape
        if (
          /^(true|false|null|-?\d+(\.\d+)?|\{[\s\S]*\}|\[[\s\S]*\])$/.test(prop)
        ) {
          try {
            return $.parseJSON(prop)
          } catch (ignore) {}
        }
        return prop
      }
    },

    getItemProperty: function (obj, property) {
      var prop = this.getDataProperty(obj, property)
      if (prop === undefined) {
        prop = obj[property]
      }
      if (prop === undefined) {
        prop = this.getNestedProperty(obj, property)
      }
      return prop
    },

    initStartIndex: function () {
      var index = this.options.index
      var urlProperty = this.options.urlProperty
      var i
      // Check if the index is given as a list object:
      if (index && typeof index !== 'number') {
        for (i = 0; i < this.num; i += 1) {
          if (
            this.list[i] === index ||
            this.getItemProperty(this.list[i], urlProperty) ===
              this.getItemProperty(index, urlProperty)
          ) {
            index = i
            break
          }
        }
      }
      // Make sure the index is in the list range:
      this.index = this.circle(parseInt(index, 10) || 0)
    },

    initEventListeners: function () {
      var that = this
      var slidesContainer = this.slidesContainer
      function proxyListener (event) {
        var type =
          that.support.transition && that.support.transition.end === event.type
            ? 'transitionend'
            : event.type
        that['on' + type](event)
      }
      $(window).on('resize', proxyListener)
      $(document.body).on('keydown', proxyListener)
      this.container.on('click', proxyListener)
      if (this.support.touch) {
        slidesContainer.on(
          'touchstart touchmove touchend touchcancel',
          proxyListener
        )
      } else if (this.options.emulateTouchEvents && this.support.transition) {
        slidesContainer.on(
          'mousedown mousemove mouseup mouseout',
          proxyListener
        )
      }
      if (this.support.transition) {
        slidesContainer.on(this.support.transition.end, proxyListener)
      }
      this.proxyListener = proxyListener
    },

    destroyEventListeners: function () {
      var slidesContainer = this.slidesContainer
      var proxyListener = this.proxyListener
      $(window).off('resize', proxyListener)
      $(document.body).off('keydown', proxyListener)
      this.container.off('click', proxyListener)
      if (this.support.touch) {
        slidesContainer.off(
          'touchstart touchmove touchend touchcancel',
          proxyListener
        )
      } else if (this.options.emulateTouchEvents && this.support.transition) {
        slidesContainer.off(
          'mousedown mousemove mouseup mouseout',
          proxyListener
        )
      }
      if (this.support.transition) {
        slidesContainer.off(this.support.transition.end, proxyListener)
      }
    },

    handleOpen: function () {
      if (this.options.onopened) {
        this.options.onopened.call(this)
      }
    },

    initWidget: function () {
      var that = this
      function openHandler (event) {
        if (event.target === that.container[0]) {
          that.container.off(that.support.transition.end, openHandler)
          that.handleOpen()
        }
      }
      this.container = $(this.options.container)
      if (!this.container.length) {
        this.console.log(
          'blueimp Gallery: Widget container not found.',
          this.options.container
        )
        return false
      }
      this.slidesContainer = this.container
        .find(this.options.slidesContainer)
        .first()
      if (!this.slidesContainer.length) {
        this.console.log(
          'blueimp Gallery: Slides container not found.',
          this.options.slidesContainer
        )
        return false
      }
      this.titleElement = this.container.find(this.options.titleElement).first()
      if (this.num === 1) {
        this.container.addClass(this.options.singleClass)
      }
      if (this.options.onopen) {
        this.options.onopen.call(this)
      }
      if (this.support.transition && this.options.displayTransition) {
        this.container.on(this.support.transition.end, openHandler)
      } else {
        this.handleOpen()
      }
      if (this.options.hidePageScrollbars) {
        // Hide the page scrollbars:
        this.bodyOverflowStyle = document.body.style.overflow
        document.body.style.overflow = 'hidden'
      }
      this.container[0].style.display = 'block'
      this.initSlides()
      this.container.addClass(this.options.displayClass)
    },

    initOptions: function (options) {
      // Create a copy of the prototype options:
      this.options = $.extend({}, this.options)
      // Check if carousel mode is enabled:
      if (
        (options && options.carousel) ||
        (this.options.carousel && (!options || options.carousel !== false))
      ) {
        $.extend(this.options, this.carouselOptions)
      }
      // Override any given options:
      $.extend(this.options, options)
      if (this.num < 3) {
        // 1 or 2 slides cannot be displayed continuous,
        // remember the original option by setting to null instead of false:
        this.options.continuous = this.options.continuous ? null : false
      }
      if (!this.support.transition) {
        this.options.emulateTouchEvents = false
      }
      if (this.options.event) {
        this.preventDefault(this.options.event)
      }
    }
  })

  return Gallery
})


/***/ }),

/***/ "./node_modules/blueimp-gallery/js/blueimp-helper.js":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/*
 * blueimp helper JS
 * https://github.com/blueimp/Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global define, window, document */

;(function () {
  'use strict'

  function extend (obj1, obj2) {
    var prop
    for (prop in obj2) {
      if (obj2.hasOwnProperty(prop)) {
        obj1[prop] = obj2[prop]
      }
    }
    return obj1
  }

  function Helper (query) {
    if (!this || this.find !== Helper.prototype.find) {
      // Called as function instead of as constructor,
      // so we simply return a new instance:
      return new Helper(query)
    }
    this.length = 0
    if (query) {
      if (typeof query === 'string') {
        query = this.find(query)
      }
      if (query.nodeType || query === query.window) {
        // Single HTML element
        this.length = 1
        this[0] = query
      } else {
        // HTML element collection
        var i = query.length
        this.length = i
        while (i) {
          i -= 1
          this[i] = query[i]
        }
      }
    }
  }

  Helper.extend = extend

  Helper.contains = function (container, element) {
    do {
      element = element.parentNode
      if (element === container) {
        return true
      }
    } while (element)
    return false
  }

  Helper.parseJSON = function (string) {
    return window.JSON && JSON.parse(string)
  }

  extend(Helper.prototype, {
    find: function (query) {
      var container = this[0] || document
      if (typeof query === 'string') {
        if (container.querySelectorAll) {
          query = container.querySelectorAll(query)
        } else if (query.charAt(0) === '#') {
          query = container.getElementById(query.slice(1))
        } else {
          query = container.getElementsByTagName(query)
        }
      }
      return new Helper(query)
    },

    hasClass: function (className) {
      if (!this[0]) {
        return false
      }
      return new RegExp('(^|\\s+)' + className + '(\\s+|$)').test(
        this[0].className
      )
    },

    addClass: function (className) {
      var i = this.length
      var element
      while (i) {
        i -= 1
        element = this[i]
        if (!element.className) {
          element.className = className
          return this
        }
        if (this.hasClass(className)) {
          return this
        }
        element.className += ' ' + className
      }
      return this
    },

    removeClass: function (className) {
      var regexp = new RegExp('(^|\\s+)' + className + '(\\s+|$)')
      var i = this.length
      var element
      while (i) {
        i -= 1
        element = this[i]
        element.className = element.className.replace(regexp, ' ')
      }
      return this
    },

    on: function (eventName, handler) {
      var eventNames = eventName.split(/\s+/)
      var i
      var element
      while (eventNames.length) {
        eventName = eventNames.shift()
        i = this.length
        while (i) {
          i -= 1
          element = this[i]
          if (element.addEventListener) {
            element.addEventListener(eventName, handler, false)
          } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, handler)
          }
        }
      }
      return this
    },

    off: function (eventName, handler) {
      var eventNames = eventName.split(/\s+/)
      var i
      var element
      while (eventNames.length) {
        eventName = eventNames.shift()
        i = this.length
        while (i) {
          i -= 1
          element = this[i]
          if (element.removeEventListener) {
            element.removeEventListener(eventName, handler, false)
          } else if (element.detachEvent) {
            element.detachEvent('on' + eventName, handler)
          }
        }
      }
      return this
    },

    empty: function () {
      var i = this.length
      var element
      while (i) {
        i -= 1
        element = this[i]
        while (element.hasChildNodes()) {
          element.removeChild(element.lastChild)
        }
      }
      return this
    },

    first: function () {
      return new Helper(this[0])
    }
  })

  if (true) {
    !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
      return Helper
    }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))
  } else {
    window.blueimp = window.blueimp || {}
    window.blueimp.helper = Helper
  }
})()


/***/ }),

/***/ "./node_modules/blueimp-gallery/js/jquery.blueimp-gallery.js":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
 * blueimp Gallery jQuery plugin
 * https://github.com/blueimp/Gallery
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global define, window, document */

;(function (factory) {
  'use strict'
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__("./node_modules/jquery/src/jquery.js"), __webpack_require__("./node_modules/blueimp-gallery/js/blueimp-gallery.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))
  } else {
    factory(window.jQuery, window.blueimp.Gallery)
  }
})(function ($, Gallery) {
  'use strict'

  // Global click handler to open links with data-gallery attribute
  // in the Gallery lightbox:
  $(document).on('click', '[data-gallery]', function (event) {
    // Get the container id from the data-gallery attribute:
    var id = $(this).data('gallery')
    var widget = $(id)
    var container =
      (widget.length && widget) || $(Gallery.prototype.options.container)
    var callbacks = {
      onopen: function () {
        container.data('gallery', this).trigger('open')
      },
      onopened: function () {
        container.trigger('opened')
      },
      onslide: function () {
        container.trigger('slide', arguments)
      },
      onslideend: function () {
        container.trigger('slideend', arguments)
      },
      onslidecomplete: function () {
        container.trigger('slidecomplete', arguments)
      },
      onclose: function () {
        container.trigger('close')
      },
      onclosed: function () {
        container.trigger('closed').removeData('gallery')
      }
    }
    var options = $.extend(
      // Retrieve custom options from data-attributes
      // on the Gallery widget:
      container.data(),
      {
        container: container[0],
        index: this,
        event: event
      },
      callbacks
    )
    // Select all links with the same data-gallery attribute:
    var links = $(this)
      .closest('[data-gallery-group], body')
      .find('[data-gallery="' + id + '"]')
    if (options.filter) {
      links = links.filter(options.filter)
    }
    return new Gallery(links, options)
  })
})


/***/ }),

/***/ "./node_modules/bootstrap-growl/jquery.bootstrap-growl.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(jQuery) {(function() {
  var $;

  $ = jQuery;

  $.bootstrapGrowl = function(message, options) {
    var $alert, css, offsetAmount;
    options = $.extend({}, $.bootstrapGrowl.default_options, options);
    $alert = $("<div>");
    $alert.attr("class", "bootstrap-growl alert");
    if (options.class) {
      $alert.addClass(options.class);
    }
    if (options.type) {
      $alert.addClass("alert-" + options.type);
    }
    if (options.allow_dismiss) {
      $alert.addClass("alert-dismissible");
      $alert.append("<button  class=\"close\" data-dismiss=\"alert\" type=\"button\"><span aria-hidden=\"true\">&#215;</span><span class=\"sr-only\">Close</span></button>");
    }
    $alert.append(message);
    if (options.top_offset) {
      options.offset = {
        from: "top",
        amount: options.top_offset
      };
    }
    offsetAmount = options.offset.amount;
    $(".bootstrap-growl").each(function() {
      return offsetAmount = Math.max(offsetAmount, parseInt($(this).css(options.offset.from)) + $(this).outerHeight() + options.stackup_spacing);
    });
    css = {
      "position": (options.ele === "body" ? "fixed" : "absolute"),
      "margin": 0,
      "z-index": "9999",
      "display": "none"
    };
    css[options.offset.from] = offsetAmount + "px";
    $alert.css(css);
    if (options.width !== "auto") {
      $alert.css("width", options.width + "px");
    }
    $(options.ele).append($alert);
    switch (options.align) {
      case "center":
        $alert.css({
          "left": "50%",
          "margin-left": "-" + ($alert.outerWidth() / 2) + "px"
        });
        break;
      case "left":
        $alert.css("left", "20px");
        break;
      default:
        $alert.css("right", "20px");
    }
    $alert.fadeIn();
    if (options.delay > 0) {
      $alert.delay(options.delay).fadeOut(function() {
        return $(this).alert("close");
      });
    }
    return $alert;
  };

  $.bootstrapGrowl.default_options = {
    ele: "body",
    type: "info",
    offset: {
      from: "top",
      amount: 20
    },
    align: "right",
    width: 250,
    delay: 4000,
    allow_dismiss: true,
    stackup_spacing: 10
  };

}).call(this);

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/src/jquery.js")))

/***/ }),

/***/ "./node_modules/cropit/dist/jquery.cropit.js":
/***/ (function(module, exports, __webpack_require__) {

/*! cropit - v0.5.0 <https://github.com/scottcheng/cropit> */
(function webpackUniversalModuleDefinition(root, factory) {
	if(true)
		module.exports = factory(__webpack_require__("./node_modules/jquery/src/jquery.js"));
	else if(typeof define === 'function' && define.amd)
		define(["jquery"], factory);
	else if(typeof exports === 'object')
		exports["cropit"] = factory(require("jquery"));
	else
		root["cropit"] = factory(root["jQuery"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var _slice = Array.prototype.slice;

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	var _jquery = __webpack_require__(1);

	var _jquery2 = _interopRequireDefault(_jquery);

	var _cropit = __webpack_require__(2);

	var _cropit2 = _interopRequireDefault(_cropit);

	var _constants = __webpack_require__(4);

	var _utils = __webpack_require__(6);

	var applyOnEach = function applyOnEach($el, callback) {
	  return $el.each(function () {
	    var cropit = _jquery2['default'].data(this, _constants.PLUGIN_KEY);

	    if (!cropit) {
	      return;
	    }
	    callback(cropit);
	  });
	};

	var callOnFirst = function callOnFirst($el, method, options) {
	  var cropit = $el.first().data(_constants.PLUGIN_KEY);

	  if (!cropit || !_jquery2['default'].isFunction(cropit[method])) {
	    return null;
	  }
	  return cropit[method](options);
	};

	var methods = {
	  init: function init(options) {
	    return this.each(function () {
	      // Only instantiate once per element
	      if (_jquery2['default'].data(this, _constants.PLUGIN_KEY)) {
	        return;
	      }

	      var cropit = new _cropit2['default'](_jquery2['default'], this, options);
	      _jquery2['default'].data(this, _constants.PLUGIN_KEY, cropit);
	    });
	  },

	  destroy: function destroy() {
	    return this.each(function () {
	      _jquery2['default'].removeData(this, _constants.PLUGIN_KEY);
	    });
	  },

	  isZoomable: function isZoomable() {
	    return callOnFirst(this, 'isZoomable');
	  },

	  'export': function _export(options) {
	    return callOnFirst(this, 'getCroppedImageData', options);
	  }
	};

	var delegate = function delegate($el, fnName) {
	  return applyOnEach($el, function (cropit) {
	    cropit[fnName]();
	  });
	};

	var prop = function prop($el, name, value) {
	  if ((0, _utils.exists)(value)) {
	    return applyOnEach($el, function (cropit) {
	      cropit[name] = value;
	    });
	  } else {
	    var cropit = $el.first().data(_constants.PLUGIN_KEY);
	    return cropit[name];
	  }
	};

	_jquery2['default'].fn.cropit = function (method) {
	  if (methods[method]) {
	    return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
	  } else if (['imageState', 'imageSrc', 'offset', 'previewSize', 'imageSize', 'zoom', 'initialZoom', 'exportZoom', 'minZoom', 'maxZoom'].indexOf(method) >= 0) {
	    return prop.apply(undefined, [this].concat(_slice.call(arguments)));
	  } else if (['rotateCW', 'rotateCCW', 'disable', 'reenable'].indexOf(method) >= 0) {
	    return delegate.apply(undefined, [this].concat(_slice.call(arguments)));
	  } else {
	    return methods.init.apply(this, arguments);
	  }
	};

/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	var _jquery = __webpack_require__(1);

	var _jquery2 = _interopRequireDefault(_jquery);

	var _Zoomer = __webpack_require__(3);

	var _Zoomer2 = _interopRequireDefault(_Zoomer);

	var _constants = __webpack_require__(4);

	var _options = __webpack_require__(5);

	var _utils = __webpack_require__(6);

	var Cropit = (function () {
	  function Cropit(jQuery, element, options) {
	    _classCallCheck(this, Cropit);

	    this.$el = (0, _jquery2['default'])(element);

	    var defaults = (0, _options.loadDefaults)(this.$el);
	    this.options = _jquery2['default'].extend({}, defaults, options);

	    this.init();
	  }

	  _createClass(Cropit, [{
	    key: 'init',
	    value: function init() {
	      var _this = this;

	      this.image = new Image();
	      this.preImage = new Image();
	      this.image.onload = this.onImageLoaded.bind(this);
	      this.preImage.onload = this.onPreImageLoaded.bind(this);
	      this.image.onerror = this.preImage.onerror = function () {
	        _this.onImageError.call(_this, _constants.ERRORS.IMAGE_FAILED_TO_LOAD);
	      };

	      this.$preview = this.options.$preview.css('position', 'relative');
	      this.$fileInput = this.options.$fileInput.attr({ accept: 'image/*' });
	      this.$zoomSlider = this.options.$zoomSlider.attr({ min: 0, max: 1, step: 0.01 });

	      this.previewSize = {
	        width: this.options.width || this.$preview.width(),
	        height: this.options.height || this.$preview.height()
	      };

	      this.$image = (0, _jquery2['default'])('<img />').addClass(_constants.CLASS_NAMES.PREVIEW_IMAGE).attr('alt', '').css({
	        transformOrigin: 'top left',
	        webkitTransformOrigin: 'top left',
	        willChange: 'transform'
	      });
	      this.$imageContainer = (0, _jquery2['default'])('<div />').addClass(_constants.CLASS_NAMES.PREVIEW_IMAGE_CONTAINER).css({
	        position: 'absolute',
	        overflow: 'hidden',
	        left: 0,
	        top: 0,
	        width: '100%',
	        height: '100%'
	      }).append(this.$image);
	      this.$preview.append(this.$imageContainer);

	      if (this.options.imageBackground) {
	        if (_jquery2['default'].isArray(this.options.imageBackgroundBorderWidth)) {
	          this.bgBorderWidthArray = this.options.imageBackgroundBorderWidth;
	        } else {
	          this.bgBorderWidthArray = [0, 1, 2, 3].map(function () {
	            return _this.options.imageBackgroundBorderWidth;
	          });
	        }

	        this.$bg = (0, _jquery2['default'])('<img />').addClass(_constants.CLASS_NAMES.PREVIEW_BACKGROUND).attr('alt', '').css({
	          position: 'relative',
	          left: this.bgBorderWidthArray[3],
	          top: this.bgBorderWidthArray[0],
	          transformOrigin: 'top left',
	          webkitTransformOrigin: 'top left',
	          willChange: 'transform'
	        });
	        this.$bgContainer = (0, _jquery2['default'])('<div />').addClass(_constants.CLASS_NAMES.PREVIEW_BACKGROUND_CONTAINER).css({
	          position: 'absolute',
	          zIndex: 0,
	          top: -this.bgBorderWidthArray[0],
	          right: -this.bgBorderWidthArray[1],
	          bottom: -this.bgBorderWidthArray[2],
	          left: -this.bgBorderWidthArray[3]
	        }).append(this.$bg);
	        if (this.bgBorderWidthArray[0] > 0) {
	          this.$bgContainer.css('overflow', 'hidden');
	        }
	        this.$preview.prepend(this.$bgContainer);
	      }

	      this.initialZoom = this.options.initialZoom;

	      this.imageLoaded = false;

	      this.moveContinue = false;

	      this.zoomer = new _Zoomer2['default']();

	      if (this.options.allowDragNDrop) {
	        _jquery2['default'].event.props.push('dataTransfer');
	      }

	      this.bindListeners();

	      if (this.options.imageState && this.options.imageState.src) {
	        this.loadImage(this.options.imageState.src);
	      }
	    }
	  }, {
	    key: 'bindListeners',
	    value: function bindListeners() {
	      this.$fileInput.on('change.cropit', this.onFileChange.bind(this));
	      this.$imageContainer.on(_constants.EVENTS.PREVIEW, this.onPreviewEvent.bind(this));
	      this.$zoomSlider.on(_constants.EVENTS.ZOOM_INPUT, this.onZoomSliderChange.bind(this));

	      if (this.options.allowDragNDrop) {
	        this.$imageContainer.on('dragover.cropit dragleave.cropit', this.onDragOver.bind(this));
	        this.$imageContainer.on('drop.cropit', this.onDrop.bind(this));
	      }
	    }
	  }, {
	    key: 'unbindListeners',
	    value: function unbindListeners() {
	      this.$fileInput.off('change.cropit');
	      this.$imageContainer.off(_constants.EVENTS.PREVIEW);
	      this.$imageContainer.off('dragover.cropit dragleave.cropit drop.cropit');
	      this.$zoomSlider.off(_constants.EVENTS.ZOOM_INPUT);
	    }
	  }, {
	    key: 'onFileChange',
	    value: function onFileChange(e) {
	      this.options.onFileChange(e);

	      if (this.$fileInput.get(0).files) {
	        this.loadFile(this.$fileInput.get(0).files[0]);
	      }
	    }
	  }, {
	    key: 'loadFile',
	    value: function loadFile(file) {
	      var fileReader = new FileReader();
	      if (file && file.type.match('image')) {
	        fileReader.readAsDataURL(file);
	        fileReader.onload = this.onFileReaderLoaded.bind(this);
	        fileReader.onerror = this.onFileReaderError.bind(this);
	      } else if (file) {
	        this.onFileReaderError();
	      }
	    }
	  }, {
	    key: 'onFileReaderLoaded',
	    value: function onFileReaderLoaded(e) {
	      this.loadImage(e.target.result);
	    }
	  }, {
	    key: 'onFileReaderError',
	    value: function onFileReaderError() {
	      this.options.onFileReaderError();
	    }
	  }, {
	    key: 'onDragOver',
	    value: function onDragOver(e) {
	      e.preventDefault();
	      e.dataTransfer.dropEffect = 'copy';
	      this.$preview.toggleClass(_constants.CLASS_NAMES.DRAG_HOVERED, e.type === 'dragover');
	    }
	  }, {
	    key: 'onDrop',
	    value: function onDrop(e) {
	      var _this2 = this;

	      e.preventDefault();
	      e.stopPropagation();

	      var files = Array.prototype.slice.call(e.dataTransfer.files, 0);
	      files.some(function (file) {
	        if (!file.type.match('image')) {
	          return false;
	        }

	        _this2.loadFile(file);
	        return true;
	      });

	      this.$preview.removeClass(_constants.CLASS_NAMES.DRAG_HOVERED);
	    }
	  }, {
	    key: 'loadImage',
	    value: function loadImage(imageSrc) {
	      var _this3 = this;

	      if (!imageSrc) {
	        return;
	      }

	      this.options.onImageLoading();
	      this.setImageLoadingClass();

	      if (imageSrc.indexOf('data') === 0) {
	        this.preImage.src = imageSrc;
	      } else {
	        var xhr = new XMLHttpRequest();
	        xhr.onload = function (e) {
	          if (e.target.status >= 300) {
	            _this3.onImageError.call(_this3, _constants.ERRORS.IMAGE_FAILED_TO_LOAD);
	            return;
	          }

	          _this3.loadFile(e.target.response);
	        };
	        xhr.open('GET', imageSrc);
	        xhr.responseType = 'blob';
	        xhr.send();
	      }
	    }
	  }, {
	    key: 'onPreImageLoaded',
	    value: function onPreImageLoaded() {
	      if (this.shouldRejectImage({
	        imageWidth: this.preImage.width,
	        imageHeight: this.preImage.height,
	        previewSize: this.previewSize,
	        maxZoom: this.options.maxZoom,
	        exportZoom: this.options.exportZoom,
	        smallImage: this.options.smallImage
	      })) {
	        this.onImageError(_constants.ERRORS.SMALL_IMAGE);
	        if (this.image.src) {
	          this.setImageLoadedClass();
	        }
	        return;
	      }

	      this.image.src = this.preImage.src;
	    }
	  }, {
	    key: 'onImageLoaded',
	    value: function onImageLoaded() {
	      this.rotation = 0;
	      this.setupZoomer(this.options.imageState && this.options.imageState.zoom || this._initialZoom);
	      if (this.options.imageState && this.options.imageState.offset) {
	        this.offset = this.options.imageState.offset;
	      } else {
	        this.centerImage();
	      }

	      this.options.imageState = {};

	      this.$image.attr('src', this.image.src);
	      if (this.options.imageBackground) {
	        this.$bg.attr('src', this.image.src);
	      }

	      this.setImageLoadedClass();

	      this.imageLoaded = true;

	      this.options.onImageLoaded();
	    }
	  }, {
	    key: 'onImageError',
	    value: function onImageError() {
	      this.options.onImageError.apply(this, arguments);
	      this.removeImageLoadingClass();
	    }
	  }, {
	    key: 'setImageLoadingClass',
	    value: function setImageLoadingClass() {
	      this.$preview.removeClass(_constants.CLASS_NAMES.IMAGE_LOADED).addClass(_constants.CLASS_NAMES.IMAGE_LOADING);
	    }
	  }, {
	    key: 'setImageLoadedClass',
	    value: function setImageLoadedClass() {
	      this.$preview.removeClass(_constants.CLASS_NAMES.IMAGE_LOADING).addClass(_constants.CLASS_NAMES.IMAGE_LOADED);
	    }
	  }, {
	    key: 'removeImageLoadingClass',
	    value: function removeImageLoadingClass() {
	      this.$preview.removeClass(_constants.CLASS_NAMES.IMAGE_LOADING);
	    }
	  }, {
	    key: 'getEventPosition',
	    value: function getEventPosition(e) {
	      if (e.originalEvent && e.originalEvent.touches && e.originalEvent.touches[0]) {
	        e = e.originalEvent.touches[0];
	      }
	      if (e.clientX && e.clientY) {
	        return { x: e.clientX, y: e.clientY };
	      }
	    }
	  }, {
	    key: 'onPreviewEvent',
	    value: function onPreviewEvent(e) {
	      if (!this.imageLoaded) {
	        return;
	      }

	      this.moveContinue = false;
	      this.$imageContainer.off(_constants.EVENTS.PREVIEW_MOVE);

	      if (e.type === 'mousedown' || e.type === 'touchstart') {
	        this.origin = this.getEventPosition(e);
	        this.moveContinue = true;
	        this.$imageContainer.on(_constants.EVENTS.PREVIEW_MOVE, this.onMove.bind(this));
	      } else {
	        (0, _jquery2['default'])(document.body).focus();
	      }

	      e.stopPropagation();
	      return false;
	    }
	  }, {
	    key: 'onMove',
	    value: function onMove(e) {
	      var eventPosition = this.getEventPosition(e);

	      if (this.moveContinue && eventPosition) {
	        this.offset = {
	          x: this.offset.x + eventPosition.x - this.origin.x,
	          y: this.offset.y + eventPosition.y - this.origin.y
	        };
	      }

	      this.origin = eventPosition;

	      e.stopPropagation();
	      return false;
	    }
	  }, {
	    key: 'fixOffset',
	    value: function fixOffset(offset) {
	      if (!this.imageLoaded) {
	        return offset;
	      }

	      var ret = { x: offset.x, y: offset.y };

	      if (!this.options.freeMove) {
	        if (this.imageWidth * this.zoom >= this.previewSize.width) {
	          ret.x = Math.min(0, Math.max(ret.x, this.previewSize.width - this.imageWidth * this.zoom));
	        } else {
	          ret.x = Math.max(0, Math.min(ret.x, this.previewSize.width - this.imageWidth * this.zoom));
	        }

	        if (this.imageHeight * this.zoom >= this.previewSize.height) {
	          ret.y = Math.min(0, Math.max(ret.y, this.previewSize.height - this.imageHeight * this.zoom));
	        } else {
	          ret.y = Math.max(0, Math.min(ret.y, this.previewSize.height - this.imageHeight * this.zoom));
	        }
	      }

	      ret.x = (0, _utils.round)(ret.x);
	      ret.y = (0, _utils.round)(ret.y);

	      return ret;
	    }
	  }, {
	    key: 'centerImage',
	    value: function centerImage() {
	      if (!this.image.width || !this.image.height || !this.zoom) {
	        return;
	      }

	      this.offset = {
	        x: (this.previewSize.width - this.imageWidth * this.zoom) / 2,
	        y: (this.previewSize.height - this.imageHeight * this.zoom) / 2
	      };
	    }
	  }, {
	    key: 'onZoomSliderChange',
	    value: function onZoomSliderChange() {
	      if (!this.imageLoaded) {
	        return;
	      }

	      this.zoomSliderPos = Number(this.$zoomSlider.val());
	      var newZoom = this.zoomer.getZoom(this.zoomSliderPos);
	      if (newZoom === this.zoom) {
	        return;
	      }
	      this.zoom = newZoom;
	    }
	  }, {
	    key: 'enableZoomSlider',
	    value: function enableZoomSlider() {
	      this.$zoomSlider.removeAttr('disabled');
	      this.options.onZoomEnabled();
	    }
	  }, {
	    key: 'disableZoomSlider',
	    value: function disableZoomSlider() {
	      this.$zoomSlider.attr('disabled', true);
	      this.options.onZoomDisabled();
	    }
	  }, {
	    key: 'setupZoomer',
	    value: function setupZoomer(zoom) {
	      this.zoomer.setup({
	        imageSize: this.imageSize,
	        previewSize: this.previewSize,
	        exportZoom: this.options.exportZoom,
	        maxZoom: this.options.maxZoom,
	        minZoom: this.options.minZoom,
	        smallImage: this.options.smallImage
	      });
	      this.zoom = (0, _utils.exists)(zoom) ? zoom : this._zoom;

	      if (this.isZoomable()) {
	        this.enableZoomSlider();
	      } else {
	        this.disableZoomSlider();
	      }
	    }
	  }, {
	    key: 'fixZoom',
	    value: function fixZoom(zoom) {
	      return this.zoomer.fixZoom(zoom);
	    }
	  }, {
	    key: 'isZoomable',
	    value: function isZoomable() {
	      return this.zoomer.isZoomable();
	    }
	  }, {
	    key: 'renderImage',
	    value: function renderImage() {
	      var transformation = '\n      translate(' + this.rotatedOffset.x + 'px, ' + this.rotatedOffset.y + 'px)\n      scale(' + this.zoom + ')\n      rotate(' + this.rotation + 'deg)';

	      this.$image.css({
	        transform: transformation,
	        webkitTransform: transformation
	      });
	      if (this.options.imageBackground) {
	        this.$bg.css({
	          transform: transformation,
	          webkitTransform: transformation
	        });
	      }
	    }
	  }, {
	    key: 'rotateCW',
	    value: function rotateCW() {
	      if (this.shouldRejectImage({
	        imageWidth: this.image.height,
	        imageHeight: this.image.width,
	        previewSize: this.previewSize,
	        maxZoom: this.options.maxZoom,
	        exportZoom: this.options.exportZoom,
	        smallImage: this.options.smallImage
	      })) {
	        this.rotation = (this.rotation + 180) % 360;
	      } else {
	        this.rotation = (this.rotation + 90) % 360;
	      }
	    }
	  }, {
	    key: 'rotateCCW',
	    value: function rotateCCW() {
	      if (this.shouldRejectImage({
	        imageWidth: this.image.height,
	        imageHeight: this.image.width,
	        previewSize: this.previewSize,
	        maxZoom: this.options.maxZoom,
	        exportZoom: this.options.exportZoom,
	        smallImage: this.options.smallImage
	      })) {
	        this.rotation = (this.rotation + 180) % 360;
	      } else {
	        this.rotation = (this.rotation + 270) % 360;
	      }
	    }
	  }, {
	    key: 'shouldRejectImage',
	    value: function shouldRejectImage(_ref) {
	      var imageWidth = _ref.imageWidth;
	      var imageHeight = _ref.imageHeight;
	      var previewSize = _ref.previewSize;
	      var maxZoom = _ref.maxZoom;
	      var exportZoom = _ref.exportZoom;
	      var smallImage = _ref.smallImage;

	      if (smallImage !== 'reject') {
	        return false;
	      }

	      return imageWidth * maxZoom < previewSize.width * exportZoom || imageHeight * maxZoom < previewSize.height * exportZoom;
	    }
	  }, {
	    key: 'getCroppedImageData',
	    value: function getCroppedImageData(exportOptions) {
	      if (!this.image.src) {
	        return;
	      }

	      var exportDefaults = {
	        type: 'image/png',
	        quality: 0.75,
	        originalSize: false,
	        fillBg: '#fff'
	      };
	      exportOptions = _jquery2['default'].extend({}, exportDefaults, exportOptions);

	      var exportZoom = exportOptions.originalSize ? 1 / this.zoom : this.options.exportZoom;

	      var zoomedSize = {
	        width: this.zoom * exportZoom * this.image.width,
	        height: this.zoom * exportZoom * this.image.height
	      };

	      var canvas = (0, _jquery2['default'])('<canvas />').attr({
	        width: this.previewSize.width * exportZoom,
	        height: this.previewSize.height * exportZoom
	      }).get(0);
	      var canvasContext = canvas.getContext('2d');

	      if (exportOptions.type === 'image/jpeg') {
	        canvasContext.fillStyle = exportOptions.fillBg;
	        canvasContext.fillRect(0, 0, canvas.width, canvas.height);
	      }

	      canvasContext.translate(this.rotatedOffset.x * exportZoom, this.rotatedOffset.y * exportZoom);
	      canvasContext.rotate(this.rotation * Math.PI / 180);
	      canvasContext.drawImage(this.image, 0, 0, zoomedSize.width, zoomedSize.height);

	      return canvas.toDataURL(exportOptions.type, exportOptions.quality);
	    }
	  }, {
	    key: 'disable',
	    value: function disable() {
	      this.unbindListeners();
	      this.disableZoomSlider();
	      this.$el.addClass(_constants.CLASS_NAMES.DISABLED);
	    }
	  }, {
	    key: 'reenable',
	    value: function reenable() {
	      this.bindListeners();
	      this.enableZoomSlider();
	      this.$el.removeClass(_constants.CLASS_NAMES.DISABLED);
	    }
	  }, {
	    key: '$',
	    value: function $(selector) {
	      if (!this.$el) {
	        return null;
	      }
	      return this.$el.find(selector);
	    }
	  }, {
	    key: 'offset',
	    set: function (position) {
	      if (!position || !(0, _utils.exists)(position.x) || !(0, _utils.exists)(position.y)) {
	        return;
	      }

	      this._offset = this.fixOffset(position);
	      this.renderImage();

	      this.options.onOffsetChange(position);
	    },
	    get: function () {
	      return this._offset;
	    }
	  }, {
	    key: 'zoom',
	    set: function (newZoom) {
	      newZoom = this.fixZoom(newZoom);

	      if (this.imageLoaded) {
	        var oldZoom = this.zoom;

	        var newX = this.previewSize.width / 2 - (this.previewSize.width / 2 - this.offset.x) * newZoom / oldZoom;
	        var newY = this.previewSize.height / 2 - (this.previewSize.height / 2 - this.offset.y) * newZoom / oldZoom;

	        this._zoom = newZoom;
	        this.offset = { x: newX, y: newY }; // Triggers renderImage()
	      } else {
	        this._zoom = newZoom;
	      }

	      this.zoomSliderPos = this.zoomer.getSliderPos(this.zoom);
	      this.$zoomSlider.val(this.zoomSliderPos);

	      this.options.onZoomChange(newZoom);
	    },
	    get: function () {
	      return this._zoom;
	    }
	  }, {
	    key: 'rotatedOffset',
	    get: function () {
	      return {
	        x: this.offset.x + (this.rotation === 90 ? this.image.height * this.zoom : 0) + (this.rotation === 180 ? this.image.width * this.zoom : 0),
	        y: this.offset.y + (this.rotation === 180 ? this.image.height * this.zoom : 0) + (this.rotation === 270 ? this.image.width * this.zoom : 0)
	      };
	    }
	  }, {
	    key: 'rotation',
	    set: function (newRotation) {
	      this._rotation = newRotation;

	      if (this.imageLoaded) {
	        // Change in image size may lead to change in zoom range
	        this.setupZoomer();
	      }
	    },
	    get: function () {
	      return this._rotation;
	    }
	  }, {
	    key: 'imageState',
	    get: function () {
	      return {
	        src: this.image.src,
	        offset: this.offset,
	        zoom: this.zoom
	      };
	    }
	  }, {
	    key: 'imageSrc',
	    get: function () {
	      return this.image.src;
	    },
	    set: function (imageSrc) {
	      this.loadImage(imageSrc);
	    }
	  }, {
	    key: 'imageWidth',
	    get: function () {
	      return this.rotation % 180 === 0 ? this.image.width : this.image.height;
	    }
	  }, {
	    key: 'imageHeight',
	    get: function () {
	      return this.rotation % 180 === 0 ? this.image.height : this.image.width;
	    }
	  }, {
	    key: 'imageSize',
	    get: function () {
	      return {
	        width: this.imageWidth,
	        height: this.imageHeight
	      };
	    }
	  }, {
	    key: 'initialZoom',
	    get: function () {
	      return this.options.initialZoom;
	    },
	    set: function (initialZoomOption) {
	      this.options.initialZoom = initialZoomOption;
	      if (initialZoomOption === 'min') {
	        this._initialZoom = 0; // Will be fixed when image loads
	      } else if (initialZoomOption === 'image') {
	        this._initialZoom = 1;
	      } else {
	        this._initialZoom = 0;
	      }
	    }
	  }, {
	    key: 'exportZoom',
	    get: function () {
	      return this.options.exportZoom;
	    },
	    set: function (exportZoom) {
	      this.options.exportZoom = exportZoom;
	      this.setupZoomer();
	    }
	  }, {
	    key: 'minZoom',
	    get: function () {
	      return this.options.minZoom;
	    },
	    set: function (minZoom) {
	      this.options.minZoom = minZoom;
	      this.setupZoomer();
	    }
	  }, {
	    key: 'maxZoom',
	    get: function () {
	      return this.options.maxZoom;
	    },
	    set: function (maxZoom) {
	      this.options.maxZoom = maxZoom;
	      this.setupZoomer();
	    }
	  }, {
	    key: 'previewSize',
	    get: function () {
	      return this._previewSize;
	    },
	    set: function (size) {
	      if (!size || size.width <= 0 || size.height <= 0) {
	        return;
	      }

	      this._previewSize = {
	        width: size.width,
	        height: size.height
	      };
	      this.$preview.css({
	        width: this.previewSize.width,
	        height: this.previewSize.height
	      });

	      if (this.imageLoaded) {
	        this.setupZoomer();
	      }
	    }
	  }]);

	  return Cropit;
	})();

	exports['default'] = Cropit;
	module.exports = exports['default'];

/***/ },
/* 3 */
/***/ function(module, exports) {

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	var Zoomer = (function () {
	  function Zoomer() {
	    _classCallCheck(this, Zoomer);

	    this.minZoom = this.maxZoom = 1;
	  }

	  _createClass(Zoomer, [{
	    key: 'setup',
	    value: function setup(_ref) {
	      var imageSize = _ref.imageSize;
	      var previewSize = _ref.previewSize;
	      var exportZoom = _ref.exportZoom;
	      var maxZoom = _ref.maxZoom;
	      var minZoom = _ref.minZoom;
	      var smallImage = _ref.smallImage;

	      var widthRatio = previewSize.width / imageSize.width;
	      var heightRatio = previewSize.height / imageSize.height;

	      if (minZoom === 'fit') {
	        this.minZoom = Math.min(widthRatio, heightRatio);
	      } else {
	        this.minZoom = Math.max(widthRatio, heightRatio);
	      }

	      if (smallImage === 'allow') {
	        this.minZoom = Math.min(this.minZoom, 1);
	      }

	      this.maxZoom = Math.max(this.minZoom, maxZoom / exportZoom);
	    }
	  }, {
	    key: 'getZoom',
	    value: function getZoom(sliderPos) {
	      if (!this.minZoom || !this.maxZoom) {
	        return null;
	      }

	      return sliderPos * (this.maxZoom - this.minZoom) + this.minZoom;
	    }
	  }, {
	    key: 'getSliderPos',
	    value: function getSliderPos(zoom) {
	      if (!this.minZoom || !this.maxZoom) {
	        return null;
	      }

	      if (this.minZoom === this.maxZoom) {
	        return 0;
	      } else {
	        return (zoom - this.minZoom) / (this.maxZoom - this.minZoom);
	      }
	    }
	  }, {
	    key: 'isZoomable',
	    value: function isZoomable() {
	      if (!this.minZoom || !this.maxZoom) {
	        return null;
	      }

	      return this.minZoom !== this.maxZoom;
	    }
	  }, {
	    key: 'fixZoom',
	    value: function fixZoom(zoom) {
	      return Math.max(this.minZoom, Math.min(this.maxZoom, zoom));
	    }
	  }]);

	  return Zoomer;
	})();

	exports['default'] = Zoomer;
	module.exports = exports['default'];

/***/ },
/* 4 */
/***/ function(module, exports) {

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	var PLUGIN_KEY = 'cropit';

	exports.PLUGIN_KEY = PLUGIN_KEY;
	var CLASS_NAMES = {
	  PREVIEW: 'cropit-preview',
	  PREVIEW_IMAGE_CONTAINER: 'cropit-preview-image-container',
	  PREVIEW_IMAGE: 'cropit-preview-image',
	  PREVIEW_BACKGROUND_CONTAINER: 'cropit-preview-background-container',
	  PREVIEW_BACKGROUND: 'cropit-preview-background',
	  FILE_INPUT: 'cropit-image-input',
	  ZOOM_SLIDER: 'cropit-image-zoom-input',

	  DRAG_HOVERED: 'cropit-drag-hovered',
	  IMAGE_LOADING: 'cropit-image-loading',
	  IMAGE_LOADED: 'cropit-image-loaded',
	  DISABLED: 'cropit-disabled'
	};

	exports.CLASS_NAMES = CLASS_NAMES;
	var ERRORS = {
	  IMAGE_FAILED_TO_LOAD: { code: 0, message: 'Image failed to load.' },
	  SMALL_IMAGE: { code: 1, message: 'Image is too small.' }
	};

	exports.ERRORS = ERRORS;
	var eventName = function eventName(events) {
	  return events.map(function (e) {
	    return '' + e + '.cropit';
	  }).join(' ');
	};
	var EVENTS = {
	  PREVIEW: eventName(['mousedown', 'mouseup', 'mouseleave', 'touchstart', 'touchend', 'touchcancel', 'touchleave']),
	  PREVIEW_MOVE: eventName(['mousemove', 'touchmove']),
	  ZOOM_INPUT: eventName(['mousemove', 'touchmove', 'change'])
	};
	exports.EVENTS = EVENTS;

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _constants = __webpack_require__(4);

	var options = {
	  elements: [{
	    name: '$preview',
	    description: 'The HTML element that displays image preview.',
	    defaultSelector: '.' + _constants.CLASS_NAMES.PREVIEW
	  }, {
	    name: '$fileInput',
	    description: 'File input element.',
	    defaultSelector: 'input.' + _constants.CLASS_NAMES.FILE_INPUT
	  }, {
	    name: '$zoomSlider',
	    description: 'Range input element that controls image zoom.',
	    defaultSelector: 'input.' + _constants.CLASS_NAMES.ZOOM_SLIDER
	  }].map(function (o) {
	    o.type = 'jQuery element';
	    o['default'] = '$imageCropper.find(\'' + o.defaultSelector + '\')';
	    return o;
	  }),

	  values: [{
	    name: 'width',
	    type: 'number',
	    description: 'Width of image preview in pixels. If set, it will override the CSS property.',
	    'default': null
	  }, {
	    name: 'height',
	    type: 'number',
	    description: 'Height of image preview in pixels. If set, it will override the CSS property.',
	    'default': null
	  }, {
	    name: 'imageBackground',
	    type: 'boolean',
	    description: 'Whether or not to display the background image beyond the preview area.',
	    'default': false
	  }, {
	    name: 'imageBackgroundBorderWidth',
	    type: 'array or number',
	    description: 'Width of background image border in pixels.\n        The four array elements specify the width of background image width on the top, right, bottom, left side respectively.\n        The background image beyond the width will be hidden.\n        If specified as a number, border with uniform width on all sides will be applied.',
	    'default': [0, 0, 0, 0]
	  }, {
	    name: 'exportZoom',
	    type: 'number',
	    description: 'The ratio between the desired image size to export and the preview size.\n        For example, if the preview size is `300px * 200px`, and `exportZoom = 2`, then\n        the exported image size will be `600px * 400px`.\n        This also affects the maximum zoom level, since the exported image cannot be zoomed to larger than its original size.',
	    'default': 1
	  }, {
	    name: 'allowDragNDrop',
	    type: 'boolean',
	    description: 'When set to true, you can load an image by dragging it from local file browser onto the preview area.',
	    'default': true
	  }, {
	    name: 'minZoom',
	    type: 'string',
	    description: 'This options decides the minimal zoom level of the image.\n        If set to `\'fill\'`, the image has to fill the preview area, i.e. both width and height must not go smaller than the preview area.\n        If set to `\'fit\'`, the image can shrink further to fit the preview area, i.e. at least one of its edges must not go smaller than the preview area.',
	    'default': 'fill'
	  }, {
	    name: 'maxZoom',
	    type: 'number',
	    description: 'Determines how big the image can be zoomed. E.g. if set to 1.5, the image can be zoomed to 150% of its original size.',
	    'default': 1
	  }, {
	    name: 'initialZoom',
	    type: 'string',
	    description: 'Determines the zoom when an image is loaded.\n        When set to `\'min\'`, image is zoomed to the smallest when loaded.\n        When set to `\'image\'`, image is zoomed to 100% when loaded.',
	    'default': 'min'
	  }, {
	    name: 'freeMove',
	    type: 'boolean',
	    description: 'When set to true, you can freely move the image instead of being bound to the container borders',
	    'default': false
	  }, {
	    name: 'smallImage',
	    type: 'string',
	    description: 'When set to `\'reject\'`, `onImageError` would be called when cropit loads an image that is smaller than the container.\n        When set to `\'allow\'`, images smaller than the container can be zoomed down to its original size, overiding `minZoom` option.\n        When set to `\'stretch\'`, the minimum zoom of small images would follow `minZoom` option.',
	    'default': 'reject'
	  }],

	  callbacks: [{
	    name: 'onFileChange',
	    description: 'Called when user selects a file in the select file input.',
	    params: [{
	      name: 'event',
	      type: 'object',
	      description: 'File change event object'
	    }]
	  }, {
	    name: 'onFileReaderError',
	    description: 'Called when `FileReader` encounters an error while loading the image file.'
	  }, {
	    name: 'onImageLoading',
	    description: 'Called when image starts to be loaded.'
	  }, {
	    name: 'onImageLoaded',
	    description: 'Called when image is loaded.'
	  }, {
	    name: 'onImageError',
	    description: 'Called when image cannot be loaded.',
	    params: [{
	      name: 'error',
	      type: 'object',
	      description: 'Error object.'
	    }, {
	      name: 'error.code',
	      type: 'number',
	      description: 'Error code. `0` means generic image loading failure. `1` means image is too small.'
	    }, {
	      name: 'error.message',
	      type: 'string',
	      description: 'A message explaining the error.'
	    }]
	  }, {
	    name: 'onZoomEnabled',
	    description: 'Called when image the zoom slider is enabled.'
	  }, {
	    name: 'onZoomDisabled',
	    description: 'Called when image the zoom slider is disabled.'
	  }, {
	    name: 'onZoomChange',
	    description: 'Called when zoom changes.',
	    params: [{
	      name: 'zoom',
	      type: 'number',
	      description: 'New zoom.'
	    }]
	  }, {
	    name: 'onOffsetChange',
	    description: 'Called when image offset changes.',
	    params: [{
	      name: 'offset',
	      type: 'object',
	      description: 'New offset, with `x` and `y` values.'
	    }]
	  }].map(function (o) {
	    o.type = 'function';return o;
	  })
	};

	var loadDefaults = function loadDefaults($el) {
	  var defaults = {};
	  if ($el) {
	    options.elements.forEach(function (o) {
	      defaults[o.name] = $el.find(o.defaultSelector);
	    });
	  }
	  options.values.forEach(function (o) {
	    defaults[o.name] = o['default'];
	  });
	  options.callbacks.forEach(function (o) {
	    defaults[o.name] = function () {};
	  });

	  return defaults;
	};

	exports.loadDefaults = loadDefaults;
	exports['default'] = options;

/***/ },
/* 6 */
/***/ function(module, exports) {

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	var exists = function exists(v) {
	  return typeof v !== 'undefined';
	};

	exports.exists = exists;
	var round = function round(x) {
	  return +(Math.round(x * 100) + 'e-2');
	};
	exports.round = round;

/***/ }
/******/ ])
});
;

/***/ }),

/***/ "./resources/assets/js/utilities.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {/* 
 * Utilities for ThaiLovely.co
 */

// Font Observer
var FontFaceObserver = __webpack_require__("./node_modules/fontfaceobserver/fontfaceobserver.standalone.js");

// Import Scripts
__webpack_require__("./node_modules/cropit/dist/jquery.cropit.js");
__webpack_require__("./node_modules/bootstrap-growl/jquery.bootstrap-growl.js");
__webpack_require__("./node_modules/blueimp-gallery/js/jquery.blueimp-gallery.js");

// Set cookie
function setCookie(cname, cvalue, expire) {
    var d = new Date();
    d.setTime(d.getTime() + expire);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
window.setCookie = setCookie;

// Get cookie
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
window.getCookie = getCookie;

// Font Observer
var fontA = new FontFaceObserver('Fira Sans');
var fontB = new FontFaceObserver('Roboto Slab');

Promise.all([fontA.load(), fontB.load()]).then(function () {
    document.documentElement.classList.add('fonts-loaded');
    setCookie('fonts_loaded');
});

// Animate.css Extension
$.fn.extend({
    animateCss: function animateCss(animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function () {
            $(this).removeClass('animated ' + animationName);
        });
        return this;
    }
});

// Process Submit
$(function () {
    $('.process-submit').on('click', function (e) {
        e.preventDefault();
        var btn = $(this),
            f = btn.closest('form'),
            n = f.find('input'),
            w = btn.outerWidth(),
            h = btn.outerHeight(),
            v = true,
            i = $('<i>', { class: 'fas fa-circle-notch fa-spin' });

        n.each(function (i, e) {
            if (e.checkValidity() == false) {
                v = false;
                console.log('Invalid');
            }
        });

        if (v !== false) {
            btn.html(i).css({ 'width': w + 'px', 'height': h + 'px' }).attr('disabled', true);
            f.submit();
        }
    });
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/src/jquery.js")))

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/utilities.js");


/***/ })

},[1]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYmx1ZWltcC1nYWxsZXJ5L2pzL2JsdWVpbXAtZ2FsbGVyeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYmx1ZWltcC1nYWxsZXJ5L2pzL2JsdWVpbXAtaGVscGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9ibHVlaW1wLWdhbGxlcnkvanMvanF1ZXJ5LmJsdWVpbXAtZ2FsbGVyeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYm9vdHN0cmFwLWdyb3dsL2pxdWVyeS5ib290c3RyYXAtZ3Jvd2wuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2Nyb3BpdC9kaXN0L2pxdWVyeS5jcm9waXQuanMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy91dGlsaXRpZXMuanMiXSwibmFtZXMiOlsiRm9udEZhY2VPYnNlcnZlciIsInJlcXVpcmUiLCJzZXRDb29raWUiLCJjbmFtZSIsImN2YWx1ZSIsImV4cGlyZSIsImQiLCJEYXRlIiwic2V0VGltZSIsImdldFRpbWUiLCJleHBpcmVzIiwidG9VVENTdHJpbmciLCJkb2N1bWVudCIsImNvb2tpZSIsIndpbmRvdyIsImdldENvb2tpZSIsIm5hbWUiLCJkZWNvZGVkQ29va2llIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiY2EiLCJzcGxpdCIsImkiLCJsZW5ndGgiLCJjIiwiY2hhckF0Iiwic3Vic3RyaW5nIiwiaW5kZXhPZiIsImZvbnRBIiwiZm9udEIiLCJQcm9taXNlIiwiYWxsIiwibG9hZCIsInRoZW4iLCJkb2N1bWVudEVsZW1lbnQiLCJjbGFzc0xpc3QiLCJhZGQiLCIkIiwiZm4iLCJleHRlbmQiLCJhbmltYXRlQ3NzIiwiYW5pbWF0aW9uTmFtZSIsImFuaW1hdGlvbkVuZCIsImFkZENsYXNzIiwib25lIiwicmVtb3ZlQ2xhc3MiLCJvbiIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImJ0biIsImYiLCJjbG9zZXN0IiwibiIsImZpbmQiLCJ3Iiwib3V0ZXJXaWR0aCIsImgiLCJvdXRlckhlaWdodCIsInYiLCJjbGFzcyIsImVhY2giLCJjaGVja1ZhbGlkaXR5IiwiY29uc29sZSIsImxvZyIsImh0bWwiLCJjc3MiLCJhdHRyIiwic3VibWl0Il0sIm1hcHBpbmdzIjoiOzs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBLFdBQVcsb0JBQW9CLEVBQUU7O0FBRWpDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0MsY0FBYztBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixXQUFXO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixjQUFjO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0EsMkNBQTJDLFNBQVM7QUFDcEQ7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkNBQTZDLFNBQVM7QUFDdEQ7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsY0FBYztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSxnQ0FBZ0M7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLENBQUM7Ozs7Ozs7O0FDeDRDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDQUFDO0FBQ0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUFBO0FBQ0wsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7O0FDN0xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQ0FBQzs7Ozs7Ozs7QUMxRUQ7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0hBQXNIO0FBQ3RIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUM7Ozs7Ozs7OztBQy9FRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxvQ0FBb0M7QUFDcEM7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLHVDQUF1Qyx1Q0FBdUMsa0JBQWtCOztBQUVoRzs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxNQUFNO0FBQ04sSUFBSTs7QUFFSjtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ04sSUFBSTs7QUFFSjtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ04sSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBLElBQUk7QUFDSjtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0E7O0FBRUE7O0FBRUEsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7O0FBRUYsa0NBQWtDLDJDQUEyQyxnQkFBZ0Isa0JBQWtCLE9BQU8sMkJBQTJCLHdEQUF3RCxnQ0FBZ0MsdURBQXVELDJEQUEyRCxFQUFFLEVBQUUseURBQXlELHFFQUFxRSw2REFBNkQsb0JBQW9CLEdBQUcsRUFBRTs7QUFFbmpCLHVDQUF1Qyx1Q0FBdUMsa0JBQWtCOztBQUVoRyxrREFBa0QsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV4Sjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxpREFBaUQ7O0FBRWpEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx1REFBdUQsb0JBQW9CO0FBQzNFLHlEQUF5RCw2QkFBNkI7O0FBRXRGO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0EsWUFBWTtBQUNaOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxRQUFROztBQUVSO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsa0JBQWtCOztBQUVsQjtBQUNBO0FBQ0E7QUFDQSxVQUFVO0FBQ1Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjs7QUFFQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVTtBQUNWO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRO0FBQ1I7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvREFBb0Q7O0FBRXBEOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7QUFDUjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLHdCQUF3QixvQkFBb0I7QUFDNUMsUUFBUTtBQUNSO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQixRQUFRO0FBQ1I7QUFDQSxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7O0FBRVI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJOztBQUVKO0FBQ0EsRUFBRTs7QUFFRjtBQUNBOztBQUVBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFOztBQUVGLGtDQUFrQywyQ0FBMkMsZ0JBQWdCLGtCQUFrQixPQUFPLDJCQUEyQix3REFBd0QsZ0NBQWdDLHVEQUF1RCwyREFBMkQsRUFBRSxFQUFFLHlEQUF5RCxxRUFBcUUsNkRBQTZELG9CQUFvQixHQUFHLEVBQUU7O0FBRW5qQixrREFBa0QsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV4SjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBLEVBQUU7O0FBRUY7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSwwQkFBMEIsNENBQTRDO0FBQ3RFLGlCQUFpQjtBQUNqQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTixJQUFJO0FBQ0o7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ04sSUFBSTtBQUNKO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ04sSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTixJQUFJO0FBQ0oseUJBQXlCO0FBQ3pCLElBQUk7QUFDSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsSUFBSTs7QUFFSjtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNELEM7Ozs7Ozs7QUN6ckNBOzs7O0FBSUE7QUFDQSxJQUFJQSxtQkFBbUIsbUJBQUFDLENBQVEsZ0VBQVIsQ0FBdkI7O0FBRUE7QUFDQSxtQkFBQUEsQ0FBUSw2Q0FBUjtBQUNBLG1CQUFBQSxDQUFRLDBEQUFSO0FBQ0EsbUJBQUFBLENBQVEsNkRBQVI7O0FBRUE7QUFDQSxTQUFTQyxTQUFULENBQW1CQyxLQUFuQixFQUEwQkMsTUFBMUIsRUFBa0NDLE1BQWxDLEVBQTBDO0FBQ3RDLFFBQUlDLElBQUksSUFBSUMsSUFBSixFQUFSO0FBQ0FELE1BQUVFLE9BQUYsQ0FBVUYsRUFBRUcsT0FBRixLQUFlSixNQUF6QjtBQUNBLFFBQUlLLFVBQVUsYUFBWUosRUFBRUssV0FBRixFQUExQjtBQUNBQyxhQUFTQyxNQUFULEdBQWtCVixRQUFRLEdBQVIsR0FBY0MsTUFBZCxHQUF1QixHQUF2QixHQUE2Qk0sT0FBN0IsR0FBdUMsU0FBekQ7QUFDSDtBQUNESSxPQUFPWixTQUFQLEdBQW1CQSxTQUFuQjs7QUFFQTtBQUNBLFNBQVNhLFNBQVQsQ0FBbUJaLEtBQW5CLEVBQTBCO0FBQ3RCLFFBQUlhLE9BQU9iLFFBQVEsR0FBbkI7QUFDQSxRQUFJYyxnQkFBZ0JDLG1CQUFtQk4sU0FBU0MsTUFBNUIsQ0FBcEI7QUFDQSxRQUFJTSxLQUFLRixjQUFjRyxLQUFkLENBQW9CLEdBQXBCLENBQVQ7QUFDQSxTQUFJLElBQUlDLElBQUksQ0FBWixFQUFlQSxJQUFHRixHQUFHRyxNQUFyQixFQUE2QkQsR0FBN0IsRUFBa0M7QUFDOUIsWUFBSUUsSUFBSUosR0FBR0UsQ0FBSCxDQUFSO0FBQ0EsZUFBT0UsRUFBRUMsTUFBRixDQUFTLENBQVQsS0FBZSxHQUF0QixFQUEyQjtBQUN2QkQsZ0JBQUlBLEVBQUVFLFNBQUYsQ0FBWSxDQUFaLENBQUo7QUFDSDtBQUNELFlBQUlGLEVBQUVHLE9BQUYsQ0FBVVYsSUFBVixLQUFtQixDQUF2QixFQUEwQjtBQUN0QixtQkFBT08sRUFBRUUsU0FBRixDQUFZVCxLQUFLTSxNQUFqQixFQUF5QkMsRUFBRUQsTUFBM0IsQ0FBUDtBQUNIO0FBQ0o7QUFDRCxXQUFPLEVBQVA7QUFDSDtBQUNEUixPQUFPQyxTQUFQLEdBQW1CQSxTQUFuQjs7QUFFQTtBQUNBLElBQUlZLFFBQVEsSUFBSTNCLGdCQUFKLENBQXFCLFdBQXJCLENBQVo7QUFDQSxJQUFJNEIsUUFBUSxJQUFJNUIsZ0JBQUosQ0FBcUIsYUFBckIsQ0FBWjs7QUFFQTZCLFFBQVFDLEdBQVIsQ0FBWSxDQUFDSCxNQUFNSSxJQUFOLEVBQUQsRUFBZUgsTUFBTUcsSUFBTixFQUFmLENBQVosRUFBMENDLElBQTFDLENBQStDLFlBQVk7QUFDekRwQixhQUFTcUIsZUFBVCxDQUF5QkMsU0FBekIsQ0FBbUNDLEdBQW5DLENBQXVDLGNBQXZDO0FBQ0FqQyxjQUFVLGNBQVY7QUFDRCxDQUhEOztBQUtBO0FBQ0FrQyxFQUFFQyxFQUFGLENBQUtDLE1BQUwsQ0FBWTtBQUNSQyxnQkFBWSxvQkFBVUMsYUFBVixFQUF5QjtBQUNqQyxZQUFJQyxlQUFlLDhFQUFuQjtBQUNBLGFBQUtDLFFBQUwsQ0FBYyxjQUFjRixhQUE1QixFQUEyQ0csR0FBM0MsQ0FBK0NGLFlBQS9DLEVBQTZELFlBQVc7QUFDcEVMLGNBQUUsSUFBRixFQUFRUSxXQUFSLENBQW9CLGNBQWNKLGFBQWxDO0FBQ0gsU0FGRDtBQUdBLGVBQU8sSUFBUDtBQUNIO0FBUE8sQ0FBWjs7QUFVQTtBQUNBSixFQUFFLFlBQVU7QUFDUkEsTUFBRSxpQkFBRixFQUFxQlMsRUFBckIsQ0FBd0IsT0FBeEIsRUFBaUMsVUFBU0MsQ0FBVCxFQUFXO0FBQ3hDQSxVQUFFQyxjQUFGO0FBQ0EsWUFBSUMsTUFBTVosRUFBRSxJQUFGLENBQVY7QUFBQSxZQUNJYSxJQUFNRCxJQUFJRSxPQUFKLENBQVksTUFBWixDQURWO0FBQUEsWUFFSUMsSUFBTUYsRUFBRUcsSUFBRixDQUFPLE9BQVAsQ0FGVjtBQUFBLFlBR0lDLElBQU1MLElBQUlNLFVBQUosRUFIVjtBQUFBLFlBSUlDLElBQU1QLElBQUlRLFdBQUosRUFKVjtBQUFBLFlBS0lDLElBQU0sSUFMVjtBQUFBLFlBTUlwQyxJQUFNZSxFQUFFLEtBQUYsRUFBUyxFQUFFc0IsT0FBTyw2QkFBVCxFQUFULENBTlY7O0FBUUFQLFVBQUVRLElBQUYsQ0FBTyxVQUFTdEMsQ0FBVCxFQUFZeUIsQ0FBWixFQUFjO0FBQ2pCLGdCQUFHQSxFQUFFYyxhQUFGLE1BQXFCLEtBQXhCLEVBQStCO0FBQzNCSCxvQkFBSSxLQUFKO0FBQ0FJLHdCQUFRQyxHQUFSLENBQVksU0FBWjtBQUNIO0FBQ0osU0FMRDs7QUFPQSxZQUFHTCxNQUFNLEtBQVQsRUFBZTtBQUNYVCxnQkFBSWUsSUFBSixDQUFTMUMsQ0FBVCxFQUFZMkMsR0FBWixDQUFnQixFQUFDLFNBQVNYLElBQUksSUFBZCxFQUFvQixVQUFVRSxJQUFJLElBQWxDLEVBQWhCLEVBQXlEVSxJQUF6RCxDQUE4RCxVQUE5RCxFQUF5RSxJQUF6RTtBQUNBaEIsY0FBRWlCLE1BQUY7QUFDSDtBQUNKLEtBckJEO0FBc0JILENBdkJELEUiLCJmaWxlIjoiXFxqc1xcdXRpbGl0aWVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIGJsdWVpbXAgR2FsbGVyeSBKU1xuICogaHR0cHM6Ly9naXRodWIuY29tL2JsdWVpbXAvR2FsbGVyeVxuICpcbiAqIENvcHlyaWdodCAyMDEzLCBTZWJhc3RpYW4gVHNjaGFuXG4gKiBodHRwczovL2JsdWVpbXAubmV0XG4gKlxuICogU3dpcGUgaW1wbGVtZW50YXRpb24gYmFzZWQgb25cbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9icmFkYmlyZHNhbGwvU3dpcGVcbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2U6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICovXG5cbi8qIGdsb2JhbCBkZWZpbmUsIHdpbmRvdywgZG9jdW1lbnQsIERvY3VtZW50VG91Y2ggKi9cblxuOyhmdW5jdGlvbiAoZmFjdG9yeSkge1xuICAndXNlIHN0cmljdCdcbiAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xuICAgIC8vIFJlZ2lzdGVyIGFzIGFuIGFub255bW91cyBBTUQgbW9kdWxlOlxuICAgIGRlZmluZShbJy4vYmx1ZWltcC1oZWxwZXInXSwgZmFjdG9yeSlcbiAgfSBlbHNlIHtcbiAgICAvLyBCcm93c2VyIGdsb2JhbHM6XG4gICAgd2luZG93LmJsdWVpbXAgPSB3aW5kb3cuYmx1ZWltcCB8fCB7fVxuICAgIHdpbmRvdy5ibHVlaW1wLkdhbGxlcnkgPSBmYWN0b3J5KHdpbmRvdy5ibHVlaW1wLmhlbHBlciB8fCB3aW5kb3cualF1ZXJ5KVxuICB9XG59KShmdW5jdGlvbiAoJCkge1xuICAndXNlIHN0cmljdCdcblxuICBmdW5jdGlvbiBHYWxsZXJ5IChsaXN0LCBvcHRpb25zKSB7XG4gICAgaWYgKGRvY3VtZW50LmJvZHkuc3R5bGUubWF4SGVpZ2h0ID09PSB1bmRlZmluZWQpIHtcbiAgICAgIC8vIGRvY3VtZW50LmJvZHkuc3R5bGUubWF4SGVpZ2h0IGlzIHVuZGVmaW5lZCBvbiBJRTYgYW5kIGxvd2VyXG4gICAgICByZXR1cm4gbnVsbFxuICAgIH1cbiAgICBpZiAoIXRoaXMgfHwgdGhpcy5vcHRpb25zICE9PSBHYWxsZXJ5LnByb3RvdHlwZS5vcHRpb25zKSB7XG4gICAgICAvLyBDYWxsZWQgYXMgZnVuY3Rpb24gaW5zdGVhZCBvZiBhcyBjb25zdHJ1Y3RvcixcbiAgICAgIC8vIHNvIHdlIHNpbXBseSByZXR1cm4gYSBuZXcgaW5zdGFuY2U6XG4gICAgICByZXR1cm4gbmV3IEdhbGxlcnkobGlzdCwgb3B0aW9ucylcbiAgICB9XG4gICAgaWYgKCFsaXN0IHx8ICFsaXN0Lmxlbmd0aCkge1xuICAgICAgdGhpcy5jb25zb2xlLmxvZyhcbiAgICAgICAgJ2JsdWVpbXAgR2FsbGVyeTogTm8gb3IgZW1wdHkgbGlzdCBwcm92aWRlZCBhcyBmaXJzdCBhcmd1bWVudC4nLFxuICAgICAgICBsaXN0XG4gICAgICApXG4gICAgICByZXR1cm5cbiAgICB9XG4gICAgdGhpcy5saXN0ID0gbGlzdFxuICAgIHRoaXMubnVtID0gbGlzdC5sZW5ndGhcbiAgICB0aGlzLmluaXRPcHRpb25zKG9wdGlvbnMpXG4gICAgdGhpcy5pbml0aWFsaXplKClcbiAgfVxuXG4gICQuZXh0ZW5kKEdhbGxlcnkucHJvdG90eXBlLCB7XG4gICAgb3B0aW9uczoge1xuICAgICAgLy8gVGhlIElkLCBlbGVtZW50IG9yIHF1ZXJ5U2VsZWN0b3Igb2YgdGhlIGdhbGxlcnkgd2lkZ2V0OlxuICAgICAgY29udGFpbmVyOiAnI2JsdWVpbXAtZ2FsbGVyeScsXG4gICAgICAvLyBUaGUgdGFnIG5hbWUsIElkLCBlbGVtZW50IG9yIHF1ZXJ5U2VsZWN0b3Igb2YgdGhlIHNsaWRlcyBjb250YWluZXI6XG4gICAgICBzbGlkZXNDb250YWluZXI6ICdkaXYnLFxuICAgICAgLy8gVGhlIHRhZyBuYW1lLCBJZCwgZWxlbWVudCBvciBxdWVyeVNlbGVjdG9yIG9mIHRoZSB0aXRsZSBlbGVtZW50OlxuICAgICAgdGl0bGVFbGVtZW50OiAnaDMnLFxuICAgICAgLy8gVGhlIGNsYXNzIHRvIGFkZCB3aGVuIHRoZSBnYWxsZXJ5IGlzIHZpc2libGU6XG4gICAgICBkaXNwbGF5Q2xhc3M6ICdibHVlaW1wLWdhbGxlcnktZGlzcGxheScsXG4gICAgICAvLyBUaGUgY2xhc3MgdG8gYWRkIHdoZW4gdGhlIGdhbGxlcnkgY29udHJvbHMgYXJlIHZpc2libGU6XG4gICAgICBjb250cm9sc0NsYXNzOiAnYmx1ZWltcC1nYWxsZXJ5LWNvbnRyb2xzJyxcbiAgICAgIC8vIFRoZSBjbGFzcyB0byBhZGQgd2hlbiB0aGUgZ2FsbGVyeSBvbmx5IGRpc3BsYXlzIG9uZSBlbGVtZW50OlxuICAgICAgc2luZ2xlQ2xhc3M6ICdibHVlaW1wLWdhbGxlcnktc2luZ2xlJyxcbiAgICAgIC8vIFRoZSBjbGFzcyB0byBhZGQgd2hlbiB0aGUgbGVmdCBlZGdlIGhhcyBiZWVuIHJlYWNoZWQ6XG4gICAgICBsZWZ0RWRnZUNsYXNzOiAnYmx1ZWltcC1nYWxsZXJ5LWxlZnQnLFxuICAgICAgLy8gVGhlIGNsYXNzIHRvIGFkZCB3aGVuIHRoZSByaWdodCBlZGdlIGhhcyBiZWVuIHJlYWNoZWQ6XG4gICAgICByaWdodEVkZ2VDbGFzczogJ2JsdWVpbXAtZ2FsbGVyeS1yaWdodCcsXG4gICAgICAvLyBUaGUgY2xhc3MgdG8gYWRkIHdoZW4gdGhlIGF1dG9tYXRpYyBzbGlkZXNob3cgaXMgYWN0aXZlOlxuICAgICAgcGxheWluZ0NsYXNzOiAnYmx1ZWltcC1nYWxsZXJ5LXBsYXlpbmcnLFxuICAgICAgLy8gVGhlIGNsYXNzIGZvciBhbGwgc2xpZGVzOlxuICAgICAgc2xpZGVDbGFzczogJ3NsaWRlJyxcbiAgICAgIC8vIFRoZSBzbGlkZSBjbGFzcyBmb3IgbG9hZGluZyBlbGVtZW50czpcbiAgICAgIHNsaWRlTG9hZGluZ0NsYXNzOiAnc2xpZGUtbG9hZGluZycsXG4gICAgICAvLyBUaGUgc2xpZGUgY2xhc3MgZm9yIGVsZW1lbnRzIHRoYXQgZmFpbGVkIHRvIGxvYWQ6XG4gICAgICBzbGlkZUVycm9yQ2xhc3M6ICdzbGlkZS1lcnJvcicsXG4gICAgICAvLyBUaGUgY2xhc3MgZm9yIHRoZSBjb250ZW50IGVsZW1lbnQgbG9hZGVkIGludG8gZWFjaCBzbGlkZTpcbiAgICAgIHNsaWRlQ29udGVudENsYXNzOiAnc2xpZGUtY29udGVudCcsXG4gICAgICAvLyBUaGUgY2xhc3MgZm9yIHRoZSBcInRvZ2dsZVwiIGNvbnRyb2w6XG4gICAgICB0b2dnbGVDbGFzczogJ3RvZ2dsZScsXG4gICAgICAvLyBUaGUgY2xhc3MgZm9yIHRoZSBcInByZXZcIiBjb250cm9sOlxuICAgICAgcHJldkNsYXNzOiAncHJldicsXG4gICAgICAvLyBUaGUgY2xhc3MgZm9yIHRoZSBcIm5leHRcIiBjb250cm9sOlxuICAgICAgbmV4dENsYXNzOiAnbmV4dCcsXG4gICAgICAvLyBUaGUgY2xhc3MgZm9yIHRoZSBcImNsb3NlXCIgY29udHJvbDpcbiAgICAgIGNsb3NlQ2xhc3M6ICdjbG9zZScsXG4gICAgICAvLyBUaGUgY2xhc3MgZm9yIHRoZSBcInBsYXktcGF1c2VcIiB0b2dnbGUgY29udHJvbDpcbiAgICAgIHBsYXlQYXVzZUNsYXNzOiAncGxheS1wYXVzZScsXG4gICAgICAvLyBUaGUgbGlzdCBvYmplY3QgcHJvcGVydHkgKG9yIGRhdGEgYXR0cmlidXRlKSB3aXRoIHRoZSBvYmplY3QgdHlwZTpcbiAgICAgIHR5cGVQcm9wZXJ0eTogJ3R5cGUnLFxuICAgICAgLy8gVGhlIGxpc3Qgb2JqZWN0IHByb3BlcnR5IChvciBkYXRhIGF0dHJpYnV0ZSkgd2l0aCB0aGUgb2JqZWN0IHRpdGxlOlxuICAgICAgdGl0bGVQcm9wZXJ0eTogJ3RpdGxlJyxcbiAgICAgIC8vIFRoZSBsaXN0IG9iamVjdCBwcm9wZXJ0eSAob3IgZGF0YSBhdHRyaWJ1dGUpIHdpdGggdGhlIG9iamVjdCBhbHQgdGV4dDpcbiAgICAgIGFsdFRleHRQcm9wZXJ0eTogJ2FsdCcsXG4gICAgICAvLyBUaGUgbGlzdCBvYmplY3QgcHJvcGVydHkgKG9yIGRhdGEgYXR0cmlidXRlKSB3aXRoIHRoZSBvYmplY3QgVVJMOlxuICAgICAgdXJsUHJvcGVydHk6ICdocmVmJyxcbiAgICAgIC8vIFRoZSBsaXN0IG9iamVjdCBwcm9wZXJ0eSAob3IgZGF0YSBhdHRyaWJ1dGUpIHdpdGggdGhlIG9iamVjdCBzcmNzZXQgVVJMKHMpOlxuICAgICAgc3Jjc2V0UHJvcGVydHk6ICd1cmxzZXQnLFxuICAgICAgLy8gVGhlIGdhbGxlcnkgbGlzdGVucyBmb3IgdHJhbnNpdGlvbmVuZCBldmVudHMgYmVmb3JlIHRyaWdnZXJpbmcgdGhlXG4gICAgICAvLyBvcGVuZWQgYW5kIGNsb3NlZCBldmVudHMsIHVubGVzcyB0aGUgZm9sbG93aW5nIG9wdGlvbiBpcyBzZXQgdG8gZmFsc2U6XG4gICAgICBkaXNwbGF5VHJhbnNpdGlvbjogdHJ1ZSxcbiAgICAgIC8vIERlZmluZXMgaWYgdGhlIGdhbGxlcnkgc2xpZGVzIGFyZSBjbGVhcmVkIGZyb20gdGhlIGdhbGxlcnkgbW9kYWwsXG4gICAgICAvLyBvciByZXVzZWQgZm9yIHRoZSBuZXh0IGdhbGxlcnkgaW5pdGlhbGl6YXRpb246XG4gICAgICBjbGVhclNsaWRlczogdHJ1ZSxcbiAgICAgIC8vIERlZmluZXMgaWYgaW1hZ2VzIHNob3VsZCBiZSBzdHJldGNoZWQgdG8gZmlsbCB0aGUgYXZhaWxhYmxlIHNwYWNlLFxuICAgICAgLy8gd2hpbGUgbWFpbnRhaW5pbmcgdGhlaXIgYXNwZWN0IHJhdGlvICh3aWxsIG9ubHkgYmUgZW5hYmxlZCBmb3IgYnJvd3NlcnNcbiAgICAgIC8vIHN1cHBvcnRpbmcgYmFja2dyb3VuZC1zaXplPVwiY29udGFpblwiLCB3aGljaCBleGNsdWRlcyBJRSA8IDkpLlxuICAgICAgLy8gU2V0IHRvIFwiY292ZXJcIiwgdG8gbWFrZSBpbWFnZXMgY292ZXIgYWxsIGF2YWlsYWJsZSBzcGFjZSAocmVxdWlyZXNcbiAgICAgIC8vIHN1cHBvcnQgZm9yIGJhY2tncm91bmQtc2l6ZT1cImNvdmVyXCIsIHdoaWNoIGV4Y2x1ZGVzIElFIDwgOSk6XG4gICAgICBzdHJldGNoSW1hZ2VzOiBmYWxzZSxcbiAgICAgIC8vIFRvZ2dsZSB0aGUgY29udHJvbHMgb24gcHJlc3NpbmcgdGhlIFJldHVybiBrZXk6XG4gICAgICB0b2dnbGVDb250cm9sc09uUmV0dXJuOiB0cnVlLFxuICAgICAgLy8gVG9nZ2xlIHRoZSBjb250cm9scyBvbiBzbGlkZSBjbGljazpcbiAgICAgIHRvZ2dsZUNvbnRyb2xzT25TbGlkZUNsaWNrOiB0cnVlLFxuICAgICAgLy8gVG9nZ2xlIHRoZSBhdXRvbWF0aWMgc2xpZGVzaG93IGludGVydmFsIG9uIHByZXNzaW5nIHRoZSBTcGFjZSBrZXk6XG4gICAgICB0b2dnbGVTbGlkZXNob3dPblNwYWNlOiB0cnVlLFxuICAgICAgLy8gTmF2aWdhdGUgdGhlIGdhbGxlcnkgYnkgcHJlc3NpbmcgbGVmdCBhbmQgcmlnaHQgb24gdGhlIGtleWJvYXJkOlxuICAgICAgZW5hYmxlS2V5Ym9hcmROYXZpZ2F0aW9uOiB0cnVlLFxuICAgICAgLy8gQ2xvc2UgdGhlIGdhbGxlcnkgb24gcHJlc3NpbmcgdGhlIEVzYyBrZXk6XG4gICAgICBjbG9zZU9uRXNjYXBlOiB0cnVlLFxuICAgICAgLy8gQ2xvc2UgdGhlIGdhbGxlcnkgd2hlbiBjbGlja2luZyBvbiBhbiBlbXB0eSBzbGlkZSBhcmVhOlxuICAgICAgY2xvc2VPblNsaWRlQ2xpY2s6IHRydWUsXG4gICAgICAvLyBDbG9zZSB0aGUgZ2FsbGVyeSBieSBzd2lwaW5nIHVwIG9yIGRvd246XG4gICAgICBjbG9zZU9uU3dpcGVVcE9yRG93bjogdHJ1ZSxcbiAgICAgIC8vIEVtdWxhdGUgdG91Y2ggZXZlbnRzIG9uIG1vdXNlLXBvaW50ZXIgZGV2aWNlcyBzdWNoIGFzIGRlc2t0b3AgYnJvd3NlcnM6XG4gICAgICBlbXVsYXRlVG91Y2hFdmVudHM6IHRydWUsXG4gICAgICAvLyBTdG9wIHRvdWNoIGV2ZW50cyBmcm9tIGJ1YmJsaW5nIHVwIHRvIGFuY2VzdG9yIGVsZW1lbnRzIG9mIHRoZSBHYWxsZXJ5OlxuICAgICAgc3RvcFRvdWNoRXZlbnRzUHJvcGFnYXRpb246IGZhbHNlLFxuICAgICAgLy8gSGlkZSB0aGUgcGFnZSBzY3JvbGxiYXJzOlxuICAgICAgaGlkZVBhZ2VTY3JvbGxiYXJzOiB0cnVlLFxuICAgICAgLy8gU3RvcHMgYW55IHRvdWNoZXMgb24gdGhlIGNvbnRhaW5lciBmcm9tIHNjcm9sbGluZyB0aGUgcGFnZTpcbiAgICAgIGRpc2FibGVTY3JvbGw6IHRydWUsXG4gICAgICAvLyBDYXJvdXNlbCBtb2RlIChzaG9ydGN1dCBmb3IgY2Fyb3VzZWwgc3BlY2lmaWMgb3B0aW9ucyk6XG4gICAgICBjYXJvdXNlbDogZmFsc2UsXG4gICAgICAvLyBBbGxvdyBjb250aW51b3VzIG5hdmlnYXRpb24sIG1vdmluZyBmcm9tIGxhc3QgdG8gZmlyc3RcbiAgICAgIC8vIGFuZCBmcm9tIGZpcnN0IHRvIGxhc3Qgc2xpZGU6XG4gICAgICBjb250aW51b3VzOiB0cnVlLFxuICAgICAgLy8gUmVtb3ZlIGVsZW1lbnRzIG91dHNpZGUgb2YgdGhlIHByZWxvYWQgcmFuZ2UgZnJvbSB0aGUgRE9NOlxuICAgICAgdW5sb2FkRWxlbWVudHM6IHRydWUsXG4gICAgICAvLyBTdGFydCB3aXRoIHRoZSBhdXRvbWF0aWMgc2xpZGVzaG93OlxuICAgICAgc3RhcnRTbGlkZXNob3c6IGZhbHNlLFxuICAgICAgLy8gRGVsYXkgaW4gbWlsbGlzZWNvbmRzIGJldHdlZW4gc2xpZGVzIGZvciB0aGUgYXV0b21hdGljIHNsaWRlc2hvdzpcbiAgICAgIHNsaWRlc2hvd0ludGVydmFsOiA1MDAwLFxuICAgICAgLy8gVGhlIHN0YXJ0aW5nIGluZGV4IGFzIGludGVnZXIuXG4gICAgICAvLyBDYW4gYWxzbyBiZSBhbiBvYmplY3Qgb2YgdGhlIGdpdmVuIGxpc3QsXG4gICAgICAvLyBvciBhbiBlcXVhbCBvYmplY3Qgd2l0aCB0aGUgc2FtZSB1cmwgcHJvcGVydHk6XG4gICAgICBpbmRleDogMCxcbiAgICAgIC8vIFRoZSBudW1iZXIgb2YgZWxlbWVudHMgdG8gbG9hZCBhcm91bmQgdGhlIGN1cnJlbnQgaW5kZXg6XG4gICAgICBwcmVsb2FkUmFuZ2U6IDIsXG4gICAgICAvLyBUaGUgdHJhbnNpdGlvbiBzcGVlZCBiZXR3ZWVuIHNsaWRlIGNoYW5nZXMgaW4gbWlsbGlzZWNvbmRzOlxuICAgICAgdHJhbnNpdGlvblNwZWVkOiA0MDAsXG4gICAgICAvLyBUaGUgdHJhbnNpdGlvbiBzcGVlZCBmb3IgYXV0b21hdGljIHNsaWRlIGNoYW5nZXMsIHNldCB0byBhbiBpbnRlZ2VyXG4gICAgICAvLyBncmVhdGVyIDAgdG8gb3ZlcnJpZGUgdGhlIGRlZmF1bHQgdHJhbnNpdGlvbiBzcGVlZDpcbiAgICAgIHNsaWRlc2hvd1RyYW5zaXRpb25TcGVlZDogdW5kZWZpbmVkLFxuICAgICAgLy8gVGhlIGV2ZW50IG9iamVjdCBmb3Igd2hpY2ggdGhlIGRlZmF1bHQgYWN0aW9uIHdpbGwgYmUgY2FuY2VsZWRcbiAgICAgIC8vIG9uIEdhbGxlcnkgaW5pdGlhbGl6YXRpb24gKGUuZy4gdGhlIGNsaWNrIGV2ZW50IHRvIG9wZW4gdGhlIEdhbGxlcnkpOlxuICAgICAgZXZlbnQ6IHVuZGVmaW5lZCxcbiAgICAgIC8vIENhbGxiYWNrIGZ1bmN0aW9uIGV4ZWN1dGVkIHdoZW4gdGhlIEdhbGxlcnkgaXMgaW5pdGlhbGl6ZWQuXG4gICAgICAvLyBJcyBjYWxsZWQgd2l0aCB0aGUgZ2FsbGVyeSBpbnN0YW5jZSBhcyBcInRoaXNcIiBvYmplY3Q6XG4gICAgICBvbm9wZW46IHVuZGVmaW5lZCxcbiAgICAgIC8vIENhbGxiYWNrIGZ1bmN0aW9uIGV4ZWN1dGVkIHdoZW4gdGhlIEdhbGxlcnkgaGFzIGJlZW4gaW5pdGlhbGl6ZWRcbiAgICAgIC8vIGFuZCB0aGUgaW5pdGlhbGl6YXRpb24gdHJhbnNpdGlvbiBoYXMgYmVlbiBjb21wbGV0ZWQuXG4gICAgICAvLyBJcyBjYWxsZWQgd2l0aCB0aGUgZ2FsbGVyeSBpbnN0YW5jZSBhcyBcInRoaXNcIiBvYmplY3Q6XG4gICAgICBvbm9wZW5lZDogdW5kZWZpbmVkLFxuICAgICAgLy8gQ2FsbGJhY2sgZnVuY3Rpb24gZXhlY3V0ZWQgb24gc2xpZGUgY2hhbmdlLlxuICAgICAgLy8gSXMgY2FsbGVkIHdpdGggdGhlIGdhbGxlcnkgaW5zdGFuY2UgYXMgXCJ0aGlzXCIgb2JqZWN0IGFuZCB0aGVcbiAgICAgIC8vIGN1cnJlbnQgaW5kZXggYW5kIHNsaWRlIGFzIGFyZ3VtZW50czpcbiAgICAgIG9uc2xpZGU6IHVuZGVmaW5lZCxcbiAgICAgIC8vIENhbGxiYWNrIGZ1bmN0aW9uIGV4ZWN1dGVkIGFmdGVyIHRoZSBzbGlkZSBjaGFuZ2UgdHJhbnNpdGlvbi5cbiAgICAgIC8vIElzIGNhbGxlZCB3aXRoIHRoZSBnYWxsZXJ5IGluc3RhbmNlIGFzIFwidGhpc1wiIG9iamVjdCBhbmQgdGhlXG4gICAgICAvLyBjdXJyZW50IGluZGV4IGFuZCBzbGlkZSBhcyBhcmd1bWVudHM6XG4gICAgICBvbnNsaWRlZW5kOiB1bmRlZmluZWQsXG4gICAgICAvLyBDYWxsYmFjayBmdW5jdGlvbiBleGVjdXRlZCBvbiBzbGlkZSBjb250ZW50IGxvYWQuXG4gICAgICAvLyBJcyBjYWxsZWQgd2l0aCB0aGUgZ2FsbGVyeSBpbnN0YW5jZSBhcyBcInRoaXNcIiBvYmplY3QgYW5kIHRoZVxuICAgICAgLy8gc2xpZGUgaW5kZXggYW5kIHNsaWRlIGVsZW1lbnQgYXMgYXJndW1lbnRzOlxuICAgICAgb25zbGlkZWNvbXBsZXRlOiB1bmRlZmluZWQsXG4gICAgICAvLyBDYWxsYmFjayBmdW5jdGlvbiBleGVjdXRlZCB3aGVuIHRoZSBHYWxsZXJ5IGlzIGFib3V0IHRvIGJlIGNsb3NlZC5cbiAgICAgIC8vIElzIGNhbGxlZCB3aXRoIHRoZSBnYWxsZXJ5IGluc3RhbmNlIGFzIFwidGhpc1wiIG9iamVjdDpcbiAgICAgIG9uY2xvc2U6IHVuZGVmaW5lZCxcbiAgICAgIC8vIENhbGxiYWNrIGZ1bmN0aW9uIGV4ZWN1dGVkIHdoZW4gdGhlIEdhbGxlcnkgaGFzIGJlZW4gY2xvc2VkXG4gICAgICAvLyBhbmQgdGhlIGNsb3NpbmcgdHJhbnNpdGlvbiBoYXMgYmVlbiBjb21wbGV0ZWQuXG4gICAgICAvLyBJcyBjYWxsZWQgd2l0aCB0aGUgZ2FsbGVyeSBpbnN0YW5jZSBhcyBcInRoaXNcIiBvYmplY3Q6XG4gICAgICBvbmNsb3NlZDogdW5kZWZpbmVkXG4gICAgfSxcblxuICAgIGNhcm91c2VsT3B0aW9uczoge1xuICAgICAgaGlkZVBhZ2VTY3JvbGxiYXJzOiBmYWxzZSxcbiAgICAgIHRvZ2dsZUNvbnRyb2xzT25SZXR1cm46IGZhbHNlLFxuICAgICAgdG9nZ2xlU2xpZGVzaG93T25TcGFjZTogZmFsc2UsXG4gICAgICBlbmFibGVLZXlib2FyZE5hdmlnYXRpb246IGZhbHNlLFxuICAgICAgY2xvc2VPbkVzY2FwZTogZmFsc2UsXG4gICAgICBjbG9zZU9uU2xpZGVDbGljazogZmFsc2UsXG4gICAgICBjbG9zZU9uU3dpcGVVcE9yRG93bjogZmFsc2UsXG4gICAgICBkaXNhYmxlU2Nyb2xsOiBmYWxzZSxcbiAgICAgIHN0YXJ0U2xpZGVzaG93OiB0cnVlXG4gICAgfSxcblxuICAgIGNvbnNvbGU6XG4gICAgICB3aW5kb3cuY29uc29sZSAmJiB0eXBlb2Ygd2luZG93LmNvbnNvbGUubG9nID09PSAnZnVuY3Rpb24nXG4gICAgICAgID8gd2luZG93LmNvbnNvbGVcbiAgICAgICAgOiB7IGxvZzogZnVuY3Rpb24gKCkge30gfSxcblxuICAgIC8vIERldGVjdCB0b3VjaCwgdHJhbnNpdGlvbiwgdHJhbnNmb3JtIGFuZCBiYWNrZ3JvdW5kLXNpemUgc3VwcG9ydDpcbiAgICBzdXBwb3J0OiAoZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgIHZhciBzdXBwb3J0ID0ge1xuICAgICAgICB0b3VjaDpcbiAgICAgICAgICB3aW5kb3cub250b3VjaHN0YXJ0ICE9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAod2luZG93LkRvY3VtZW50VG91Y2ggJiYgZG9jdW1lbnQgaW5zdGFuY2VvZiBEb2N1bWVudFRvdWNoKVxuICAgICAgfVxuICAgICAgdmFyIHRyYW5zaXRpb25zID0ge1xuICAgICAgICB3ZWJraXRUcmFuc2l0aW9uOiB7XG4gICAgICAgICAgZW5kOiAnd2Via2l0VHJhbnNpdGlvbkVuZCcsXG4gICAgICAgICAgcHJlZml4OiAnLXdlYmtpdC0nXG4gICAgICAgIH0sXG4gICAgICAgIE1velRyYW5zaXRpb246IHtcbiAgICAgICAgICBlbmQ6ICd0cmFuc2l0aW9uZW5kJyxcbiAgICAgICAgICBwcmVmaXg6ICctbW96LSdcbiAgICAgICAgfSxcbiAgICAgICAgT1RyYW5zaXRpb246IHtcbiAgICAgICAgICBlbmQ6ICdvdHJhbnNpdGlvbmVuZCcsXG4gICAgICAgICAgcHJlZml4OiAnLW8tJ1xuICAgICAgICB9LFxuICAgICAgICB0cmFuc2l0aW9uOiB7XG4gICAgICAgICAgZW5kOiAndHJhbnNpdGlvbmVuZCcsXG4gICAgICAgICAgcHJlZml4OiAnJ1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICB2YXIgcHJvcFxuICAgICAgZm9yIChwcm9wIGluIHRyYW5zaXRpb25zKSB7XG4gICAgICAgIGlmIChcbiAgICAgICAgICB0cmFuc2l0aW9ucy5oYXNPd25Qcm9wZXJ0eShwcm9wKSAmJlxuICAgICAgICAgIGVsZW1lbnQuc3R5bGVbcHJvcF0gIT09IHVuZGVmaW5lZFxuICAgICAgICApIHtcbiAgICAgICAgICBzdXBwb3J0LnRyYW5zaXRpb24gPSB0cmFuc2l0aW9uc1twcm9wXVxuICAgICAgICAgIHN1cHBvcnQudHJhbnNpdGlvbi5uYW1lID0gcHJvcFxuICAgICAgICAgIGJyZWFrXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGZ1bmN0aW9uIGVsZW1lbnRUZXN0cyAoKSB7XG4gICAgICAgIHZhciB0cmFuc2l0aW9uID0gc3VwcG9ydC50cmFuc2l0aW9uXG4gICAgICAgIHZhciBwcm9wXG4gICAgICAgIHZhciB0cmFuc2xhdGVaXG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoZWxlbWVudClcbiAgICAgICAgaWYgKHRyYW5zaXRpb24pIHtcbiAgICAgICAgICBwcm9wID0gdHJhbnNpdGlvbi5uYW1lLnNsaWNlKDAsIC05KSArICdyYW5zZm9ybSdcbiAgICAgICAgICBpZiAoZWxlbWVudC5zdHlsZVtwcm9wXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBlbGVtZW50LnN0eWxlW3Byb3BdID0gJ3RyYW5zbGF0ZVooMCknXG4gICAgICAgICAgICB0cmFuc2xhdGVaID0gd2luZG93XG4gICAgICAgICAgICAgIC5nZXRDb21wdXRlZFN0eWxlKGVsZW1lbnQpXG4gICAgICAgICAgICAgIC5nZXRQcm9wZXJ0eVZhbHVlKHRyYW5zaXRpb24ucHJlZml4ICsgJ3RyYW5zZm9ybScpXG4gICAgICAgICAgICBzdXBwb3J0LnRyYW5zZm9ybSA9IHtcbiAgICAgICAgICAgICAgcHJlZml4OiB0cmFuc2l0aW9uLnByZWZpeCxcbiAgICAgICAgICAgICAgbmFtZTogcHJvcCxcbiAgICAgICAgICAgICAgdHJhbnNsYXRlOiB0cnVlLFxuICAgICAgICAgICAgICB0cmFuc2xhdGVaOiAhIXRyYW5zbGF0ZVogJiYgdHJhbnNsYXRlWiAhPT0gJ25vbmUnXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmIChlbGVtZW50LnN0eWxlLmJhY2tncm91bmRTaXplICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBzdXBwb3J0LmJhY2tncm91bmRTaXplID0ge31cbiAgICAgICAgICBlbGVtZW50LnN0eWxlLmJhY2tncm91bmRTaXplID0gJ2NvbnRhaW4nXG4gICAgICAgICAgc3VwcG9ydC5iYWNrZ3JvdW5kU2l6ZS5jb250YWluID1cbiAgICAgICAgICAgIHdpbmRvd1xuICAgICAgICAgICAgICAuZ2V0Q29tcHV0ZWRTdHlsZShlbGVtZW50KVxuICAgICAgICAgICAgICAuZ2V0UHJvcGVydHlWYWx1ZSgnYmFja2dyb3VuZC1zaXplJykgPT09ICdjb250YWluJ1xuICAgICAgICAgIGVsZW1lbnQuc3R5bGUuYmFja2dyb3VuZFNpemUgPSAnY292ZXInXG4gICAgICAgICAgc3VwcG9ydC5iYWNrZ3JvdW5kU2l6ZS5jb3ZlciA9XG4gICAgICAgICAgICB3aW5kb3dcbiAgICAgICAgICAgICAgLmdldENvbXB1dGVkU3R5bGUoZWxlbWVudClcbiAgICAgICAgICAgICAgLmdldFByb3BlcnR5VmFsdWUoJ2JhY2tncm91bmQtc2l6ZScpID09PSAnY292ZXInXG4gICAgICAgIH1cbiAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChlbGVtZW50KVxuICAgICAgfVxuICAgICAgaWYgKGRvY3VtZW50LmJvZHkpIHtcbiAgICAgICAgZWxlbWVudFRlc3RzKClcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgICQoZG9jdW1lbnQpLm9uKCdET01Db250ZW50TG9hZGVkJywgZWxlbWVudFRlc3RzKVxuICAgICAgfVxuICAgICAgcmV0dXJuIHN1cHBvcnRcbiAgICAgIC8vIFRlc3QgZWxlbWVudCwgaGFzIHRvIGJlIHN0YW5kYXJkIEhUTUwgYW5kIG11c3Qgbm90IGJlIGhpZGRlblxuICAgICAgLy8gZm9yIHRoZSBDU1MzIHRlc3RzIHVzaW5nIHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlIHRvIGJlIGFwcGxpY2FibGU6XG4gICAgfSkoZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JykpLFxuXG4gICAgcmVxdWVzdEFuaW1hdGlvbkZyYW1lOlxuICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgd2luZG93LndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZSB8fFxuICAgICAgd2luZG93Lm1velJlcXVlc3RBbmltYXRpb25GcmFtZSxcblxuICAgIGNhbmNlbEFuaW1hdGlvbkZyYW1lOlxuICAgICAgd2luZG93LmNhbmNlbEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICB3aW5kb3cud2Via2l0Q2FuY2VsUmVxdWVzdEFuaW1hdGlvbkZyYW1lIHx8XG4gICAgICB3aW5kb3cud2Via2l0Q2FuY2VsQW5pbWF0aW9uRnJhbWUgfHxcbiAgICAgIHdpbmRvdy5tb3pDYW5jZWxBbmltYXRpb25GcmFtZSxcblxuICAgIGluaXRpYWxpemU6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuaW5pdFN0YXJ0SW5kZXgoKVxuICAgICAgaWYgKHRoaXMuaW5pdFdpZGdldCgpID09PSBmYWxzZSkge1xuICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgIH1cbiAgICAgIHRoaXMuaW5pdEV2ZW50TGlzdGVuZXJzKClcbiAgICAgIC8vIExvYWQgdGhlIHNsaWRlIGF0IHRoZSBnaXZlbiBpbmRleDpcbiAgICAgIHRoaXMub25zbGlkZSh0aGlzLmluZGV4KVxuICAgICAgLy8gTWFudWFsbHkgdHJpZ2dlciB0aGUgc2xpZGVlbmQgZXZlbnQgZm9yIHRoZSBpbml0aWFsIHNsaWRlOlxuICAgICAgdGhpcy5vbnRyYW5zaXRpb25lbmQoKVxuICAgICAgLy8gU3RhcnQgdGhlIGF1dG9tYXRpYyBzbGlkZXNob3cgaWYgYXBwbGljYWJsZTpcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuc3RhcnRTbGlkZXNob3cpIHtcbiAgICAgICAgdGhpcy5wbGF5KClcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgc2xpZGU6IGZ1bmN0aW9uICh0bywgc3BlZWQpIHtcbiAgICAgIHdpbmRvdy5jbGVhclRpbWVvdXQodGhpcy50aW1lb3V0KVxuICAgICAgdmFyIGluZGV4ID0gdGhpcy5pbmRleFxuICAgICAgdmFyIGRpcmVjdGlvblxuICAgICAgdmFyIG5hdHVyYWxEaXJlY3Rpb25cbiAgICAgIHZhciBkaWZmXG4gICAgICBpZiAoaW5kZXggPT09IHRvIHx8IHRoaXMubnVtID09PSAxKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuICAgICAgaWYgKCFzcGVlZCkge1xuICAgICAgICBzcGVlZCA9IHRoaXMub3B0aW9ucy50cmFuc2l0aW9uU3BlZWRcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLnN1cHBvcnQudHJhbnNmb3JtKSB7XG4gICAgICAgIGlmICghdGhpcy5vcHRpb25zLmNvbnRpbnVvdXMpIHtcbiAgICAgICAgICB0byA9IHRoaXMuY2lyY2xlKHRvKVxuICAgICAgICB9XG4gICAgICAgIC8vIDE6IGJhY2t3YXJkLCAtMTogZm9yd2FyZDpcbiAgICAgICAgZGlyZWN0aW9uID0gTWF0aC5hYnMoaW5kZXggLSB0bykgLyAoaW5kZXggLSB0bylcbiAgICAgICAgLy8gR2V0IHRoZSBhY3R1YWwgcG9zaXRpb24gb2YgdGhlIHNsaWRlOlxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNvbnRpbnVvdXMpIHtcbiAgICAgICAgICBuYXR1cmFsRGlyZWN0aW9uID0gZGlyZWN0aW9uXG4gICAgICAgICAgZGlyZWN0aW9uID0gLXRoaXMucG9zaXRpb25zW3RoaXMuY2lyY2xlKHRvKV0gLyB0aGlzLnNsaWRlV2lkdGhcbiAgICAgICAgICAvLyBJZiBnb2luZyBmb3J3YXJkIGJ1dCB0byA8IGluZGV4LCB1c2UgdG8gPSBzbGlkZXMubGVuZ3RoICsgdG9cbiAgICAgICAgICAvLyBJZiBnb2luZyBiYWNrd2FyZCBidXQgdG8gPiBpbmRleCwgdXNlIHRvID0gLXNsaWRlcy5sZW5ndGggKyB0b1xuICAgICAgICAgIGlmIChkaXJlY3Rpb24gIT09IG5hdHVyYWxEaXJlY3Rpb24pIHtcbiAgICAgICAgICAgIHRvID0gLWRpcmVjdGlvbiAqIHRoaXMubnVtICsgdG9cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgZGlmZiA9IE1hdGguYWJzKGluZGV4IC0gdG8pIC0gMVxuICAgICAgICAvLyBNb3ZlIGFsbCB0aGUgc2xpZGVzIGJldHdlZW4gaW5kZXggYW5kIHRvIGluIHRoZSByaWdodCBkaXJlY3Rpb246XG4gICAgICAgIHdoaWxlIChkaWZmKSB7XG4gICAgICAgICAgZGlmZiAtPSAxXG4gICAgICAgICAgdGhpcy5tb3ZlKFxuICAgICAgICAgICAgdGhpcy5jaXJjbGUoKHRvID4gaW5kZXggPyB0byA6IGluZGV4KSAtIGRpZmYgLSAxKSxcbiAgICAgICAgICAgIHRoaXMuc2xpZGVXaWR0aCAqIGRpcmVjdGlvbixcbiAgICAgICAgICAgIDBcbiAgICAgICAgICApXG4gICAgICAgIH1cbiAgICAgICAgdG8gPSB0aGlzLmNpcmNsZSh0bylcbiAgICAgICAgdGhpcy5tb3ZlKGluZGV4LCB0aGlzLnNsaWRlV2lkdGggKiBkaXJlY3Rpb24sIHNwZWVkKVxuICAgICAgICB0aGlzLm1vdmUodG8sIDAsIHNwZWVkKVxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNvbnRpbnVvdXMpIHtcbiAgICAgICAgICB0aGlzLm1vdmUoXG4gICAgICAgICAgICB0aGlzLmNpcmNsZSh0byAtIGRpcmVjdGlvbiksXG4gICAgICAgICAgICAtKHRoaXMuc2xpZGVXaWR0aCAqIGRpcmVjdGlvbiksXG4gICAgICAgICAgICAwXG4gICAgICAgICAgKVxuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0byA9IHRoaXMuY2lyY2xlKHRvKVxuICAgICAgICB0aGlzLmFuaW1hdGUoaW5kZXggKiAtdGhpcy5zbGlkZVdpZHRoLCB0byAqIC10aGlzLnNsaWRlV2lkdGgsIHNwZWVkKVxuICAgICAgfVxuICAgICAgdGhpcy5vbnNsaWRlKHRvKVxuICAgIH0sXG5cbiAgICBnZXRJbmRleDogZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIHRoaXMuaW5kZXhcbiAgICB9LFxuXG4gICAgZ2V0TnVtYmVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gdGhpcy5udW1cbiAgICB9LFxuXG4gICAgcHJldjogZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHRoaXMub3B0aW9ucy5jb250aW51b3VzIHx8IHRoaXMuaW5kZXgpIHtcbiAgICAgICAgdGhpcy5zbGlkZSh0aGlzLmluZGV4IC0gMSlcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgbmV4dDogZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKHRoaXMub3B0aW9ucy5jb250aW51b3VzIHx8IHRoaXMuaW5kZXggPCB0aGlzLm51bSAtIDEpIHtcbiAgICAgICAgdGhpcy5zbGlkZSh0aGlzLmluZGV4ICsgMSlcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgcGxheTogZnVuY3Rpb24gKHRpbWUpIHtcbiAgICAgIHZhciB0aGF0ID0gdGhpc1xuICAgICAgd2luZG93LmNsZWFyVGltZW91dCh0aGlzLnRpbWVvdXQpXG4gICAgICB0aGlzLmludGVydmFsID0gdGltZSB8fCB0aGlzLm9wdGlvbnMuc2xpZGVzaG93SW50ZXJ2YWxcbiAgICAgIGlmICh0aGlzLmVsZW1lbnRzW3RoaXMuaW5kZXhdID4gMSkge1xuICAgICAgICB0aGlzLnRpbWVvdXQgPSB0aGlzLnNldFRpbWVvdXQoXG4gICAgICAgICAgKCF0aGlzLnJlcXVlc3RBbmltYXRpb25GcmFtZSAmJiB0aGlzLnNsaWRlKSB8fFxuICAgICAgICAgICAgZnVuY3Rpb24gKHRvLCBzcGVlZCkge1xuICAgICAgICAgICAgICB0aGF0LmFuaW1hdGlvbkZyYW1lSWQgPSB0aGF0LnJlcXVlc3RBbmltYXRpb25GcmFtZS5jYWxsKFxuICAgICAgICAgICAgICAgIHdpbmRvdyxcbiAgICAgICAgICAgICAgICBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICB0aGF0LnNsaWRlKHRvLCBzcGVlZClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgW3RoaXMuaW5kZXggKyAxLCB0aGlzLm9wdGlvbnMuc2xpZGVzaG93VHJhbnNpdGlvblNwZWVkXSxcbiAgICAgICAgICB0aGlzLmludGVydmFsXG4gICAgICAgIClcbiAgICAgIH1cbiAgICAgIHRoaXMuY29udGFpbmVyLmFkZENsYXNzKHRoaXMub3B0aW9ucy5wbGF5aW5nQ2xhc3MpXG4gICAgfSxcblxuICAgIHBhdXNlOiBmdW5jdGlvbiAoKSB7XG4gICAgICB3aW5kb3cuY2xlYXJUaW1lb3V0KHRoaXMudGltZW91dClcbiAgICAgIHRoaXMuaW50ZXJ2YWwgPSBudWxsXG4gICAgICBpZiAodGhpcy5jYW5jZWxBbmltYXRpb25GcmFtZSkge1xuICAgICAgICB0aGlzLmNhbmNlbEFuaW1hdGlvbkZyYW1lLmNhbGwod2luZG93LCB0aGlzLmFuaW1hdGlvbkZyYW1lSWQpXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uRnJhbWVJZCA9IG51bGxcbiAgICAgIH1cbiAgICAgIHRoaXMuY29udGFpbmVyLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5wbGF5aW5nQ2xhc3MpXG4gICAgfSxcblxuICAgIGFkZDogZnVuY3Rpb24gKGxpc3QpIHtcbiAgICAgIHZhciBpXG4gICAgICBpZiAoIWxpc3QuY29uY2F0KSB7XG4gICAgICAgIC8vIE1ha2UgYSByZWFsIGFycmF5IG91dCBvZiB0aGUgbGlzdCB0byBhZGQ6XG4gICAgICAgIGxpc3QgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChsaXN0KVxuICAgICAgfVxuICAgICAgaWYgKCF0aGlzLmxpc3QuY29uY2F0KSB7XG4gICAgICAgIC8vIE1ha2UgYSByZWFsIGFycmF5IG91dCBvZiB0aGUgR2FsbGVyeSBsaXN0OlxuICAgICAgICB0aGlzLmxpc3QgPSBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0aGlzLmxpc3QpXG4gICAgICB9XG4gICAgICB0aGlzLmxpc3QgPSB0aGlzLmxpc3QuY29uY2F0KGxpc3QpXG4gICAgICB0aGlzLm51bSA9IHRoaXMubGlzdC5sZW5ndGhcbiAgICAgIGlmICh0aGlzLm51bSA+IDIgJiYgdGhpcy5vcHRpb25zLmNvbnRpbnVvdXMgPT09IG51bGwpIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLmNvbnRpbnVvdXMgPSB0cnVlXG4gICAgICAgIHRoaXMuY29udGFpbmVyLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5sZWZ0RWRnZUNsYXNzKVxuICAgICAgfVxuICAgICAgdGhpcy5jb250YWluZXJcbiAgICAgICAgLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5yaWdodEVkZ2VDbGFzcylcbiAgICAgICAgLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5zaW5nbGVDbGFzcylcbiAgICAgIGZvciAoaSA9IHRoaXMubnVtIC0gbGlzdC5sZW5ndGg7IGkgPCB0aGlzLm51bTsgaSArPSAxKSB7XG4gICAgICAgIHRoaXMuYWRkU2xpZGUoaSlcbiAgICAgICAgdGhpcy5wb3NpdGlvblNsaWRlKGkpXG4gICAgICB9XG4gICAgICB0aGlzLnBvc2l0aW9ucy5sZW5ndGggPSB0aGlzLm51bVxuICAgICAgdGhpcy5pbml0U2xpZGVzKHRydWUpXG4gICAgfSxcblxuICAgIHJlc2V0U2xpZGVzOiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLnNsaWRlc0NvbnRhaW5lci5lbXB0eSgpXG4gICAgICB0aGlzLnVubG9hZEFsbFNsaWRlcygpXG4gICAgICB0aGlzLnNsaWRlcyA9IFtdXG4gICAgfSxcblxuICAgIGhhbmRsZUNsb3NlOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgb3B0aW9ucyA9IHRoaXMub3B0aW9uc1xuICAgICAgdGhpcy5kZXN0cm95RXZlbnRMaXN0ZW5lcnMoKVxuICAgICAgLy8gQ2FuY2VsIHRoZSBzbGlkZXNob3c6XG4gICAgICB0aGlzLnBhdXNlKClcbiAgICAgIHRoaXMuY29udGFpbmVyWzBdLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcbiAgICAgIHRoaXMuY29udGFpbmVyXG4gICAgICAgIC5yZW1vdmVDbGFzcyhvcHRpb25zLmRpc3BsYXlDbGFzcylcbiAgICAgICAgLnJlbW92ZUNsYXNzKG9wdGlvbnMuc2luZ2xlQ2xhc3MpXG4gICAgICAgIC5yZW1vdmVDbGFzcyhvcHRpb25zLmxlZnRFZGdlQ2xhc3MpXG4gICAgICAgIC5yZW1vdmVDbGFzcyhvcHRpb25zLnJpZ2h0RWRnZUNsYXNzKVxuICAgICAgaWYgKG9wdGlvbnMuaGlkZVBhZ2VTY3JvbGxiYXJzKSB7XG4gICAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSB0aGlzLmJvZHlPdmVyZmxvd1N0eWxlXG4gICAgICB9XG4gICAgICBpZiAodGhpcy5vcHRpb25zLmNsZWFyU2xpZGVzKSB7XG4gICAgICAgIHRoaXMucmVzZXRTbGlkZXMoKVxuICAgICAgfVxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5vbmNsb3NlZCkge1xuICAgICAgICB0aGlzLm9wdGlvbnMub25jbG9zZWQuY2FsbCh0aGlzKVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBjbG9zZTogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHRoYXQgPSB0aGlzXG4gICAgICBmdW5jdGlvbiBjbG9zZUhhbmRsZXIgKGV2ZW50KSB7XG4gICAgICAgIGlmIChldmVudC50YXJnZXQgPT09IHRoYXQuY29udGFpbmVyWzBdKSB7XG4gICAgICAgICAgdGhhdC5jb250YWluZXIub2ZmKHRoYXQuc3VwcG9ydC50cmFuc2l0aW9uLmVuZCwgY2xvc2VIYW5kbGVyKVxuICAgICAgICAgIHRoYXQuaGFuZGxlQ2xvc2UoKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAodGhpcy5vcHRpb25zLm9uY2xvc2UpIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLm9uY2xvc2UuY2FsbCh0aGlzKVxuICAgICAgfVxuICAgICAgaWYgKHRoaXMuc3VwcG9ydC50cmFuc2l0aW9uICYmIHRoaXMub3B0aW9ucy5kaXNwbGF5VHJhbnNpdGlvbikge1xuICAgICAgICB0aGlzLmNvbnRhaW5lci5vbih0aGlzLnN1cHBvcnQudHJhbnNpdGlvbi5lbmQsIGNsb3NlSGFuZGxlcilcbiAgICAgICAgdGhpcy5jb250YWluZXIucmVtb3ZlQ2xhc3ModGhpcy5vcHRpb25zLmRpc3BsYXlDbGFzcylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuaGFuZGxlQ2xvc2UoKVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBjaXJjbGU6IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgLy8gQWx3YXlzIHJldHVybiBhIG51bWJlciBpbnNpZGUgb2YgdGhlIHNsaWRlcyBpbmRleCByYW5nZTpcbiAgICAgIHJldHVybiAodGhpcy5udW0gKyBpbmRleCAlIHRoaXMubnVtKSAlIHRoaXMubnVtXG4gICAgfSxcblxuICAgIG1vdmU6IGZ1bmN0aW9uIChpbmRleCwgZGlzdCwgc3BlZWQpIHtcbiAgICAgIHRoaXMudHJhbnNsYXRlWChpbmRleCwgZGlzdCwgc3BlZWQpXG4gICAgICB0aGlzLnBvc2l0aW9uc1tpbmRleF0gPSBkaXN0XG4gICAgfSxcblxuICAgIHRyYW5zbGF0ZTogZnVuY3Rpb24gKGluZGV4LCB4LCB5LCBzcGVlZCkge1xuICAgICAgdmFyIHN0eWxlID0gdGhpcy5zbGlkZXNbaW5kZXhdLnN0eWxlXG4gICAgICB2YXIgdHJhbnNpdGlvbiA9IHRoaXMuc3VwcG9ydC50cmFuc2l0aW9uXG4gICAgICB2YXIgdHJhbnNmb3JtID0gdGhpcy5zdXBwb3J0LnRyYW5zZm9ybVxuICAgICAgc3R5bGVbdHJhbnNpdGlvbi5uYW1lICsgJ0R1cmF0aW9uJ10gPSBzcGVlZCArICdtcydcbiAgICAgIHN0eWxlW3RyYW5zZm9ybS5uYW1lXSA9XG4gICAgICAgICd0cmFuc2xhdGUoJyArXG4gICAgICAgIHggK1xuICAgICAgICAncHgsICcgK1xuICAgICAgICB5ICtcbiAgICAgICAgJ3B4KScgK1xuICAgICAgICAodHJhbnNmb3JtLnRyYW5zbGF0ZVogPyAnIHRyYW5zbGF0ZVooMCknIDogJycpXG4gICAgfSxcblxuICAgIHRyYW5zbGF0ZVg6IGZ1bmN0aW9uIChpbmRleCwgeCwgc3BlZWQpIHtcbiAgICAgIHRoaXMudHJhbnNsYXRlKGluZGV4LCB4LCAwLCBzcGVlZClcbiAgICB9LFxuXG4gICAgdHJhbnNsYXRlWTogZnVuY3Rpb24gKGluZGV4LCB5LCBzcGVlZCkge1xuICAgICAgdGhpcy50cmFuc2xhdGUoaW5kZXgsIDAsIHksIHNwZWVkKVxuICAgIH0sXG5cbiAgICBhbmltYXRlOiBmdW5jdGlvbiAoZnJvbSwgdG8sIHNwZWVkKSB7XG4gICAgICBpZiAoIXNwZWVkKSB7XG4gICAgICAgIHRoaXMuc2xpZGVzQ29udGFpbmVyWzBdLnN0eWxlLmxlZnQgPSB0byArICdweCdcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICB2YXIgdGhhdCA9IHRoaXNcbiAgICAgIHZhciBzdGFydCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpXG4gICAgICB2YXIgdGltZXIgPSB3aW5kb3cuc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgdGltZUVsYXAgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIHN0YXJ0XG4gICAgICAgIGlmICh0aW1lRWxhcCA+IHNwZWVkKSB7XG4gICAgICAgICAgdGhhdC5zbGlkZXNDb250YWluZXJbMF0uc3R5bGUubGVmdCA9IHRvICsgJ3B4J1xuICAgICAgICAgIHRoYXQub250cmFuc2l0aW9uZW5kKClcbiAgICAgICAgICB3aW5kb3cuY2xlYXJJbnRlcnZhbCh0aW1lcilcbiAgICAgICAgICByZXR1cm5cbiAgICAgICAgfVxuICAgICAgICB0aGF0LnNsaWRlc0NvbnRhaW5lclswXS5zdHlsZS5sZWZ0ID1cbiAgICAgICAgICAodG8gLSBmcm9tKSAqIChNYXRoLmZsb29yKHRpbWVFbGFwIC8gc3BlZWQgKiAxMDApIC8gMTAwKSArIGZyb20gKyAncHgnXG4gICAgICB9LCA0KVxuICAgIH0sXG5cbiAgICBwcmV2ZW50RGVmYXVsdDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAoZXZlbnQucHJldmVudERlZmF1bHQpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZXZlbnQucmV0dXJuVmFsdWUgPSBmYWxzZVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBzdG9wUHJvcGFnYXRpb246IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgaWYgKGV2ZW50LnN0b3BQcm9wYWdhdGlvbikge1xuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZXZlbnQuY2FuY2VsQnViYmxlID0gdHJ1ZVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBvbnJlc2l6ZTogZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5pbml0U2xpZGVzKHRydWUpXG4gICAgfSxcblxuICAgIG9ubW91c2Vkb3duOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIC8vIFRyaWdnZXIgb24gY2xpY2tzIG9mIHRoZSBsZWZ0IG1vdXNlIGJ1dHRvbiBvbmx5XG4gICAgICAvLyBhbmQgZXhjbHVkZSB2aWRlbyAmIGF1ZGlvIGVsZW1lbnRzOlxuICAgICAgaWYgKFxuICAgICAgICBldmVudC53aGljaCAmJlxuICAgICAgICBldmVudC53aGljaCA9PT0gMSAmJlxuICAgICAgICBldmVudC50YXJnZXQubm9kZU5hbWUgIT09ICdWSURFTycgJiZcbiAgICAgICAgZXZlbnQudGFyZ2V0Lm5vZGVOYW1lICE9PSAnQVVESU8nXG4gICAgICApIHtcbiAgICAgICAgLy8gUHJldmVudGluZyB0aGUgZGVmYXVsdCBtb3VzZWRvd24gYWN0aW9uIGlzIHJlcXVpcmVkXG4gICAgICAgIC8vIHRvIG1ha2UgdG91Y2ggZW11bGF0aW9uIHdvcmsgd2l0aCBGaXJlZm94OlxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgIDsoZXZlbnQub3JpZ2luYWxFdmVudCB8fCBldmVudCkudG91Y2hlcyA9IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBwYWdlWDogZXZlbnQucGFnZVgsXG4gICAgICAgICAgICBwYWdlWTogZXZlbnQucGFnZVlcbiAgICAgICAgICB9XG4gICAgICAgIF1cbiAgICAgICAgdGhpcy5vbnRvdWNoc3RhcnQoZXZlbnQpXG4gICAgICB9XG4gICAgfSxcblxuICAgIG9ubW91c2Vtb3ZlOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmICh0aGlzLnRvdWNoU3RhcnQpIHtcbiAgICAgICAgOyhldmVudC5vcmlnaW5hbEV2ZW50IHx8IGV2ZW50KS50b3VjaGVzID0gW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIHBhZ2VYOiBldmVudC5wYWdlWCxcbiAgICAgICAgICAgIHBhZ2VZOiBldmVudC5wYWdlWVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgICB0aGlzLm9udG91Y2htb3ZlKGV2ZW50KVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBvbm1vdXNldXA6IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgaWYgKHRoaXMudG91Y2hTdGFydCkge1xuICAgICAgICB0aGlzLm9udG91Y2hlbmQoZXZlbnQpXG4gICAgICAgIGRlbGV0ZSB0aGlzLnRvdWNoU3RhcnRcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgb25tb3VzZW91dDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAodGhpcy50b3VjaFN0YXJ0KSB7XG4gICAgICAgIHZhciB0YXJnZXQgPSBldmVudC50YXJnZXRcbiAgICAgICAgdmFyIHJlbGF0ZWQgPSBldmVudC5yZWxhdGVkVGFyZ2V0XG4gICAgICAgIGlmICghcmVsYXRlZCB8fCAocmVsYXRlZCAhPT0gdGFyZ2V0ICYmICEkLmNvbnRhaW5zKHRhcmdldCwgcmVsYXRlZCkpKSB7XG4gICAgICAgICAgdGhpcy5vbm1vdXNldXAoZXZlbnQpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgb250b3VjaHN0YXJ0OiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuc3RvcFRvdWNoRXZlbnRzUHJvcGFnYXRpb24pIHtcbiAgICAgICAgdGhpcy5zdG9wUHJvcGFnYXRpb24oZXZlbnQpXG4gICAgICB9XG4gICAgICAvLyBqUXVlcnkgZG9lc24ndCBjb3B5IHRvdWNoIGV2ZW50IHByb3BlcnRpZXMgYnkgZGVmYXVsdCxcbiAgICAgIC8vIHNvIHdlIGhhdmUgdG8gYWNjZXNzIHRoZSBvcmlnaW5hbEV2ZW50IG9iamVjdDpcbiAgICAgIHZhciB0b3VjaGVzID0gKGV2ZW50Lm9yaWdpbmFsRXZlbnQgfHwgZXZlbnQpLnRvdWNoZXNbMF1cbiAgICAgIHRoaXMudG91Y2hTdGFydCA9IHtcbiAgICAgICAgLy8gUmVtZW1iZXIgdGhlIGluaXRpYWwgdG91Y2ggY29vcmRpbmF0ZXM6XG4gICAgICAgIHg6IHRvdWNoZXMucGFnZVgsXG4gICAgICAgIHk6IHRvdWNoZXMucGFnZVksXG4gICAgICAgIC8vIFN0b3JlIHRoZSB0aW1lIHRvIGRldGVybWluZSB0b3VjaCBkdXJhdGlvbjpcbiAgICAgICAgdGltZTogRGF0ZS5ub3coKVxuICAgICAgfVxuICAgICAgLy8gSGVscGVyIHZhcmlhYmxlIHRvIGRldGVjdCBzY3JvbGwgbW92ZW1lbnQ6XG4gICAgICB0aGlzLmlzU2Nyb2xsaW5nID0gdW5kZWZpbmVkXG4gICAgICAvLyBSZXNldCBkZWx0YSB2YWx1ZXM6XG4gICAgICB0aGlzLnRvdWNoRGVsdGEgPSB7fVxuICAgIH0sXG5cbiAgICBvbnRvdWNobW92ZTogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAodGhpcy5vcHRpb25zLnN0b3BUb3VjaEV2ZW50c1Byb3BhZ2F0aW9uKSB7XG4gICAgICAgIHRoaXMuc3RvcFByb3BhZ2F0aW9uKGV2ZW50KVxuICAgICAgfVxuICAgICAgLy8galF1ZXJ5IGRvZXNuJ3QgY29weSB0b3VjaCBldmVudCBwcm9wZXJ0aWVzIGJ5IGRlZmF1bHQsXG4gICAgICAvLyBzbyB3ZSBoYXZlIHRvIGFjY2VzcyB0aGUgb3JpZ2luYWxFdmVudCBvYmplY3Q6XG4gICAgICB2YXIgdG91Y2hlcyA9IChldmVudC5vcmlnaW5hbEV2ZW50IHx8IGV2ZW50KS50b3VjaGVzWzBdXG4gICAgICB2YXIgc2NhbGUgPSAoZXZlbnQub3JpZ2luYWxFdmVudCB8fCBldmVudCkuc2NhbGVcbiAgICAgIHZhciBpbmRleCA9IHRoaXMuaW5kZXhcbiAgICAgIHZhciB0b3VjaERlbHRhWFxuICAgICAgdmFyIGluZGljZXNcbiAgICAgIC8vIEVuc3VyZSB0aGlzIGlzIGEgb25lIHRvdWNoIHN3aXBlIGFuZCBub3QsIGUuZy4gYSBwaW5jaDpcbiAgICAgIGlmICh0b3VjaGVzLmxlbmd0aCA+IDEgfHwgKHNjYWxlICYmIHNjYWxlICE9PSAxKSkge1xuICAgICAgICByZXR1cm5cbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuZGlzYWJsZVNjcm9sbCkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICB9XG4gICAgICAvLyBNZWFzdXJlIGNoYW5nZSBpbiB4IGFuZCB5IGNvb3JkaW5hdGVzOlxuICAgICAgdGhpcy50b3VjaERlbHRhID0ge1xuICAgICAgICB4OiB0b3VjaGVzLnBhZ2VYIC0gdGhpcy50b3VjaFN0YXJ0LngsXG4gICAgICAgIHk6IHRvdWNoZXMucGFnZVkgLSB0aGlzLnRvdWNoU3RhcnQueVxuICAgICAgfVxuICAgICAgdG91Y2hEZWx0YVggPSB0aGlzLnRvdWNoRGVsdGEueFxuICAgICAgLy8gRGV0ZWN0IGlmIHRoaXMgaXMgYSB2ZXJ0aWNhbCBzY3JvbGwgbW92ZW1lbnQgKHJ1biBvbmx5IG9uY2UgcGVyIHRvdWNoKTpcbiAgICAgIGlmICh0aGlzLmlzU2Nyb2xsaW5nID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgdGhpcy5pc1Njcm9sbGluZyA9XG4gICAgICAgICAgdGhpcy5pc1Njcm9sbGluZyB8fFxuICAgICAgICAgIE1hdGguYWJzKHRvdWNoRGVsdGFYKSA8IE1hdGguYWJzKHRoaXMudG91Y2hEZWx0YS55KVxuICAgICAgfVxuICAgICAgaWYgKCF0aGlzLmlzU2Nyb2xsaW5nKSB7XG4gICAgICAgIC8vIEFsd2F5cyBwcmV2ZW50IGhvcml6b250YWwgc2Nyb2xsOlxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpXG4gICAgICAgIC8vIFN0b3AgdGhlIHNsaWRlc2hvdzpcbiAgICAgICAgd2luZG93LmNsZWFyVGltZW91dCh0aGlzLnRpbWVvdXQpXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29udGludW91cykge1xuICAgICAgICAgIGluZGljZXMgPSBbdGhpcy5jaXJjbGUoaW5kZXggKyAxKSwgaW5kZXgsIHRoaXMuY2lyY2xlKGluZGV4IC0gMSldXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gSW5jcmVhc2UgcmVzaXN0YW5jZSBpZiBmaXJzdCBzbGlkZSBhbmQgc2xpZGluZyBsZWZ0XG4gICAgICAgICAgLy8gb3IgbGFzdCBzbGlkZSBhbmQgc2xpZGluZyByaWdodDpcbiAgICAgICAgICB0aGlzLnRvdWNoRGVsdGEueCA9IHRvdWNoRGVsdGFYID1cbiAgICAgICAgICAgIHRvdWNoRGVsdGFYIC9cbiAgICAgICAgICAgICgoIWluZGV4ICYmIHRvdWNoRGVsdGFYID4gMCkgfHxcbiAgICAgICAgICAgIChpbmRleCA9PT0gdGhpcy5udW0gLSAxICYmIHRvdWNoRGVsdGFYIDwgMClcbiAgICAgICAgICAgICAgPyBNYXRoLmFicyh0b3VjaERlbHRhWCkgLyB0aGlzLnNsaWRlV2lkdGggKyAxXG4gICAgICAgICAgICAgIDogMSlcbiAgICAgICAgICBpbmRpY2VzID0gW2luZGV4XVxuICAgICAgICAgIGlmIChpbmRleCkge1xuICAgICAgICAgICAgaW5kaWNlcy5wdXNoKGluZGV4IC0gMSlcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGluZGV4IDwgdGhpcy5udW0gLSAxKSB7XG4gICAgICAgICAgICBpbmRpY2VzLnVuc2hpZnQoaW5kZXggKyAxKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB3aGlsZSAoaW5kaWNlcy5sZW5ndGgpIHtcbiAgICAgICAgICBpbmRleCA9IGluZGljZXMucG9wKClcbiAgICAgICAgICB0aGlzLnRyYW5zbGF0ZVgoaW5kZXgsIHRvdWNoRGVsdGFYICsgdGhpcy5wb3NpdGlvbnNbaW5kZXhdLCAwKVxuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnRyYW5zbGF0ZVkoaW5kZXgsIHRoaXMudG91Y2hEZWx0YS55ICsgdGhpcy5wb3NpdGlvbnNbaW5kZXhdLCAwKVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBvbnRvdWNoZW5kOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuc3RvcFRvdWNoRXZlbnRzUHJvcGFnYXRpb24pIHtcbiAgICAgICAgdGhpcy5zdG9wUHJvcGFnYXRpb24oZXZlbnQpXG4gICAgICB9XG4gICAgICB2YXIgaW5kZXggPSB0aGlzLmluZGV4XG4gICAgICB2YXIgc3BlZWQgPSB0aGlzLm9wdGlvbnMudHJhbnNpdGlvblNwZWVkXG4gICAgICB2YXIgc2xpZGVXaWR0aCA9IHRoaXMuc2xpZGVXaWR0aFxuICAgICAgdmFyIGlzU2hvcnREdXJhdGlvbiA9IE51bWJlcihEYXRlLm5vdygpIC0gdGhpcy50b3VjaFN0YXJ0LnRpbWUpIDwgMjUwXG4gICAgICAvLyBEZXRlcm1pbmUgaWYgc2xpZGUgYXR0ZW1wdCB0cmlnZ2VycyBuZXh0L3ByZXYgc2xpZGU6XG4gICAgICB2YXIgaXNWYWxpZFNsaWRlID1cbiAgICAgICAgKGlzU2hvcnREdXJhdGlvbiAmJiBNYXRoLmFicyh0aGlzLnRvdWNoRGVsdGEueCkgPiAyMCkgfHxcbiAgICAgICAgTWF0aC5hYnModGhpcy50b3VjaERlbHRhLngpID4gc2xpZGVXaWR0aCAvIDJcbiAgICAgIC8vIERldGVybWluZSBpZiBzbGlkZSBhdHRlbXB0IGlzIHBhc3Qgc3RhcnQgb3IgZW5kOlxuICAgICAgdmFyIGlzUGFzdEJvdW5kcyA9XG4gICAgICAgICghaW5kZXggJiYgdGhpcy50b3VjaERlbHRhLnggPiAwKSB8fFxuICAgICAgICAoaW5kZXggPT09IHRoaXMubnVtIC0gMSAmJiB0aGlzLnRvdWNoRGVsdGEueCA8IDApXG4gICAgICB2YXIgaXNWYWxpZENsb3NlID1cbiAgICAgICAgIWlzVmFsaWRTbGlkZSAmJlxuICAgICAgICB0aGlzLm9wdGlvbnMuY2xvc2VPblN3aXBlVXBPckRvd24gJiZcbiAgICAgICAgKChpc1Nob3J0RHVyYXRpb24gJiYgTWF0aC5hYnModGhpcy50b3VjaERlbHRhLnkpID4gMjApIHx8XG4gICAgICAgICAgTWF0aC5hYnModGhpcy50b3VjaERlbHRhLnkpID4gdGhpcy5zbGlkZUhlaWdodCAvIDIpXG4gICAgICB2YXIgZGlyZWN0aW9uXG4gICAgICB2YXIgaW5kZXhGb3J3YXJkXG4gICAgICB2YXIgaW5kZXhCYWNrd2FyZFxuICAgICAgdmFyIGRpc3RhbmNlRm9yd2FyZFxuICAgICAgdmFyIGRpc3RhbmNlQmFja3dhcmRcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29udGludW91cykge1xuICAgICAgICBpc1Bhc3RCb3VuZHMgPSBmYWxzZVxuICAgICAgfVxuICAgICAgLy8gRGV0ZXJtaW5lIGRpcmVjdGlvbiBvZiBzd2lwZSAodHJ1ZTogcmlnaHQsIGZhbHNlOiBsZWZ0KTpcbiAgICAgIGRpcmVjdGlvbiA9IHRoaXMudG91Y2hEZWx0YS54IDwgMCA/IC0xIDogMVxuICAgICAgaWYgKCF0aGlzLmlzU2Nyb2xsaW5nKSB7XG4gICAgICAgIGlmIChpc1ZhbGlkU2xpZGUgJiYgIWlzUGFzdEJvdW5kcykge1xuICAgICAgICAgIGluZGV4Rm9yd2FyZCA9IGluZGV4ICsgZGlyZWN0aW9uXG4gICAgICAgICAgaW5kZXhCYWNrd2FyZCA9IGluZGV4IC0gZGlyZWN0aW9uXG4gICAgICAgICAgZGlzdGFuY2VGb3J3YXJkID0gc2xpZGVXaWR0aCAqIGRpcmVjdGlvblxuICAgICAgICAgIGRpc3RhbmNlQmFja3dhcmQgPSAtc2xpZGVXaWR0aCAqIGRpcmVjdGlvblxuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29udGludW91cykge1xuICAgICAgICAgICAgdGhpcy5tb3ZlKHRoaXMuY2lyY2xlKGluZGV4Rm9yd2FyZCksIGRpc3RhbmNlRm9yd2FyZCwgMClcbiAgICAgICAgICAgIHRoaXMubW92ZSh0aGlzLmNpcmNsZShpbmRleCAtIDIgKiBkaXJlY3Rpb24pLCBkaXN0YW5jZUJhY2t3YXJkLCAwKVxuICAgICAgICAgIH0gZWxzZSBpZiAoaW5kZXhGb3J3YXJkID49IDAgJiYgaW5kZXhGb3J3YXJkIDwgdGhpcy5udW0pIHtcbiAgICAgICAgICAgIHRoaXMubW92ZShpbmRleEZvcndhcmQsIGRpc3RhbmNlRm9yd2FyZCwgMClcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5tb3ZlKGluZGV4LCB0aGlzLnBvc2l0aW9uc1tpbmRleF0gKyBkaXN0YW5jZUZvcndhcmQsIHNwZWVkKVxuICAgICAgICAgIHRoaXMubW92ZShcbiAgICAgICAgICAgIHRoaXMuY2lyY2xlKGluZGV4QmFja3dhcmQpLFxuICAgICAgICAgICAgdGhpcy5wb3NpdGlvbnNbdGhpcy5jaXJjbGUoaW5kZXhCYWNrd2FyZCldICsgZGlzdGFuY2VGb3J3YXJkLFxuICAgICAgICAgICAgc3BlZWRcbiAgICAgICAgICApXG4gICAgICAgICAgaW5kZXggPSB0aGlzLmNpcmNsZShpbmRleEJhY2t3YXJkKVxuICAgICAgICAgIHRoaXMub25zbGlkZShpbmRleClcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBNb3ZlIGJhY2sgaW50byBwb3NpdGlvblxuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29udGludW91cykge1xuICAgICAgICAgICAgdGhpcy5tb3ZlKHRoaXMuY2lyY2xlKGluZGV4IC0gMSksIC1zbGlkZVdpZHRoLCBzcGVlZClcbiAgICAgICAgICAgIHRoaXMubW92ZShpbmRleCwgMCwgc3BlZWQpXG4gICAgICAgICAgICB0aGlzLm1vdmUodGhpcy5jaXJjbGUoaW5kZXggKyAxKSwgc2xpZGVXaWR0aCwgc3BlZWQpXG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGlmIChpbmRleCkge1xuICAgICAgICAgICAgICB0aGlzLm1vdmUoaW5kZXggLSAxLCAtc2xpZGVXaWR0aCwgc3BlZWQpXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLm1vdmUoaW5kZXgsIDAsIHNwZWVkKVxuICAgICAgICAgICAgaWYgKGluZGV4IDwgdGhpcy5udW0gLSAxKSB7XG4gICAgICAgICAgICAgIHRoaXMubW92ZShpbmRleCArIDEsIHNsaWRlV2lkdGgsIHNwZWVkKVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKGlzVmFsaWRDbG9zZSkge1xuICAgICAgICAgIHRoaXMuY2xvc2UoKVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIE1vdmUgYmFjayBpbnRvIHBvc2l0aW9uXG4gICAgICAgICAgdGhpcy50cmFuc2xhdGVZKGluZGV4LCAwLCBzcGVlZClcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBvbnRvdWNoY2FuY2VsOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmICh0aGlzLnRvdWNoU3RhcnQpIHtcbiAgICAgICAgdGhpcy5vbnRvdWNoZW5kKGV2ZW50KVxuICAgICAgICBkZWxldGUgdGhpcy50b3VjaFN0YXJ0XG4gICAgICB9XG4gICAgfSxcblxuICAgIG9udHJhbnNpdGlvbmVuZDogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB2YXIgc2xpZGUgPSB0aGlzLnNsaWRlc1t0aGlzLmluZGV4XVxuICAgICAgaWYgKCFldmVudCB8fCBzbGlkZSA9PT0gZXZlbnQudGFyZ2V0KSB7XG4gICAgICAgIGlmICh0aGlzLmludGVydmFsKSB7XG4gICAgICAgICAgdGhpcy5wbGF5KClcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNldFRpbWVvdXQodGhpcy5vcHRpb25zLm9uc2xpZGVlbmQsIFt0aGlzLmluZGV4LCBzbGlkZV0pXG4gICAgICB9XG4gICAgfSxcblxuICAgIG9uY29tcGxldGU6IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIHRhcmdldCA9IGV2ZW50LnRhcmdldCB8fCBldmVudC5zcmNFbGVtZW50XG4gICAgICB2YXIgcGFyZW50ID0gdGFyZ2V0ICYmIHRhcmdldC5wYXJlbnROb2RlXG4gICAgICB2YXIgaW5kZXhcbiAgICAgIGlmICghdGFyZ2V0IHx8ICFwYXJlbnQpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG4gICAgICBpbmRleCA9IHRoaXMuZ2V0Tm9kZUluZGV4KHBhcmVudClcbiAgICAgICQocGFyZW50KS5yZW1vdmVDbGFzcyh0aGlzLm9wdGlvbnMuc2xpZGVMb2FkaW5nQ2xhc3MpXG4gICAgICBpZiAoZXZlbnQudHlwZSA9PT0gJ2Vycm9yJykge1xuICAgICAgICAkKHBhcmVudCkuYWRkQ2xhc3ModGhpcy5vcHRpb25zLnNsaWRlRXJyb3JDbGFzcylcbiAgICAgICAgdGhpcy5lbGVtZW50c1tpbmRleF0gPSAzIC8vIEZhaWxcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuZWxlbWVudHNbaW5kZXhdID0gMiAvLyBEb25lXG4gICAgICB9XG4gICAgICAvLyBGaXggZm9yIElFNydzIGxhY2sgb2Ygc3VwcG9ydCBmb3IgcGVyY2VudGFnZSBtYXgtaGVpZ2h0OlxuICAgICAgaWYgKHRhcmdldC5jbGllbnRIZWlnaHQgPiB0aGlzLmNvbnRhaW5lclswXS5jbGllbnRIZWlnaHQpIHtcbiAgICAgICAgdGFyZ2V0LnN0eWxlLm1heEhlaWdodCA9IHRoaXMuY29udGFpbmVyWzBdLmNsaWVudEhlaWdodFxuICAgICAgfVxuICAgICAgaWYgKHRoaXMuaW50ZXJ2YWwgJiYgdGhpcy5zbGlkZXNbdGhpcy5pbmRleF0gPT09IHBhcmVudCkge1xuICAgICAgICB0aGlzLnBsYXkoKVxuICAgICAgfVxuICAgICAgdGhpcy5zZXRUaW1lb3V0KHRoaXMub3B0aW9ucy5vbnNsaWRlY29tcGxldGUsIFtpbmRleCwgcGFyZW50XSlcbiAgICB9LFxuXG4gICAgb25sb2FkOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHRoaXMub25jb21wbGV0ZShldmVudClcbiAgICB9LFxuXG4gICAgb25lcnJvcjogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB0aGlzLm9uY29tcGxldGUoZXZlbnQpXG4gICAgfSxcblxuICAgIG9ua2V5ZG93bjogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBzd2l0Y2ggKGV2ZW50LndoaWNoIHx8IGV2ZW50LmtleUNvZGUpIHtcbiAgICAgICAgY2FzZSAxMzogLy8gUmV0dXJuXG4gICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50b2dnbGVDb250cm9sc09uUmV0dXJuKSB7XG4gICAgICAgICAgICB0aGlzLnByZXZlbnREZWZhdWx0KGV2ZW50KVxuICAgICAgICAgICAgdGhpcy50b2dnbGVDb250cm9scygpXG4gICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrXG4gICAgICAgIGNhc2UgMjc6IC8vIEVzY1xuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY2xvc2VPbkVzY2FwZSkge1xuICAgICAgICAgICAgdGhpcy5jbG9zZSgpXG4gICAgICAgICAgICAvLyBwcmV2ZW50IEVzYyBmcm9tIGNsb3Npbmcgb3RoZXIgdGhpbmdzXG4gICAgICAgICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIDMyOiAvLyBTcGFjZVxuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudG9nZ2xlU2xpZGVzaG93T25TcGFjZSkge1xuICAgICAgICAgICAgdGhpcy5wcmV2ZW50RGVmYXVsdChldmVudClcbiAgICAgICAgICAgIHRoaXMudG9nZ2xlU2xpZGVzaG93KClcbiAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgY2FzZSAzNzogLy8gTGVmdFxuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuZW5hYmxlS2V5Ym9hcmROYXZpZ2F0aW9uKSB7XG4gICAgICAgICAgICB0aGlzLnByZXZlbnREZWZhdWx0KGV2ZW50KVxuICAgICAgICAgICAgdGhpcy5wcmV2KClcbiAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgY2FzZSAzOTogLy8gUmlnaHRcbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmVuYWJsZUtleWJvYXJkTmF2aWdhdGlvbikge1xuICAgICAgICAgICAgdGhpcy5wcmV2ZW50RGVmYXVsdChldmVudClcbiAgICAgICAgICAgIHRoaXMubmV4dCgpXG4gICAgICAgICAgfVxuICAgICAgICAgIGJyZWFrXG4gICAgICB9XG4gICAgfSxcblxuICAgIGhhbmRsZUNsaWNrOiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciBvcHRpb25zID0gdGhpcy5vcHRpb25zXG4gICAgICB2YXIgdGFyZ2V0ID0gZXZlbnQudGFyZ2V0IHx8IGV2ZW50LnNyY0VsZW1lbnRcbiAgICAgIHZhciBwYXJlbnQgPSB0YXJnZXQucGFyZW50Tm9kZVxuICAgICAgZnVuY3Rpb24gaXNUYXJnZXQgKGNsYXNzTmFtZSkge1xuICAgICAgICByZXR1cm4gJCh0YXJnZXQpLmhhc0NsYXNzKGNsYXNzTmFtZSkgfHwgJChwYXJlbnQpLmhhc0NsYXNzKGNsYXNzTmFtZSlcbiAgICAgIH1cbiAgICAgIGlmIChpc1RhcmdldChvcHRpb25zLnRvZ2dsZUNsYXNzKSkge1xuICAgICAgICAvLyBDbGljayBvbiBcInRvZ2dsZVwiIGNvbnRyb2xcbiAgICAgICAgdGhpcy5wcmV2ZW50RGVmYXVsdChldmVudClcbiAgICAgICAgdGhpcy50b2dnbGVDb250cm9scygpXG4gICAgICB9IGVsc2UgaWYgKGlzVGFyZ2V0KG9wdGlvbnMucHJldkNsYXNzKSkge1xuICAgICAgICAvLyBDbGljayBvbiBcInByZXZcIiBjb250cm9sXG4gICAgICAgIHRoaXMucHJldmVudERlZmF1bHQoZXZlbnQpXG4gICAgICAgIHRoaXMucHJldigpXG4gICAgICB9IGVsc2UgaWYgKGlzVGFyZ2V0KG9wdGlvbnMubmV4dENsYXNzKSkge1xuICAgICAgICAvLyBDbGljayBvbiBcIm5leHRcIiBjb250cm9sXG4gICAgICAgIHRoaXMucHJldmVudERlZmF1bHQoZXZlbnQpXG4gICAgICAgIHRoaXMubmV4dCgpXG4gICAgICB9IGVsc2UgaWYgKGlzVGFyZ2V0KG9wdGlvbnMuY2xvc2VDbGFzcykpIHtcbiAgICAgICAgLy8gQ2xpY2sgb24gXCJjbG9zZVwiIGNvbnRyb2xcbiAgICAgICAgdGhpcy5wcmV2ZW50RGVmYXVsdChldmVudClcbiAgICAgICAgdGhpcy5jbG9zZSgpXG4gICAgICB9IGVsc2UgaWYgKGlzVGFyZ2V0KG9wdGlvbnMucGxheVBhdXNlQ2xhc3MpKSB7XG4gICAgICAgIC8vIENsaWNrIG9uIFwicGxheS1wYXVzZVwiIGNvbnRyb2xcbiAgICAgICAgdGhpcy5wcmV2ZW50RGVmYXVsdChldmVudClcbiAgICAgICAgdGhpcy50b2dnbGVTbGlkZXNob3coKVxuICAgICAgfSBlbHNlIGlmIChwYXJlbnQgPT09IHRoaXMuc2xpZGVzQ29udGFpbmVyWzBdKSB7XG4gICAgICAgIC8vIENsaWNrIG9uIHNsaWRlIGJhY2tncm91bmRcbiAgICAgICAgaWYgKG9wdGlvbnMuY2xvc2VPblNsaWRlQ2xpY2spIHtcbiAgICAgICAgICB0aGlzLnByZXZlbnREZWZhdWx0KGV2ZW50KVxuICAgICAgICAgIHRoaXMuY2xvc2UoKVxuICAgICAgICB9IGVsc2UgaWYgKG9wdGlvbnMudG9nZ2xlQ29udHJvbHNPblNsaWRlQ2xpY2spIHtcbiAgICAgICAgICB0aGlzLnByZXZlbnREZWZhdWx0KGV2ZW50KVxuICAgICAgICAgIHRoaXMudG9nZ2xlQ29udHJvbHMoKVxuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKFxuICAgICAgICBwYXJlbnQucGFyZW50Tm9kZSAmJlxuICAgICAgICBwYXJlbnQucGFyZW50Tm9kZSA9PT0gdGhpcy5zbGlkZXNDb250YWluZXJbMF1cbiAgICAgICkge1xuICAgICAgICAvLyBDbGljayBvbiBkaXNwbGF5ZWQgZWxlbWVudFxuICAgICAgICBpZiAob3B0aW9ucy50b2dnbGVDb250cm9sc09uU2xpZGVDbGljaykge1xuICAgICAgICAgIHRoaXMucHJldmVudERlZmF1bHQoZXZlbnQpXG4gICAgICAgICAgdGhpcy50b2dnbGVDb250cm9scygpXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgb25jbGljazogZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAoXG4gICAgICAgIHRoaXMub3B0aW9ucy5lbXVsYXRlVG91Y2hFdmVudHMgJiZcbiAgICAgICAgdGhpcy50b3VjaERlbHRhICYmXG4gICAgICAgIChNYXRoLmFicyh0aGlzLnRvdWNoRGVsdGEueCkgPiAyMCB8fCBNYXRoLmFicyh0aGlzLnRvdWNoRGVsdGEueSkgPiAyMClcbiAgICAgICkge1xuICAgICAgICBkZWxldGUgdGhpcy50b3VjaERlbHRhXG4gICAgICAgIHJldHVyblxuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXMuaGFuZGxlQ2xpY2soZXZlbnQpXG4gICAgfSxcblxuICAgIHVwZGF0ZUVkZ2VDbGFzc2VzOiBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgIGlmICghaW5kZXgpIHtcbiAgICAgICAgdGhpcy5jb250YWluZXIuYWRkQ2xhc3ModGhpcy5vcHRpb25zLmxlZnRFZGdlQ2xhc3MpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmNvbnRhaW5lci5yZW1vdmVDbGFzcyh0aGlzLm9wdGlvbnMubGVmdEVkZ2VDbGFzcylcbiAgICAgIH1cbiAgICAgIGlmIChpbmRleCA9PT0gdGhpcy5udW0gLSAxKSB7XG4gICAgICAgIHRoaXMuY29udGFpbmVyLmFkZENsYXNzKHRoaXMub3B0aW9ucy5yaWdodEVkZ2VDbGFzcylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuY29udGFpbmVyLnJlbW92ZUNsYXNzKHRoaXMub3B0aW9ucy5yaWdodEVkZ2VDbGFzcylcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgaGFuZGxlU2xpZGU6IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgaWYgKCF0aGlzLm9wdGlvbnMuY29udGludW91cykge1xuICAgICAgICB0aGlzLnVwZGF0ZUVkZ2VDbGFzc2VzKGluZGV4KVxuICAgICAgfVxuICAgICAgdGhpcy5sb2FkRWxlbWVudHMoaW5kZXgpXG4gICAgICBpZiAodGhpcy5vcHRpb25zLnVubG9hZEVsZW1lbnRzKSB7XG4gICAgICAgIHRoaXMudW5sb2FkRWxlbWVudHMoaW5kZXgpXG4gICAgICB9XG4gICAgICB0aGlzLnNldFRpdGxlKGluZGV4KVxuICAgIH0sXG5cbiAgICBvbnNsaWRlOiBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgIHRoaXMuaW5kZXggPSBpbmRleFxuICAgICAgdGhpcy5oYW5kbGVTbGlkZShpbmRleClcbiAgICAgIHRoaXMuc2V0VGltZW91dCh0aGlzLm9wdGlvbnMub25zbGlkZSwgW2luZGV4LCB0aGlzLnNsaWRlc1tpbmRleF1dKVxuICAgIH0sXG5cbiAgICBzZXRUaXRsZTogZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICB2YXIgZmlyc3RDaGlsZCA9IHRoaXMuc2xpZGVzW2luZGV4XS5maXJzdENoaWxkXG4gICAgICB2YXIgdGV4dCA9IGZpcnN0Q2hpbGQudGl0bGUgfHwgZmlyc3RDaGlsZC5hbHRcbiAgICAgIHZhciB0aXRsZUVsZW1lbnQgPSB0aGlzLnRpdGxlRWxlbWVudFxuICAgICAgaWYgKHRpdGxlRWxlbWVudC5sZW5ndGgpIHtcbiAgICAgICAgdGhpcy50aXRsZUVsZW1lbnQuZW1wdHkoKVxuICAgICAgICBpZiAodGV4dCkge1xuICAgICAgICAgIHRpdGxlRWxlbWVudFswXS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSh0ZXh0KSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBzZXRUaW1lb3V0OiBmdW5jdGlvbiAoZnVuYywgYXJncywgd2FpdCkge1xuICAgICAgdmFyIHRoYXQgPSB0aGlzXG4gICAgICByZXR1cm4gKFxuICAgICAgICBmdW5jICYmXG4gICAgICAgIHdpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBmdW5jLmFwcGx5KHRoYXQsIGFyZ3MgfHwgW10pXG4gICAgICAgIH0sIHdhaXQgfHwgMClcbiAgICAgIClcbiAgICB9LFxuXG4gICAgaW1hZ2VGYWN0b3J5OiBmdW5jdGlvbiAob2JqLCBjYWxsYmFjaykge1xuICAgICAgdmFyIHRoYXQgPSB0aGlzXG4gICAgICB2YXIgaW1nID0gdGhpcy5pbWFnZVByb3RvdHlwZS5jbG9uZU5vZGUoZmFsc2UpXG4gICAgICB2YXIgdXJsID0gb2JqXG4gICAgICB2YXIgYmFja2dyb3VuZFNpemUgPSB0aGlzLm9wdGlvbnMuc3RyZXRjaEltYWdlc1xuICAgICAgdmFyIGNhbGxlZFxuICAgICAgdmFyIGVsZW1lbnRcbiAgICAgIHZhciB0aXRsZVxuICAgICAgdmFyIGFsdFRleHRcbiAgICAgIGZ1bmN0aW9uIGNhbGxiYWNrV3JhcHBlciAoZXZlbnQpIHtcbiAgICAgICAgaWYgKCFjYWxsZWQpIHtcbiAgICAgICAgICBldmVudCA9IHtcbiAgICAgICAgICAgIHR5cGU6IGV2ZW50LnR5cGUsXG4gICAgICAgICAgICB0YXJnZXQ6IGVsZW1lbnRcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCFlbGVtZW50LnBhcmVudE5vZGUpIHtcbiAgICAgICAgICAgIC8vIEZpeCBmb3IgSUU3IGZpcmluZyB0aGUgbG9hZCBldmVudCBmb3JcbiAgICAgICAgICAgIC8vIGNhY2hlZCBpbWFnZXMgYmVmb3JlIHRoZSBlbGVtZW50IGNvdWxkXG4gICAgICAgICAgICAvLyBiZSBhZGRlZCB0byB0aGUgRE9NOlxuICAgICAgICAgICAgcmV0dXJuIHRoYXQuc2V0VGltZW91dChjYWxsYmFja1dyYXBwZXIsIFtldmVudF0pXG4gICAgICAgICAgfVxuICAgICAgICAgIGNhbGxlZCA9IHRydWVcbiAgICAgICAgICAkKGltZykub2ZmKCdsb2FkIGVycm9yJywgY2FsbGJhY2tXcmFwcGVyKVxuICAgICAgICAgIGlmIChiYWNrZ3JvdW5kU2l6ZSkge1xuICAgICAgICAgICAgaWYgKGV2ZW50LnR5cGUgPT09ICdsb2FkJykge1xuICAgICAgICAgICAgICBlbGVtZW50LnN0eWxlLmJhY2tncm91bmQgPSAndXJsKFwiJyArIHVybCArICdcIikgY2VudGVyIG5vLXJlcGVhdCdcbiAgICAgICAgICAgICAgZWxlbWVudC5zdHlsZS5iYWNrZ3JvdW5kU2l6ZSA9IGJhY2tncm91bmRTaXplXG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGNhbGxiYWNrKGV2ZW50KVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBpZiAodHlwZW9mIHVybCAhPT0gJ3N0cmluZycpIHtcbiAgICAgICAgdXJsID0gdGhpcy5nZXRJdGVtUHJvcGVydHkob2JqLCB0aGlzLm9wdGlvbnMudXJsUHJvcGVydHkpXG4gICAgICAgIHRpdGxlID0gdGhpcy5nZXRJdGVtUHJvcGVydHkob2JqLCB0aGlzLm9wdGlvbnMudGl0bGVQcm9wZXJ0eSlcbiAgICAgICAgYWx0VGV4dCA9XG4gICAgICAgICAgdGhpcy5nZXRJdGVtUHJvcGVydHkob2JqLCB0aGlzLm9wdGlvbnMuYWx0VGV4dFByb3BlcnR5KSB8fCB0aXRsZVxuICAgICAgfVxuICAgICAgaWYgKGJhY2tncm91bmRTaXplID09PSB0cnVlKSB7XG4gICAgICAgIGJhY2tncm91bmRTaXplID0gJ2NvbnRhaW4nXG4gICAgICB9XG4gICAgICBiYWNrZ3JvdW5kU2l6ZSA9XG4gICAgICAgIHRoaXMuc3VwcG9ydC5iYWNrZ3JvdW5kU2l6ZSAmJlxuICAgICAgICB0aGlzLnN1cHBvcnQuYmFja2dyb3VuZFNpemVbYmFja2dyb3VuZFNpemVdICYmXG4gICAgICAgIGJhY2tncm91bmRTaXplXG4gICAgICBpZiAoYmFja2dyb3VuZFNpemUpIHtcbiAgICAgICAgZWxlbWVudCA9IHRoaXMuZWxlbWVudFByb3RvdHlwZS5jbG9uZU5vZGUoZmFsc2UpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICBlbGVtZW50ID0gaW1nXG4gICAgICAgIGltZy5kcmFnZ2FibGUgPSBmYWxzZVxuICAgICAgfVxuICAgICAgaWYgKHRpdGxlKSB7XG4gICAgICAgIGVsZW1lbnQudGl0bGUgPSB0aXRsZVxuICAgICAgfVxuICAgICAgaWYgKGFsdFRleHQpIHtcbiAgICAgICAgZWxlbWVudC5hbHQgPSBhbHRUZXh0XG4gICAgICB9XG4gICAgICAkKGltZykub24oJ2xvYWQgZXJyb3InLCBjYWxsYmFja1dyYXBwZXIpXG4gICAgICBpbWcuc3JjID0gdXJsXG4gICAgICByZXR1cm4gZWxlbWVudFxuICAgIH0sXG5cbiAgICBjcmVhdGVFbGVtZW50OiBmdW5jdGlvbiAob2JqLCBjYWxsYmFjaykge1xuICAgICAgdmFyIHR5cGUgPSBvYmogJiYgdGhpcy5nZXRJdGVtUHJvcGVydHkob2JqLCB0aGlzLm9wdGlvbnMudHlwZVByb3BlcnR5KVxuICAgICAgdmFyIGZhY3RvcnkgPVxuICAgICAgICAodHlwZSAmJiB0aGlzW3R5cGUuc3BsaXQoJy8nKVswXSArICdGYWN0b3J5J10pIHx8IHRoaXMuaW1hZ2VGYWN0b3J5XG4gICAgICB2YXIgZWxlbWVudCA9IG9iaiAmJiBmYWN0b3J5LmNhbGwodGhpcywgb2JqLCBjYWxsYmFjaylcbiAgICAgIHZhciBzcmNzZXQgPSB0aGlzLmdldEl0ZW1Qcm9wZXJ0eShvYmosIHRoaXMub3B0aW9ucy5zcmNzZXRQcm9wZXJ0eSlcbiAgICAgIGlmICghZWxlbWVudCkge1xuICAgICAgICBlbGVtZW50ID0gdGhpcy5lbGVtZW50UHJvdG90eXBlLmNsb25lTm9kZShmYWxzZSlcbiAgICAgICAgdGhpcy5zZXRUaW1lb3V0KGNhbGxiYWNrLCBbXG4gICAgICAgICAge1xuICAgICAgICAgICAgdHlwZTogJ2Vycm9yJyxcbiAgICAgICAgICAgIHRhcmdldDogZWxlbWVudFxuICAgICAgICAgIH1cbiAgICAgICAgXSlcbiAgICAgIH1cbiAgICAgIGlmIChzcmNzZXQpIHtcbiAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ3NyY3NldCcsIHNyY3NldClcbiAgICAgIH1cbiAgICAgICQoZWxlbWVudCkuYWRkQ2xhc3ModGhpcy5vcHRpb25zLnNsaWRlQ29udGVudENsYXNzKVxuICAgICAgcmV0dXJuIGVsZW1lbnRcbiAgICB9LFxuXG4gICAgbG9hZEVsZW1lbnQ6IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgaWYgKCF0aGlzLmVsZW1lbnRzW2luZGV4XSkge1xuICAgICAgICBpZiAodGhpcy5zbGlkZXNbaW5kZXhdLmZpcnN0Q2hpbGQpIHtcbiAgICAgICAgICB0aGlzLmVsZW1lbnRzW2luZGV4XSA9ICQodGhpcy5zbGlkZXNbaW5kZXhdKS5oYXNDbGFzcyhcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5zbGlkZUVycm9yQ2xhc3NcbiAgICAgICAgICApXG4gICAgICAgICAgICA/IDNcbiAgICAgICAgICAgIDogMlxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHRoaXMuZWxlbWVudHNbaW5kZXhdID0gMSAvLyBMb2FkaW5nXG4gICAgICAgICAgJCh0aGlzLnNsaWRlc1tpbmRleF0pLmFkZENsYXNzKHRoaXMub3B0aW9ucy5zbGlkZUxvYWRpbmdDbGFzcylcbiAgICAgICAgICB0aGlzLnNsaWRlc1tpbmRleF0uYXBwZW5kQ2hpbGQoXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUVsZW1lbnQodGhpcy5saXN0W2luZGV4XSwgdGhpcy5wcm94eUxpc3RlbmVyKVxuICAgICAgICAgIClcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBsb2FkRWxlbWVudHM6IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgdmFyIGxpbWl0ID0gTWF0aC5taW4odGhpcy5udW0sIHRoaXMub3B0aW9ucy5wcmVsb2FkUmFuZ2UgKiAyICsgMSlcbiAgICAgIHZhciBqID0gaW5kZXhcbiAgICAgIHZhciBpXG4gICAgICBmb3IgKGkgPSAwOyBpIDwgbGltaXQ7IGkgKz0gMSkge1xuICAgICAgICAvLyBGaXJzdCBsb2FkIHRoZSBjdXJyZW50IHNsaWRlIGVsZW1lbnQgKDApLFxuICAgICAgICAvLyB0aGVuIHRoZSBuZXh0IG9uZSAoKzEpLFxuICAgICAgICAvLyB0aGVuIHRoZSBwcmV2aW91cyBvbmUgKC0yKSxcbiAgICAgICAgLy8gdGhlbiB0aGUgbmV4dCBhZnRlciBuZXh0ICgrMiksIGV0Yy46XG4gICAgICAgIGogKz0gaSAqIChpICUgMiA9PT0gMCA/IC0xIDogMSlcbiAgICAgICAgLy8gQ29ubmVjdCB0aGUgZW5kcyBvZiB0aGUgbGlzdCB0byBsb2FkIHNsaWRlIGVsZW1lbnRzIGZvclxuICAgICAgICAvLyBjb250aW51b3VzIG5hdmlnYXRpb246XG4gICAgICAgIGogPSB0aGlzLmNpcmNsZShqKVxuICAgICAgICB0aGlzLmxvYWRFbGVtZW50KGopXG4gICAgICB9XG4gICAgfSxcblxuICAgIHVubG9hZEVsZW1lbnRzOiBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgIHZhciBpLCBkaWZmXG4gICAgICBmb3IgKGkgaW4gdGhpcy5lbGVtZW50cykge1xuICAgICAgICBpZiAodGhpcy5lbGVtZW50cy5oYXNPd25Qcm9wZXJ0eShpKSkge1xuICAgICAgICAgIGRpZmYgPSBNYXRoLmFicyhpbmRleCAtIGkpXG4gICAgICAgICAgaWYgKFxuICAgICAgICAgICAgZGlmZiA+IHRoaXMub3B0aW9ucy5wcmVsb2FkUmFuZ2UgJiZcbiAgICAgICAgICAgIGRpZmYgKyB0aGlzLm9wdGlvbnMucHJlbG9hZFJhbmdlIDwgdGhpcy5udW1cbiAgICAgICAgICApIHtcbiAgICAgICAgICAgIHRoaXMudW5sb2FkU2xpZGUoaSlcbiAgICAgICAgICAgIGRlbGV0ZSB0aGlzLmVsZW1lbnRzW2ldXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcblxuICAgIGFkZFNsaWRlOiBmdW5jdGlvbiAoaW5kZXgpIHtcbiAgICAgIHZhciBzbGlkZSA9IHRoaXMuc2xpZGVQcm90b3R5cGUuY2xvbmVOb2RlKGZhbHNlKVxuICAgICAgc2xpZGUuc2V0QXR0cmlidXRlKCdkYXRhLWluZGV4JywgaW5kZXgpXG4gICAgICB0aGlzLnNsaWRlc0NvbnRhaW5lclswXS5hcHBlbmRDaGlsZChzbGlkZSlcbiAgICAgIHRoaXMuc2xpZGVzLnB1c2goc2xpZGUpXG4gICAgfSxcblxuICAgIHBvc2l0aW9uU2xpZGU6IGZ1bmN0aW9uIChpbmRleCkge1xuICAgICAgdmFyIHNsaWRlID0gdGhpcy5zbGlkZXNbaW5kZXhdXG4gICAgICBzbGlkZS5zdHlsZS53aWR0aCA9IHRoaXMuc2xpZGVXaWR0aCArICdweCdcbiAgICAgIGlmICh0aGlzLnN1cHBvcnQudHJhbnNmb3JtKSB7XG4gICAgICAgIHNsaWRlLnN0eWxlLmxlZnQgPSBpbmRleCAqIC10aGlzLnNsaWRlV2lkdGggKyAncHgnXG4gICAgICAgIHRoaXMubW92ZShcbiAgICAgICAgICBpbmRleCxcbiAgICAgICAgICB0aGlzLmluZGV4ID4gaW5kZXhcbiAgICAgICAgICAgID8gLXRoaXMuc2xpZGVXaWR0aFxuICAgICAgICAgICAgOiB0aGlzLmluZGV4IDwgaW5kZXggPyB0aGlzLnNsaWRlV2lkdGggOiAwLFxuICAgICAgICAgIDBcbiAgICAgICAgKVxuICAgICAgfVxuICAgIH0sXG5cbiAgICBpbml0U2xpZGVzOiBmdW5jdGlvbiAocmVsb2FkKSB7XG4gICAgICB2YXIgY2xlYXJTbGlkZXMsIGlcbiAgICAgIGlmICghcmVsb2FkKSB7XG4gICAgICAgIHRoaXMucG9zaXRpb25zID0gW11cbiAgICAgICAgdGhpcy5wb3NpdGlvbnMubGVuZ3RoID0gdGhpcy5udW1cbiAgICAgICAgdGhpcy5lbGVtZW50cyA9IHt9XG4gICAgICAgIHRoaXMuaW1hZ2VQcm90b3R5cGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKVxuICAgICAgICB0aGlzLmVsZW1lbnRQcm90b3R5cGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgICAgICB0aGlzLnNsaWRlUHJvdG90eXBlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2JylcbiAgICAgICAgJCh0aGlzLnNsaWRlUHJvdG90eXBlKS5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuc2xpZGVDbGFzcylcbiAgICAgICAgdGhpcy5zbGlkZXMgPSB0aGlzLnNsaWRlc0NvbnRhaW5lclswXS5jaGlsZHJlblxuICAgICAgICBjbGVhclNsaWRlcyA9XG4gICAgICAgICAgdGhpcy5vcHRpb25zLmNsZWFyU2xpZGVzIHx8IHRoaXMuc2xpZGVzLmxlbmd0aCAhPT0gdGhpcy5udW1cbiAgICAgIH1cbiAgICAgIHRoaXMuc2xpZGVXaWR0aCA9IHRoaXMuY29udGFpbmVyWzBdLmNsaWVudFdpZHRoXG4gICAgICB0aGlzLnNsaWRlSGVpZ2h0ID0gdGhpcy5jb250YWluZXJbMF0uY2xpZW50SGVpZ2h0XG4gICAgICB0aGlzLnNsaWRlc0NvbnRhaW5lclswXS5zdHlsZS53aWR0aCA9IHRoaXMubnVtICogdGhpcy5zbGlkZVdpZHRoICsgJ3B4J1xuICAgICAgaWYgKGNsZWFyU2xpZGVzKSB7XG4gICAgICAgIHRoaXMucmVzZXRTbGlkZXMoKVxuICAgICAgfVxuICAgICAgZm9yIChpID0gMDsgaSA8IHRoaXMubnVtOyBpICs9IDEpIHtcbiAgICAgICAgaWYgKGNsZWFyU2xpZGVzKSB7XG4gICAgICAgICAgdGhpcy5hZGRTbGlkZShpKVxuICAgICAgICB9XG4gICAgICAgIHRoaXMucG9zaXRpb25TbGlkZShpKVxuICAgICAgfVxuICAgICAgLy8gUmVwb3NpdGlvbiB0aGUgc2xpZGVzIGJlZm9yZSBhbmQgYWZ0ZXIgdGhlIGdpdmVuIGluZGV4OlxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5jb250aW51b3VzICYmIHRoaXMuc3VwcG9ydC50cmFuc2Zvcm0pIHtcbiAgICAgICAgdGhpcy5tb3ZlKHRoaXMuY2lyY2xlKHRoaXMuaW5kZXggLSAxKSwgLXRoaXMuc2xpZGVXaWR0aCwgMClcbiAgICAgICAgdGhpcy5tb3ZlKHRoaXMuY2lyY2xlKHRoaXMuaW5kZXggKyAxKSwgdGhpcy5zbGlkZVdpZHRoLCAwKVxuICAgICAgfVxuICAgICAgaWYgKCF0aGlzLnN1cHBvcnQudHJhbnNmb3JtKSB7XG4gICAgICAgIHRoaXMuc2xpZGVzQ29udGFpbmVyWzBdLnN0eWxlLmxlZnQgPVxuICAgICAgICAgIHRoaXMuaW5kZXggKiAtdGhpcy5zbGlkZVdpZHRoICsgJ3B4J1xuICAgICAgfVxuICAgIH0sXG5cbiAgICB1bmxvYWRTbGlkZTogZnVuY3Rpb24gKGluZGV4KSB7XG4gICAgICB2YXIgc2xpZGUsIGZpcnN0Q2hpbGRcbiAgICAgIHNsaWRlID0gdGhpcy5zbGlkZXNbaW5kZXhdXG4gICAgICBmaXJzdENoaWxkID0gc2xpZGUuZmlyc3RDaGlsZFxuICAgICAgaWYgKGZpcnN0Q2hpbGQgIT09IG51bGwpIHtcbiAgICAgICAgc2xpZGUucmVtb3ZlQ2hpbGQoZmlyc3RDaGlsZClcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgdW5sb2FkQWxsU2xpZGVzOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgaSwgbGVuXG4gICAgICBmb3IgKGkgPSAwLCBsZW4gPSB0aGlzLnNsaWRlcy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICB0aGlzLnVubG9hZFNsaWRlKGkpXG4gICAgICB9XG4gICAgfSxcblxuICAgIHRvZ2dsZUNvbnRyb2xzOiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgY29udHJvbHNDbGFzcyA9IHRoaXMub3B0aW9ucy5jb250cm9sc0NsYXNzXG4gICAgICBpZiAodGhpcy5jb250YWluZXIuaGFzQ2xhc3MoY29udHJvbHNDbGFzcykpIHtcbiAgICAgICAgdGhpcy5jb250YWluZXIucmVtb3ZlQ2xhc3MoY29udHJvbHNDbGFzcylcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuY29udGFpbmVyLmFkZENsYXNzKGNvbnRyb2xzQ2xhc3MpXG4gICAgICB9XG4gICAgfSxcblxuICAgIHRvZ2dsZVNsaWRlc2hvdzogZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKCF0aGlzLmludGVydmFsKSB7XG4gICAgICAgIHRoaXMucGxheSgpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnBhdXNlKClcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgZ2V0Tm9kZUluZGV4OiBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgcmV0dXJuIHBhcnNlSW50KGVsZW1lbnQuZ2V0QXR0cmlidXRlKCdkYXRhLWluZGV4JyksIDEwKVxuICAgIH0sXG5cbiAgICBnZXROZXN0ZWRQcm9wZXJ0eTogZnVuY3Rpb24gKG9iaiwgcHJvcGVydHkpIHtcbiAgICAgIHByb3BlcnR5LnJlcGxhY2UoXG4gICAgICAgIC8vIE1hdGNoZXMgbmF0aXZlIEphdmFTY3JpcHQgbm90YXRpb24gaW4gYSBTdHJpbmcsXG4gICAgICAgIC8vIGUuZy4gJ1tcImRvdWJsZVF1b3RlUHJvcFwiXS5kb3RQcm9wWzJdJ1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdXNlbGVzcy1lc2NhcGVcbiAgICAgICAgL1xcWyg/OicoW14nXSspJ3xcIihbXlwiXSspXCJ8KFxcZCspKVxcXXwoPzooPzpefFxcLikoW15cXC5cXFtdKykpL2csXG4gICAgICAgIGZ1bmN0aW9uIChzdHIsIHNpbmdsZVF1b3RlUHJvcCwgZG91YmxlUXVvdGVQcm9wLCBhcnJheUluZGV4LCBkb3RQcm9wKSB7XG4gICAgICAgICAgdmFyIHByb3AgPVxuICAgICAgICAgICAgZG90UHJvcCB8fFxuICAgICAgICAgICAgc2luZ2xlUXVvdGVQcm9wIHx8XG4gICAgICAgICAgICBkb3VibGVRdW90ZVByb3AgfHxcbiAgICAgICAgICAgIChhcnJheUluZGV4ICYmIHBhcnNlSW50KGFycmF5SW5kZXgsIDEwKSlcbiAgICAgICAgICBpZiAoc3RyICYmIG9iaikge1xuICAgICAgICAgICAgb2JqID0gb2JqW3Byb3BdXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICApXG4gICAgICByZXR1cm4gb2JqXG4gICAgfSxcblxuICAgIGdldERhdGFQcm9wZXJ0eTogZnVuY3Rpb24gKG9iaiwgcHJvcGVydHkpIHtcbiAgICAgIHZhciBrZXlcbiAgICAgIHZhciBwcm9wXG4gICAgICBpZiAob2JqLmRhdGFzZXQpIHtcbiAgICAgICAga2V5ID0gcHJvcGVydHkucmVwbGFjZSgvLShbYS16XSkvZywgZnVuY3Rpb24gKF8sIGIpIHtcbiAgICAgICAgICByZXR1cm4gYi50b1VwcGVyQ2FzZSgpXG4gICAgICAgIH0pXG4gICAgICAgIHByb3AgPSBvYmouZGF0YXNldFtrZXldXG4gICAgICB9IGVsc2UgaWYgKG9iai5nZXRBdHRyaWJ1dGUpIHtcbiAgICAgICAgcHJvcCA9IG9iai5nZXRBdHRyaWJ1dGUoXG4gICAgICAgICAgJ2RhdGEtJyArIHByb3BlcnR5LnJlcGxhY2UoLyhbQS1aXSkvZywgJy0kMScpLnRvTG93ZXJDYXNlKClcbiAgICAgICAgKVxuICAgICAgfVxuICAgICAgaWYgKHR5cGVvZiBwcm9wID09PSAnc3RyaW5nJykge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdXNlbGVzcy1lc2NhcGVcbiAgICAgICAgaWYgKFxuICAgICAgICAgIC9eKHRydWV8ZmFsc2V8bnVsbHwtP1xcZCsoXFwuXFxkKyk/fFxce1tcXHNcXFNdKlxcfXxcXFtbXFxzXFxTXSpcXF0pJC8udGVzdChwcm9wKVxuICAgICAgICApIHtcbiAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgcmV0dXJuICQucGFyc2VKU09OKHByb3ApXG4gICAgICAgICAgfSBjYXRjaCAoaWdub3JlKSB7fVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBwcm9wXG4gICAgICB9XG4gICAgfSxcblxuICAgIGdldEl0ZW1Qcm9wZXJ0eTogZnVuY3Rpb24gKG9iaiwgcHJvcGVydHkpIHtcbiAgICAgIHZhciBwcm9wID0gdGhpcy5nZXREYXRhUHJvcGVydHkob2JqLCBwcm9wZXJ0eSlcbiAgICAgIGlmIChwcm9wID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgcHJvcCA9IG9ialtwcm9wZXJ0eV1cbiAgICAgIH1cbiAgICAgIGlmIChwcm9wID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgcHJvcCA9IHRoaXMuZ2V0TmVzdGVkUHJvcGVydHkob2JqLCBwcm9wZXJ0eSlcbiAgICAgIH1cbiAgICAgIHJldHVybiBwcm9wXG4gICAgfSxcblxuICAgIGluaXRTdGFydEluZGV4OiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgaW5kZXggPSB0aGlzLm9wdGlvbnMuaW5kZXhcbiAgICAgIHZhciB1cmxQcm9wZXJ0eSA9IHRoaXMub3B0aW9ucy51cmxQcm9wZXJ0eVxuICAgICAgdmFyIGlcbiAgICAgIC8vIENoZWNrIGlmIHRoZSBpbmRleCBpcyBnaXZlbiBhcyBhIGxpc3Qgb2JqZWN0OlxuICAgICAgaWYgKGluZGV4ICYmIHR5cGVvZiBpbmRleCAhPT0gJ251bWJlcicpIHtcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IHRoaXMubnVtOyBpICs9IDEpIHtcbiAgICAgICAgICBpZiAoXG4gICAgICAgICAgICB0aGlzLmxpc3RbaV0gPT09IGluZGV4IHx8XG4gICAgICAgICAgICB0aGlzLmdldEl0ZW1Qcm9wZXJ0eSh0aGlzLmxpc3RbaV0sIHVybFByb3BlcnR5KSA9PT1cbiAgICAgICAgICAgICAgdGhpcy5nZXRJdGVtUHJvcGVydHkoaW5kZXgsIHVybFByb3BlcnR5KVxuICAgICAgICAgICkge1xuICAgICAgICAgICAgaW5kZXggPSBpXG4gICAgICAgICAgICBicmVha1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgLy8gTWFrZSBzdXJlIHRoZSBpbmRleCBpcyBpbiB0aGUgbGlzdCByYW5nZTpcbiAgICAgIHRoaXMuaW5kZXggPSB0aGlzLmNpcmNsZShwYXJzZUludChpbmRleCwgMTApIHx8IDApXG4gICAgfSxcblxuICAgIGluaXRFdmVudExpc3RlbmVyczogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHRoYXQgPSB0aGlzXG4gICAgICB2YXIgc2xpZGVzQ29udGFpbmVyID0gdGhpcy5zbGlkZXNDb250YWluZXJcbiAgICAgIGZ1bmN0aW9uIHByb3h5TGlzdGVuZXIgKGV2ZW50KSB7XG4gICAgICAgIHZhciB0eXBlID1cbiAgICAgICAgICB0aGF0LnN1cHBvcnQudHJhbnNpdGlvbiAmJiB0aGF0LnN1cHBvcnQudHJhbnNpdGlvbi5lbmQgPT09IGV2ZW50LnR5cGVcbiAgICAgICAgICAgID8gJ3RyYW5zaXRpb25lbmQnXG4gICAgICAgICAgICA6IGV2ZW50LnR5cGVcbiAgICAgICAgdGhhdFsnb24nICsgdHlwZV0oZXZlbnQpXG4gICAgICB9XG4gICAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIHByb3h5TGlzdGVuZXIpXG4gICAgICAkKGRvY3VtZW50LmJvZHkpLm9uKCdrZXlkb3duJywgcHJveHlMaXN0ZW5lcilcbiAgICAgIHRoaXMuY29udGFpbmVyLm9uKCdjbGljaycsIHByb3h5TGlzdGVuZXIpXG4gICAgICBpZiAodGhpcy5zdXBwb3J0LnRvdWNoKSB7XG4gICAgICAgIHNsaWRlc0NvbnRhaW5lci5vbihcbiAgICAgICAgICAndG91Y2hzdGFydCB0b3VjaG1vdmUgdG91Y2hlbmQgdG91Y2hjYW5jZWwnLFxuICAgICAgICAgIHByb3h5TGlzdGVuZXJcbiAgICAgICAgKVxuICAgICAgfSBlbHNlIGlmICh0aGlzLm9wdGlvbnMuZW11bGF0ZVRvdWNoRXZlbnRzICYmIHRoaXMuc3VwcG9ydC50cmFuc2l0aW9uKSB7XG4gICAgICAgIHNsaWRlc0NvbnRhaW5lci5vbihcbiAgICAgICAgICAnbW91c2Vkb3duIG1vdXNlbW92ZSBtb3VzZXVwIG1vdXNlb3V0JyxcbiAgICAgICAgICBwcm94eUxpc3RlbmVyXG4gICAgICAgIClcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLnN1cHBvcnQudHJhbnNpdGlvbikge1xuICAgICAgICBzbGlkZXNDb250YWluZXIub24odGhpcy5zdXBwb3J0LnRyYW5zaXRpb24uZW5kLCBwcm94eUxpc3RlbmVyKVxuICAgICAgfVxuICAgICAgdGhpcy5wcm94eUxpc3RlbmVyID0gcHJveHlMaXN0ZW5lclxuICAgIH0sXG5cbiAgICBkZXN0cm95RXZlbnRMaXN0ZW5lcnM6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBzbGlkZXNDb250YWluZXIgPSB0aGlzLnNsaWRlc0NvbnRhaW5lclxuICAgICAgdmFyIHByb3h5TGlzdGVuZXIgPSB0aGlzLnByb3h5TGlzdGVuZXJcbiAgICAgICQod2luZG93KS5vZmYoJ3Jlc2l6ZScsIHByb3h5TGlzdGVuZXIpXG4gICAgICAkKGRvY3VtZW50LmJvZHkpLm9mZigna2V5ZG93bicsIHByb3h5TGlzdGVuZXIpXG4gICAgICB0aGlzLmNvbnRhaW5lci5vZmYoJ2NsaWNrJywgcHJveHlMaXN0ZW5lcilcbiAgICAgIGlmICh0aGlzLnN1cHBvcnQudG91Y2gpIHtcbiAgICAgICAgc2xpZGVzQ29udGFpbmVyLm9mZihcbiAgICAgICAgICAndG91Y2hzdGFydCB0b3VjaG1vdmUgdG91Y2hlbmQgdG91Y2hjYW5jZWwnLFxuICAgICAgICAgIHByb3h5TGlzdGVuZXJcbiAgICAgICAgKVxuICAgICAgfSBlbHNlIGlmICh0aGlzLm9wdGlvbnMuZW11bGF0ZVRvdWNoRXZlbnRzICYmIHRoaXMuc3VwcG9ydC50cmFuc2l0aW9uKSB7XG4gICAgICAgIHNsaWRlc0NvbnRhaW5lci5vZmYoXG4gICAgICAgICAgJ21vdXNlZG93biBtb3VzZW1vdmUgbW91c2V1cCBtb3VzZW91dCcsXG4gICAgICAgICAgcHJveHlMaXN0ZW5lclxuICAgICAgICApXG4gICAgICB9XG4gICAgICBpZiAodGhpcy5zdXBwb3J0LnRyYW5zaXRpb24pIHtcbiAgICAgICAgc2xpZGVzQ29udGFpbmVyLm9mZih0aGlzLnN1cHBvcnQudHJhbnNpdGlvbi5lbmQsIHByb3h5TGlzdGVuZXIpXG4gICAgICB9XG4gICAgfSxcblxuICAgIGhhbmRsZU9wZW46IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMub25vcGVuZWQpIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLm9ub3BlbmVkLmNhbGwodGhpcylcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgaW5pdFdpZGdldDogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHRoYXQgPSB0aGlzXG4gICAgICBmdW5jdGlvbiBvcGVuSGFuZGxlciAoZXZlbnQpIHtcbiAgICAgICAgaWYgKGV2ZW50LnRhcmdldCA9PT0gdGhhdC5jb250YWluZXJbMF0pIHtcbiAgICAgICAgICB0aGF0LmNvbnRhaW5lci5vZmYodGhhdC5zdXBwb3J0LnRyYW5zaXRpb24uZW5kLCBvcGVuSGFuZGxlcilcbiAgICAgICAgICB0aGF0LmhhbmRsZU9wZW4oKVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICB0aGlzLmNvbnRhaW5lciA9ICQodGhpcy5vcHRpb25zLmNvbnRhaW5lcilcbiAgICAgIGlmICghdGhpcy5jb250YWluZXIubGVuZ3RoKSB7XG4gICAgICAgIHRoaXMuY29uc29sZS5sb2coXG4gICAgICAgICAgJ2JsdWVpbXAgR2FsbGVyeTogV2lkZ2V0IGNvbnRhaW5lciBub3QgZm91bmQuJyxcbiAgICAgICAgICB0aGlzLm9wdGlvbnMuY29udGFpbmVyXG4gICAgICAgIClcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICB9XG4gICAgICB0aGlzLnNsaWRlc0NvbnRhaW5lciA9IHRoaXMuY29udGFpbmVyXG4gICAgICAgIC5maW5kKHRoaXMub3B0aW9ucy5zbGlkZXNDb250YWluZXIpXG4gICAgICAgIC5maXJzdCgpXG4gICAgICBpZiAoIXRoaXMuc2xpZGVzQ29udGFpbmVyLmxlbmd0aCkge1xuICAgICAgICB0aGlzLmNvbnNvbGUubG9nKFxuICAgICAgICAgICdibHVlaW1wIEdhbGxlcnk6IFNsaWRlcyBjb250YWluZXIgbm90IGZvdW5kLicsXG4gICAgICAgICAgdGhpcy5vcHRpb25zLnNsaWRlc0NvbnRhaW5lclxuICAgICAgICApXG4gICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgfVxuICAgICAgdGhpcy50aXRsZUVsZW1lbnQgPSB0aGlzLmNvbnRhaW5lci5maW5kKHRoaXMub3B0aW9ucy50aXRsZUVsZW1lbnQpLmZpcnN0KClcbiAgICAgIGlmICh0aGlzLm51bSA9PT0gMSkge1xuICAgICAgICB0aGlzLmNvbnRhaW5lci5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuc2luZ2xlQ2xhc3MpXG4gICAgICB9XG4gICAgICBpZiAodGhpcy5vcHRpb25zLm9ub3Blbikge1xuICAgICAgICB0aGlzLm9wdGlvbnMub25vcGVuLmNhbGwodGhpcylcbiAgICAgIH1cbiAgICAgIGlmICh0aGlzLnN1cHBvcnQudHJhbnNpdGlvbiAmJiB0aGlzLm9wdGlvbnMuZGlzcGxheVRyYW5zaXRpb24pIHtcbiAgICAgICAgdGhpcy5jb250YWluZXIub24odGhpcy5zdXBwb3J0LnRyYW5zaXRpb24uZW5kLCBvcGVuSGFuZGxlcilcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuaGFuZGxlT3BlbigpXG4gICAgICB9XG4gICAgICBpZiAodGhpcy5vcHRpb25zLmhpZGVQYWdlU2Nyb2xsYmFycykge1xuICAgICAgICAvLyBIaWRlIHRoZSBwYWdlIHNjcm9sbGJhcnM6XG4gICAgICAgIHRoaXMuYm9keU92ZXJmbG93U3R5bGUgPSBkb2N1bWVudC5ib2R5LnN0eWxlLm92ZXJmbG93XG4gICAgICAgIGRvY3VtZW50LmJvZHkuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJ1xuICAgICAgfVxuICAgICAgdGhpcy5jb250YWluZXJbMF0uc3R5bGUuZGlzcGxheSA9ICdibG9jaydcbiAgICAgIHRoaXMuaW5pdFNsaWRlcygpXG4gICAgICB0aGlzLmNvbnRhaW5lci5hZGRDbGFzcyh0aGlzLm9wdGlvbnMuZGlzcGxheUNsYXNzKVxuICAgIH0sXG5cbiAgICBpbml0T3B0aW9uczogZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgIC8vIENyZWF0ZSBhIGNvcHkgb2YgdGhlIHByb3RvdHlwZSBvcHRpb25zOlxuICAgICAgdGhpcy5vcHRpb25zID0gJC5leHRlbmQoe30sIHRoaXMub3B0aW9ucylcbiAgICAgIC8vIENoZWNrIGlmIGNhcm91c2VsIG1vZGUgaXMgZW5hYmxlZDpcbiAgICAgIGlmIChcbiAgICAgICAgKG9wdGlvbnMgJiYgb3B0aW9ucy5jYXJvdXNlbCkgfHxcbiAgICAgICAgKHRoaXMub3B0aW9ucy5jYXJvdXNlbCAmJiAoIW9wdGlvbnMgfHwgb3B0aW9ucy5jYXJvdXNlbCAhPT0gZmFsc2UpKVxuICAgICAgKSB7XG4gICAgICAgICQuZXh0ZW5kKHRoaXMub3B0aW9ucywgdGhpcy5jYXJvdXNlbE9wdGlvbnMpXG4gICAgICB9XG4gICAgICAvLyBPdmVycmlkZSBhbnkgZ2l2ZW4gb3B0aW9uczpcbiAgICAgICQuZXh0ZW5kKHRoaXMub3B0aW9ucywgb3B0aW9ucylcbiAgICAgIGlmICh0aGlzLm51bSA8IDMpIHtcbiAgICAgICAgLy8gMSBvciAyIHNsaWRlcyBjYW5ub3QgYmUgZGlzcGxheWVkIGNvbnRpbnVvdXMsXG4gICAgICAgIC8vIHJlbWVtYmVyIHRoZSBvcmlnaW5hbCBvcHRpb24gYnkgc2V0dGluZyB0byBudWxsIGluc3RlYWQgb2YgZmFsc2U6XG4gICAgICAgIHRoaXMub3B0aW9ucy5jb250aW51b3VzID0gdGhpcy5vcHRpb25zLmNvbnRpbnVvdXMgPyBudWxsIDogZmFsc2VcbiAgICAgIH1cbiAgICAgIGlmICghdGhpcy5zdXBwb3J0LnRyYW5zaXRpb24pIHtcbiAgICAgICAgdGhpcy5vcHRpb25zLmVtdWxhdGVUb3VjaEV2ZW50cyA9IGZhbHNlXG4gICAgICB9XG4gICAgICBpZiAodGhpcy5vcHRpb25zLmV2ZW50KSB7XG4gICAgICAgIHRoaXMucHJldmVudERlZmF1bHQodGhpcy5vcHRpb25zLmV2ZW50KVxuICAgICAgfVxuICAgIH1cbiAgfSlcblxuICByZXR1cm4gR2FsbGVyeVxufSlcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2JsdWVpbXAtZ2FsbGVyeS9qcy9ibHVlaW1wLWdhbGxlcnkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2JsdWVpbXAtZ2FsbGVyeS9qcy9ibHVlaW1wLWdhbGxlcnkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiLypcbiAqIGJsdWVpbXAgaGVscGVyIEpTXG4gKiBodHRwczovL2dpdGh1Yi5jb20vYmx1ZWltcC9HYWxsZXJ5XG4gKlxuICogQ29weXJpZ2h0IDIwMTMsIFNlYmFzdGlhbiBUc2NoYW5cbiAqIGh0dHBzOi8vYmx1ZWltcC5uZXRcbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2U6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICovXG5cbi8qIGdsb2JhbCBkZWZpbmUsIHdpbmRvdywgZG9jdW1lbnQgKi9cblxuOyhmdW5jdGlvbiAoKSB7XG4gICd1c2Ugc3RyaWN0J1xuXG4gIGZ1bmN0aW9uIGV4dGVuZCAob2JqMSwgb2JqMikge1xuICAgIHZhciBwcm9wXG4gICAgZm9yIChwcm9wIGluIG9iajIpIHtcbiAgICAgIGlmIChvYmoyLmhhc093blByb3BlcnR5KHByb3ApKSB7XG4gICAgICAgIG9iajFbcHJvcF0gPSBvYmoyW3Byb3BdXG4gICAgICB9XG4gICAgfVxuICAgIHJldHVybiBvYmoxXG4gIH1cblxuICBmdW5jdGlvbiBIZWxwZXIgKHF1ZXJ5KSB7XG4gICAgaWYgKCF0aGlzIHx8IHRoaXMuZmluZCAhPT0gSGVscGVyLnByb3RvdHlwZS5maW5kKSB7XG4gICAgICAvLyBDYWxsZWQgYXMgZnVuY3Rpb24gaW5zdGVhZCBvZiBhcyBjb25zdHJ1Y3RvcixcbiAgICAgIC8vIHNvIHdlIHNpbXBseSByZXR1cm4gYSBuZXcgaW5zdGFuY2U6XG4gICAgICByZXR1cm4gbmV3IEhlbHBlcihxdWVyeSlcbiAgICB9XG4gICAgdGhpcy5sZW5ndGggPSAwXG4gICAgaWYgKHF1ZXJ5KSB7XG4gICAgICBpZiAodHlwZW9mIHF1ZXJ5ID09PSAnc3RyaW5nJykge1xuICAgICAgICBxdWVyeSA9IHRoaXMuZmluZChxdWVyeSlcbiAgICAgIH1cbiAgICAgIGlmIChxdWVyeS5ub2RlVHlwZSB8fCBxdWVyeSA9PT0gcXVlcnkud2luZG93KSB7XG4gICAgICAgIC8vIFNpbmdsZSBIVE1MIGVsZW1lbnRcbiAgICAgICAgdGhpcy5sZW5ndGggPSAxXG4gICAgICAgIHRoaXNbMF0gPSBxdWVyeVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gSFRNTCBlbGVtZW50IGNvbGxlY3Rpb25cbiAgICAgICAgdmFyIGkgPSBxdWVyeS5sZW5ndGhcbiAgICAgICAgdGhpcy5sZW5ndGggPSBpXG4gICAgICAgIHdoaWxlIChpKSB7XG4gICAgICAgICAgaSAtPSAxXG4gICAgICAgICAgdGhpc1tpXSA9IHF1ZXJ5W2ldXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBIZWxwZXIuZXh0ZW5kID0gZXh0ZW5kXG5cbiAgSGVscGVyLmNvbnRhaW5zID0gZnVuY3Rpb24gKGNvbnRhaW5lciwgZWxlbWVudCkge1xuICAgIGRvIHtcbiAgICAgIGVsZW1lbnQgPSBlbGVtZW50LnBhcmVudE5vZGVcbiAgICAgIGlmIChlbGVtZW50ID09PSBjb250YWluZXIpIHtcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cbiAgICB9IHdoaWxlIChlbGVtZW50KVxuICAgIHJldHVybiBmYWxzZVxuICB9XG5cbiAgSGVscGVyLnBhcnNlSlNPTiA9IGZ1bmN0aW9uIChzdHJpbmcpIHtcbiAgICByZXR1cm4gd2luZG93LkpTT04gJiYgSlNPTi5wYXJzZShzdHJpbmcpXG4gIH1cblxuICBleHRlbmQoSGVscGVyLnByb3RvdHlwZSwge1xuICAgIGZpbmQ6IGZ1bmN0aW9uIChxdWVyeSkge1xuICAgICAgdmFyIGNvbnRhaW5lciA9IHRoaXNbMF0gfHwgZG9jdW1lbnRcbiAgICAgIGlmICh0eXBlb2YgcXVlcnkgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIGlmIChjb250YWluZXIucXVlcnlTZWxlY3RvckFsbCkge1xuICAgICAgICAgIHF1ZXJ5ID0gY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3JBbGwocXVlcnkpXG4gICAgICAgIH0gZWxzZSBpZiAocXVlcnkuY2hhckF0KDApID09PSAnIycpIHtcbiAgICAgICAgICBxdWVyeSA9IGNvbnRhaW5lci5nZXRFbGVtZW50QnlJZChxdWVyeS5zbGljZSgxKSlcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBxdWVyeSA9IGNvbnRhaW5lci5nZXRFbGVtZW50c0J5VGFnTmFtZShxdWVyeSlcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIG5ldyBIZWxwZXIocXVlcnkpXG4gICAgfSxcblxuICAgIGhhc0NsYXNzOiBmdW5jdGlvbiAoY2xhc3NOYW1lKSB7XG4gICAgICBpZiAoIXRoaXNbMF0pIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlXG4gICAgICB9XG4gICAgICByZXR1cm4gbmV3IFJlZ0V4cCgnKF58XFxcXHMrKScgKyBjbGFzc05hbWUgKyAnKFxcXFxzK3wkKScpLnRlc3QoXG4gICAgICAgIHRoaXNbMF0uY2xhc3NOYW1lXG4gICAgICApXG4gICAgfSxcblxuICAgIGFkZENsYXNzOiBmdW5jdGlvbiAoY2xhc3NOYW1lKSB7XG4gICAgICB2YXIgaSA9IHRoaXMubGVuZ3RoXG4gICAgICB2YXIgZWxlbWVudFxuICAgICAgd2hpbGUgKGkpIHtcbiAgICAgICAgaSAtPSAxXG4gICAgICAgIGVsZW1lbnQgPSB0aGlzW2ldXG4gICAgICAgIGlmICghZWxlbWVudC5jbGFzc05hbWUpIHtcbiAgICAgICAgICBlbGVtZW50LmNsYXNzTmFtZSA9IGNsYXNzTmFtZVxuICAgICAgICAgIHJldHVybiB0aGlzXG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuaGFzQ2xhc3MoY2xhc3NOYW1lKSkge1xuICAgICAgICAgIHJldHVybiB0aGlzXG4gICAgICAgIH1cbiAgICAgICAgZWxlbWVudC5jbGFzc05hbWUgKz0gJyAnICsgY2xhc3NOYW1lXG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpc1xuICAgIH0sXG5cbiAgICByZW1vdmVDbGFzczogZnVuY3Rpb24gKGNsYXNzTmFtZSkge1xuICAgICAgdmFyIHJlZ2V4cCA9IG5ldyBSZWdFeHAoJyhefFxcXFxzKyknICsgY2xhc3NOYW1lICsgJyhcXFxccyt8JCknKVxuICAgICAgdmFyIGkgPSB0aGlzLmxlbmd0aFxuICAgICAgdmFyIGVsZW1lbnRcbiAgICAgIHdoaWxlIChpKSB7XG4gICAgICAgIGkgLT0gMVxuICAgICAgICBlbGVtZW50ID0gdGhpc1tpXVxuICAgICAgICBlbGVtZW50LmNsYXNzTmFtZSA9IGVsZW1lbnQuY2xhc3NOYW1lLnJlcGxhY2UocmVnZXhwLCAnICcpXG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpc1xuICAgIH0sXG5cbiAgICBvbjogZnVuY3Rpb24gKGV2ZW50TmFtZSwgaGFuZGxlcikge1xuICAgICAgdmFyIGV2ZW50TmFtZXMgPSBldmVudE5hbWUuc3BsaXQoL1xccysvKVxuICAgICAgdmFyIGlcbiAgICAgIHZhciBlbGVtZW50XG4gICAgICB3aGlsZSAoZXZlbnROYW1lcy5sZW5ndGgpIHtcbiAgICAgICAgZXZlbnROYW1lID0gZXZlbnROYW1lcy5zaGlmdCgpXG4gICAgICAgIGkgPSB0aGlzLmxlbmd0aFxuICAgICAgICB3aGlsZSAoaSkge1xuICAgICAgICAgIGkgLT0gMVxuICAgICAgICAgIGVsZW1lbnQgPSB0aGlzW2ldXG4gICAgICAgICAgaWYgKGVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcikge1xuICAgICAgICAgICAgZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKGV2ZW50TmFtZSwgaGFuZGxlciwgZmFsc2UpXG4gICAgICAgICAgfSBlbHNlIGlmIChlbGVtZW50LmF0dGFjaEV2ZW50KSB7XG4gICAgICAgICAgICBlbGVtZW50LmF0dGFjaEV2ZW50KCdvbicgKyBldmVudE5hbWUsIGhhbmRsZXIpXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpc1xuICAgIH0sXG5cbiAgICBvZmY6IGZ1bmN0aW9uIChldmVudE5hbWUsIGhhbmRsZXIpIHtcbiAgICAgIHZhciBldmVudE5hbWVzID0gZXZlbnROYW1lLnNwbGl0KC9cXHMrLylcbiAgICAgIHZhciBpXG4gICAgICB2YXIgZWxlbWVudFxuICAgICAgd2hpbGUgKGV2ZW50TmFtZXMubGVuZ3RoKSB7XG4gICAgICAgIGV2ZW50TmFtZSA9IGV2ZW50TmFtZXMuc2hpZnQoKVxuICAgICAgICBpID0gdGhpcy5sZW5ndGhcbiAgICAgICAgd2hpbGUgKGkpIHtcbiAgICAgICAgICBpIC09IDFcbiAgICAgICAgICBlbGVtZW50ID0gdGhpc1tpXVxuICAgICAgICAgIGlmIChlbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIpIHtcbiAgICAgICAgICAgIGVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcihldmVudE5hbWUsIGhhbmRsZXIsIGZhbHNlKVxuICAgICAgICAgIH0gZWxzZSBpZiAoZWxlbWVudC5kZXRhY2hFdmVudCkge1xuICAgICAgICAgICAgZWxlbWVudC5kZXRhY2hFdmVudCgnb24nICsgZXZlbnROYW1lLCBoYW5kbGVyKVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXNcbiAgICB9LFxuXG4gICAgZW1wdHk6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBpID0gdGhpcy5sZW5ndGhcbiAgICAgIHZhciBlbGVtZW50XG4gICAgICB3aGlsZSAoaSkge1xuICAgICAgICBpIC09IDFcbiAgICAgICAgZWxlbWVudCA9IHRoaXNbaV1cbiAgICAgICAgd2hpbGUgKGVsZW1lbnQuaGFzQ2hpbGROb2RlcygpKSB7XG4gICAgICAgICAgZWxlbWVudC5yZW1vdmVDaGlsZChlbGVtZW50Lmxhc3RDaGlsZClcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHRoaXNcbiAgICB9LFxuXG4gICAgZmlyc3Q6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBuZXcgSGVscGVyKHRoaXNbMF0pXG4gICAgfVxuICB9KVxuXG4gIGlmICh0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpIHtcbiAgICBkZWZpbmUoZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIEhlbHBlclxuICAgIH0pXG4gIH0gZWxzZSB7XG4gICAgd2luZG93LmJsdWVpbXAgPSB3aW5kb3cuYmx1ZWltcCB8fCB7fVxuICAgIHdpbmRvdy5ibHVlaW1wLmhlbHBlciA9IEhlbHBlclxuICB9XG59KSgpXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9ibHVlaW1wLWdhbGxlcnkvanMvYmx1ZWltcC1oZWxwZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2JsdWVpbXAtZ2FsbGVyeS9qcy9ibHVlaW1wLWhlbHBlci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvKlxuICogYmx1ZWltcCBHYWxsZXJ5IGpRdWVyeSBwbHVnaW5cbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9ibHVlaW1wL0dhbGxlcnlcbiAqXG4gKiBDb3B5cmlnaHQgMjAxMywgU2ViYXN0aWFuIFRzY2hhblxuICogaHR0cHM6Ly9ibHVlaW1wLm5ldFxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZTpcbiAqIGh0dHBzOi8vb3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvTUlUXG4gKi9cblxuLyogZ2xvYmFsIGRlZmluZSwgd2luZG93LCBkb2N1bWVudCAqL1xuXG47KGZ1bmN0aW9uIChmYWN0b3J5KSB7XG4gICd1c2Ugc3RyaWN0J1xuICBpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKSB7XG4gICAgZGVmaW5lKFsnanF1ZXJ5JywgJy4vYmx1ZWltcC1nYWxsZXJ5J10sIGZhY3RvcnkpXG4gIH0gZWxzZSB7XG4gICAgZmFjdG9yeSh3aW5kb3cualF1ZXJ5LCB3aW5kb3cuYmx1ZWltcC5HYWxsZXJ5KVxuICB9XG59KShmdW5jdGlvbiAoJCwgR2FsbGVyeSkge1xuICAndXNlIHN0cmljdCdcblxuICAvLyBHbG9iYWwgY2xpY2sgaGFuZGxlciB0byBvcGVuIGxpbmtzIHdpdGggZGF0YS1nYWxsZXJ5IGF0dHJpYnV0ZVxuICAvLyBpbiB0aGUgR2FsbGVyeSBsaWdodGJveDpcbiAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgJ1tkYXRhLWdhbGxlcnldJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgLy8gR2V0IHRoZSBjb250YWluZXIgaWQgZnJvbSB0aGUgZGF0YS1nYWxsZXJ5IGF0dHJpYnV0ZTpcbiAgICB2YXIgaWQgPSAkKHRoaXMpLmRhdGEoJ2dhbGxlcnknKVxuICAgIHZhciB3aWRnZXQgPSAkKGlkKVxuICAgIHZhciBjb250YWluZXIgPVxuICAgICAgKHdpZGdldC5sZW5ndGggJiYgd2lkZ2V0KSB8fCAkKEdhbGxlcnkucHJvdG90eXBlLm9wdGlvbnMuY29udGFpbmVyKVxuICAgIHZhciBjYWxsYmFja3MgPSB7XG4gICAgICBvbm9wZW46IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29udGFpbmVyLmRhdGEoJ2dhbGxlcnknLCB0aGlzKS50cmlnZ2VyKCdvcGVuJylcbiAgICAgIH0sXG4gICAgICBvbm9wZW5lZDogZnVuY3Rpb24gKCkge1xuICAgICAgICBjb250YWluZXIudHJpZ2dlcignb3BlbmVkJylcbiAgICAgIH0sXG4gICAgICBvbnNsaWRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNvbnRhaW5lci50cmlnZ2VyKCdzbGlkZScsIGFyZ3VtZW50cylcbiAgICAgIH0sXG4gICAgICBvbnNsaWRlZW5kOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNvbnRhaW5lci50cmlnZ2VyKCdzbGlkZWVuZCcsIGFyZ3VtZW50cylcbiAgICAgIH0sXG4gICAgICBvbnNsaWRlY29tcGxldGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY29udGFpbmVyLnRyaWdnZXIoJ3NsaWRlY29tcGxldGUnLCBhcmd1bWVudHMpXG4gICAgICB9LFxuICAgICAgb25jbG9zZTogZnVuY3Rpb24gKCkge1xuICAgICAgICBjb250YWluZXIudHJpZ2dlcignY2xvc2UnKVxuICAgICAgfSxcbiAgICAgIG9uY2xvc2VkOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNvbnRhaW5lci50cmlnZ2VyKCdjbG9zZWQnKS5yZW1vdmVEYXRhKCdnYWxsZXJ5JylcbiAgICAgIH1cbiAgICB9XG4gICAgdmFyIG9wdGlvbnMgPSAkLmV4dGVuZChcbiAgICAgIC8vIFJldHJpZXZlIGN1c3RvbSBvcHRpb25zIGZyb20gZGF0YS1hdHRyaWJ1dGVzXG4gICAgICAvLyBvbiB0aGUgR2FsbGVyeSB3aWRnZXQ6XG4gICAgICBjb250YWluZXIuZGF0YSgpLFxuICAgICAge1xuICAgICAgICBjb250YWluZXI6IGNvbnRhaW5lclswXSxcbiAgICAgICAgaW5kZXg6IHRoaXMsXG4gICAgICAgIGV2ZW50OiBldmVudFxuICAgICAgfSxcbiAgICAgIGNhbGxiYWNrc1xuICAgIClcbiAgICAvLyBTZWxlY3QgYWxsIGxpbmtzIHdpdGggdGhlIHNhbWUgZGF0YS1nYWxsZXJ5IGF0dHJpYnV0ZTpcbiAgICB2YXIgbGlua3MgPSAkKHRoaXMpXG4gICAgICAuY2xvc2VzdCgnW2RhdGEtZ2FsbGVyeS1ncm91cF0sIGJvZHknKVxuICAgICAgLmZpbmQoJ1tkYXRhLWdhbGxlcnk9XCInICsgaWQgKyAnXCJdJylcbiAgICBpZiAob3B0aW9ucy5maWx0ZXIpIHtcbiAgICAgIGxpbmtzID0gbGlua3MuZmlsdGVyKG9wdGlvbnMuZmlsdGVyKVxuICAgIH1cbiAgICByZXR1cm4gbmV3IEdhbGxlcnkobGlua3MsIG9wdGlvbnMpXG4gIH0pXG59KVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYmx1ZWltcC1nYWxsZXJ5L2pzL2pxdWVyeS5ibHVlaW1wLWdhbGxlcnkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2JsdWVpbXAtZ2FsbGVyeS9qcy9qcXVlcnkuYmx1ZWltcC1nYWxsZXJ5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIihmdW5jdGlvbigpIHtcclxuICB2YXIgJDtcclxuXHJcbiAgJCA9IGpRdWVyeTtcclxuXHJcbiAgJC5ib290c3RyYXBHcm93bCA9IGZ1bmN0aW9uKG1lc3NhZ2UsIG9wdGlvbnMpIHtcclxuICAgIHZhciAkYWxlcnQsIGNzcywgb2Zmc2V0QW1vdW50O1xyXG4gICAgb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCAkLmJvb3RzdHJhcEdyb3dsLmRlZmF1bHRfb3B0aW9ucywgb3B0aW9ucyk7XHJcbiAgICAkYWxlcnQgPSAkKFwiPGRpdj5cIik7XHJcbiAgICAkYWxlcnQuYXR0cihcImNsYXNzXCIsIFwiYm9vdHN0cmFwLWdyb3dsIGFsZXJ0XCIpO1xyXG4gICAgaWYgKG9wdGlvbnMuY2xhc3MpIHtcclxuICAgICAgJGFsZXJ0LmFkZENsYXNzKG9wdGlvbnMuY2xhc3MpO1xyXG4gICAgfVxyXG4gICAgaWYgKG9wdGlvbnMudHlwZSkge1xyXG4gICAgICAkYWxlcnQuYWRkQ2xhc3MoXCJhbGVydC1cIiArIG9wdGlvbnMudHlwZSk7XHJcbiAgICB9XHJcbiAgICBpZiAob3B0aW9ucy5hbGxvd19kaXNtaXNzKSB7XHJcbiAgICAgICRhbGVydC5hZGRDbGFzcyhcImFsZXJ0LWRpc21pc3NpYmxlXCIpO1xyXG4gICAgICAkYWxlcnQuYXBwZW5kKFwiPGJ1dHRvbiAgY2xhc3M9XFxcImNsb3NlXFxcIiBkYXRhLWRpc21pc3M9XFxcImFsZXJ0XFxcIiB0eXBlPVxcXCJidXR0b25cXFwiPjxzcGFuIGFyaWEtaGlkZGVuPVxcXCJ0cnVlXFxcIj4mIzIxNTs8L3NwYW4+PHNwYW4gY2xhc3M9XFxcInNyLW9ubHlcXFwiPkNsb3NlPC9zcGFuPjwvYnV0dG9uPlwiKTtcclxuICAgIH1cclxuICAgICRhbGVydC5hcHBlbmQobWVzc2FnZSk7XHJcbiAgICBpZiAob3B0aW9ucy50b3Bfb2Zmc2V0KSB7XHJcbiAgICAgIG9wdGlvbnMub2Zmc2V0ID0ge1xyXG4gICAgICAgIGZyb206IFwidG9wXCIsXHJcbiAgICAgICAgYW1vdW50OiBvcHRpb25zLnRvcF9vZmZzZXRcclxuICAgICAgfTtcclxuICAgIH1cclxuICAgIG9mZnNldEFtb3VudCA9IG9wdGlvbnMub2Zmc2V0LmFtb3VudDtcclxuICAgICQoXCIuYm9vdHN0cmFwLWdyb3dsXCIpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgIHJldHVybiBvZmZzZXRBbW91bnQgPSBNYXRoLm1heChvZmZzZXRBbW91bnQsIHBhcnNlSW50KCQodGhpcykuY3NzKG9wdGlvbnMub2Zmc2V0LmZyb20pKSArICQodGhpcykub3V0ZXJIZWlnaHQoKSArIG9wdGlvbnMuc3RhY2t1cF9zcGFjaW5nKTtcclxuICAgIH0pO1xyXG4gICAgY3NzID0ge1xyXG4gICAgICBcInBvc2l0aW9uXCI6IChvcHRpb25zLmVsZSA9PT0gXCJib2R5XCIgPyBcImZpeGVkXCIgOiBcImFic29sdXRlXCIpLFxyXG4gICAgICBcIm1hcmdpblwiOiAwLFxyXG4gICAgICBcInotaW5kZXhcIjogXCI5OTk5XCIsXHJcbiAgICAgIFwiZGlzcGxheVwiOiBcIm5vbmVcIlxyXG4gICAgfTtcclxuICAgIGNzc1tvcHRpb25zLm9mZnNldC5mcm9tXSA9IG9mZnNldEFtb3VudCArIFwicHhcIjtcclxuICAgICRhbGVydC5jc3MoY3NzKTtcclxuICAgIGlmIChvcHRpb25zLndpZHRoICE9PSBcImF1dG9cIikge1xyXG4gICAgICAkYWxlcnQuY3NzKFwid2lkdGhcIiwgb3B0aW9ucy53aWR0aCArIFwicHhcIik7XHJcbiAgICB9XHJcbiAgICAkKG9wdGlvbnMuZWxlKS5hcHBlbmQoJGFsZXJ0KTtcclxuICAgIHN3aXRjaCAob3B0aW9ucy5hbGlnbikge1xyXG4gICAgICBjYXNlIFwiY2VudGVyXCI6XHJcbiAgICAgICAgJGFsZXJ0LmNzcyh7XHJcbiAgICAgICAgICBcImxlZnRcIjogXCI1MCVcIixcclxuICAgICAgICAgIFwibWFyZ2luLWxlZnRcIjogXCItXCIgKyAoJGFsZXJ0Lm91dGVyV2lkdGgoKSAvIDIpICsgXCJweFwiXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgXCJsZWZ0XCI6XHJcbiAgICAgICAgJGFsZXJ0LmNzcyhcImxlZnRcIiwgXCIyMHB4XCIpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBkZWZhdWx0OlxyXG4gICAgICAgICRhbGVydC5jc3MoXCJyaWdodFwiLCBcIjIwcHhcIik7XHJcbiAgICB9XHJcbiAgICAkYWxlcnQuZmFkZUluKCk7XHJcbiAgICBpZiAob3B0aW9ucy5kZWxheSA+IDApIHtcclxuICAgICAgJGFsZXJ0LmRlbGF5KG9wdGlvbnMuZGVsYXkpLmZhZGVPdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuICQodGhpcykuYWxlcnQoXCJjbG9zZVwiKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gJGFsZXJ0O1xyXG4gIH07XHJcblxyXG4gICQuYm9vdHN0cmFwR3Jvd2wuZGVmYXVsdF9vcHRpb25zID0ge1xyXG4gICAgZWxlOiBcImJvZHlcIixcclxuICAgIHR5cGU6IFwiaW5mb1wiLFxyXG4gICAgb2Zmc2V0OiB7XHJcbiAgICAgIGZyb206IFwidG9wXCIsXHJcbiAgICAgIGFtb3VudDogMjBcclxuICAgIH0sXHJcbiAgICBhbGlnbjogXCJyaWdodFwiLFxyXG4gICAgd2lkdGg6IDI1MCxcclxuICAgIGRlbGF5OiA0MDAwLFxyXG4gICAgYWxsb3dfZGlzbWlzczogdHJ1ZSxcclxuICAgIHN0YWNrdXBfc3BhY2luZzogMTBcclxuICB9O1xyXG5cclxufSkuY2FsbCh0aGlzKTtcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYm9vdHN0cmFwLWdyb3dsL2pxdWVyeS5ib290c3RyYXAtZ3Jvd2wuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2Jvb3RzdHJhcC1ncm93bC9qcXVlcnkuYm9vdHN0cmFwLWdyb3dsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIi8qISBjcm9waXQgLSB2MC41LjAgPGh0dHBzOi8vZ2l0aHViLmNvbS9zY290dGNoZW5nL2Nyb3BpdD4gKi9cbihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkocmVxdWlyZShcImpxdWVyeVwiKSk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShbXCJqcXVlcnlcIl0sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wiY3JvcGl0XCJdID0gZmFjdG9yeShyZXF1aXJlKFwianF1ZXJ5XCIpKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJjcm9waXRcIl0gPSBmYWN0b3J5KHJvb3RbXCJqUXVlcnlcIl0pO1xufSkodGhpcywgZnVuY3Rpb24oX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8xX18pIHtcbnJldHVybiAvKioqKioqLyAoZnVuY3Rpb24obW9kdWxlcykgeyAvLyB3ZWJwYWNrQm9vdHN0cmFwXG4vKioqKioqLyBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbi8qKioqKiovIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuLyoqKioqKi8gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuLyoqKioqKi8gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbi8qKioqKiovIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbi8qKioqKiovIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSlcbi8qKioqKiovIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4vKioqKioqLyBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbi8qKioqKiovIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4vKioqKioqLyBcdFx0XHRleHBvcnRzOiB7fSxcbi8qKioqKiovIFx0XHRcdGlkOiBtb2R1bGVJZCxcbi8qKioqKiovIFx0XHRcdGxvYWRlZDogZmFsc2Vcbi8qKioqKiovIFx0XHR9O1xuXG4vKioqKioqLyBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4vKioqKioqLyBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbi8qKioqKiovIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4vKioqKioqLyBcdFx0bW9kdWxlLmxvYWRlZCA9IHRydWU7XG5cbi8qKioqKiovIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuLyoqKioqKi8gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbi8qKioqKiovIFx0fVxuXG5cbi8qKioqKiovIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbi8qKioqKiovIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuLyoqKioqKi8gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuLyoqKioqKi8gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4vKioqKioqLyBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4vKioqKioqLyBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbi8qKioqKiovIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4vKioqKioqLyBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuLyoqKioqKi8gfSlcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4vKioqKioqLyAoW1xuLyogMCAqL1xuLyoqKi8gZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblx0dmFyIF9zbGljZSA9IEFycmF5LnByb3RvdHlwZS5zbGljZTtcblxuXHRmdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyAnZGVmYXVsdCc6IG9iaiB9OyB9XG5cblx0dmFyIF9qcXVlcnkgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDEpO1xuXG5cdHZhciBfanF1ZXJ5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2pxdWVyeSk7XG5cblx0dmFyIF9jcm9waXQgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDIpO1xuXG5cdHZhciBfY3JvcGl0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2Nyb3BpdCk7XG5cblx0dmFyIF9jb25zdGFudHMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDQpO1xuXG5cdHZhciBfdXRpbHMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDYpO1xuXG5cdHZhciBhcHBseU9uRWFjaCA9IGZ1bmN0aW9uIGFwcGx5T25FYWNoKCRlbCwgY2FsbGJhY2spIHtcblx0ICByZXR1cm4gJGVsLmVhY2goZnVuY3Rpb24gKCkge1xuXHQgICAgdmFyIGNyb3BpdCA9IF9qcXVlcnkyWydkZWZhdWx0J10uZGF0YSh0aGlzLCBfY29uc3RhbnRzLlBMVUdJTl9LRVkpO1xuXG5cdCAgICBpZiAoIWNyb3BpdCkge1xuXHQgICAgICByZXR1cm47XG5cdCAgICB9XG5cdCAgICBjYWxsYmFjayhjcm9waXQpO1xuXHQgIH0pO1xuXHR9O1xuXG5cdHZhciBjYWxsT25GaXJzdCA9IGZ1bmN0aW9uIGNhbGxPbkZpcnN0KCRlbCwgbWV0aG9kLCBvcHRpb25zKSB7XG5cdCAgdmFyIGNyb3BpdCA9ICRlbC5maXJzdCgpLmRhdGEoX2NvbnN0YW50cy5QTFVHSU5fS0VZKTtcblxuXHQgIGlmICghY3JvcGl0IHx8ICFfanF1ZXJ5MlsnZGVmYXVsdCddLmlzRnVuY3Rpb24oY3JvcGl0W21ldGhvZF0pKSB7XG5cdCAgICByZXR1cm4gbnVsbDtcblx0ICB9XG5cdCAgcmV0dXJuIGNyb3BpdFttZXRob2RdKG9wdGlvbnMpO1xuXHR9O1xuXG5cdHZhciBtZXRob2RzID0ge1xuXHQgIGluaXQ6IGZ1bmN0aW9uIGluaXQob3B0aW9ucykge1xuXHQgICAgcmV0dXJuIHRoaXMuZWFjaChmdW5jdGlvbiAoKSB7XG5cdCAgICAgIC8vIE9ubHkgaW5zdGFudGlhdGUgb25jZSBwZXIgZWxlbWVudFxuXHQgICAgICBpZiAoX2pxdWVyeTJbJ2RlZmF1bHQnXS5kYXRhKHRoaXMsIF9jb25zdGFudHMuUExVR0lOX0tFWSkpIHtcblx0ICAgICAgICByZXR1cm47XG5cdCAgICAgIH1cblxuXHQgICAgICB2YXIgY3JvcGl0ID0gbmV3IF9jcm9waXQyWydkZWZhdWx0J10oX2pxdWVyeTJbJ2RlZmF1bHQnXSwgdGhpcywgb3B0aW9ucyk7XG5cdCAgICAgIF9qcXVlcnkyWydkZWZhdWx0J10uZGF0YSh0aGlzLCBfY29uc3RhbnRzLlBMVUdJTl9LRVksIGNyb3BpdCk7XG5cdCAgICB9KTtcblx0ICB9LFxuXG5cdCAgZGVzdHJveTogZnVuY3Rpb24gZGVzdHJveSgpIHtcblx0ICAgIHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24gKCkge1xuXHQgICAgICBfanF1ZXJ5MlsnZGVmYXVsdCddLnJlbW92ZURhdGEodGhpcywgX2NvbnN0YW50cy5QTFVHSU5fS0VZKTtcblx0ICAgIH0pO1xuXHQgIH0sXG5cblx0ICBpc1pvb21hYmxlOiBmdW5jdGlvbiBpc1pvb21hYmxlKCkge1xuXHQgICAgcmV0dXJuIGNhbGxPbkZpcnN0KHRoaXMsICdpc1pvb21hYmxlJyk7XG5cdCAgfSxcblxuXHQgICdleHBvcnQnOiBmdW5jdGlvbiBfZXhwb3J0KG9wdGlvbnMpIHtcblx0ICAgIHJldHVybiBjYWxsT25GaXJzdCh0aGlzLCAnZ2V0Q3JvcHBlZEltYWdlRGF0YScsIG9wdGlvbnMpO1xuXHQgIH1cblx0fTtcblxuXHR2YXIgZGVsZWdhdGUgPSBmdW5jdGlvbiBkZWxlZ2F0ZSgkZWwsIGZuTmFtZSkge1xuXHQgIHJldHVybiBhcHBseU9uRWFjaCgkZWwsIGZ1bmN0aW9uIChjcm9waXQpIHtcblx0ICAgIGNyb3BpdFtmbk5hbWVdKCk7XG5cdCAgfSk7XG5cdH07XG5cblx0dmFyIHByb3AgPSBmdW5jdGlvbiBwcm9wKCRlbCwgbmFtZSwgdmFsdWUpIHtcblx0ICBpZiAoKDAsIF91dGlscy5leGlzdHMpKHZhbHVlKSkge1xuXHQgICAgcmV0dXJuIGFwcGx5T25FYWNoKCRlbCwgZnVuY3Rpb24gKGNyb3BpdCkge1xuXHQgICAgICBjcm9waXRbbmFtZV0gPSB2YWx1ZTtcblx0ICAgIH0pO1xuXHQgIH0gZWxzZSB7XG5cdCAgICB2YXIgY3JvcGl0ID0gJGVsLmZpcnN0KCkuZGF0YShfY29uc3RhbnRzLlBMVUdJTl9LRVkpO1xuXHQgICAgcmV0dXJuIGNyb3BpdFtuYW1lXTtcblx0ICB9XG5cdH07XG5cblx0X2pxdWVyeTJbJ2RlZmF1bHQnXS5mbi5jcm9waXQgPSBmdW5jdGlvbiAobWV0aG9kKSB7XG5cdCAgaWYgKG1ldGhvZHNbbWV0aG9kXSkge1xuXHQgICAgcmV0dXJuIG1ldGhvZHNbbWV0aG9kXS5hcHBseSh0aGlzLCBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpKTtcblx0ICB9IGVsc2UgaWYgKFsnaW1hZ2VTdGF0ZScsICdpbWFnZVNyYycsICdvZmZzZXQnLCAncHJldmlld1NpemUnLCAnaW1hZ2VTaXplJywgJ3pvb20nLCAnaW5pdGlhbFpvb20nLCAnZXhwb3J0Wm9vbScsICdtaW5ab29tJywgJ21heFpvb20nXS5pbmRleE9mKG1ldGhvZCkgPj0gMCkge1xuXHQgICAgcmV0dXJuIHByb3AuYXBwbHkodW5kZWZpbmVkLCBbdGhpc10uY29uY2F0KF9zbGljZS5jYWxsKGFyZ3VtZW50cykpKTtcblx0ICB9IGVsc2UgaWYgKFsncm90YXRlQ1cnLCAncm90YXRlQ0NXJywgJ2Rpc2FibGUnLCAncmVlbmFibGUnXS5pbmRleE9mKG1ldGhvZCkgPj0gMCkge1xuXHQgICAgcmV0dXJuIGRlbGVnYXRlLmFwcGx5KHVuZGVmaW5lZCwgW3RoaXNdLmNvbmNhdChfc2xpY2UuY2FsbChhcmd1bWVudHMpKSk7XG5cdCAgfSBlbHNlIHtcblx0ICAgIHJldHVybiBtZXRob2RzLmluaXQuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0ICB9XG5cdH07XG5cbi8qKiovIH0sXG4vKiAxICovXG4vKioqLyBmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcblxuXHRtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfMV9fO1xuXG4vKioqLyB9LFxuLyogMiAqL1xuLyoqKi8gZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKSB7XG5cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywge1xuXHQgIHZhbHVlOiB0cnVlXG5cdH0pO1xuXG5cdHZhciBfY3JlYXRlQ2xhc3MgPSAoZnVuY3Rpb24gKCkgeyBmdW5jdGlvbiBkZWZpbmVQcm9wZXJ0aWVzKHRhcmdldCwgcHJvcHMpIHsgZm9yICh2YXIgaSA9IDA7IGkgPCBwcm9wcy5sZW5ndGg7IGkrKykgeyB2YXIgZGVzY3JpcHRvciA9IHByb3BzW2ldOyBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7IGRlc2NyaXB0b3IuY29uZmlndXJhYmxlID0gdHJ1ZTsgaWYgKCd2YWx1ZScgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGRlc2NyaXB0b3Iua2V5LCBkZXNjcmlwdG9yKTsgfSB9IHJldHVybiBmdW5jdGlvbiAoQ29uc3RydWN0b3IsIHByb3RvUHJvcHMsIHN0YXRpY1Byb3BzKSB7IGlmIChwcm90b1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7IGlmIChzdGF0aWNQcm9wcykgZGVmaW5lUHJvcGVydGllcyhDb25zdHJ1Y3Rvciwgc3RhdGljUHJvcHMpOyByZXR1cm4gQ29uc3RydWN0b3I7IH07IH0pKCk7XG5cblx0ZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgJ2RlZmF1bHQnOiBvYmogfTsgfVxuXG5cdGZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcignQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uJyk7IH0gfVxuXG5cdHZhciBfanF1ZXJ5ID0gX193ZWJwYWNrX3JlcXVpcmVfXygxKTtcblxuXHR2YXIgX2pxdWVyeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9qcXVlcnkpO1xuXG5cdHZhciBfWm9vbWVyID0gX193ZWJwYWNrX3JlcXVpcmVfXygzKTtcblxuXHR2YXIgX1pvb21lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9ab29tZXIpO1xuXG5cdHZhciBfY29uc3RhbnRzID0gX193ZWJwYWNrX3JlcXVpcmVfXyg0KTtcblxuXHR2YXIgX29wdGlvbnMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDUpO1xuXG5cdHZhciBfdXRpbHMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDYpO1xuXG5cdHZhciBDcm9waXQgPSAoZnVuY3Rpb24gKCkge1xuXHQgIGZ1bmN0aW9uIENyb3BpdChqUXVlcnksIGVsZW1lbnQsIG9wdGlvbnMpIHtcblx0ICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDcm9waXQpO1xuXG5cdCAgICB0aGlzLiRlbCA9ICgwLCBfanF1ZXJ5MlsnZGVmYXVsdCddKShlbGVtZW50KTtcblxuXHQgICAgdmFyIGRlZmF1bHRzID0gKDAsIF9vcHRpb25zLmxvYWREZWZhdWx0cykodGhpcy4kZWwpO1xuXHQgICAgdGhpcy5vcHRpb25zID0gX2pxdWVyeTJbJ2RlZmF1bHQnXS5leHRlbmQoe30sIGRlZmF1bHRzLCBvcHRpb25zKTtcblxuXHQgICAgdGhpcy5pbml0KCk7XG5cdCAgfVxuXG5cdCAgX2NyZWF0ZUNsYXNzKENyb3BpdCwgW3tcblx0ICAgIGtleTogJ2luaXQnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIGluaXQoKSB7XG5cdCAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG5cblx0ICAgICAgdGhpcy5pbWFnZSA9IG5ldyBJbWFnZSgpO1xuXHQgICAgICB0aGlzLnByZUltYWdlID0gbmV3IEltYWdlKCk7XG5cdCAgICAgIHRoaXMuaW1hZ2Uub25sb2FkID0gdGhpcy5vbkltYWdlTG9hZGVkLmJpbmQodGhpcyk7XG5cdCAgICAgIHRoaXMucHJlSW1hZ2Uub25sb2FkID0gdGhpcy5vblByZUltYWdlTG9hZGVkLmJpbmQodGhpcyk7XG5cdCAgICAgIHRoaXMuaW1hZ2Uub25lcnJvciA9IHRoaXMucHJlSW1hZ2Uub25lcnJvciA9IGZ1bmN0aW9uICgpIHtcblx0ICAgICAgICBfdGhpcy5vbkltYWdlRXJyb3IuY2FsbChfdGhpcywgX2NvbnN0YW50cy5FUlJPUlMuSU1BR0VfRkFJTEVEX1RPX0xPQUQpO1xuXHQgICAgICB9O1xuXG5cdCAgICAgIHRoaXMuJHByZXZpZXcgPSB0aGlzLm9wdGlvbnMuJHByZXZpZXcuY3NzKCdwb3NpdGlvbicsICdyZWxhdGl2ZScpO1xuXHQgICAgICB0aGlzLiRmaWxlSW5wdXQgPSB0aGlzLm9wdGlvbnMuJGZpbGVJbnB1dC5hdHRyKHsgYWNjZXB0OiAnaW1hZ2UvKicgfSk7XG5cdCAgICAgIHRoaXMuJHpvb21TbGlkZXIgPSB0aGlzLm9wdGlvbnMuJHpvb21TbGlkZXIuYXR0cih7IG1pbjogMCwgbWF4OiAxLCBzdGVwOiAwLjAxIH0pO1xuXG5cdCAgICAgIHRoaXMucHJldmlld1NpemUgPSB7XG5cdCAgICAgICAgd2lkdGg6IHRoaXMub3B0aW9ucy53aWR0aCB8fCB0aGlzLiRwcmV2aWV3LndpZHRoKCksXG5cdCAgICAgICAgaGVpZ2h0OiB0aGlzLm9wdGlvbnMuaGVpZ2h0IHx8IHRoaXMuJHByZXZpZXcuaGVpZ2h0KClcblx0ICAgICAgfTtcblxuXHQgICAgICB0aGlzLiRpbWFnZSA9ICgwLCBfanF1ZXJ5MlsnZGVmYXVsdCddKSgnPGltZyAvPicpLmFkZENsYXNzKF9jb25zdGFudHMuQ0xBU1NfTkFNRVMuUFJFVklFV19JTUFHRSkuYXR0cignYWx0JywgJycpLmNzcyh7XG5cdCAgICAgICAgdHJhbnNmb3JtT3JpZ2luOiAndG9wIGxlZnQnLFxuXHQgICAgICAgIHdlYmtpdFRyYW5zZm9ybU9yaWdpbjogJ3RvcCBsZWZ0Jyxcblx0ICAgICAgICB3aWxsQ2hhbmdlOiAndHJhbnNmb3JtJ1xuXHQgICAgICB9KTtcblx0ICAgICAgdGhpcy4kaW1hZ2VDb250YWluZXIgPSAoMCwgX2pxdWVyeTJbJ2RlZmF1bHQnXSkoJzxkaXYgLz4nKS5hZGRDbGFzcyhfY29uc3RhbnRzLkNMQVNTX05BTUVTLlBSRVZJRVdfSU1BR0VfQ09OVEFJTkVSKS5jc3Moe1xuXHQgICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuXHQgICAgICAgIG92ZXJmbG93OiAnaGlkZGVuJyxcblx0ICAgICAgICBsZWZ0OiAwLFxuXHQgICAgICAgIHRvcDogMCxcblx0ICAgICAgICB3aWR0aDogJzEwMCUnLFxuXHQgICAgICAgIGhlaWdodDogJzEwMCUnXG5cdCAgICAgIH0pLmFwcGVuZCh0aGlzLiRpbWFnZSk7XG5cdCAgICAgIHRoaXMuJHByZXZpZXcuYXBwZW5kKHRoaXMuJGltYWdlQ29udGFpbmVyKTtcblxuXHQgICAgICBpZiAodGhpcy5vcHRpb25zLmltYWdlQmFja2dyb3VuZCkge1xuXHQgICAgICAgIGlmIChfanF1ZXJ5MlsnZGVmYXVsdCddLmlzQXJyYXkodGhpcy5vcHRpb25zLmltYWdlQmFja2dyb3VuZEJvcmRlcldpZHRoKSkge1xuXHQgICAgICAgICAgdGhpcy5iZ0JvcmRlcldpZHRoQXJyYXkgPSB0aGlzLm9wdGlvbnMuaW1hZ2VCYWNrZ3JvdW5kQm9yZGVyV2lkdGg7XG5cdCAgICAgICAgfSBlbHNlIHtcblx0ICAgICAgICAgIHRoaXMuYmdCb3JkZXJXaWR0aEFycmF5ID0gWzAsIDEsIDIsIDNdLm1hcChmdW5jdGlvbiAoKSB7XG5cdCAgICAgICAgICAgIHJldHVybiBfdGhpcy5vcHRpb25zLmltYWdlQmFja2dyb3VuZEJvcmRlcldpZHRoO1xuXHQgICAgICAgICAgfSk7XG5cdCAgICAgICAgfVxuXG5cdCAgICAgICAgdGhpcy4kYmcgPSAoMCwgX2pxdWVyeTJbJ2RlZmF1bHQnXSkoJzxpbWcgLz4nKS5hZGRDbGFzcyhfY29uc3RhbnRzLkNMQVNTX05BTUVTLlBSRVZJRVdfQkFDS0dST1VORCkuYXR0cignYWx0JywgJycpLmNzcyh7XG5cdCAgICAgICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcblx0ICAgICAgICAgIGxlZnQ6IHRoaXMuYmdCb3JkZXJXaWR0aEFycmF5WzNdLFxuXHQgICAgICAgICAgdG9wOiB0aGlzLmJnQm9yZGVyV2lkdGhBcnJheVswXSxcblx0ICAgICAgICAgIHRyYW5zZm9ybU9yaWdpbjogJ3RvcCBsZWZ0Jyxcblx0ICAgICAgICAgIHdlYmtpdFRyYW5zZm9ybU9yaWdpbjogJ3RvcCBsZWZ0Jyxcblx0ICAgICAgICAgIHdpbGxDaGFuZ2U6ICd0cmFuc2Zvcm0nXG5cdCAgICAgICAgfSk7XG5cdCAgICAgICAgdGhpcy4kYmdDb250YWluZXIgPSAoMCwgX2pxdWVyeTJbJ2RlZmF1bHQnXSkoJzxkaXYgLz4nKS5hZGRDbGFzcyhfY29uc3RhbnRzLkNMQVNTX05BTUVTLlBSRVZJRVdfQkFDS0dST1VORF9DT05UQUlORVIpLmNzcyh7XG5cdCAgICAgICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcblx0ICAgICAgICAgIHpJbmRleDogMCxcblx0ICAgICAgICAgIHRvcDogLXRoaXMuYmdCb3JkZXJXaWR0aEFycmF5WzBdLFxuXHQgICAgICAgICAgcmlnaHQ6IC10aGlzLmJnQm9yZGVyV2lkdGhBcnJheVsxXSxcblx0ICAgICAgICAgIGJvdHRvbTogLXRoaXMuYmdCb3JkZXJXaWR0aEFycmF5WzJdLFxuXHQgICAgICAgICAgbGVmdDogLXRoaXMuYmdCb3JkZXJXaWR0aEFycmF5WzNdXG5cdCAgICAgICAgfSkuYXBwZW5kKHRoaXMuJGJnKTtcblx0ICAgICAgICBpZiAodGhpcy5iZ0JvcmRlcldpZHRoQXJyYXlbMF0gPiAwKSB7XG5cdCAgICAgICAgICB0aGlzLiRiZ0NvbnRhaW5lci5jc3MoJ292ZXJmbG93JywgJ2hpZGRlbicpO1xuXHQgICAgICAgIH1cblx0ICAgICAgICB0aGlzLiRwcmV2aWV3LnByZXBlbmQodGhpcy4kYmdDb250YWluZXIpO1xuXHQgICAgICB9XG5cblx0ICAgICAgdGhpcy5pbml0aWFsWm9vbSA9IHRoaXMub3B0aW9ucy5pbml0aWFsWm9vbTtcblxuXHQgICAgICB0aGlzLmltYWdlTG9hZGVkID0gZmFsc2U7XG5cblx0ICAgICAgdGhpcy5tb3ZlQ29udGludWUgPSBmYWxzZTtcblxuXHQgICAgICB0aGlzLnpvb21lciA9IG5ldyBfWm9vbWVyMlsnZGVmYXVsdCddKCk7XG5cblx0ICAgICAgaWYgKHRoaXMub3B0aW9ucy5hbGxvd0RyYWdORHJvcCkge1xuXHQgICAgICAgIF9qcXVlcnkyWydkZWZhdWx0J10uZXZlbnQucHJvcHMucHVzaCgnZGF0YVRyYW5zZmVyJyk7XG5cdCAgICAgIH1cblxuXHQgICAgICB0aGlzLmJpbmRMaXN0ZW5lcnMoKTtcblxuXHQgICAgICBpZiAodGhpcy5vcHRpb25zLmltYWdlU3RhdGUgJiYgdGhpcy5vcHRpb25zLmltYWdlU3RhdGUuc3JjKSB7XG5cdCAgICAgICAgdGhpcy5sb2FkSW1hZ2UodGhpcy5vcHRpb25zLmltYWdlU3RhdGUuc3JjKTtcblx0ICAgICAgfVxuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2JpbmRMaXN0ZW5lcnMnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIGJpbmRMaXN0ZW5lcnMoKSB7XG5cdCAgICAgIHRoaXMuJGZpbGVJbnB1dC5vbignY2hhbmdlLmNyb3BpdCcsIHRoaXMub25GaWxlQ2hhbmdlLmJpbmQodGhpcykpO1xuXHQgICAgICB0aGlzLiRpbWFnZUNvbnRhaW5lci5vbihfY29uc3RhbnRzLkVWRU5UUy5QUkVWSUVXLCB0aGlzLm9uUHJldmlld0V2ZW50LmJpbmQodGhpcykpO1xuXHQgICAgICB0aGlzLiR6b29tU2xpZGVyLm9uKF9jb25zdGFudHMuRVZFTlRTLlpPT01fSU5QVVQsIHRoaXMub25ab29tU2xpZGVyQ2hhbmdlLmJpbmQodGhpcykpO1xuXG5cdCAgICAgIGlmICh0aGlzLm9wdGlvbnMuYWxsb3dEcmFnTkRyb3ApIHtcblx0ICAgICAgICB0aGlzLiRpbWFnZUNvbnRhaW5lci5vbignZHJhZ292ZXIuY3JvcGl0IGRyYWdsZWF2ZS5jcm9waXQnLCB0aGlzLm9uRHJhZ092ZXIuYmluZCh0aGlzKSk7XG5cdCAgICAgICAgdGhpcy4kaW1hZ2VDb250YWluZXIub24oJ2Ryb3AuY3JvcGl0JywgdGhpcy5vbkRyb3AuYmluZCh0aGlzKSk7XG5cdCAgICAgIH1cblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICd1bmJpbmRMaXN0ZW5lcnMnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIHVuYmluZExpc3RlbmVycygpIHtcblx0ICAgICAgdGhpcy4kZmlsZUlucHV0Lm9mZignY2hhbmdlLmNyb3BpdCcpO1xuXHQgICAgICB0aGlzLiRpbWFnZUNvbnRhaW5lci5vZmYoX2NvbnN0YW50cy5FVkVOVFMuUFJFVklFVyk7XG5cdCAgICAgIHRoaXMuJGltYWdlQ29udGFpbmVyLm9mZignZHJhZ292ZXIuY3JvcGl0IGRyYWdsZWF2ZS5jcm9waXQgZHJvcC5jcm9waXQnKTtcblx0ICAgICAgdGhpcy4kem9vbVNsaWRlci5vZmYoX2NvbnN0YW50cy5FVkVOVFMuWk9PTV9JTlBVVCk7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnb25GaWxlQ2hhbmdlJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBvbkZpbGVDaGFuZ2UoZSkge1xuXHQgICAgICB0aGlzLm9wdGlvbnMub25GaWxlQ2hhbmdlKGUpO1xuXG5cdCAgICAgIGlmICh0aGlzLiRmaWxlSW5wdXQuZ2V0KDApLmZpbGVzKSB7XG5cdCAgICAgICAgdGhpcy5sb2FkRmlsZSh0aGlzLiRmaWxlSW5wdXQuZ2V0KDApLmZpbGVzWzBdKTtcblx0ICAgICAgfVxuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2xvYWRGaWxlJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBsb2FkRmlsZShmaWxlKSB7XG5cdCAgICAgIHZhciBmaWxlUmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcblx0ICAgICAgaWYgKGZpbGUgJiYgZmlsZS50eXBlLm1hdGNoKCdpbWFnZScpKSB7XG5cdCAgICAgICAgZmlsZVJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuXHQgICAgICAgIGZpbGVSZWFkZXIub25sb2FkID0gdGhpcy5vbkZpbGVSZWFkZXJMb2FkZWQuYmluZCh0aGlzKTtcblx0ICAgICAgICBmaWxlUmVhZGVyLm9uZXJyb3IgPSB0aGlzLm9uRmlsZVJlYWRlckVycm9yLmJpbmQodGhpcyk7XG5cdCAgICAgIH0gZWxzZSBpZiAoZmlsZSkge1xuXHQgICAgICAgIHRoaXMub25GaWxlUmVhZGVyRXJyb3IoKTtcblx0ICAgICAgfVxuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ29uRmlsZVJlYWRlckxvYWRlZCcsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gb25GaWxlUmVhZGVyTG9hZGVkKGUpIHtcblx0ICAgICAgdGhpcy5sb2FkSW1hZ2UoZS50YXJnZXQucmVzdWx0KTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdvbkZpbGVSZWFkZXJFcnJvcicsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gb25GaWxlUmVhZGVyRXJyb3IoKSB7XG5cdCAgICAgIHRoaXMub3B0aW9ucy5vbkZpbGVSZWFkZXJFcnJvcigpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ29uRHJhZ092ZXInLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIG9uRHJhZ092ZXIoZSkge1xuXHQgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cdCAgICAgIGUuZGF0YVRyYW5zZmVyLmRyb3BFZmZlY3QgPSAnY29weSc7XG5cdCAgICAgIHRoaXMuJHByZXZpZXcudG9nZ2xlQ2xhc3MoX2NvbnN0YW50cy5DTEFTU19OQU1FUy5EUkFHX0hPVkVSRUQsIGUudHlwZSA9PT0gJ2RyYWdvdmVyJyk7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnb25Ecm9wJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBvbkRyb3AoZSkge1xuXHQgICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuXHQgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cdCAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cblx0ICAgICAgdmFyIGZpbGVzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoZS5kYXRhVHJhbnNmZXIuZmlsZXMsIDApO1xuXHQgICAgICBmaWxlcy5zb21lKGZ1bmN0aW9uIChmaWxlKSB7XG5cdCAgICAgICAgaWYgKCFmaWxlLnR5cGUubWF0Y2goJ2ltYWdlJykpIHtcblx0ICAgICAgICAgIHJldHVybiBmYWxzZTtcblx0ICAgICAgICB9XG5cblx0ICAgICAgICBfdGhpczIubG9hZEZpbGUoZmlsZSk7XG5cdCAgICAgICAgcmV0dXJuIHRydWU7XG5cdCAgICAgIH0pO1xuXG5cdCAgICAgIHRoaXMuJHByZXZpZXcucmVtb3ZlQ2xhc3MoX2NvbnN0YW50cy5DTEFTU19OQU1FUy5EUkFHX0hPVkVSRUQpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2xvYWRJbWFnZScsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gbG9hZEltYWdlKGltYWdlU3JjKSB7XG5cdCAgICAgIHZhciBfdGhpczMgPSB0aGlzO1xuXG5cdCAgICAgIGlmICghaW1hZ2VTcmMpIHtcblx0ICAgICAgICByZXR1cm47XG5cdCAgICAgIH1cblxuXHQgICAgICB0aGlzLm9wdGlvbnMub25JbWFnZUxvYWRpbmcoKTtcblx0ICAgICAgdGhpcy5zZXRJbWFnZUxvYWRpbmdDbGFzcygpO1xuXG5cdCAgICAgIGlmIChpbWFnZVNyYy5pbmRleE9mKCdkYXRhJykgPT09IDApIHtcblx0ICAgICAgICB0aGlzLnByZUltYWdlLnNyYyA9IGltYWdlU3JjO1xuXHQgICAgICB9IGVsc2Uge1xuXHQgICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblx0ICAgICAgICB4aHIub25sb2FkID0gZnVuY3Rpb24gKGUpIHtcblx0ICAgICAgICAgIGlmIChlLnRhcmdldC5zdGF0dXMgPj0gMzAwKSB7XG5cdCAgICAgICAgICAgIF90aGlzMy5vbkltYWdlRXJyb3IuY2FsbChfdGhpczMsIF9jb25zdGFudHMuRVJST1JTLklNQUdFX0ZBSUxFRF9UT19MT0FEKTtcblx0ICAgICAgICAgICAgcmV0dXJuO1xuXHQgICAgICAgICAgfVxuXG5cdCAgICAgICAgICBfdGhpczMubG9hZEZpbGUoZS50YXJnZXQucmVzcG9uc2UpO1xuXHQgICAgICAgIH07XG5cdCAgICAgICAgeGhyLm9wZW4oJ0dFVCcsIGltYWdlU3JjKTtcblx0ICAgICAgICB4aHIucmVzcG9uc2VUeXBlID0gJ2Jsb2InO1xuXHQgICAgICAgIHhoci5zZW5kKCk7XG5cdCAgICAgIH1cblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdvblByZUltYWdlTG9hZGVkJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBvblByZUltYWdlTG9hZGVkKCkge1xuXHQgICAgICBpZiAodGhpcy5zaG91bGRSZWplY3RJbWFnZSh7XG5cdCAgICAgICAgaW1hZ2VXaWR0aDogdGhpcy5wcmVJbWFnZS53aWR0aCxcblx0ICAgICAgICBpbWFnZUhlaWdodDogdGhpcy5wcmVJbWFnZS5oZWlnaHQsXG5cdCAgICAgICAgcHJldmlld1NpemU6IHRoaXMucHJldmlld1NpemUsXG5cdCAgICAgICAgbWF4Wm9vbTogdGhpcy5vcHRpb25zLm1heFpvb20sXG5cdCAgICAgICAgZXhwb3J0Wm9vbTogdGhpcy5vcHRpb25zLmV4cG9ydFpvb20sXG5cdCAgICAgICAgc21hbGxJbWFnZTogdGhpcy5vcHRpb25zLnNtYWxsSW1hZ2Vcblx0ICAgICAgfSkpIHtcblx0ICAgICAgICB0aGlzLm9uSW1hZ2VFcnJvcihfY29uc3RhbnRzLkVSUk9SUy5TTUFMTF9JTUFHRSk7XG5cdCAgICAgICAgaWYgKHRoaXMuaW1hZ2Uuc3JjKSB7XG5cdCAgICAgICAgICB0aGlzLnNldEltYWdlTG9hZGVkQ2xhc3MoKTtcblx0ICAgICAgICB9XG5cdCAgICAgICAgcmV0dXJuO1xuXHQgICAgICB9XG5cblx0ICAgICAgdGhpcy5pbWFnZS5zcmMgPSB0aGlzLnByZUltYWdlLnNyYztcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdvbkltYWdlTG9hZGVkJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBvbkltYWdlTG9hZGVkKCkge1xuXHQgICAgICB0aGlzLnJvdGF0aW9uID0gMDtcblx0ICAgICAgdGhpcy5zZXR1cFpvb21lcih0aGlzLm9wdGlvbnMuaW1hZ2VTdGF0ZSAmJiB0aGlzLm9wdGlvbnMuaW1hZ2VTdGF0ZS56b29tIHx8IHRoaXMuX2luaXRpYWxab29tKTtcblx0ICAgICAgaWYgKHRoaXMub3B0aW9ucy5pbWFnZVN0YXRlICYmIHRoaXMub3B0aW9ucy5pbWFnZVN0YXRlLm9mZnNldCkge1xuXHQgICAgICAgIHRoaXMub2Zmc2V0ID0gdGhpcy5vcHRpb25zLmltYWdlU3RhdGUub2Zmc2V0O1xuXHQgICAgICB9IGVsc2Uge1xuXHQgICAgICAgIHRoaXMuY2VudGVySW1hZ2UoKTtcblx0ICAgICAgfVxuXG5cdCAgICAgIHRoaXMub3B0aW9ucy5pbWFnZVN0YXRlID0ge307XG5cblx0ICAgICAgdGhpcy4kaW1hZ2UuYXR0cignc3JjJywgdGhpcy5pbWFnZS5zcmMpO1xuXHQgICAgICBpZiAodGhpcy5vcHRpb25zLmltYWdlQmFja2dyb3VuZCkge1xuXHQgICAgICAgIHRoaXMuJGJnLmF0dHIoJ3NyYycsIHRoaXMuaW1hZ2Uuc3JjKTtcblx0ICAgICAgfVxuXG5cdCAgICAgIHRoaXMuc2V0SW1hZ2VMb2FkZWRDbGFzcygpO1xuXG5cdCAgICAgIHRoaXMuaW1hZ2VMb2FkZWQgPSB0cnVlO1xuXG5cdCAgICAgIHRoaXMub3B0aW9ucy5vbkltYWdlTG9hZGVkKCk7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnb25JbWFnZUVycm9yJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBvbkltYWdlRXJyb3IoKSB7XG5cdCAgICAgIHRoaXMub3B0aW9ucy5vbkltYWdlRXJyb3IuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0ICAgICAgdGhpcy5yZW1vdmVJbWFnZUxvYWRpbmdDbGFzcygpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ3NldEltYWdlTG9hZGluZ0NsYXNzJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRJbWFnZUxvYWRpbmdDbGFzcygpIHtcblx0ICAgICAgdGhpcy4kcHJldmlldy5yZW1vdmVDbGFzcyhfY29uc3RhbnRzLkNMQVNTX05BTUVTLklNQUdFX0xPQURFRCkuYWRkQ2xhc3MoX2NvbnN0YW50cy5DTEFTU19OQU1FUy5JTUFHRV9MT0FESU5HKTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdzZXRJbWFnZUxvYWRlZENsYXNzJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRJbWFnZUxvYWRlZENsYXNzKCkge1xuXHQgICAgICB0aGlzLiRwcmV2aWV3LnJlbW92ZUNsYXNzKF9jb25zdGFudHMuQ0xBU1NfTkFNRVMuSU1BR0VfTE9BRElORykuYWRkQ2xhc3MoX2NvbnN0YW50cy5DTEFTU19OQU1FUy5JTUFHRV9MT0FERUQpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ3JlbW92ZUltYWdlTG9hZGluZ0NsYXNzJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiByZW1vdmVJbWFnZUxvYWRpbmdDbGFzcygpIHtcblx0ICAgICAgdGhpcy4kcHJldmlldy5yZW1vdmVDbGFzcyhfY29uc3RhbnRzLkNMQVNTX05BTUVTLklNQUdFX0xPQURJTkcpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2dldEV2ZW50UG9zaXRpb24nLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIGdldEV2ZW50UG9zaXRpb24oZSkge1xuXHQgICAgICBpZiAoZS5vcmlnaW5hbEV2ZW50ICYmIGUub3JpZ2luYWxFdmVudC50b3VjaGVzICYmIGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdKSB7XG5cdCAgICAgICAgZSA9IGUub3JpZ2luYWxFdmVudC50b3VjaGVzWzBdO1xuXHQgICAgICB9XG5cdCAgICAgIGlmIChlLmNsaWVudFggJiYgZS5jbGllbnRZKSB7XG5cdCAgICAgICAgcmV0dXJuIHsgeDogZS5jbGllbnRYLCB5OiBlLmNsaWVudFkgfTtcblx0ICAgICAgfVxuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ29uUHJldmlld0V2ZW50Jyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBvblByZXZpZXdFdmVudChlKSB7XG5cdCAgICAgIGlmICghdGhpcy5pbWFnZUxvYWRlZCkge1xuXHQgICAgICAgIHJldHVybjtcblx0ICAgICAgfVxuXG5cdCAgICAgIHRoaXMubW92ZUNvbnRpbnVlID0gZmFsc2U7XG5cdCAgICAgIHRoaXMuJGltYWdlQ29udGFpbmVyLm9mZihfY29uc3RhbnRzLkVWRU5UUy5QUkVWSUVXX01PVkUpO1xuXG5cdCAgICAgIGlmIChlLnR5cGUgPT09ICdtb3VzZWRvd24nIHx8IGUudHlwZSA9PT0gJ3RvdWNoc3RhcnQnKSB7XG5cdCAgICAgICAgdGhpcy5vcmlnaW4gPSB0aGlzLmdldEV2ZW50UG9zaXRpb24oZSk7XG5cdCAgICAgICAgdGhpcy5tb3ZlQ29udGludWUgPSB0cnVlO1xuXHQgICAgICAgIHRoaXMuJGltYWdlQ29udGFpbmVyLm9uKF9jb25zdGFudHMuRVZFTlRTLlBSRVZJRVdfTU9WRSwgdGhpcy5vbk1vdmUuYmluZCh0aGlzKSk7XG5cdCAgICAgIH0gZWxzZSB7XG5cdCAgICAgICAgKDAsIF9qcXVlcnkyWydkZWZhdWx0J10pKGRvY3VtZW50LmJvZHkpLmZvY3VzKCk7XG5cdCAgICAgIH1cblxuXHQgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuXHQgICAgICByZXR1cm4gZmFsc2U7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnb25Nb3ZlJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBvbk1vdmUoZSkge1xuXHQgICAgICB2YXIgZXZlbnRQb3NpdGlvbiA9IHRoaXMuZ2V0RXZlbnRQb3NpdGlvbihlKTtcblxuXHQgICAgICBpZiAodGhpcy5tb3ZlQ29udGludWUgJiYgZXZlbnRQb3NpdGlvbikge1xuXHQgICAgICAgIHRoaXMub2Zmc2V0ID0ge1xuXHQgICAgICAgICAgeDogdGhpcy5vZmZzZXQueCArIGV2ZW50UG9zaXRpb24ueCAtIHRoaXMub3JpZ2luLngsXG5cdCAgICAgICAgICB5OiB0aGlzLm9mZnNldC55ICsgZXZlbnRQb3NpdGlvbi55IC0gdGhpcy5vcmlnaW4ueVxuXHQgICAgICAgIH07XG5cdCAgICAgIH1cblxuXHQgICAgICB0aGlzLm9yaWdpbiA9IGV2ZW50UG9zaXRpb247XG5cblx0ICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcblx0ICAgICAgcmV0dXJuIGZhbHNlO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2ZpeE9mZnNldCcsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gZml4T2Zmc2V0KG9mZnNldCkge1xuXHQgICAgICBpZiAoIXRoaXMuaW1hZ2VMb2FkZWQpIHtcblx0ICAgICAgICByZXR1cm4gb2Zmc2V0O1xuXHQgICAgICB9XG5cblx0ICAgICAgdmFyIHJldCA9IHsgeDogb2Zmc2V0LngsIHk6IG9mZnNldC55IH07XG5cblx0ICAgICAgaWYgKCF0aGlzLm9wdGlvbnMuZnJlZU1vdmUpIHtcblx0ICAgICAgICBpZiAodGhpcy5pbWFnZVdpZHRoICogdGhpcy56b29tID49IHRoaXMucHJldmlld1NpemUud2lkdGgpIHtcblx0ICAgICAgICAgIHJldC54ID0gTWF0aC5taW4oMCwgTWF0aC5tYXgocmV0LngsIHRoaXMucHJldmlld1NpemUud2lkdGggLSB0aGlzLmltYWdlV2lkdGggKiB0aGlzLnpvb20pKTtcblx0ICAgICAgICB9IGVsc2Uge1xuXHQgICAgICAgICAgcmV0LnggPSBNYXRoLm1heCgwLCBNYXRoLm1pbihyZXQueCwgdGhpcy5wcmV2aWV3U2l6ZS53aWR0aCAtIHRoaXMuaW1hZ2VXaWR0aCAqIHRoaXMuem9vbSkpO1xuXHQgICAgICAgIH1cblxuXHQgICAgICAgIGlmICh0aGlzLmltYWdlSGVpZ2h0ICogdGhpcy56b29tID49IHRoaXMucHJldmlld1NpemUuaGVpZ2h0KSB7XG5cdCAgICAgICAgICByZXQueSA9IE1hdGgubWluKDAsIE1hdGgubWF4KHJldC55LCB0aGlzLnByZXZpZXdTaXplLmhlaWdodCAtIHRoaXMuaW1hZ2VIZWlnaHQgKiB0aGlzLnpvb20pKTtcblx0ICAgICAgICB9IGVsc2Uge1xuXHQgICAgICAgICAgcmV0LnkgPSBNYXRoLm1heCgwLCBNYXRoLm1pbihyZXQueSwgdGhpcy5wcmV2aWV3U2l6ZS5oZWlnaHQgLSB0aGlzLmltYWdlSGVpZ2h0ICogdGhpcy56b29tKSk7XG5cdCAgICAgICAgfVxuXHQgICAgICB9XG5cblx0ICAgICAgcmV0LnggPSAoMCwgX3V0aWxzLnJvdW5kKShyZXQueCk7XG5cdCAgICAgIHJldC55ID0gKDAsIF91dGlscy5yb3VuZCkocmV0LnkpO1xuXG5cdCAgICAgIHJldHVybiByZXQ7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnY2VudGVySW1hZ2UnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIGNlbnRlckltYWdlKCkge1xuXHQgICAgICBpZiAoIXRoaXMuaW1hZ2Uud2lkdGggfHwgIXRoaXMuaW1hZ2UuaGVpZ2h0IHx8ICF0aGlzLnpvb20pIHtcblx0ICAgICAgICByZXR1cm47XG5cdCAgICAgIH1cblxuXHQgICAgICB0aGlzLm9mZnNldCA9IHtcblx0ICAgICAgICB4OiAodGhpcy5wcmV2aWV3U2l6ZS53aWR0aCAtIHRoaXMuaW1hZ2VXaWR0aCAqIHRoaXMuem9vbSkgLyAyLFxuXHQgICAgICAgIHk6ICh0aGlzLnByZXZpZXdTaXplLmhlaWdodCAtIHRoaXMuaW1hZ2VIZWlnaHQgKiB0aGlzLnpvb20pIC8gMlxuXHQgICAgICB9O1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ29uWm9vbVNsaWRlckNoYW5nZScsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gb25ab29tU2xpZGVyQ2hhbmdlKCkge1xuXHQgICAgICBpZiAoIXRoaXMuaW1hZ2VMb2FkZWQpIHtcblx0ICAgICAgICByZXR1cm47XG5cdCAgICAgIH1cblxuXHQgICAgICB0aGlzLnpvb21TbGlkZXJQb3MgPSBOdW1iZXIodGhpcy4kem9vbVNsaWRlci52YWwoKSk7XG5cdCAgICAgIHZhciBuZXdab29tID0gdGhpcy56b29tZXIuZ2V0Wm9vbSh0aGlzLnpvb21TbGlkZXJQb3MpO1xuXHQgICAgICBpZiAobmV3Wm9vbSA9PT0gdGhpcy56b29tKSB7XG5cdCAgICAgICAgcmV0dXJuO1xuXHQgICAgICB9XG5cdCAgICAgIHRoaXMuem9vbSA9IG5ld1pvb207XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnZW5hYmxlWm9vbVNsaWRlcicsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gZW5hYmxlWm9vbVNsaWRlcigpIHtcblx0ICAgICAgdGhpcy4kem9vbVNsaWRlci5yZW1vdmVBdHRyKCdkaXNhYmxlZCcpO1xuXHQgICAgICB0aGlzLm9wdGlvbnMub25ab29tRW5hYmxlZCgpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2Rpc2FibGVab29tU2xpZGVyJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBkaXNhYmxlWm9vbVNsaWRlcigpIHtcblx0ICAgICAgdGhpcy4kem9vbVNsaWRlci5hdHRyKCdkaXNhYmxlZCcsIHRydWUpO1xuXHQgICAgICB0aGlzLm9wdGlvbnMub25ab29tRGlzYWJsZWQoKTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdzZXR1cFpvb21lcicsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gc2V0dXBab29tZXIoem9vbSkge1xuXHQgICAgICB0aGlzLnpvb21lci5zZXR1cCh7XG5cdCAgICAgICAgaW1hZ2VTaXplOiB0aGlzLmltYWdlU2l6ZSxcblx0ICAgICAgICBwcmV2aWV3U2l6ZTogdGhpcy5wcmV2aWV3U2l6ZSxcblx0ICAgICAgICBleHBvcnRab29tOiB0aGlzLm9wdGlvbnMuZXhwb3J0Wm9vbSxcblx0ICAgICAgICBtYXhab29tOiB0aGlzLm9wdGlvbnMubWF4Wm9vbSxcblx0ICAgICAgICBtaW5ab29tOiB0aGlzLm9wdGlvbnMubWluWm9vbSxcblx0ICAgICAgICBzbWFsbEltYWdlOiB0aGlzLm9wdGlvbnMuc21hbGxJbWFnZVxuXHQgICAgICB9KTtcblx0ICAgICAgdGhpcy56b29tID0gKDAsIF91dGlscy5leGlzdHMpKHpvb20pID8gem9vbSA6IHRoaXMuX3pvb207XG5cblx0ICAgICAgaWYgKHRoaXMuaXNab29tYWJsZSgpKSB7XG5cdCAgICAgICAgdGhpcy5lbmFibGVab29tU2xpZGVyKCk7XG5cdCAgICAgIH0gZWxzZSB7XG5cdCAgICAgICAgdGhpcy5kaXNhYmxlWm9vbVNsaWRlcigpO1xuXHQgICAgICB9XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnZml4Wm9vbScsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gZml4Wm9vbSh6b29tKSB7XG5cdCAgICAgIHJldHVybiB0aGlzLnpvb21lci5maXhab29tKHpvb20pO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2lzWm9vbWFibGUnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIGlzWm9vbWFibGUoKSB7XG5cdCAgICAgIHJldHVybiB0aGlzLnpvb21lci5pc1pvb21hYmxlKCk7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAncmVuZGVySW1hZ2UnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckltYWdlKCkge1xuXHQgICAgICB2YXIgdHJhbnNmb3JtYXRpb24gPSAnXFxuICAgICAgdHJhbnNsYXRlKCcgKyB0aGlzLnJvdGF0ZWRPZmZzZXQueCArICdweCwgJyArIHRoaXMucm90YXRlZE9mZnNldC55ICsgJ3B4KVxcbiAgICAgIHNjYWxlKCcgKyB0aGlzLnpvb20gKyAnKVxcbiAgICAgIHJvdGF0ZSgnICsgdGhpcy5yb3RhdGlvbiArICdkZWcpJztcblxuXHQgICAgICB0aGlzLiRpbWFnZS5jc3Moe1xuXHQgICAgICAgIHRyYW5zZm9ybTogdHJhbnNmb3JtYXRpb24sXG5cdCAgICAgICAgd2Via2l0VHJhbnNmb3JtOiB0cmFuc2Zvcm1hdGlvblxuXHQgICAgICB9KTtcblx0ICAgICAgaWYgKHRoaXMub3B0aW9ucy5pbWFnZUJhY2tncm91bmQpIHtcblx0ICAgICAgICB0aGlzLiRiZy5jc3Moe1xuXHQgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2Zvcm1hdGlvbixcblx0ICAgICAgICAgIHdlYmtpdFRyYW5zZm9ybTogdHJhbnNmb3JtYXRpb25cblx0ICAgICAgICB9KTtcblx0ICAgICAgfVxuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ3JvdGF0ZUNXJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiByb3RhdGVDVygpIHtcblx0ICAgICAgaWYgKHRoaXMuc2hvdWxkUmVqZWN0SW1hZ2Uoe1xuXHQgICAgICAgIGltYWdlV2lkdGg6IHRoaXMuaW1hZ2UuaGVpZ2h0LFxuXHQgICAgICAgIGltYWdlSGVpZ2h0OiB0aGlzLmltYWdlLndpZHRoLFxuXHQgICAgICAgIHByZXZpZXdTaXplOiB0aGlzLnByZXZpZXdTaXplLFxuXHQgICAgICAgIG1heFpvb206IHRoaXMub3B0aW9ucy5tYXhab29tLFxuXHQgICAgICAgIGV4cG9ydFpvb206IHRoaXMub3B0aW9ucy5leHBvcnRab29tLFxuXHQgICAgICAgIHNtYWxsSW1hZ2U6IHRoaXMub3B0aW9ucy5zbWFsbEltYWdlXG5cdCAgICAgIH0pKSB7XG5cdCAgICAgICAgdGhpcy5yb3RhdGlvbiA9ICh0aGlzLnJvdGF0aW9uICsgMTgwKSAlIDM2MDtcblx0ICAgICAgfSBlbHNlIHtcblx0ICAgICAgICB0aGlzLnJvdGF0aW9uID0gKHRoaXMucm90YXRpb24gKyA5MCkgJSAzNjA7XG5cdCAgICAgIH1cblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdyb3RhdGVDQ1cnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIHJvdGF0ZUNDVygpIHtcblx0ICAgICAgaWYgKHRoaXMuc2hvdWxkUmVqZWN0SW1hZ2Uoe1xuXHQgICAgICAgIGltYWdlV2lkdGg6IHRoaXMuaW1hZ2UuaGVpZ2h0LFxuXHQgICAgICAgIGltYWdlSGVpZ2h0OiB0aGlzLmltYWdlLndpZHRoLFxuXHQgICAgICAgIHByZXZpZXdTaXplOiB0aGlzLnByZXZpZXdTaXplLFxuXHQgICAgICAgIG1heFpvb206IHRoaXMub3B0aW9ucy5tYXhab29tLFxuXHQgICAgICAgIGV4cG9ydFpvb206IHRoaXMub3B0aW9ucy5leHBvcnRab29tLFxuXHQgICAgICAgIHNtYWxsSW1hZ2U6IHRoaXMub3B0aW9ucy5zbWFsbEltYWdlXG5cdCAgICAgIH0pKSB7XG5cdCAgICAgICAgdGhpcy5yb3RhdGlvbiA9ICh0aGlzLnJvdGF0aW9uICsgMTgwKSAlIDM2MDtcblx0ICAgICAgfSBlbHNlIHtcblx0ICAgICAgICB0aGlzLnJvdGF0aW9uID0gKHRoaXMucm90YXRpb24gKyAyNzApICUgMzYwO1xuXHQgICAgICB9XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnc2hvdWxkUmVqZWN0SW1hZ2UnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIHNob3VsZFJlamVjdEltYWdlKF9yZWYpIHtcblx0ICAgICAgdmFyIGltYWdlV2lkdGggPSBfcmVmLmltYWdlV2lkdGg7XG5cdCAgICAgIHZhciBpbWFnZUhlaWdodCA9IF9yZWYuaW1hZ2VIZWlnaHQ7XG5cdCAgICAgIHZhciBwcmV2aWV3U2l6ZSA9IF9yZWYucHJldmlld1NpemU7XG5cdCAgICAgIHZhciBtYXhab29tID0gX3JlZi5tYXhab29tO1xuXHQgICAgICB2YXIgZXhwb3J0Wm9vbSA9IF9yZWYuZXhwb3J0Wm9vbTtcblx0ICAgICAgdmFyIHNtYWxsSW1hZ2UgPSBfcmVmLnNtYWxsSW1hZ2U7XG5cblx0ICAgICAgaWYgKHNtYWxsSW1hZ2UgIT09ICdyZWplY3QnKSB7XG5cdCAgICAgICAgcmV0dXJuIGZhbHNlO1xuXHQgICAgICB9XG5cblx0ICAgICAgcmV0dXJuIGltYWdlV2lkdGggKiBtYXhab29tIDwgcHJldmlld1NpemUud2lkdGggKiBleHBvcnRab29tIHx8IGltYWdlSGVpZ2h0ICogbWF4Wm9vbSA8IHByZXZpZXdTaXplLmhlaWdodCAqIGV4cG9ydFpvb207XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnZ2V0Q3JvcHBlZEltYWdlRGF0YScsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0Q3JvcHBlZEltYWdlRGF0YShleHBvcnRPcHRpb25zKSB7XG5cdCAgICAgIGlmICghdGhpcy5pbWFnZS5zcmMpIHtcblx0ICAgICAgICByZXR1cm47XG5cdCAgICAgIH1cblxuXHQgICAgICB2YXIgZXhwb3J0RGVmYXVsdHMgPSB7XG5cdCAgICAgICAgdHlwZTogJ2ltYWdlL3BuZycsXG5cdCAgICAgICAgcXVhbGl0eTogMC43NSxcblx0ICAgICAgICBvcmlnaW5hbFNpemU6IGZhbHNlLFxuXHQgICAgICAgIGZpbGxCZzogJyNmZmYnXG5cdCAgICAgIH07XG5cdCAgICAgIGV4cG9ydE9wdGlvbnMgPSBfanF1ZXJ5MlsnZGVmYXVsdCddLmV4dGVuZCh7fSwgZXhwb3J0RGVmYXVsdHMsIGV4cG9ydE9wdGlvbnMpO1xuXG5cdCAgICAgIHZhciBleHBvcnRab29tID0gZXhwb3J0T3B0aW9ucy5vcmlnaW5hbFNpemUgPyAxIC8gdGhpcy56b29tIDogdGhpcy5vcHRpb25zLmV4cG9ydFpvb207XG5cblx0ICAgICAgdmFyIHpvb21lZFNpemUgPSB7XG5cdCAgICAgICAgd2lkdGg6IHRoaXMuem9vbSAqIGV4cG9ydFpvb20gKiB0aGlzLmltYWdlLndpZHRoLFxuXHQgICAgICAgIGhlaWdodDogdGhpcy56b29tICogZXhwb3J0Wm9vbSAqIHRoaXMuaW1hZ2UuaGVpZ2h0XG5cdCAgICAgIH07XG5cblx0ICAgICAgdmFyIGNhbnZhcyA9ICgwLCBfanF1ZXJ5MlsnZGVmYXVsdCddKSgnPGNhbnZhcyAvPicpLmF0dHIoe1xuXHQgICAgICAgIHdpZHRoOiB0aGlzLnByZXZpZXdTaXplLndpZHRoICogZXhwb3J0Wm9vbSxcblx0ICAgICAgICBoZWlnaHQ6IHRoaXMucHJldmlld1NpemUuaGVpZ2h0ICogZXhwb3J0Wm9vbVxuXHQgICAgICB9KS5nZXQoMCk7XG5cdCAgICAgIHZhciBjYW52YXNDb250ZXh0ID0gY2FudmFzLmdldENvbnRleHQoJzJkJyk7XG5cblx0ICAgICAgaWYgKGV4cG9ydE9wdGlvbnMudHlwZSA9PT0gJ2ltYWdlL2pwZWcnKSB7XG5cdCAgICAgICAgY2FudmFzQ29udGV4dC5maWxsU3R5bGUgPSBleHBvcnRPcHRpb25zLmZpbGxCZztcblx0ICAgICAgICBjYW52YXNDb250ZXh0LmZpbGxSZWN0KDAsIDAsIGNhbnZhcy53aWR0aCwgY2FudmFzLmhlaWdodCk7XG5cdCAgICAgIH1cblxuXHQgICAgICBjYW52YXNDb250ZXh0LnRyYW5zbGF0ZSh0aGlzLnJvdGF0ZWRPZmZzZXQueCAqIGV4cG9ydFpvb20sIHRoaXMucm90YXRlZE9mZnNldC55ICogZXhwb3J0Wm9vbSk7XG5cdCAgICAgIGNhbnZhc0NvbnRleHQucm90YXRlKHRoaXMucm90YXRpb24gKiBNYXRoLlBJIC8gMTgwKTtcblx0ICAgICAgY2FudmFzQ29udGV4dC5kcmF3SW1hZ2UodGhpcy5pbWFnZSwgMCwgMCwgem9vbWVkU2l6ZS53aWR0aCwgem9vbWVkU2l6ZS5oZWlnaHQpO1xuXG5cdCAgICAgIHJldHVybiBjYW52YXMudG9EYXRhVVJMKGV4cG9ydE9wdGlvbnMudHlwZSwgZXhwb3J0T3B0aW9ucy5xdWFsaXR5KTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdkaXNhYmxlJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBkaXNhYmxlKCkge1xuXHQgICAgICB0aGlzLnVuYmluZExpc3RlbmVycygpO1xuXHQgICAgICB0aGlzLmRpc2FibGVab29tU2xpZGVyKCk7XG5cdCAgICAgIHRoaXMuJGVsLmFkZENsYXNzKF9jb25zdGFudHMuQ0xBU1NfTkFNRVMuRElTQUJMRUQpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ3JlZW5hYmxlJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiByZWVuYWJsZSgpIHtcblx0ICAgICAgdGhpcy5iaW5kTGlzdGVuZXJzKCk7XG5cdCAgICAgIHRoaXMuZW5hYmxlWm9vbVNsaWRlcigpO1xuXHQgICAgICB0aGlzLiRlbC5yZW1vdmVDbGFzcyhfY29uc3RhbnRzLkNMQVNTX05BTUVTLkRJU0FCTEVEKTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICckJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiAkKHNlbGVjdG9yKSB7XG5cdCAgICAgIGlmICghdGhpcy4kZWwpIHtcblx0ICAgICAgICByZXR1cm4gbnVsbDtcblx0ICAgICAgfVxuXHQgICAgICByZXR1cm4gdGhpcy4kZWwuZmluZChzZWxlY3Rvcik7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnb2Zmc2V0Jyxcblx0ICAgIHNldDogZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG5cdCAgICAgIGlmICghcG9zaXRpb24gfHwgISgwLCBfdXRpbHMuZXhpc3RzKShwb3NpdGlvbi54KSB8fCAhKDAsIF91dGlscy5leGlzdHMpKHBvc2l0aW9uLnkpKSB7XG5cdCAgICAgICAgcmV0dXJuO1xuXHQgICAgICB9XG5cblx0ICAgICAgdGhpcy5fb2Zmc2V0ID0gdGhpcy5maXhPZmZzZXQocG9zaXRpb24pO1xuXHQgICAgICB0aGlzLnJlbmRlckltYWdlKCk7XG5cblx0ICAgICAgdGhpcy5vcHRpb25zLm9uT2Zmc2V0Q2hhbmdlKHBvc2l0aW9uKTtcblx0ICAgIH0sXG5cdCAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcblx0ICAgICAgcmV0dXJuIHRoaXMuX29mZnNldDtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICd6b29tJyxcblx0ICAgIHNldDogZnVuY3Rpb24gKG5ld1pvb20pIHtcblx0ICAgICAgbmV3Wm9vbSA9IHRoaXMuZml4Wm9vbShuZXdab29tKTtcblxuXHQgICAgICBpZiAodGhpcy5pbWFnZUxvYWRlZCkge1xuXHQgICAgICAgIHZhciBvbGRab29tID0gdGhpcy56b29tO1xuXG5cdCAgICAgICAgdmFyIG5ld1ggPSB0aGlzLnByZXZpZXdTaXplLndpZHRoIC8gMiAtICh0aGlzLnByZXZpZXdTaXplLndpZHRoIC8gMiAtIHRoaXMub2Zmc2V0LngpICogbmV3Wm9vbSAvIG9sZFpvb207XG5cdCAgICAgICAgdmFyIG5ld1kgPSB0aGlzLnByZXZpZXdTaXplLmhlaWdodCAvIDIgLSAodGhpcy5wcmV2aWV3U2l6ZS5oZWlnaHQgLyAyIC0gdGhpcy5vZmZzZXQueSkgKiBuZXdab29tIC8gb2xkWm9vbTtcblxuXHQgICAgICAgIHRoaXMuX3pvb20gPSBuZXdab29tO1xuXHQgICAgICAgIHRoaXMub2Zmc2V0ID0geyB4OiBuZXdYLCB5OiBuZXdZIH07IC8vIFRyaWdnZXJzIHJlbmRlckltYWdlKClcblx0ICAgICAgfSBlbHNlIHtcblx0ICAgICAgICB0aGlzLl96b29tID0gbmV3Wm9vbTtcblx0ICAgICAgfVxuXG5cdCAgICAgIHRoaXMuem9vbVNsaWRlclBvcyA9IHRoaXMuem9vbWVyLmdldFNsaWRlclBvcyh0aGlzLnpvb20pO1xuXHQgICAgICB0aGlzLiR6b29tU2xpZGVyLnZhbCh0aGlzLnpvb21TbGlkZXJQb3MpO1xuXG5cdCAgICAgIHRoaXMub3B0aW9ucy5vblpvb21DaGFuZ2UobmV3Wm9vbSk7XG5cdCAgICB9LFxuXHQgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG5cdCAgICAgIHJldHVybiB0aGlzLl96b29tO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ3JvdGF0ZWRPZmZzZXQnLFxuXHQgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG5cdCAgICAgIHJldHVybiB7XG5cdCAgICAgICAgeDogdGhpcy5vZmZzZXQueCArICh0aGlzLnJvdGF0aW9uID09PSA5MCA/IHRoaXMuaW1hZ2UuaGVpZ2h0ICogdGhpcy56b29tIDogMCkgKyAodGhpcy5yb3RhdGlvbiA9PT0gMTgwID8gdGhpcy5pbWFnZS53aWR0aCAqIHRoaXMuem9vbSA6IDApLFxuXHQgICAgICAgIHk6IHRoaXMub2Zmc2V0LnkgKyAodGhpcy5yb3RhdGlvbiA9PT0gMTgwID8gdGhpcy5pbWFnZS5oZWlnaHQgKiB0aGlzLnpvb20gOiAwKSArICh0aGlzLnJvdGF0aW9uID09PSAyNzAgPyB0aGlzLmltYWdlLndpZHRoICogdGhpcy56b29tIDogMClcblx0ICAgICAgfTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdyb3RhdGlvbicsXG5cdCAgICBzZXQ6IGZ1bmN0aW9uIChuZXdSb3RhdGlvbikge1xuXHQgICAgICB0aGlzLl9yb3RhdGlvbiA9IG5ld1JvdGF0aW9uO1xuXG5cdCAgICAgIGlmICh0aGlzLmltYWdlTG9hZGVkKSB7XG5cdCAgICAgICAgLy8gQ2hhbmdlIGluIGltYWdlIHNpemUgbWF5IGxlYWQgdG8gY2hhbmdlIGluIHpvb20gcmFuZ2Vcblx0ICAgICAgICB0aGlzLnNldHVwWm9vbWVyKCk7XG5cdCAgICAgIH1cblx0ICAgIH0sXG5cdCAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcblx0ICAgICAgcmV0dXJuIHRoaXMuX3JvdGF0aW9uO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2ltYWdlU3RhdGUnLFxuXHQgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG5cdCAgICAgIHJldHVybiB7XG5cdCAgICAgICAgc3JjOiB0aGlzLmltYWdlLnNyYyxcblx0ICAgICAgICBvZmZzZXQ6IHRoaXMub2Zmc2V0LFxuXHQgICAgICAgIHpvb206IHRoaXMuem9vbVxuXHQgICAgICB9O1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2ltYWdlU3JjJyxcblx0ICAgIGdldDogZnVuY3Rpb24gKCkge1xuXHQgICAgICByZXR1cm4gdGhpcy5pbWFnZS5zcmM7XG5cdCAgICB9LFxuXHQgICAgc2V0OiBmdW5jdGlvbiAoaW1hZ2VTcmMpIHtcblx0ICAgICAgdGhpcy5sb2FkSW1hZ2UoaW1hZ2VTcmMpO1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2ltYWdlV2lkdGgnLFxuXHQgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG5cdCAgICAgIHJldHVybiB0aGlzLnJvdGF0aW9uICUgMTgwID09PSAwID8gdGhpcy5pbWFnZS53aWR0aCA6IHRoaXMuaW1hZ2UuaGVpZ2h0O1xuXHQgICAgfVxuXHQgIH0sIHtcblx0ICAgIGtleTogJ2ltYWdlSGVpZ2h0Jyxcblx0ICAgIGdldDogZnVuY3Rpb24gKCkge1xuXHQgICAgICByZXR1cm4gdGhpcy5yb3RhdGlvbiAlIDE4MCA9PT0gMCA/IHRoaXMuaW1hZ2UuaGVpZ2h0IDogdGhpcy5pbWFnZS53aWR0aDtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdpbWFnZVNpemUnLFxuXHQgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG5cdCAgICAgIHJldHVybiB7XG5cdCAgICAgICAgd2lkdGg6IHRoaXMuaW1hZ2VXaWR0aCxcblx0ICAgICAgICBoZWlnaHQ6IHRoaXMuaW1hZ2VIZWlnaHRcblx0ICAgICAgfTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdpbml0aWFsWm9vbScsXG5cdCAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcblx0ICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5pbml0aWFsWm9vbTtcblx0ICAgIH0sXG5cdCAgICBzZXQ6IGZ1bmN0aW9uIChpbml0aWFsWm9vbU9wdGlvbikge1xuXHQgICAgICB0aGlzLm9wdGlvbnMuaW5pdGlhbFpvb20gPSBpbml0aWFsWm9vbU9wdGlvbjtcblx0ICAgICAgaWYgKGluaXRpYWxab29tT3B0aW9uID09PSAnbWluJykge1xuXHQgICAgICAgIHRoaXMuX2luaXRpYWxab29tID0gMDsgLy8gV2lsbCBiZSBmaXhlZCB3aGVuIGltYWdlIGxvYWRzXG5cdCAgICAgIH0gZWxzZSBpZiAoaW5pdGlhbFpvb21PcHRpb24gPT09ICdpbWFnZScpIHtcblx0ICAgICAgICB0aGlzLl9pbml0aWFsWm9vbSA9IDE7XG5cdCAgICAgIH0gZWxzZSB7XG5cdCAgICAgICAgdGhpcy5faW5pdGlhbFpvb20gPSAwO1xuXHQgICAgICB9XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnZXhwb3J0Wm9vbScsXG5cdCAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcblx0ICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5leHBvcnRab29tO1xuXHQgICAgfSxcblx0ICAgIHNldDogZnVuY3Rpb24gKGV4cG9ydFpvb20pIHtcblx0ICAgICAgdGhpcy5vcHRpb25zLmV4cG9ydFpvb20gPSBleHBvcnRab29tO1xuXHQgICAgICB0aGlzLnNldHVwWm9vbWVyKCk7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnbWluWm9vbScsXG5cdCAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcblx0ICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5taW5ab29tO1xuXHQgICAgfSxcblx0ICAgIHNldDogZnVuY3Rpb24gKG1pblpvb20pIHtcblx0ICAgICAgdGhpcy5vcHRpb25zLm1pblpvb20gPSBtaW5ab29tO1xuXHQgICAgICB0aGlzLnNldHVwWm9vbWVyKCk7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnbWF4Wm9vbScsXG5cdCAgICBnZXQ6IGZ1bmN0aW9uICgpIHtcblx0ICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5tYXhab29tO1xuXHQgICAgfSxcblx0ICAgIHNldDogZnVuY3Rpb24gKG1heFpvb20pIHtcblx0ICAgICAgdGhpcy5vcHRpb25zLm1heFpvb20gPSBtYXhab29tO1xuXHQgICAgICB0aGlzLnNldHVwWm9vbWVyKCk7XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAncHJldmlld1NpemUnLFxuXHQgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG5cdCAgICAgIHJldHVybiB0aGlzLl9wcmV2aWV3U2l6ZTtcblx0ICAgIH0sXG5cdCAgICBzZXQ6IGZ1bmN0aW9uIChzaXplKSB7XG5cdCAgICAgIGlmICghc2l6ZSB8fCBzaXplLndpZHRoIDw9IDAgfHwgc2l6ZS5oZWlnaHQgPD0gMCkge1xuXHQgICAgICAgIHJldHVybjtcblx0ICAgICAgfVxuXG5cdCAgICAgIHRoaXMuX3ByZXZpZXdTaXplID0ge1xuXHQgICAgICAgIHdpZHRoOiBzaXplLndpZHRoLFxuXHQgICAgICAgIGhlaWdodDogc2l6ZS5oZWlnaHRcblx0ICAgICAgfTtcblx0ICAgICAgdGhpcy4kcHJldmlldy5jc3Moe1xuXHQgICAgICAgIHdpZHRoOiB0aGlzLnByZXZpZXdTaXplLndpZHRoLFxuXHQgICAgICAgIGhlaWdodDogdGhpcy5wcmV2aWV3U2l6ZS5oZWlnaHRcblx0ICAgICAgfSk7XG5cblx0ICAgICAgaWYgKHRoaXMuaW1hZ2VMb2FkZWQpIHtcblx0ICAgICAgICB0aGlzLnNldHVwWm9vbWVyKCk7XG5cdCAgICAgIH1cblx0ICAgIH1cblx0ICB9XSk7XG5cblx0ICByZXR1cm4gQ3JvcGl0O1xuXHR9KSgpO1xuXG5cdGV4cG9ydHNbJ2RlZmF1bHQnXSA9IENyb3BpdDtcblx0bW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzWydkZWZhdWx0J107XG5cbi8qKiovIH0sXG4vKiAzICovXG4vKioqLyBmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMpIHtcblxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7XG5cdCAgdmFsdWU6IHRydWVcblx0fSk7XG5cblx0dmFyIF9jcmVhdGVDbGFzcyA9IChmdW5jdGlvbiAoKSB7IGZ1bmN0aW9uIGRlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykgeyBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7IHZhciBkZXNjcmlwdG9yID0gcHJvcHNbaV07IGRlc2NyaXB0b3IuZW51bWVyYWJsZSA9IGRlc2NyaXB0b3IuZW51bWVyYWJsZSB8fCBmYWxzZTsgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlOyBpZiAoJ3ZhbHVlJyBpbiBkZXNjcmlwdG9yKSBkZXNjcmlwdG9yLndyaXRhYmxlID0gdHJ1ZTsgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpOyB9IH0gcmV0dXJuIGZ1bmN0aW9uIChDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHsgaWYgKHByb3RvUHJvcHMpIGRlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IucHJvdG90eXBlLCBwcm90b1Byb3BzKTsgaWYgKHN0YXRpY1Byb3BzKSBkZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLCBzdGF0aWNQcm9wcyk7IHJldHVybiBDb25zdHJ1Y3RvcjsgfTsgfSkoKTtcblxuXHRmdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoJ0Nhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvbicpOyB9IH1cblxuXHR2YXIgWm9vbWVyID0gKGZ1bmN0aW9uICgpIHtcblx0ICBmdW5jdGlvbiBab29tZXIoKSB7XG5cdCAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgWm9vbWVyKTtcblxuXHQgICAgdGhpcy5taW5ab29tID0gdGhpcy5tYXhab29tID0gMTtcblx0ICB9XG5cblx0ICBfY3JlYXRlQ2xhc3MoWm9vbWVyLCBbe1xuXHQgICAga2V5OiAnc2V0dXAnLFxuXHQgICAgdmFsdWU6IGZ1bmN0aW9uIHNldHVwKF9yZWYpIHtcblx0ICAgICAgdmFyIGltYWdlU2l6ZSA9IF9yZWYuaW1hZ2VTaXplO1xuXHQgICAgICB2YXIgcHJldmlld1NpemUgPSBfcmVmLnByZXZpZXdTaXplO1xuXHQgICAgICB2YXIgZXhwb3J0Wm9vbSA9IF9yZWYuZXhwb3J0Wm9vbTtcblx0ICAgICAgdmFyIG1heFpvb20gPSBfcmVmLm1heFpvb207XG5cdCAgICAgIHZhciBtaW5ab29tID0gX3JlZi5taW5ab29tO1xuXHQgICAgICB2YXIgc21hbGxJbWFnZSA9IF9yZWYuc21hbGxJbWFnZTtcblxuXHQgICAgICB2YXIgd2lkdGhSYXRpbyA9IHByZXZpZXdTaXplLndpZHRoIC8gaW1hZ2VTaXplLndpZHRoO1xuXHQgICAgICB2YXIgaGVpZ2h0UmF0aW8gPSBwcmV2aWV3U2l6ZS5oZWlnaHQgLyBpbWFnZVNpemUuaGVpZ2h0O1xuXG5cdCAgICAgIGlmIChtaW5ab29tID09PSAnZml0Jykge1xuXHQgICAgICAgIHRoaXMubWluWm9vbSA9IE1hdGgubWluKHdpZHRoUmF0aW8sIGhlaWdodFJhdGlvKTtcblx0ICAgICAgfSBlbHNlIHtcblx0ICAgICAgICB0aGlzLm1pblpvb20gPSBNYXRoLm1heCh3aWR0aFJhdGlvLCBoZWlnaHRSYXRpbyk7XG5cdCAgICAgIH1cblxuXHQgICAgICBpZiAoc21hbGxJbWFnZSA9PT0gJ2FsbG93Jykge1xuXHQgICAgICAgIHRoaXMubWluWm9vbSA9IE1hdGgubWluKHRoaXMubWluWm9vbSwgMSk7XG5cdCAgICAgIH1cblxuXHQgICAgICB0aGlzLm1heFpvb20gPSBNYXRoLm1heCh0aGlzLm1pblpvb20sIG1heFpvb20gLyBleHBvcnRab29tKTtcblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdnZXRab29tJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRab29tKHNsaWRlclBvcykge1xuXHQgICAgICBpZiAoIXRoaXMubWluWm9vbSB8fCAhdGhpcy5tYXhab29tKSB7XG5cdCAgICAgICAgcmV0dXJuIG51bGw7XG5cdCAgICAgIH1cblxuXHQgICAgICByZXR1cm4gc2xpZGVyUG9zICogKHRoaXMubWF4Wm9vbSAtIHRoaXMubWluWm9vbSkgKyB0aGlzLm1pblpvb207XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnZ2V0U2xpZGVyUG9zJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRTbGlkZXJQb3Moem9vbSkge1xuXHQgICAgICBpZiAoIXRoaXMubWluWm9vbSB8fCAhdGhpcy5tYXhab29tKSB7XG5cdCAgICAgICAgcmV0dXJuIG51bGw7XG5cdCAgICAgIH1cblxuXHQgICAgICBpZiAodGhpcy5taW5ab29tID09PSB0aGlzLm1heFpvb20pIHtcblx0ICAgICAgICByZXR1cm4gMDtcblx0ICAgICAgfSBlbHNlIHtcblx0ICAgICAgICByZXR1cm4gKHpvb20gLSB0aGlzLm1pblpvb20pIC8gKHRoaXMubWF4Wm9vbSAtIHRoaXMubWluWm9vbSk7XG5cdCAgICAgIH1cblx0ICAgIH1cblx0ICB9LCB7XG5cdCAgICBrZXk6ICdpc1pvb21hYmxlJyxcblx0ICAgIHZhbHVlOiBmdW5jdGlvbiBpc1pvb21hYmxlKCkge1xuXHQgICAgICBpZiAoIXRoaXMubWluWm9vbSB8fCAhdGhpcy5tYXhab29tKSB7XG5cdCAgICAgICAgcmV0dXJuIG51bGw7XG5cdCAgICAgIH1cblxuXHQgICAgICByZXR1cm4gdGhpcy5taW5ab29tICE9PSB0aGlzLm1heFpvb207XG5cdCAgICB9XG5cdCAgfSwge1xuXHQgICAga2V5OiAnZml4Wm9vbScsXG5cdCAgICB2YWx1ZTogZnVuY3Rpb24gZml4Wm9vbSh6b29tKSB7XG5cdCAgICAgIHJldHVybiBNYXRoLm1heCh0aGlzLm1pblpvb20sIE1hdGgubWluKHRoaXMubWF4Wm9vbSwgem9vbSkpO1xuXHQgICAgfVxuXHQgIH1dKTtcblxuXHQgIHJldHVybiBab29tZXI7XG5cdH0pKCk7XG5cblx0ZXhwb3J0c1snZGVmYXVsdCddID0gWm9vbWVyO1xuXHRtb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuLyoqKi8gfSxcbi8qIDQgKi9cbi8qKiovIGZ1bmN0aW9uKG1vZHVsZSwgZXhwb3J0cykge1xuXG5cdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHtcblx0ICB2YWx1ZTogdHJ1ZVxuXHR9KTtcblx0dmFyIFBMVUdJTl9LRVkgPSAnY3JvcGl0JztcblxuXHRleHBvcnRzLlBMVUdJTl9LRVkgPSBQTFVHSU5fS0VZO1xuXHR2YXIgQ0xBU1NfTkFNRVMgPSB7XG5cdCAgUFJFVklFVzogJ2Nyb3BpdC1wcmV2aWV3Jyxcblx0ICBQUkVWSUVXX0lNQUdFX0NPTlRBSU5FUjogJ2Nyb3BpdC1wcmV2aWV3LWltYWdlLWNvbnRhaW5lcicsXG5cdCAgUFJFVklFV19JTUFHRTogJ2Nyb3BpdC1wcmV2aWV3LWltYWdlJyxcblx0ICBQUkVWSUVXX0JBQ0tHUk9VTkRfQ09OVEFJTkVSOiAnY3JvcGl0LXByZXZpZXctYmFja2dyb3VuZC1jb250YWluZXInLFxuXHQgIFBSRVZJRVdfQkFDS0dST1VORDogJ2Nyb3BpdC1wcmV2aWV3LWJhY2tncm91bmQnLFxuXHQgIEZJTEVfSU5QVVQ6ICdjcm9waXQtaW1hZ2UtaW5wdXQnLFxuXHQgIFpPT01fU0xJREVSOiAnY3JvcGl0LWltYWdlLXpvb20taW5wdXQnLFxuXG5cdCAgRFJBR19IT1ZFUkVEOiAnY3JvcGl0LWRyYWctaG92ZXJlZCcsXG5cdCAgSU1BR0VfTE9BRElORzogJ2Nyb3BpdC1pbWFnZS1sb2FkaW5nJyxcblx0ICBJTUFHRV9MT0FERUQ6ICdjcm9waXQtaW1hZ2UtbG9hZGVkJyxcblx0ICBESVNBQkxFRDogJ2Nyb3BpdC1kaXNhYmxlZCdcblx0fTtcblxuXHRleHBvcnRzLkNMQVNTX05BTUVTID0gQ0xBU1NfTkFNRVM7XG5cdHZhciBFUlJPUlMgPSB7XG5cdCAgSU1BR0VfRkFJTEVEX1RPX0xPQUQ6IHsgY29kZTogMCwgbWVzc2FnZTogJ0ltYWdlIGZhaWxlZCB0byBsb2FkLicgfSxcblx0ICBTTUFMTF9JTUFHRTogeyBjb2RlOiAxLCBtZXNzYWdlOiAnSW1hZ2UgaXMgdG9vIHNtYWxsLicgfVxuXHR9O1xuXG5cdGV4cG9ydHMuRVJST1JTID0gRVJST1JTO1xuXHR2YXIgZXZlbnROYW1lID0gZnVuY3Rpb24gZXZlbnROYW1lKGV2ZW50cykge1xuXHQgIHJldHVybiBldmVudHMubWFwKGZ1bmN0aW9uIChlKSB7XG5cdCAgICByZXR1cm4gJycgKyBlICsgJy5jcm9waXQnO1xuXHQgIH0pLmpvaW4oJyAnKTtcblx0fTtcblx0dmFyIEVWRU5UUyA9IHtcblx0ICBQUkVWSUVXOiBldmVudE5hbWUoWydtb3VzZWRvd24nLCAnbW91c2V1cCcsICdtb3VzZWxlYXZlJywgJ3RvdWNoc3RhcnQnLCAndG91Y2hlbmQnLCAndG91Y2hjYW5jZWwnLCAndG91Y2hsZWF2ZSddKSxcblx0ICBQUkVWSUVXX01PVkU6IGV2ZW50TmFtZShbJ21vdXNlbW92ZScsICd0b3VjaG1vdmUnXSksXG5cdCAgWk9PTV9JTlBVVDogZXZlbnROYW1lKFsnbW91c2Vtb3ZlJywgJ3RvdWNobW92ZScsICdjaGFuZ2UnXSlcblx0fTtcblx0ZXhwb3J0cy5FVkVOVFMgPSBFVkVOVFM7XG5cbi8qKiovIH0sXG4vKiA1ICovXG4vKioqLyBmdW5jdGlvbihtb2R1bGUsIGV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pIHtcblxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7XG5cdCAgdmFsdWU6IHRydWVcblx0fSk7XG5cblx0dmFyIF9jb25zdGFudHMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKDQpO1xuXG5cdHZhciBvcHRpb25zID0ge1xuXHQgIGVsZW1lbnRzOiBbe1xuXHQgICAgbmFtZTogJyRwcmV2aWV3Jyxcblx0ICAgIGRlc2NyaXB0aW9uOiAnVGhlIEhUTUwgZWxlbWVudCB0aGF0IGRpc3BsYXlzIGltYWdlIHByZXZpZXcuJyxcblx0ICAgIGRlZmF1bHRTZWxlY3RvcjogJy4nICsgX2NvbnN0YW50cy5DTEFTU19OQU1FUy5QUkVWSUVXXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJyRmaWxlSW5wdXQnLFxuXHQgICAgZGVzY3JpcHRpb246ICdGaWxlIGlucHV0IGVsZW1lbnQuJyxcblx0ICAgIGRlZmF1bHRTZWxlY3RvcjogJ2lucHV0LicgKyBfY29uc3RhbnRzLkNMQVNTX05BTUVTLkZJTEVfSU5QVVRcblx0ICB9LCB7XG5cdCAgICBuYW1lOiAnJHpvb21TbGlkZXInLFxuXHQgICAgZGVzY3JpcHRpb246ICdSYW5nZSBpbnB1dCBlbGVtZW50IHRoYXQgY29udHJvbHMgaW1hZ2Ugem9vbS4nLFxuXHQgICAgZGVmYXVsdFNlbGVjdG9yOiAnaW5wdXQuJyArIF9jb25zdGFudHMuQ0xBU1NfTkFNRVMuWk9PTV9TTElERVJcblx0ICB9XS5tYXAoZnVuY3Rpb24gKG8pIHtcblx0ICAgIG8udHlwZSA9ICdqUXVlcnkgZWxlbWVudCc7XG5cdCAgICBvWydkZWZhdWx0J10gPSAnJGltYWdlQ3JvcHBlci5maW5kKFxcJycgKyBvLmRlZmF1bHRTZWxlY3RvciArICdcXCcpJztcblx0ICAgIHJldHVybiBvO1xuXHQgIH0pLFxuXG5cdCAgdmFsdWVzOiBbe1xuXHQgICAgbmFtZTogJ3dpZHRoJyxcblx0ICAgIHR5cGU6ICdudW1iZXInLFxuXHQgICAgZGVzY3JpcHRpb246ICdXaWR0aCBvZiBpbWFnZSBwcmV2aWV3IGluIHBpeGVscy4gSWYgc2V0LCBpdCB3aWxsIG92ZXJyaWRlIHRoZSBDU1MgcHJvcGVydHkuJyxcblx0ICAgICdkZWZhdWx0JzogbnVsbFxuXHQgIH0sIHtcblx0ICAgIG5hbWU6ICdoZWlnaHQnLFxuXHQgICAgdHlwZTogJ251bWJlcicsXG5cdCAgICBkZXNjcmlwdGlvbjogJ0hlaWdodCBvZiBpbWFnZSBwcmV2aWV3IGluIHBpeGVscy4gSWYgc2V0LCBpdCB3aWxsIG92ZXJyaWRlIHRoZSBDU1MgcHJvcGVydHkuJyxcblx0ICAgICdkZWZhdWx0JzogbnVsbFxuXHQgIH0sIHtcblx0ICAgIG5hbWU6ICdpbWFnZUJhY2tncm91bmQnLFxuXHQgICAgdHlwZTogJ2Jvb2xlYW4nLFxuXHQgICAgZGVzY3JpcHRpb246ICdXaGV0aGVyIG9yIG5vdCB0byBkaXNwbGF5IHRoZSBiYWNrZ3JvdW5kIGltYWdlIGJleW9uZCB0aGUgcHJldmlldyBhcmVhLicsXG5cdCAgICAnZGVmYXVsdCc6IGZhbHNlXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ2ltYWdlQmFja2dyb3VuZEJvcmRlcldpZHRoJyxcblx0ICAgIHR5cGU6ICdhcnJheSBvciBudW1iZXInLFxuXHQgICAgZGVzY3JpcHRpb246ICdXaWR0aCBvZiBiYWNrZ3JvdW5kIGltYWdlIGJvcmRlciBpbiBwaXhlbHMuXFxuICAgICAgICBUaGUgZm91ciBhcnJheSBlbGVtZW50cyBzcGVjaWZ5IHRoZSB3aWR0aCBvZiBiYWNrZ3JvdW5kIGltYWdlIHdpZHRoIG9uIHRoZSB0b3AsIHJpZ2h0LCBib3R0b20sIGxlZnQgc2lkZSByZXNwZWN0aXZlbHkuXFxuICAgICAgICBUaGUgYmFja2dyb3VuZCBpbWFnZSBiZXlvbmQgdGhlIHdpZHRoIHdpbGwgYmUgaGlkZGVuLlxcbiAgICAgICAgSWYgc3BlY2lmaWVkIGFzIGEgbnVtYmVyLCBib3JkZXIgd2l0aCB1bmlmb3JtIHdpZHRoIG9uIGFsbCBzaWRlcyB3aWxsIGJlIGFwcGxpZWQuJyxcblx0ICAgICdkZWZhdWx0JzogWzAsIDAsIDAsIDBdXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ2V4cG9ydFpvb20nLFxuXHQgICAgdHlwZTogJ251bWJlcicsXG5cdCAgICBkZXNjcmlwdGlvbjogJ1RoZSByYXRpbyBiZXR3ZWVuIHRoZSBkZXNpcmVkIGltYWdlIHNpemUgdG8gZXhwb3J0IGFuZCB0aGUgcHJldmlldyBzaXplLlxcbiAgICAgICAgRm9yIGV4YW1wbGUsIGlmIHRoZSBwcmV2aWV3IHNpemUgaXMgYDMwMHB4ICogMjAwcHhgLCBhbmQgYGV4cG9ydFpvb20gPSAyYCwgdGhlblxcbiAgICAgICAgdGhlIGV4cG9ydGVkIGltYWdlIHNpemUgd2lsbCBiZSBgNjAwcHggKiA0MDBweGAuXFxuICAgICAgICBUaGlzIGFsc28gYWZmZWN0cyB0aGUgbWF4aW11bSB6b29tIGxldmVsLCBzaW5jZSB0aGUgZXhwb3J0ZWQgaW1hZ2UgY2Fubm90IGJlIHpvb21lZCB0byBsYXJnZXIgdGhhbiBpdHMgb3JpZ2luYWwgc2l6ZS4nLFxuXHQgICAgJ2RlZmF1bHQnOiAxXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ2FsbG93RHJhZ05Ecm9wJyxcblx0ICAgIHR5cGU6ICdib29sZWFuJyxcblx0ICAgIGRlc2NyaXB0aW9uOiAnV2hlbiBzZXQgdG8gdHJ1ZSwgeW91IGNhbiBsb2FkIGFuIGltYWdlIGJ5IGRyYWdnaW5nIGl0IGZyb20gbG9jYWwgZmlsZSBicm93c2VyIG9udG8gdGhlIHByZXZpZXcgYXJlYS4nLFxuXHQgICAgJ2RlZmF1bHQnOiB0cnVlXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ21pblpvb20nLFxuXHQgICAgdHlwZTogJ3N0cmluZycsXG5cdCAgICBkZXNjcmlwdGlvbjogJ1RoaXMgb3B0aW9ucyBkZWNpZGVzIHRoZSBtaW5pbWFsIHpvb20gbGV2ZWwgb2YgdGhlIGltYWdlLlxcbiAgICAgICAgSWYgc2V0IHRvIGBcXCdmaWxsXFwnYCwgdGhlIGltYWdlIGhhcyB0byBmaWxsIHRoZSBwcmV2aWV3IGFyZWEsIGkuZS4gYm90aCB3aWR0aCBhbmQgaGVpZ2h0IG11c3Qgbm90IGdvIHNtYWxsZXIgdGhhbiB0aGUgcHJldmlldyBhcmVhLlxcbiAgICAgICAgSWYgc2V0IHRvIGBcXCdmaXRcXCdgLCB0aGUgaW1hZ2UgY2FuIHNocmluayBmdXJ0aGVyIHRvIGZpdCB0aGUgcHJldmlldyBhcmVhLCBpLmUuIGF0IGxlYXN0IG9uZSBvZiBpdHMgZWRnZXMgbXVzdCBub3QgZ28gc21hbGxlciB0aGFuIHRoZSBwcmV2aWV3IGFyZWEuJyxcblx0ICAgICdkZWZhdWx0JzogJ2ZpbGwnXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ21heFpvb20nLFxuXHQgICAgdHlwZTogJ251bWJlcicsXG5cdCAgICBkZXNjcmlwdGlvbjogJ0RldGVybWluZXMgaG93IGJpZyB0aGUgaW1hZ2UgY2FuIGJlIHpvb21lZC4gRS5nLiBpZiBzZXQgdG8gMS41LCB0aGUgaW1hZ2UgY2FuIGJlIHpvb21lZCB0byAxNTAlIG9mIGl0cyBvcmlnaW5hbCBzaXplLicsXG5cdCAgICAnZGVmYXVsdCc6IDFcblx0ICB9LCB7XG5cdCAgICBuYW1lOiAnaW5pdGlhbFpvb20nLFxuXHQgICAgdHlwZTogJ3N0cmluZycsXG5cdCAgICBkZXNjcmlwdGlvbjogJ0RldGVybWluZXMgdGhlIHpvb20gd2hlbiBhbiBpbWFnZSBpcyBsb2FkZWQuXFxuICAgICAgICBXaGVuIHNldCB0byBgXFwnbWluXFwnYCwgaW1hZ2UgaXMgem9vbWVkIHRvIHRoZSBzbWFsbGVzdCB3aGVuIGxvYWRlZC5cXG4gICAgICAgIFdoZW4gc2V0IHRvIGBcXCdpbWFnZVxcJ2AsIGltYWdlIGlzIHpvb21lZCB0byAxMDAlIHdoZW4gbG9hZGVkLicsXG5cdCAgICAnZGVmYXVsdCc6ICdtaW4nXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ2ZyZWVNb3ZlJyxcblx0ICAgIHR5cGU6ICdib29sZWFuJyxcblx0ICAgIGRlc2NyaXB0aW9uOiAnV2hlbiBzZXQgdG8gdHJ1ZSwgeW91IGNhbiBmcmVlbHkgbW92ZSB0aGUgaW1hZ2UgaW5zdGVhZCBvZiBiZWluZyBib3VuZCB0byB0aGUgY29udGFpbmVyIGJvcmRlcnMnLFxuXHQgICAgJ2RlZmF1bHQnOiBmYWxzZVxuXHQgIH0sIHtcblx0ICAgIG5hbWU6ICdzbWFsbEltYWdlJyxcblx0ICAgIHR5cGU6ICdzdHJpbmcnLFxuXHQgICAgZGVzY3JpcHRpb246ICdXaGVuIHNldCB0byBgXFwncmVqZWN0XFwnYCwgYG9uSW1hZ2VFcnJvcmAgd291bGQgYmUgY2FsbGVkIHdoZW4gY3JvcGl0IGxvYWRzIGFuIGltYWdlIHRoYXQgaXMgc21hbGxlciB0aGFuIHRoZSBjb250YWluZXIuXFxuICAgICAgICBXaGVuIHNldCB0byBgXFwnYWxsb3dcXCdgLCBpbWFnZXMgc21hbGxlciB0aGFuIHRoZSBjb250YWluZXIgY2FuIGJlIHpvb21lZCBkb3duIHRvIGl0cyBvcmlnaW5hbCBzaXplLCBvdmVyaWRpbmcgYG1pblpvb21gIG9wdGlvbi5cXG4gICAgICAgIFdoZW4gc2V0IHRvIGBcXCdzdHJldGNoXFwnYCwgdGhlIG1pbmltdW0gem9vbSBvZiBzbWFsbCBpbWFnZXMgd291bGQgZm9sbG93IGBtaW5ab29tYCBvcHRpb24uJyxcblx0ICAgICdkZWZhdWx0JzogJ3JlamVjdCdcblx0ICB9XSxcblxuXHQgIGNhbGxiYWNrczogW3tcblx0ICAgIG5hbWU6ICdvbkZpbGVDaGFuZ2UnLFxuXHQgICAgZGVzY3JpcHRpb246ICdDYWxsZWQgd2hlbiB1c2VyIHNlbGVjdHMgYSBmaWxlIGluIHRoZSBzZWxlY3QgZmlsZSBpbnB1dC4nLFxuXHQgICAgcGFyYW1zOiBbe1xuXHQgICAgICBuYW1lOiAnZXZlbnQnLFxuXHQgICAgICB0eXBlOiAnb2JqZWN0Jyxcblx0ICAgICAgZGVzY3JpcHRpb246ICdGaWxlIGNoYW5nZSBldmVudCBvYmplY3QnXG5cdCAgICB9XVxuXHQgIH0sIHtcblx0ICAgIG5hbWU6ICdvbkZpbGVSZWFkZXJFcnJvcicsXG5cdCAgICBkZXNjcmlwdGlvbjogJ0NhbGxlZCB3aGVuIGBGaWxlUmVhZGVyYCBlbmNvdW50ZXJzIGFuIGVycm9yIHdoaWxlIGxvYWRpbmcgdGhlIGltYWdlIGZpbGUuJ1xuXHQgIH0sIHtcblx0ICAgIG5hbWU6ICdvbkltYWdlTG9hZGluZycsXG5cdCAgICBkZXNjcmlwdGlvbjogJ0NhbGxlZCB3aGVuIGltYWdlIHN0YXJ0cyB0byBiZSBsb2FkZWQuJ1xuXHQgIH0sIHtcblx0ICAgIG5hbWU6ICdvbkltYWdlTG9hZGVkJyxcblx0ICAgIGRlc2NyaXB0aW9uOiAnQ2FsbGVkIHdoZW4gaW1hZ2UgaXMgbG9hZGVkLidcblx0ICB9LCB7XG5cdCAgICBuYW1lOiAnb25JbWFnZUVycm9yJyxcblx0ICAgIGRlc2NyaXB0aW9uOiAnQ2FsbGVkIHdoZW4gaW1hZ2UgY2Fubm90IGJlIGxvYWRlZC4nLFxuXHQgICAgcGFyYW1zOiBbe1xuXHQgICAgICBuYW1lOiAnZXJyb3InLFxuXHQgICAgICB0eXBlOiAnb2JqZWN0Jyxcblx0ICAgICAgZGVzY3JpcHRpb246ICdFcnJvciBvYmplY3QuJ1xuXHQgICAgfSwge1xuXHQgICAgICBuYW1lOiAnZXJyb3IuY29kZScsXG5cdCAgICAgIHR5cGU6ICdudW1iZXInLFxuXHQgICAgICBkZXNjcmlwdGlvbjogJ0Vycm9yIGNvZGUuIGAwYCBtZWFucyBnZW5lcmljIGltYWdlIGxvYWRpbmcgZmFpbHVyZS4gYDFgIG1lYW5zIGltYWdlIGlzIHRvbyBzbWFsbC4nXG5cdCAgICB9LCB7XG5cdCAgICAgIG5hbWU6ICdlcnJvci5tZXNzYWdlJyxcblx0ICAgICAgdHlwZTogJ3N0cmluZycsXG5cdCAgICAgIGRlc2NyaXB0aW9uOiAnQSBtZXNzYWdlIGV4cGxhaW5pbmcgdGhlIGVycm9yLidcblx0ICAgIH1dXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ29uWm9vbUVuYWJsZWQnLFxuXHQgICAgZGVzY3JpcHRpb246ICdDYWxsZWQgd2hlbiBpbWFnZSB0aGUgem9vbSBzbGlkZXIgaXMgZW5hYmxlZC4nXG5cdCAgfSwge1xuXHQgICAgbmFtZTogJ29uWm9vbURpc2FibGVkJyxcblx0ICAgIGRlc2NyaXB0aW9uOiAnQ2FsbGVkIHdoZW4gaW1hZ2UgdGhlIHpvb20gc2xpZGVyIGlzIGRpc2FibGVkLidcblx0ICB9LCB7XG5cdCAgICBuYW1lOiAnb25ab29tQ2hhbmdlJyxcblx0ICAgIGRlc2NyaXB0aW9uOiAnQ2FsbGVkIHdoZW4gem9vbSBjaGFuZ2VzLicsXG5cdCAgICBwYXJhbXM6IFt7XG5cdCAgICAgIG5hbWU6ICd6b29tJyxcblx0ICAgICAgdHlwZTogJ251bWJlcicsXG5cdCAgICAgIGRlc2NyaXB0aW9uOiAnTmV3IHpvb20uJ1xuXHQgICAgfV1cblx0ICB9LCB7XG5cdCAgICBuYW1lOiAnb25PZmZzZXRDaGFuZ2UnLFxuXHQgICAgZGVzY3JpcHRpb246ICdDYWxsZWQgd2hlbiBpbWFnZSBvZmZzZXQgY2hhbmdlcy4nLFxuXHQgICAgcGFyYW1zOiBbe1xuXHQgICAgICBuYW1lOiAnb2Zmc2V0Jyxcblx0ICAgICAgdHlwZTogJ29iamVjdCcsXG5cdCAgICAgIGRlc2NyaXB0aW9uOiAnTmV3IG9mZnNldCwgd2l0aCBgeGAgYW5kIGB5YCB2YWx1ZXMuJ1xuXHQgICAgfV1cblx0ICB9XS5tYXAoZnVuY3Rpb24gKG8pIHtcblx0ICAgIG8udHlwZSA9ICdmdW5jdGlvbic7cmV0dXJuIG87XG5cdCAgfSlcblx0fTtcblxuXHR2YXIgbG9hZERlZmF1bHRzID0gZnVuY3Rpb24gbG9hZERlZmF1bHRzKCRlbCkge1xuXHQgIHZhciBkZWZhdWx0cyA9IHt9O1xuXHQgIGlmICgkZWwpIHtcblx0ICAgIG9wdGlvbnMuZWxlbWVudHMuZm9yRWFjaChmdW5jdGlvbiAobykge1xuXHQgICAgICBkZWZhdWx0c1tvLm5hbWVdID0gJGVsLmZpbmQoby5kZWZhdWx0U2VsZWN0b3IpO1xuXHQgICAgfSk7XG5cdCAgfVxuXHQgIG9wdGlvbnMudmFsdWVzLmZvckVhY2goZnVuY3Rpb24gKG8pIHtcblx0ICAgIGRlZmF1bHRzW28ubmFtZV0gPSBvWydkZWZhdWx0J107XG5cdCAgfSk7XG5cdCAgb3B0aW9ucy5jYWxsYmFja3MuZm9yRWFjaChmdW5jdGlvbiAobykge1xuXHQgICAgZGVmYXVsdHNbby5uYW1lXSA9IGZ1bmN0aW9uICgpIHt9O1xuXHQgIH0pO1xuXG5cdCAgcmV0dXJuIGRlZmF1bHRzO1xuXHR9O1xuXG5cdGV4cG9ydHMubG9hZERlZmF1bHRzID0gbG9hZERlZmF1bHRzO1xuXHRleHBvcnRzWydkZWZhdWx0J10gPSBvcHRpb25zO1xuXG4vKioqLyB9LFxuLyogNiAqL1xuLyoqKi8gZnVuY3Rpb24obW9kdWxlLCBleHBvcnRzKSB7XG5cblx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywge1xuXHQgIHZhbHVlOiB0cnVlXG5cdH0pO1xuXHR2YXIgZXhpc3RzID0gZnVuY3Rpb24gZXhpc3RzKHYpIHtcblx0ICByZXR1cm4gdHlwZW9mIHYgIT09ICd1bmRlZmluZWQnO1xuXHR9O1xuXG5cdGV4cG9ydHMuZXhpc3RzID0gZXhpc3RzO1xuXHR2YXIgcm91bmQgPSBmdW5jdGlvbiByb3VuZCh4KSB7XG5cdCAgcmV0dXJuICsoTWF0aC5yb3VuZCh4ICogMTAwKSArICdlLTInKTtcblx0fTtcblx0ZXhwb3J0cy5yb3VuZCA9IHJvdW5kO1xuXG4vKioqLyB9XG4vKioqKioqLyBdKVxufSk7XG47XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY3JvcGl0L2Rpc3QvanF1ZXJ5LmNyb3BpdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY3JvcGl0L2Rpc3QvanF1ZXJ5LmNyb3BpdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIvKiBcclxuICogVXRpbGl0aWVzIGZvciBUaGFpTG92ZWx5LmNvXHJcbiAqL1xyXG5cclxuLy8gRm9udCBPYnNlcnZlclxyXG52YXIgRm9udEZhY2VPYnNlcnZlciA9IHJlcXVpcmUoJ2ZvbnRmYWNlb2JzZXJ2ZXInKTtcclxuXHJcbi8vIEltcG9ydCBTY3JpcHRzXHJcbnJlcXVpcmUoJ2Nyb3BpdCcpO1xyXG5yZXF1aXJlKCdib290c3RyYXAtZ3Jvd2wnKTtcclxucmVxdWlyZSgnYmx1ZWltcC1nYWxsZXJ5L2pzL2pxdWVyeS5ibHVlaW1wLWdhbGxlcnkuanMnKTtcclxuXHJcbi8vIFNldCBjb29raWVcclxuZnVuY3Rpb24gc2V0Q29va2llKGNuYW1lLCBjdmFsdWUsIGV4cGlyZSkge1xyXG4gICAgdmFyIGQgPSBuZXcgRGF0ZSgpO1xyXG4gICAgZC5zZXRUaW1lKGQuZ2V0VGltZSgpICsgKGV4cGlyZSkpO1xyXG4gICAgdmFyIGV4cGlyZXMgPSBcImV4cGlyZXM9XCIrIGQudG9VVENTdHJpbmcoKTtcclxuICAgIGRvY3VtZW50LmNvb2tpZSA9IGNuYW1lICsgXCI9XCIgKyBjdmFsdWUgKyBcIjtcIiArIGV4cGlyZXMgKyBcIjtwYXRoPS9cIjtcclxufVxyXG53aW5kb3cuc2V0Q29va2llID0gc2V0Q29va2llO1xyXG5cclxuLy8gR2V0IGNvb2tpZVxyXG5mdW5jdGlvbiBnZXRDb29raWUoY25hbWUpIHtcclxuICAgIHZhciBuYW1lID0gY25hbWUgKyBcIj1cIjtcclxuICAgIHZhciBkZWNvZGVkQ29va2llID0gZGVjb2RlVVJJQ29tcG9uZW50KGRvY3VtZW50LmNvb2tpZSk7XHJcbiAgICB2YXIgY2EgPSBkZWNvZGVkQ29va2llLnNwbGl0KCc7Jyk7XHJcbiAgICBmb3IodmFyIGkgPSAwOyBpIDxjYS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIHZhciBjID0gY2FbaV07XHJcbiAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09ICcgJykge1xyXG4gICAgICAgICAgICBjID0gYy5zdWJzdHJpbmcoMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjLmluZGV4T2YobmFtZSkgPT0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gYy5zdWJzdHJpbmcobmFtZS5sZW5ndGgsIGMubGVuZ3RoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gXCJcIjtcclxufVxyXG53aW5kb3cuZ2V0Q29va2llID0gZ2V0Q29va2llO1xyXG5cclxuLy8gRm9udCBPYnNlcnZlclxyXG52YXIgZm9udEEgPSBuZXcgRm9udEZhY2VPYnNlcnZlcignRmlyYSBTYW5zJyk7XHJcbnZhciBmb250QiA9IG5ldyBGb250RmFjZU9ic2VydmVyKCdSb2JvdG8gU2xhYicpO1xyXG5cclxuUHJvbWlzZS5hbGwoW2ZvbnRBLmxvYWQoKSwgZm9udEIubG9hZCgpXSkudGhlbihmdW5jdGlvbiAoKSB7XHJcbiAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2ZvbnRzLWxvYWRlZCcpO1xyXG4gIHNldENvb2tpZSgnZm9udHNfbG9hZGVkJyk7XHJcbn0pO1xyXG5cclxuLy8gQW5pbWF0ZS5jc3MgRXh0ZW5zaW9uXHJcbiQuZm4uZXh0ZW5kKHtcclxuICAgIGFuaW1hdGVDc3M6IGZ1bmN0aW9uIChhbmltYXRpb25OYW1lKSB7XHJcbiAgICAgICAgdmFyIGFuaW1hdGlvbkVuZCA9ICd3ZWJraXRBbmltYXRpb25FbmQgbW96QW5pbWF0aW9uRW5kIE1TQW5pbWF0aW9uRW5kIG9hbmltYXRpb25lbmQgYW5pbWF0aW9uZW5kJztcclxuICAgICAgICB0aGlzLmFkZENsYXNzKCdhbmltYXRlZCAnICsgYW5pbWF0aW9uTmFtZSkub25lKGFuaW1hdGlvbkVuZCwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2FuaW1hdGVkICcgKyBhbmltYXRpb25OYW1lKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxufSk7XHJcblxyXG4vLyBQcm9jZXNzIFN1Ym1pdFxyXG4kKGZ1bmN0aW9uKCl7XHJcbiAgICAkKCcucHJvY2Vzcy1zdWJtaXQnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKXtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdmFyIGJ0biA9ICQodGhpcyksXHJcbiAgICAgICAgICAgIGYgICA9IGJ0bi5jbG9zZXN0KCdmb3JtJyksXHJcbiAgICAgICAgICAgIG4gICA9IGYuZmluZCgnaW5wdXQnKSxcclxuICAgICAgICAgICAgdyAgID0gYnRuLm91dGVyV2lkdGgoKSxcclxuICAgICAgICAgICAgaCAgID0gYnRuLm91dGVySGVpZ2h0KCksXHJcbiAgICAgICAgICAgIHYgICA9IHRydWUsXHJcbiAgICAgICAgICAgIGkgICA9ICQoJzxpPicsIHsgY2xhc3M6ICdmYXMgZmEtY2lyY2xlLW5vdGNoIGZhLXNwaW4nfSk7XHJcblxyXG4gICAgICAgIG4uZWFjaChmdW5jdGlvbihpLCBlKXtcclxuICAgICAgICAgICAgaWYoZS5jaGVja1ZhbGlkaXR5KCkgPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgIHYgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdJbnZhbGlkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYodiAhPT0gZmFsc2Upe1xyXG4gICAgICAgICAgICBidG4uaHRtbChpKS5jc3Moeyd3aWR0aCc6IHcgKyAncHgnLCAnaGVpZ2h0JzogaCArICdweCd9KS5hdHRyKCdkaXNhYmxlZCcsdHJ1ZSk7XHJcbiAgICAgICAgICAgIGYuc3VibWl0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn0pO1xuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvdXRpbGl0aWVzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==