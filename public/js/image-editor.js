$(function(){
    // Zoom Options for Photo Edtior
    var up = true,
        down = false,
        value = 0,
        increment = 1,
        ceiling = 4;

    var zoomPoints = [0,0.25,0.50,0.75,1];

    function zoom(el, action){
        if(action == 'in') {
            if (up == true && value <= ceiling) {
                value += increment;
                down = true;
                el.cropit('zoom', zoomPoints[value]);
                if (value == ceiling) {
                    up = false;
                }
            }
        }
        if(action == 'out') {
            if (down == true && value >= 0) {
                value -= increment;
                up = true;
                el.cropit('zoom', zoomPoints[value]);
                if (value == 0) {
                    down = false;
                }
            }
        }
    }

    // Cropit
    var imageEditor = $('#image-editor');
        imageEditor.cropit({allowDragNDrop: false});

    // When a photo is selected
    $('#upload-photo-input').change(function(){

        // If a file is selected and not cancled
        if(document.getElementById('upload-photo-input').files.length > 0) {

            // Hide modal & disabled file input label
            $('#settings-photo-modal .options').hide();
            $('#settings-photo-modal .photo-editor').show();

            // Image editor functions
            $('.cropit-preview img').show();
            $('.add-photo-label').hide().find('span').removeClass('animated bounceIn').addClass('animated-hidden');
            $('.image-zoom-in, .image-zoom-out, .delete-photo').addClass('animated bounceIn');

            // Allow submission
            $('#upload-btn').prop('disabled', false);

        };

    });

    // Zoom Buttons
    $('.image-zoom-in').click(function(){ zoom(imageEditor, 'in'); });
    $('.image-zoom-out').click(function(){ zoom(imageEditor, 'out'); });

    // Delete uploaded photo
    $('.delete-photo').on('click', function(){

        // Image editor
        $('.add-photo-label').show().find('span').addClass('animated bounceIn');
        $('.image-zoom-in, .image-zoom-out, .delete-photo').removeClass('animated bounceIn');
        $('.cropit-preview').removeClass('cropit-image-loaded').find('img').hide();
        $('#upload-btn').prop('disabled', true);

        // Reset edtior
        up = true;
        down = false;
        value = 0;

    });

});