webpackJsonp([3],{

/***/ "./resources/assets/js/profile.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {$(function () {
    window.Echo.private('notifications.' + user).notification(function (notification) {
        console.log(notification);
        if (url === notification.data.sender) {
            if (notification.type == 'App\\Notifications\\MessageIncoming') {
                // Create message
                var div = $('<div>', { class: 'message' });
                if (notification.data.type === 0) {
                    var p = $('<p>', { class: 'bubble', text: notification.data.body });
                }
                if (notification.data.type === 1) {
                    var p = $('<p>', { class: 'bubble photo' });
                }
                if (notification.data.type === 2) {
                    var p = $('<p>', { class: 'bubble sticker' });
                }
                if (notification.data.type === 3) {
                    var p = $('<p>', { class: 'bubble gif' });
                }
                if (notification.data.type > 0) {
                    $('<img>').attr('src', notification.data.body).appendTo(p);
                }
                p.appendTo(div);
                var avatar = $('<div>', { class: 'avatar' });
                $('<img>', { src: notification.data.photo + '.jpg', alt: 'Profile Photo' }).appendTo(avatar);
                avatar.appendTo(div);
                $('<div>', { class: 'date', text: notification.data.date }).appendTo(div);
                if ($('.messsages').length > 0) {
                    $('.messages').prepend(div);
                } else {
                    $('#message-form').after().html(div);
                }

                // Notification Sound
                var notifySound = document.createElement('audio');
                notifySound.setAttribute('src', 'sounds/notification.mp3');
                notifySound.setAttribute('autoplay', 'autoplay');
                notifySound.addEventListener("load", function () {
                    notifySound.play();
                }, true);
            }
            if (notification.type == 'App\\Notifications\\ProfileViewed') {
                // viewed..
            }
        }
    });

    // Message Function
    function message(data) {
        $.ajax({
            url: '/messages/send',
            method: 'POST',
            data: data,
            dataType: 'JSON',
            beforeSend: function beforeSend() {
                $('#message-submit').prop('diabled', true).addClass('disabled');
            }
        }).done(function (data) {
            $('.messages .empty').remove();
            var div = $('<div>', { class: 'message sender' });
            if (data.type == 0) {
                var p = $('<p>', { class: 'bubble', text: data.body });
            }
            if (data.type == 2) {
                var p = $('<p>', { class: 'bubble sticker' });
                $('<img>').attr('src', data.body).appendTo(p);
            }
            if (data.type == 3) {
                var p = $('<p>', { class: 'bubble gif' });
                $('<img>').attr('src', data.body).appendTo(p);
            }
            p.appendTo(div);
            $('<div>', { class: 'date', text: data.date }).appendTo(div);
            if ($('.messages').hasClass('d-none')) {
                $('.messages').removeClass('d-none');
            }
            $('.messages').prepend(div);
        }).always(function () {
            $('#message-submit').prop('diabled', false).removeClass('disabled');
            $('#body').val('');
        });
    }

    // Photo Message
    function photo(data) {
        $.ajax({
            url: '/messages/send',
            method: 'POST',
            data: data,
            dataType: 'JSON',
            mimeType: "multipart/form-data",
            processData: false,
            contentType: false,
            beforeSend: function beforeSend() {
                $('#message-submit').prop('diabled', true).addClass('disabled');
            }
        }).done(function (data) {
            $('.messages .empty').remove();
            var div = $('<div>', { class: 'message sender' }),
                p = $('<p>', { class: 'bubble photo' });
            $('<img>').attr('src', data.body).appendTo(p);
            p.appendTo(div);
            $('<div>', { class: 'date', text: data.date }).appendTo(div);
            if ($('.messages').hasClass('d-none')) {
                $('.messages').removeClass('d-none');
            }
            $('.messages').prepend(div);
        }).always(function () {
            $('#message-submit').prop('diabled', false).removeClass('disabled');
        });
    }

    // Stickers & Gifs by Giphy
    function gifs(path, el, type) {
        var query = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';

        $.ajax({
            url: '/giphy/' + path + '/' + query,
            method: 'GET',
            data: 'JSON',
            beforeSend: function beforeSend() {
                el.html('<i class="fas fa-circle-notch fa-spin"></i><span class="sr-only">Loading...</span>');
            }
        }).done(function (giphy) {
            el.html('');
            $.each(giphy.data, function (i, value) {
                if (type == 2) {
                    el.append($('<img>', { class: 'sticker-submit', data: { 'type': type }, src: value.images.fixed_width_small.url }));
                }
                if (type == 3) {
                    el.append($('<img>', { class: 'gif-submit', data: { 'type': type }, src: value.images.downsized.url }));
                }
            });
        });
    }

    // Pull stickers on click
    var stickersGallery = $('#stickers-dd .gallery');
    $('#stickers-btn').click(function () {
        gifs('stickers', stickersGallery, 2);
    });

    // Pull gifs on click
    $('#gifs-btn').click(function () {
        var gifGallery = $('#gifs-dd .gallery');
        gifs('trending', gifGallery, 3);
    });

    // Clear search
    $('.gifs, .stickers').on('hidden.bs.dropdown', function () {
        $(this).find('.search-input').val('');
    });

    // Search stickers
    var sThread = null;
    $('#stickers-dd .search-input').keyup(function () {
        clearTimeout(sThread);
        var that = $(this);
        sThread = setTimeout(function () {
            if (that.val() != '') {
                gifs('stickers/search', $('#stickers-dd .gallery'), 2, that.val());
            }
        }, 500);
    });

    // Search gifs
    var gThread = null;
    $('#gifs-dd .search-input').keyup(function () {
        clearTimeout(gThread);
        var that = $(this);
        gThread = setTimeout(function () {
            if (that.val() != '') {
                gifs('search', $('#gifs-dd .gallery'), 3, that.val());
            }
        }, 500);
    });

    // Photo upload
    $('#photo').change(function () {
        if (document.getElementById('photo').files.length > 0) {
            var params = new FormData();
            params.append('_token', $('#csrf-token').val());
            params.append('receiver_id', $('#receiver_id').val());
            params.append('photo', $('#photo')[0].files[0]);
            params.append('type', 1);
            photo(params);
        }
    });

    // Image, sticker or gif submit
    $(document).on('click', '.image-submit, .sticker-submit, .gif-submit', function () {
        var params = {
            _token: $('#csrf-token').val(),
            receiver_id: $('#receiver_id').val(),
            body: $(this).attr('src'),
            type: $(this).data('type')
        };
        message(params);
    });

    // Message submit
    $('#message-submit').click(function (e) {
        var body = $('#body').val();
        if (body == '') {
            return false;
        }
        var params = {
            _token: $('#csrf-token').val(),
            receiver_id: $('#receiver_id').val(),
            body: body,
            type: 0
        };
        message(params);
        e.preventDefault();
    });

    // Wave 
    $('#wave-btn').click(function () {
        var btn = $(this);
        var params = {
            _token: $('#csrf-token').val(),
            receiver_id: $('#receiver_id').val()
        };
        $.ajax({
            url: '/wave/send',
            method: 'POST',
            dataType: 'JSON',
            data: params,
            beforeSend: function beforeSend() {
                btn.prop('disabled', true).addClass('disabled');
            }
        }).done(function (data) {
            if (data.success == true) {
                $('#wave-btn').prop('disabled', true).addClass('waved').html('Waved <i class="icon-check"></i>');
            }
        }).fail(function (data) {
            // failed..
        }).always(function (data) {
            // always..
            btn.prop('disabled', false).removeClass('disabled');
        });
    });

    // Add Favorite
    $('.actions').on('click', '#add-favorite', function () {
        var btn = $(this);
        var params = {
            _token: $('#csrf-token').val(),
            username: $('#username').val()
        };
        $.ajax({
            url: '/account/favorite/add',
            method: 'POST',
            dataType: 'JSON',
            data: params,
            beforeSend: function beforeSend() {
                btn.prop('disabled', true).addClass('disabled').text('').append('<i class="fas fa-sync fa-spin"></i>');
            }
        }).done(function (data) {
            if (data.success == true) {
                btn.remove();
                var remove_btn = $('<button>', { id: 'remove-favorite', class: 'btn btn-block btn-link', type: 'button', text: 'Remove Favorite' });
                $('.actions').append(remove_btn);
            }
        }).always(function (data) {
            btn.prop('disabled', false).removeClass('disabled');
        });
    });

    // Remove Favorite
    $('.actions').on('click', '#remove-favorite', function () {
        var btn = $(this);
        var params = {
            _token: $('#csrf-token').val(),
            username: $('#username').val()
        };
        $.ajax({
            url: '/account/favorite/remove',
            method: 'POST',
            dataType: 'JSON',
            data: params,
            beforeSend: function beforeSend() {
                btn.prop('disabled', true).addClass('disabled').text('').append('<i class="fas fa-sync fa-spin"></i>');
            }
        }).done(function (data) {
            if (data.success == true) {
                btn.remove();
                var add_btn = $('<button>', { id: 'add-favorite', class: 'btn btn-block btn-link', type: 'button', text: 'Add To Favorites' });
                $('.actions').append(add_btn);
            }
        }).always(function (data) {
            btn.prop('disabled', false).removeClass('disabled');
        });
    });

    // Adding photos
    $('#user-photos-form').submit(function (e) {
        var photo = $('#image-editor').cropit('export', {
            type: 'image/jpeg',
            quality: 1,
            originalSize: true,
            exportZoom: true
        });
        $('#photo').val(photo);
    });

    // Favorites Profile Photos
    $('.fav-profile-photo').each(function (i) {
        var src = $(this).data('src');
        $(this).css('background-image', 'url(' + src + ')');
    });
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__("./node_modules/jquery/src/jquery.js")))

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/profile.js");


/***/ })

},[2]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Byb2ZpbGUuanMiXSwibmFtZXMiOlsiJCIsIndpbmRvdyIsIkVjaG8iLCJwcml2YXRlIiwidXNlciIsIm5vdGlmaWNhdGlvbiIsImNvbnNvbGUiLCJsb2ciLCJ1cmwiLCJkYXRhIiwic2VuZGVyIiwidHlwZSIsImRpdiIsImNsYXNzIiwicCIsInRleHQiLCJib2R5IiwiYXR0ciIsImFwcGVuZFRvIiwiYXZhdGFyIiwic3JjIiwicGhvdG8iLCJhbHQiLCJkYXRlIiwibGVuZ3RoIiwicHJlcGVuZCIsImFmdGVyIiwiaHRtbCIsIm5vdGlmeVNvdW5kIiwiZG9jdW1lbnQiLCJjcmVhdGVFbGVtZW50Iiwic2V0QXR0cmlidXRlIiwiYWRkRXZlbnRMaXN0ZW5lciIsInBsYXkiLCJtZXNzYWdlIiwiYWpheCIsIm1ldGhvZCIsImRhdGFUeXBlIiwiYmVmb3JlU2VuZCIsInByb3AiLCJhZGRDbGFzcyIsImRvbmUiLCJyZW1vdmUiLCJoYXNDbGFzcyIsInJlbW92ZUNsYXNzIiwiYWx3YXlzIiwidmFsIiwibWltZVR5cGUiLCJwcm9jZXNzRGF0YSIsImNvbnRlbnRUeXBlIiwiZ2lmcyIsInBhdGgiLCJlbCIsInF1ZXJ5IiwiZ2lwaHkiLCJlYWNoIiwiaSIsInZhbHVlIiwiYXBwZW5kIiwiaW1hZ2VzIiwiZml4ZWRfd2lkdGhfc21hbGwiLCJkb3duc2l6ZWQiLCJzdGlja2Vyc0dhbGxlcnkiLCJjbGljayIsImdpZkdhbGxlcnkiLCJvbiIsImZpbmQiLCJzVGhyZWFkIiwia2V5dXAiLCJjbGVhclRpbWVvdXQiLCJ0aGF0Iiwic2V0VGltZW91dCIsImdUaHJlYWQiLCJjaGFuZ2UiLCJnZXRFbGVtZW50QnlJZCIsImZpbGVzIiwicGFyYW1zIiwiRm9ybURhdGEiLCJfdG9rZW4iLCJyZWNlaXZlcl9pZCIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImJ0biIsInN1Y2Nlc3MiLCJmYWlsIiwidXNlcm5hbWUiLCJyZW1vdmVfYnRuIiwiaWQiLCJhZGRfYnRuIiwic3VibWl0IiwiY3JvcGl0IiwicXVhbGl0eSIsIm9yaWdpbmFsU2l6ZSIsImV4cG9ydFpvb20iLCJjc3MiXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEseUNBQUFBLEVBQUUsWUFBWTtBQUNWQyxXQUFPQyxJQUFQLENBQVlDLE9BQVosQ0FBb0IsbUJBQW1CQyxJQUF2QyxFQUNLQyxZQURMLENBQ2tCLFVBQUNBLFlBQUQsRUFBa0I7QUFDNUJDLGdCQUFRQyxHQUFSLENBQVlGLFlBQVo7QUFDQSxZQUFJRyxRQUFRSCxhQUFhSSxJQUFiLENBQWtCQyxNQUE5QixFQUFzQztBQUNsQyxnQkFBSUwsYUFBYU0sSUFBYixJQUFxQixxQ0FBekIsRUFBZ0U7QUFDNUQ7QUFDQSxvQkFBSUMsTUFBTVosRUFBRSxPQUFGLEVBQVcsRUFBRWEsT0FBTyxTQUFULEVBQVgsQ0FBVjtBQUNBLG9CQUFJUixhQUFhSSxJQUFiLENBQWtCRSxJQUFsQixLQUEyQixDQUEvQixFQUFrQztBQUM5Qix3QkFBSUcsSUFBSWQsRUFBRSxLQUFGLEVBQVMsRUFBRWEsT0FBTyxRQUFULEVBQW1CRSxNQUFNVixhQUFhSSxJQUFiLENBQWtCTyxJQUEzQyxFQUFULENBQVI7QUFDSDtBQUNELG9CQUFJWCxhQUFhSSxJQUFiLENBQWtCRSxJQUFsQixLQUEyQixDQUEvQixFQUFrQztBQUM5Qix3QkFBSUcsSUFBSWQsRUFBRSxLQUFGLEVBQVMsRUFBRWEsT0FBTyxjQUFULEVBQVQsQ0FBUjtBQUNIO0FBQ0Qsb0JBQUlSLGFBQWFJLElBQWIsQ0FBa0JFLElBQWxCLEtBQTJCLENBQS9CLEVBQWtDO0FBQzlCLHdCQUFJRyxJQUFJZCxFQUFFLEtBQUYsRUFBUyxFQUFFYSxPQUFPLGdCQUFULEVBQVQsQ0FBUjtBQUNIO0FBQ0Qsb0JBQUlSLGFBQWFJLElBQWIsQ0FBa0JFLElBQWxCLEtBQTJCLENBQS9CLEVBQWtDO0FBQzlCLHdCQUFJRyxJQUFJZCxFQUFFLEtBQUYsRUFBUyxFQUFFYSxPQUFPLFlBQVQsRUFBVCxDQUFSO0FBQ0g7QUFDRCxvQkFBSVIsYUFBYUksSUFBYixDQUFrQkUsSUFBbEIsR0FBeUIsQ0FBN0IsRUFBZ0M7QUFDNUJYLHNCQUFFLE9BQUYsRUFBV2lCLElBQVgsQ0FBZ0IsS0FBaEIsRUFBdUJaLGFBQWFJLElBQWIsQ0FBa0JPLElBQXpDLEVBQStDRSxRQUEvQyxDQUF3REosQ0FBeEQ7QUFDSDtBQUNEQSxrQkFBRUksUUFBRixDQUFXTixHQUFYO0FBQ0Esb0JBQUlPLFNBQVNuQixFQUFFLE9BQUYsRUFBVyxFQUFFYSxPQUFPLFFBQVQsRUFBWCxDQUFiO0FBQ0FiLGtCQUFFLE9BQUYsRUFBVyxFQUFFb0IsS0FBS2YsYUFBYUksSUFBYixDQUFrQlksS0FBbEIsR0FBMEIsTUFBakMsRUFBeUNDLEtBQUssZUFBOUMsRUFBWCxFQUE0RUosUUFBNUUsQ0FBcUZDLE1BQXJGO0FBQ0FBLHVCQUFPRCxRQUFQLENBQWdCTixHQUFoQjtBQUNBWixrQkFBRSxPQUFGLEVBQVcsRUFBRWEsT0FBTyxNQUFULEVBQWlCRSxNQUFNVixhQUFhSSxJQUFiLENBQWtCYyxJQUF6QyxFQUFYLEVBQTRETCxRQUE1RCxDQUFxRU4sR0FBckU7QUFDQSxvQkFBSVosRUFBRSxZQUFGLEVBQWdCd0IsTUFBaEIsR0FBeUIsQ0FBN0IsRUFBZ0M7QUFDNUJ4QixzQkFBRSxXQUFGLEVBQWV5QixPQUFmLENBQXVCYixHQUF2QjtBQUNILGlCQUZELE1BRU87QUFDSFosc0JBQUUsZUFBRixFQUFtQjBCLEtBQW5CLEdBQTJCQyxJQUEzQixDQUFnQ2YsR0FBaEM7QUFDSDs7QUFFRDtBQUNBLG9CQUFJZ0IsY0FBY0MsU0FBU0MsYUFBVCxDQUF1QixPQUF2QixDQUFsQjtBQUNBRiw0QkFBWUcsWUFBWixDQUF5QixLQUF6QixFQUFnQyx5QkFBaEM7QUFDQUgsNEJBQVlHLFlBQVosQ0FBeUIsVUFBekIsRUFBcUMsVUFBckM7QUFDQUgsNEJBQVlJLGdCQUFaLENBQTZCLE1BQTdCLEVBQXFDLFlBQVk7QUFDN0NKLGdDQUFZSyxJQUFaO0FBQ0gsaUJBRkQsRUFFRyxJQUZIO0FBSUg7QUFDRCxnQkFBSTVCLGFBQWFNLElBQWIsSUFBcUIsbUNBQXpCLEVBQThEO0FBQzFEO0FBQ0g7QUFDSjtBQUNKLEtBOUNMOztBQWdEQTtBQUNBLGFBQVN1QixPQUFULENBQWlCekIsSUFBakIsRUFBdUI7QUFDbkJULFVBQUVtQyxJQUFGLENBQU87QUFDSDNCLGlCQUFLLGdCQURGO0FBRUg0QixvQkFBUSxNQUZMO0FBR0gzQixrQkFBTUEsSUFISDtBQUlINEIsc0JBQVUsTUFKUDtBQUtIQyx3QkFBWSxzQkFBWTtBQUNwQnRDLGtCQUFFLGlCQUFGLEVBQXFCdUMsSUFBckIsQ0FBMEIsU0FBMUIsRUFBcUMsSUFBckMsRUFBMkNDLFFBQTNDLENBQW9ELFVBQXBEO0FBQ0g7QUFQRSxTQUFQLEVBUUdDLElBUkgsQ0FRUSxVQUFVaEMsSUFBVixFQUFnQjtBQUNwQlQsY0FBRSxrQkFBRixFQUFzQjBDLE1BQXRCO0FBQ0EsZ0JBQUk5QixNQUFNWixFQUFFLE9BQUYsRUFBVyxFQUFFYSxPQUFPLGdCQUFULEVBQVgsQ0FBVjtBQUNBLGdCQUFJSixLQUFLRSxJQUFMLElBQWEsQ0FBakIsRUFBb0I7QUFDaEIsb0JBQUlHLElBQUlkLEVBQUUsS0FBRixFQUFTLEVBQUVhLE9BQU8sUUFBVCxFQUFtQkUsTUFBTU4sS0FBS08sSUFBOUIsRUFBVCxDQUFSO0FBQ0g7QUFDRCxnQkFBSVAsS0FBS0UsSUFBTCxJQUFhLENBQWpCLEVBQW9CO0FBQ2hCLG9CQUFJRyxJQUFJZCxFQUFFLEtBQUYsRUFBUyxFQUFFYSxPQUFPLGdCQUFULEVBQVQsQ0FBUjtBQUNBYixrQkFBRSxPQUFGLEVBQVdpQixJQUFYLENBQWdCLEtBQWhCLEVBQXVCUixLQUFLTyxJQUE1QixFQUFrQ0UsUUFBbEMsQ0FBMkNKLENBQTNDO0FBQ0g7QUFDRCxnQkFBSUwsS0FBS0UsSUFBTCxJQUFhLENBQWpCLEVBQW9CO0FBQ2hCLG9CQUFJRyxJQUFJZCxFQUFFLEtBQUYsRUFBUyxFQUFFYSxPQUFPLFlBQVQsRUFBVCxDQUFSO0FBQ0FiLGtCQUFFLE9BQUYsRUFBV2lCLElBQVgsQ0FBZ0IsS0FBaEIsRUFBdUJSLEtBQUtPLElBQTVCLEVBQWtDRSxRQUFsQyxDQUEyQ0osQ0FBM0M7QUFDSDtBQUNEQSxjQUFFSSxRQUFGLENBQVdOLEdBQVg7QUFDQVosY0FBRSxPQUFGLEVBQVcsRUFBRWEsT0FBTyxNQUFULEVBQWlCRSxNQUFNTixLQUFLYyxJQUE1QixFQUFYLEVBQStDTCxRQUEvQyxDQUF3RE4sR0FBeEQ7QUFDQSxnQkFBSVosRUFBRSxXQUFGLEVBQWUyQyxRQUFmLENBQXdCLFFBQXhCLENBQUosRUFBdUM7QUFBRTNDLGtCQUFFLFdBQUYsRUFBZTRDLFdBQWYsQ0FBMkIsUUFBM0I7QUFBdUM7QUFDaEY1QyxjQUFFLFdBQUYsRUFBZXlCLE9BQWYsQ0FBdUJiLEdBQXZCO0FBRUgsU0EzQkQsRUEyQkdpQyxNQTNCSCxDQTJCVSxZQUFZO0FBQ2xCN0MsY0FBRSxpQkFBRixFQUFxQnVDLElBQXJCLENBQTBCLFNBQTFCLEVBQXFDLEtBQXJDLEVBQTRDSyxXQUE1QyxDQUF3RCxVQUF4RDtBQUNBNUMsY0FBRSxPQUFGLEVBQVc4QyxHQUFYLENBQWUsRUFBZjtBQUNILFNBOUJEO0FBK0JIOztBQUVEO0FBQ0EsYUFBU3pCLEtBQVQsQ0FBZVosSUFBZixFQUFxQjtBQUNqQlQsVUFBRW1DLElBQUYsQ0FBTztBQUNIM0IsaUJBQUssZ0JBREY7QUFFSDRCLG9CQUFRLE1BRkw7QUFHSDNCLGtCQUFNQSxJQUhIO0FBSUg0QixzQkFBVSxNQUpQO0FBS0hVLHNCQUFVLHFCQUxQO0FBTUhDLHlCQUFhLEtBTlY7QUFPSEMseUJBQWEsS0FQVjtBQVFIWCx3QkFBWSxzQkFBWTtBQUNwQnRDLGtCQUFFLGlCQUFGLEVBQXFCdUMsSUFBckIsQ0FBMEIsU0FBMUIsRUFBcUMsSUFBckMsRUFBMkNDLFFBQTNDLENBQW9ELFVBQXBEO0FBQ0g7QUFWRSxTQUFQLEVBV0dDLElBWEgsQ0FXUSxVQUFVaEMsSUFBVixFQUFnQjtBQUNwQlQsY0FBRSxrQkFBRixFQUFzQjBDLE1BQXRCO0FBQ0EsZ0JBQUk5QixNQUFNWixFQUFFLE9BQUYsRUFBVyxFQUFFYSxPQUFPLGdCQUFULEVBQVgsQ0FBVjtBQUFBLGdCQUNJQyxJQUFJZCxFQUFFLEtBQUYsRUFBUyxFQUFFYSxPQUFPLGNBQVQsRUFBVCxDQURSO0FBRUFiLGNBQUUsT0FBRixFQUFXaUIsSUFBWCxDQUFnQixLQUFoQixFQUF1QlIsS0FBS08sSUFBNUIsRUFBa0NFLFFBQWxDLENBQTJDSixDQUEzQztBQUNBQSxjQUFFSSxRQUFGLENBQVdOLEdBQVg7QUFDQVosY0FBRSxPQUFGLEVBQVcsRUFBRWEsT0FBTyxNQUFULEVBQWlCRSxNQUFNTixLQUFLYyxJQUE1QixFQUFYLEVBQStDTCxRQUEvQyxDQUF3RE4sR0FBeEQ7QUFDQSxnQkFBSVosRUFBRSxXQUFGLEVBQWUyQyxRQUFmLENBQXdCLFFBQXhCLENBQUosRUFBdUM7QUFBRTNDLGtCQUFFLFdBQUYsRUFBZTRDLFdBQWYsQ0FBMkIsUUFBM0I7QUFBdUM7QUFDaEY1QyxjQUFFLFdBQUYsRUFBZXlCLE9BQWYsQ0FBdUJiLEdBQXZCO0FBRUgsU0FyQkQsRUFxQkdpQyxNQXJCSCxDQXFCVSxZQUFZO0FBQ2xCN0MsY0FBRSxpQkFBRixFQUFxQnVDLElBQXJCLENBQTBCLFNBQTFCLEVBQXFDLEtBQXJDLEVBQTRDSyxXQUE1QyxDQUF3RCxVQUF4RDtBQUNILFNBdkJEO0FBd0JIOztBQUVEO0FBQ0EsYUFBU00sSUFBVCxDQUFjQyxJQUFkLEVBQW9CQyxFQUFwQixFQUF3QnpDLElBQXhCLEVBQTBDO0FBQUEsWUFBWjBDLEtBQVksdUVBQUosRUFBSTs7QUFDdENyRCxVQUFFbUMsSUFBRixDQUFPO0FBQ0gzQixpQkFBSyxZQUFZMkMsSUFBWixHQUFtQixHQUFuQixHQUF5QkUsS0FEM0I7QUFFSGpCLG9CQUFRLEtBRkw7QUFHSDNCLGtCQUFNLE1BSEg7QUFJSDZCLHdCQUFZLHNCQUFZO0FBQ3BCYyxtQkFBR3pCLElBQUgsQ0FBUSxvRkFBUjtBQUNIO0FBTkUsU0FBUCxFQU9HYyxJQVBILENBT1EsVUFBVWEsS0FBVixFQUFpQjtBQUNyQkYsZUFBR3pCLElBQUgsQ0FBUSxFQUFSO0FBQ0EzQixjQUFFdUQsSUFBRixDQUFPRCxNQUFNN0MsSUFBYixFQUFtQixVQUFVK0MsQ0FBVixFQUFhQyxLQUFiLEVBQW9CO0FBQ25DLG9CQUFJOUMsUUFBUSxDQUFaLEVBQWU7QUFDWHlDLHVCQUFHTSxNQUFILENBQVUxRCxFQUFFLE9BQUYsRUFBVyxFQUFFYSxPQUFPLGdCQUFULEVBQTJCSixNQUFNLEVBQUUsUUFBUUUsSUFBVixFQUFqQyxFQUFtRFMsS0FBS3FDLE1BQU1FLE1BQU4sQ0FBYUMsaUJBQWIsQ0FBK0JwRCxHQUF2RixFQUFYLENBQVY7QUFDSDtBQUNELG9CQUFJRyxRQUFRLENBQVosRUFBZTtBQUNYeUMsdUJBQUdNLE1BQUgsQ0FBVTFELEVBQUUsT0FBRixFQUFXLEVBQUVhLE9BQU8sWUFBVCxFQUF1QkosTUFBTSxFQUFFLFFBQVFFLElBQVYsRUFBN0IsRUFBK0NTLEtBQUtxQyxNQUFNRSxNQUFOLENBQWFFLFNBQWIsQ0FBdUJyRCxHQUEzRSxFQUFYLENBQVY7QUFDSDtBQUNKLGFBUEQ7QUFRSCxTQWpCRDtBQWtCSDs7QUFFRDtBQUNBLFFBQUlzRCxrQkFBa0I5RCxFQUFFLHVCQUFGLENBQXRCO0FBQ0FBLE1BQUUsZUFBRixFQUFtQitELEtBQW5CLENBQXlCLFlBQVk7QUFDakNiLGFBQUssVUFBTCxFQUFpQlksZUFBakIsRUFBa0MsQ0FBbEM7QUFDSCxLQUZEOztBQUlBO0FBQ0E5RCxNQUFFLFdBQUYsRUFBZStELEtBQWYsQ0FBcUIsWUFBWTtBQUM3QixZQUFJQyxhQUFhaEUsRUFBRSxtQkFBRixDQUFqQjtBQUNBa0QsYUFBSyxVQUFMLEVBQWlCYyxVQUFqQixFQUE2QixDQUE3QjtBQUNILEtBSEQ7O0FBS0E7QUFDQWhFLE1BQUUsa0JBQUYsRUFBc0JpRSxFQUF0QixDQUF5QixvQkFBekIsRUFBK0MsWUFBWTtBQUN2RGpFLFVBQUUsSUFBRixFQUFRa0UsSUFBUixDQUFhLGVBQWIsRUFBOEJwQixHQUE5QixDQUFrQyxFQUFsQztBQUNILEtBRkQ7O0FBSUE7QUFDQSxRQUFJcUIsVUFBVSxJQUFkO0FBQ0FuRSxNQUFFLDRCQUFGLEVBQWdDb0UsS0FBaEMsQ0FBc0MsWUFBWTtBQUM5Q0MscUJBQWFGLE9BQWI7QUFDQSxZQUFJRyxPQUFPdEUsRUFBRSxJQUFGLENBQVg7QUFDQW1FLGtCQUFVSSxXQUFXLFlBQVk7QUFDN0IsZ0JBQUlELEtBQUt4QixHQUFMLE1BQWMsRUFBbEIsRUFBc0I7QUFDbEJJLHFCQUFLLGlCQUFMLEVBQXdCbEQsRUFBRSx1QkFBRixDQUF4QixFQUFvRCxDQUFwRCxFQUF1RHNFLEtBQUt4QixHQUFMLEVBQXZEO0FBQ0g7QUFDSixTQUpTLEVBSVAsR0FKTyxDQUFWO0FBS0gsS0FSRDs7QUFVQTtBQUNBLFFBQUkwQixVQUFVLElBQWQ7QUFDQXhFLE1BQUUsd0JBQUYsRUFBNEJvRSxLQUE1QixDQUFrQyxZQUFZO0FBQzFDQyxxQkFBYUcsT0FBYjtBQUNBLFlBQUlGLE9BQU90RSxFQUFFLElBQUYsQ0FBWDtBQUNBd0Usa0JBQVVELFdBQVcsWUFBWTtBQUM3QixnQkFBSUQsS0FBS3hCLEdBQUwsTUFBYyxFQUFsQixFQUFzQjtBQUNsQkkscUJBQUssUUFBTCxFQUFlbEQsRUFBRSxtQkFBRixDQUFmLEVBQXVDLENBQXZDLEVBQTBDc0UsS0FBS3hCLEdBQUwsRUFBMUM7QUFDSDtBQUNKLFNBSlMsRUFJUCxHQUpPLENBQVY7QUFLSCxLQVJEOztBQVVBO0FBQ0E5QyxNQUFFLFFBQUYsRUFBWXlFLE1BQVosQ0FBbUIsWUFBWTtBQUMzQixZQUFJNUMsU0FBUzZDLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNDLEtBQWpDLENBQXVDbkQsTUFBdkMsR0FBZ0QsQ0FBcEQsRUFBdUQ7QUFDbkQsZ0JBQUlvRCxTQUFTLElBQUlDLFFBQUosRUFBYjtBQUNBRCxtQkFBT2xCLE1BQVAsQ0FBYyxRQUFkLEVBQXdCMUQsRUFBRSxhQUFGLEVBQWlCOEMsR0FBakIsRUFBeEI7QUFDQThCLG1CQUFPbEIsTUFBUCxDQUFjLGFBQWQsRUFBNkIxRCxFQUFFLGNBQUYsRUFBa0I4QyxHQUFsQixFQUE3QjtBQUNBOEIsbUJBQU9sQixNQUFQLENBQWMsT0FBZCxFQUF1QjFELEVBQUUsUUFBRixFQUFZLENBQVosRUFBZTJFLEtBQWYsQ0FBcUIsQ0FBckIsQ0FBdkI7QUFDQUMsbUJBQU9sQixNQUFQLENBQWMsTUFBZCxFQUFzQixDQUF0QjtBQUNBckMsa0JBQU11RCxNQUFOO0FBQ0g7QUFDSixLQVREOztBQVdBO0FBQ0E1RSxNQUFFNkIsUUFBRixFQUFZb0MsRUFBWixDQUFlLE9BQWYsRUFBd0IsNkNBQXhCLEVBQXVFLFlBQVk7QUFDL0UsWUFBSVcsU0FBUztBQUNURSxvQkFBUTlFLEVBQUUsYUFBRixFQUFpQjhDLEdBQWpCLEVBREM7QUFFVGlDLHlCQUFhL0UsRUFBRSxjQUFGLEVBQWtCOEMsR0FBbEIsRUFGSjtBQUdUOUIsa0JBQU1oQixFQUFFLElBQUYsRUFBUWlCLElBQVIsQ0FBYSxLQUFiLENBSEc7QUFJVE4sa0JBQU1YLEVBQUUsSUFBRixFQUFRUyxJQUFSLENBQWEsTUFBYjtBQUpHLFNBQWI7QUFNQXlCLGdCQUFRMEMsTUFBUjtBQUNILEtBUkQ7O0FBVUE7QUFDQTVFLE1BQUUsaUJBQUYsRUFBcUIrRCxLQUFyQixDQUEyQixVQUFVaUIsQ0FBVixFQUFhO0FBQ3BDLFlBQUloRSxPQUFPaEIsRUFBRSxPQUFGLEVBQVc4QyxHQUFYLEVBQVg7QUFDQSxZQUFJOUIsUUFBUSxFQUFaLEVBQWdCO0FBQ1osbUJBQU8sS0FBUDtBQUNIO0FBQ0QsWUFBSTRELFNBQVM7QUFDVEUsb0JBQVE5RSxFQUFFLGFBQUYsRUFBaUI4QyxHQUFqQixFQURDO0FBRVRpQyx5QkFBYS9FLEVBQUUsY0FBRixFQUFrQjhDLEdBQWxCLEVBRko7QUFHVDlCLGtCQUFNQSxJQUhHO0FBSVRMLGtCQUFNO0FBSkcsU0FBYjtBQU1BdUIsZ0JBQVEwQyxNQUFSO0FBQ0FJLFVBQUVDLGNBQUY7QUFDSCxLQWJEOztBQWVBO0FBQ0FqRixNQUFFLFdBQUYsRUFBZStELEtBQWYsQ0FBcUIsWUFBWTtBQUM3QixZQUFJbUIsTUFBTWxGLEVBQUUsSUFBRixDQUFWO0FBQ0EsWUFBSTRFLFNBQVM7QUFDVEUsb0JBQVE5RSxFQUFFLGFBQUYsRUFBaUI4QyxHQUFqQixFQURDO0FBRVRpQyx5QkFBYS9FLEVBQUUsY0FBRixFQUFrQjhDLEdBQWxCO0FBRkosU0FBYjtBQUlBOUMsVUFBRW1DLElBQUYsQ0FBTztBQUNIM0IsaUJBQUssWUFERjtBQUVINEIsb0JBQVEsTUFGTDtBQUdIQyxzQkFBVSxNQUhQO0FBSUg1QixrQkFBTW1FLE1BSkg7QUFLSHRDLHdCQUFZLHNCQUFZO0FBQ3BCNEMsb0JBQUkzQyxJQUFKLENBQVMsVUFBVCxFQUFxQixJQUFyQixFQUEyQkMsUUFBM0IsQ0FBb0MsVUFBcEM7QUFDSDtBQVBFLFNBQVAsRUFRR0MsSUFSSCxDQVFRLFVBQVVoQyxJQUFWLEVBQWdCO0FBQ3BCLGdCQUFJQSxLQUFLMEUsT0FBTCxJQUFnQixJQUFwQixFQUEwQjtBQUN0Qm5GLGtCQUFFLFdBQUYsRUFBZXVDLElBQWYsQ0FBb0IsVUFBcEIsRUFBZ0MsSUFBaEMsRUFBc0NDLFFBQXRDLENBQStDLE9BQS9DLEVBQXdEYixJQUF4RCxDQUE2RCxrQ0FBN0Q7QUFDSDtBQUNKLFNBWkQsRUFZR3lELElBWkgsQ0FZUSxVQUFVM0UsSUFBVixFQUFnQjtBQUNwQjtBQUNILFNBZEQsRUFjR29DLE1BZEgsQ0FjVSxVQUFVcEMsSUFBVixFQUFnQjtBQUN0QjtBQUNBeUUsZ0JBQUkzQyxJQUFKLENBQVMsVUFBVCxFQUFxQixLQUFyQixFQUE0QkssV0FBNUIsQ0FBd0MsVUFBeEM7QUFDSCxTQWpCRDtBQWtCSCxLQXhCRDs7QUEwQkE7QUFDQTVDLE1BQUUsVUFBRixFQUFjaUUsRUFBZCxDQUFpQixPQUFqQixFQUEwQixlQUExQixFQUEyQyxZQUFZO0FBQ25ELFlBQUlpQixNQUFNbEYsRUFBRSxJQUFGLENBQVY7QUFDQSxZQUFJNEUsU0FBUztBQUNURSxvQkFBUTlFLEVBQUUsYUFBRixFQUFpQjhDLEdBQWpCLEVBREM7QUFFVHVDLHNCQUFVckYsRUFBRSxXQUFGLEVBQWU4QyxHQUFmO0FBRkQsU0FBYjtBQUlBOUMsVUFBRW1DLElBQUYsQ0FBTztBQUNIM0IsaUJBQUssdUJBREY7QUFFSDRCLG9CQUFRLE1BRkw7QUFHSEMsc0JBQVUsTUFIUDtBQUlINUIsa0JBQU1tRSxNQUpIO0FBS0h0Qyx3QkFBWSxzQkFBWTtBQUNwQjRDLG9CQUFJM0MsSUFBSixDQUFTLFVBQVQsRUFBcUIsSUFBckIsRUFBMkJDLFFBQTNCLENBQW9DLFVBQXBDLEVBQWdEekIsSUFBaEQsQ0FBcUQsRUFBckQsRUFBeUQyQyxNQUF6RCxDQUFnRSxxQ0FBaEU7QUFDSDtBQVBFLFNBQVAsRUFRR2pCLElBUkgsQ0FRUSxVQUFVaEMsSUFBVixFQUFnQjtBQUNwQixnQkFBSUEsS0FBSzBFLE9BQUwsSUFBZ0IsSUFBcEIsRUFBMEI7QUFDdEJELG9CQUFJeEMsTUFBSjtBQUNBLG9CQUFJNEMsYUFBYXRGLEVBQUUsVUFBRixFQUFjLEVBQUV1RixJQUFJLGlCQUFOLEVBQXlCMUUsT0FBTyx3QkFBaEMsRUFBMERGLE1BQU0sUUFBaEUsRUFBMEVJLE1BQU0saUJBQWhGLEVBQWQsQ0FBakI7QUFDQWYsa0JBQUUsVUFBRixFQUFjMEQsTUFBZCxDQUFxQjRCLFVBQXJCO0FBQ0g7QUFDSixTQWRELEVBY0d6QyxNQWRILENBY1UsVUFBVXBDLElBQVYsRUFBZ0I7QUFDdEJ5RSxnQkFBSTNDLElBQUosQ0FBUyxVQUFULEVBQXFCLEtBQXJCLEVBQTRCSyxXQUE1QixDQUF3QyxVQUF4QztBQUNILFNBaEJEO0FBaUJILEtBdkJEOztBQXlCQTtBQUNBNUMsTUFBRSxVQUFGLEVBQWNpRSxFQUFkLENBQWlCLE9BQWpCLEVBQTBCLGtCQUExQixFQUE4QyxZQUFZO0FBQ3RELFlBQUlpQixNQUFNbEYsRUFBRSxJQUFGLENBQVY7QUFDQSxZQUFJNEUsU0FBUztBQUNURSxvQkFBUTlFLEVBQUUsYUFBRixFQUFpQjhDLEdBQWpCLEVBREM7QUFFVHVDLHNCQUFVckYsRUFBRSxXQUFGLEVBQWU4QyxHQUFmO0FBRkQsU0FBYjtBQUlBOUMsVUFBRW1DLElBQUYsQ0FBTztBQUNIM0IsaUJBQUssMEJBREY7QUFFSDRCLG9CQUFRLE1BRkw7QUFHSEMsc0JBQVUsTUFIUDtBQUlINUIsa0JBQU1tRSxNQUpIO0FBS0h0Qyx3QkFBWSxzQkFBWTtBQUNwQjRDLG9CQUFJM0MsSUFBSixDQUFTLFVBQVQsRUFBcUIsSUFBckIsRUFBMkJDLFFBQTNCLENBQW9DLFVBQXBDLEVBQWdEekIsSUFBaEQsQ0FBcUQsRUFBckQsRUFBeUQyQyxNQUF6RCxDQUFnRSxxQ0FBaEU7QUFDSDtBQVBFLFNBQVAsRUFRR2pCLElBUkgsQ0FRUSxVQUFVaEMsSUFBVixFQUFnQjtBQUNwQixnQkFBSUEsS0FBSzBFLE9BQUwsSUFBZ0IsSUFBcEIsRUFBMEI7QUFDdEJELG9CQUFJeEMsTUFBSjtBQUNBLG9CQUFJOEMsVUFBVXhGLEVBQUUsVUFBRixFQUFjLEVBQUV1RixJQUFJLGNBQU4sRUFBc0IxRSxPQUFPLHdCQUE3QixFQUF1REYsTUFBTSxRQUE3RCxFQUF1RUksTUFBTSxrQkFBN0UsRUFBZCxDQUFkO0FBQ0FmLGtCQUFFLFVBQUYsRUFBYzBELE1BQWQsQ0FBcUI4QixPQUFyQjtBQUNIO0FBQ0osU0FkRCxFQWNHM0MsTUFkSCxDQWNVLFVBQVVwQyxJQUFWLEVBQWdCO0FBQ3RCeUUsZ0JBQUkzQyxJQUFKLENBQVMsVUFBVCxFQUFxQixLQUFyQixFQUE0QkssV0FBNUIsQ0FBd0MsVUFBeEM7QUFDSCxTQWhCRDtBQWlCSCxLQXZCRDs7QUF5QkE7QUFDQTVDLE1BQUUsbUJBQUYsRUFBdUJ5RixNQUF2QixDQUE4QixVQUFVVCxDQUFWLEVBQWE7QUFDdkMsWUFBSTNELFFBQVFyQixFQUFFLGVBQUYsRUFBbUIwRixNQUFuQixDQUEwQixRQUExQixFQUFvQztBQUM1Qy9FLGtCQUFNLFlBRHNDO0FBRTVDZ0YscUJBQVMsQ0FGbUM7QUFHNUNDLDBCQUFjLElBSDhCO0FBSTVDQyx3QkFBWTtBQUpnQyxTQUFwQyxDQUFaO0FBTUE3RixVQUFFLFFBQUYsRUFBWThDLEdBQVosQ0FBZ0J6QixLQUFoQjtBQUNILEtBUkQ7O0FBVUE7QUFDQXJCLE1BQUUsb0JBQUYsRUFBd0J1RCxJQUF4QixDQUE2QixVQUFVQyxDQUFWLEVBQWE7QUFDdEMsWUFBSXBDLE1BQU1wQixFQUFFLElBQUYsRUFBUVMsSUFBUixDQUFhLEtBQWIsQ0FBVjtBQUNBVCxVQUFFLElBQUYsRUFBUThGLEdBQVIsQ0FBWSxrQkFBWixFQUFnQyxTQUFTMUUsR0FBVCxHQUFlLEdBQS9DO0FBQ0gsS0FIRDtBQUtILENBdFRELEUiLCJmaWxlIjoiXFxqc1xccHJvZmlsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiQoZnVuY3Rpb24gKCkge1xyXG4gICAgd2luZG93LkVjaG8ucHJpdmF0ZSgnbm90aWZpY2F0aW9ucy4nICsgdXNlcilcclxuICAgICAgICAubm90aWZpY2F0aW9uKChub3RpZmljYXRpb24pID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cobm90aWZpY2F0aW9uKTtcclxuICAgICAgICAgICAgaWYgKHVybCA9PT0gbm90aWZpY2F0aW9uLmRhdGEuc2VuZGVyKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAobm90aWZpY2F0aW9uLnR5cGUgPT0gJ0FwcFxcXFxOb3RpZmljYXRpb25zXFxcXE1lc3NhZ2VJbmNvbWluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBDcmVhdGUgbWVzc2FnZVxyXG4gICAgICAgICAgICAgICAgICAgIHZhciBkaXYgPSAkKCc8ZGl2PicsIHsgY2xhc3M6ICdtZXNzYWdlJyB9KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobm90aWZpY2F0aW9uLmRhdGEudHlwZSA9PT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcCA9ICQoJzxwPicsIHsgY2xhc3M6ICdidWJibGUnLCB0ZXh0OiBub3RpZmljYXRpb24uZGF0YS5ib2R5IH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAobm90aWZpY2F0aW9uLmRhdGEudHlwZSA9PT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcCA9ICQoJzxwPicsIHsgY2xhc3M6ICdidWJibGUgcGhvdG8nIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAobm90aWZpY2F0aW9uLmRhdGEudHlwZSA9PT0gMikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcCA9ICQoJzxwPicsIHsgY2xhc3M6ICdidWJibGUgc3RpY2tlcicgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChub3RpZmljYXRpb24uZGF0YS50eXBlID09PSAzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwID0gJCgnPHA+JywgeyBjbGFzczogJ2J1YmJsZSBnaWYnIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAobm90aWZpY2F0aW9uLmRhdGEudHlwZSA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnPGltZz4nKS5hdHRyKCdzcmMnLCBub3RpZmljYXRpb24uZGF0YS5ib2R5KS5hcHBlbmRUbyhwKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcC5hcHBlbmRUbyhkaXYpO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBhdmF0YXIgPSAkKCc8ZGl2PicsIHsgY2xhc3M6ICdhdmF0YXInIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICQoJzxpbWc+JywgeyBzcmM6IG5vdGlmaWNhdGlvbi5kYXRhLnBob3RvICsgJy5qcGcnLCBhbHQ6ICdQcm9maWxlIFBob3RvJyB9KS5hcHBlbmRUbyhhdmF0YXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGF2YXRhci5hcHBlbmRUbyhkaXYpO1xyXG4gICAgICAgICAgICAgICAgICAgICQoJzxkaXY+JywgeyBjbGFzczogJ2RhdGUnLCB0ZXh0OiBub3RpZmljYXRpb24uZGF0YS5kYXRlIH0pLmFwcGVuZFRvKGRpdik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCQoJy5tZXNzc2FnZXMnKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5tZXNzYWdlcycpLnByZXBlbmQoZGl2KTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjbWVzc2FnZS1mb3JtJykuYWZ0ZXIoKS5odG1sKGRpdik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBOb3RpZmljYXRpb24gU291bmRcclxuICAgICAgICAgICAgICAgICAgICB2YXIgbm90aWZ5U291bmQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhdWRpbycpO1xyXG4gICAgICAgICAgICAgICAgICAgIG5vdGlmeVNvdW5kLnNldEF0dHJpYnV0ZSgnc3JjJywgJ3NvdW5kcy9ub3RpZmljYXRpb24ubXAzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgbm90aWZ5U291bmQuc2V0QXR0cmlidXRlKCdhdXRvcGxheScsICdhdXRvcGxheScpO1xyXG4gICAgICAgICAgICAgICAgICAgIG5vdGlmeVNvdW5kLmFkZEV2ZW50TGlzdGVuZXIoXCJsb2FkXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbm90aWZ5U291bmQucGxheSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIHRydWUpO1xyXG5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChub3RpZmljYXRpb24udHlwZSA9PSAnQXBwXFxcXE5vdGlmaWNhdGlvbnNcXFxcUHJvZmlsZVZpZXdlZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyB2aWV3ZWQuLlxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgLy8gTWVzc2FnZSBGdW5jdGlvblxyXG4gICAgZnVuY3Rpb24gbWVzc2FnZShkYXRhKSB7XHJcbiAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgdXJsOiAnL21lc3NhZ2VzL3NlbmQnLFxyXG4gICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgZGF0YVR5cGU6ICdKU09OJyxcclxuICAgICAgICAgICAgYmVmb3JlU2VuZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgJCgnI21lc3NhZ2Utc3VibWl0JykucHJvcCgnZGlhYmxlZCcsIHRydWUpLmFkZENsYXNzKCdkaXNhYmxlZCcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICAkKCcubWVzc2FnZXMgLmVtcHR5JykucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgIHZhciBkaXYgPSAkKCc8ZGl2PicsIHsgY2xhc3M6ICdtZXNzYWdlIHNlbmRlcicgfSk7XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnR5cGUgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgdmFyIHAgPSAkKCc8cD4nLCB7IGNsYXNzOiAnYnViYmxlJywgdGV4dDogZGF0YS5ib2R5IH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnR5cGUgPT0gMikge1xyXG4gICAgICAgICAgICAgICAgdmFyIHAgPSAkKCc8cD4nLCB7IGNsYXNzOiAnYnViYmxlIHN0aWNrZXInIH0pO1xyXG4gICAgICAgICAgICAgICAgJCgnPGltZz4nKS5hdHRyKCdzcmMnLCBkYXRhLmJvZHkpLmFwcGVuZFRvKHApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnR5cGUgPT0gMykge1xyXG4gICAgICAgICAgICAgICAgdmFyIHAgPSAkKCc8cD4nLCB7IGNsYXNzOiAnYnViYmxlIGdpZicgfSk7XHJcbiAgICAgICAgICAgICAgICAkKCc8aW1nPicpLmF0dHIoJ3NyYycsIGRhdGEuYm9keSkuYXBwZW5kVG8ocCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcC5hcHBlbmRUbyhkaXYpO1xyXG4gICAgICAgICAgICAkKCc8ZGl2PicsIHsgY2xhc3M6ICdkYXRlJywgdGV4dDogZGF0YS5kYXRlIH0pLmFwcGVuZFRvKGRpdik7XHJcbiAgICAgICAgICAgIGlmICgkKCcubWVzc2FnZXMnKS5oYXNDbGFzcygnZC1ub25lJykpIHsgJCgnLm1lc3NhZ2VzJykucmVtb3ZlQ2xhc3MoJ2Qtbm9uZScpOyB9XHJcbiAgICAgICAgICAgICQoJy5tZXNzYWdlcycpLnByZXBlbmQoZGl2KTtcclxuXHJcbiAgICAgICAgfSkuYWx3YXlzKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJCgnI21lc3NhZ2Utc3VibWl0JykucHJvcCgnZGlhYmxlZCcsIGZhbHNlKS5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcclxuICAgICAgICAgICAgJCgnI2JvZHknKS52YWwoJycpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFBob3RvIE1lc3NhZ2VcclxuICAgIGZ1bmN0aW9uIHBob3RvKGRhdGEpIHtcclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICB1cmw6ICcvbWVzc2FnZXMvc2VuZCcsXHJcbiAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICBkYXRhOiBkYXRhLFxyXG4gICAgICAgICAgICBkYXRhVHlwZTogJ0pTT04nLFxyXG4gICAgICAgICAgICBtaW1lVHlwZTogXCJtdWx0aXBhcnQvZm9ybS1kYXRhXCIsXHJcbiAgICAgICAgICAgIHByb2Nlc3NEYXRhOiBmYWxzZSxcclxuICAgICAgICAgICAgY29udGVudFR5cGU6IGZhbHNlLFxyXG4gICAgICAgICAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcjbWVzc2FnZS1zdWJtaXQnKS5wcm9wKCdkaWFibGVkJywgdHJ1ZSkuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICQoJy5tZXNzYWdlcyAuZW1wdHknKS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgdmFyIGRpdiA9ICQoJzxkaXY+JywgeyBjbGFzczogJ21lc3NhZ2Ugc2VuZGVyJyB9KSxcclxuICAgICAgICAgICAgICAgIHAgPSAkKCc8cD4nLCB7IGNsYXNzOiAnYnViYmxlIHBob3RvJyB9KTtcclxuICAgICAgICAgICAgJCgnPGltZz4nKS5hdHRyKCdzcmMnLCBkYXRhLmJvZHkpLmFwcGVuZFRvKHApO1xyXG4gICAgICAgICAgICBwLmFwcGVuZFRvKGRpdik7XHJcbiAgICAgICAgICAgICQoJzxkaXY+JywgeyBjbGFzczogJ2RhdGUnLCB0ZXh0OiBkYXRhLmRhdGUgfSkuYXBwZW5kVG8oZGl2KTtcclxuICAgICAgICAgICAgaWYgKCQoJy5tZXNzYWdlcycpLmhhc0NsYXNzKCdkLW5vbmUnKSkgeyAkKCcubWVzc2FnZXMnKS5yZW1vdmVDbGFzcygnZC1ub25lJyk7IH1cclxuICAgICAgICAgICAgJCgnLm1lc3NhZ2VzJykucHJlcGVuZChkaXYpO1xyXG5cclxuICAgICAgICB9KS5hbHdheXMoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKCcjbWVzc2FnZS1zdWJtaXQnKS5wcm9wKCdkaWFibGVkJywgZmFsc2UpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFN0aWNrZXJzICYgR2lmcyBieSBHaXBoeVxyXG4gICAgZnVuY3Rpb24gZ2lmcyhwYXRoLCBlbCwgdHlwZSwgcXVlcnkgPSAnJykge1xyXG4gICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgIHVybDogJy9naXBoeS8nICsgcGF0aCArICcvJyArIHF1ZXJ5LFxyXG4gICAgICAgICAgICBtZXRob2Q6ICdHRVQnLFxyXG4gICAgICAgICAgICBkYXRhOiAnSlNPTicsXHJcbiAgICAgICAgICAgIGJlZm9yZVNlbmQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIGVsLmh0bWwoJzxpIGNsYXNzPVwiZmFzIGZhLWNpcmNsZS1ub3RjaCBmYS1zcGluXCI+PC9pPjxzcGFuIGNsYXNzPVwic3Itb25seVwiPkxvYWRpbmcuLi48L3NwYW4+Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5kb25lKGZ1bmN0aW9uIChnaXBoeSkge1xyXG4gICAgICAgICAgICBlbC5odG1sKCcnKTtcclxuICAgICAgICAgICAgJC5lYWNoKGdpcGh5LmRhdGEsIGZ1bmN0aW9uIChpLCB2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHR5cGUgPT0gMikge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsLmFwcGVuZCgkKCc8aW1nPicsIHsgY2xhc3M6ICdzdGlja2VyLXN1Ym1pdCcsIGRhdGE6IHsgJ3R5cGUnOiB0eXBlIH0sIHNyYzogdmFsdWUuaW1hZ2VzLmZpeGVkX3dpZHRoX3NtYWxsLnVybCB9KSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZSA9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZWwuYXBwZW5kKCQoJzxpbWc+JywgeyBjbGFzczogJ2dpZi1zdWJtaXQnLCBkYXRhOiB7ICd0eXBlJzogdHlwZSB9LCBzcmM6IHZhbHVlLmltYWdlcy5kb3duc2l6ZWQudXJsIH0pKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBQdWxsIHN0aWNrZXJzIG9uIGNsaWNrXHJcbiAgICB2YXIgc3RpY2tlcnNHYWxsZXJ5ID0gJCgnI3N0aWNrZXJzLWRkIC5nYWxsZXJ5Jyk7XHJcbiAgICAkKCcjc3RpY2tlcnMtYnRuJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGdpZnMoJ3N0aWNrZXJzJywgc3RpY2tlcnNHYWxsZXJ5LCAyKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIFB1bGwgZ2lmcyBvbiBjbGlja1xyXG4gICAgJCgnI2dpZnMtYnRuJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBnaWZHYWxsZXJ5ID0gJCgnI2dpZnMtZGQgLmdhbGxlcnknKTtcclxuICAgICAgICBnaWZzKCd0cmVuZGluZycsIGdpZkdhbGxlcnksIDMpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gQ2xlYXIgc2VhcmNoXHJcbiAgICAkKCcuZ2lmcywgLnN0aWNrZXJzJykub24oJ2hpZGRlbi5icy5kcm9wZG93bicsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKHRoaXMpLmZpbmQoJy5zZWFyY2gtaW5wdXQnKS52YWwoJycpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gU2VhcmNoIHN0aWNrZXJzXHJcbiAgICB2YXIgc1RocmVhZCA9IG51bGw7XHJcbiAgICAkKCcjc3RpY2tlcnMtZGQgLnNlYXJjaC1pbnB1dCcpLmtleXVwKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjbGVhclRpbWVvdXQoc1RocmVhZCk7XHJcbiAgICAgICAgdmFyIHRoYXQgPSAkKHRoaXMpO1xyXG4gICAgICAgIHNUaHJlYWQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgaWYgKHRoYXQudmFsKCkgIT0gJycpIHtcclxuICAgICAgICAgICAgICAgIGdpZnMoJ3N0aWNrZXJzL3NlYXJjaCcsICQoJyNzdGlja2Vycy1kZCAuZ2FsbGVyeScpLCAyLCB0aGF0LnZhbCgpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDUwMCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBTZWFyY2ggZ2lmc1xyXG4gICAgdmFyIGdUaHJlYWQgPSBudWxsO1xyXG4gICAgJCgnI2dpZnMtZGQgLnNlYXJjaC1pbnB1dCcpLmtleXVwKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBjbGVhclRpbWVvdXQoZ1RocmVhZCk7XHJcbiAgICAgICAgdmFyIHRoYXQgPSAkKHRoaXMpO1xyXG4gICAgICAgIGdUaHJlYWQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgaWYgKHRoYXQudmFsKCkgIT0gJycpIHtcclxuICAgICAgICAgICAgICAgIGdpZnMoJ3NlYXJjaCcsICQoJyNnaWZzLWRkIC5nYWxsZXJ5JyksIDMsIHRoYXQudmFsKCkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgNTAwKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIFBob3RvIHVwbG9hZFxyXG4gICAgJCgnI3Bob3RvJykuY2hhbmdlKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Bob3RvJykuZmlsZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB2YXIgcGFyYW1zID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgICAgIHBhcmFtcy5hcHBlbmQoJ190b2tlbicsICQoJyNjc3JmLXRva2VuJykudmFsKCkpO1xyXG4gICAgICAgICAgICBwYXJhbXMuYXBwZW5kKCdyZWNlaXZlcl9pZCcsICQoJyNyZWNlaXZlcl9pZCcpLnZhbCgpKTtcclxuICAgICAgICAgICAgcGFyYW1zLmFwcGVuZCgncGhvdG8nLCAkKCcjcGhvdG8nKVswXS5maWxlc1swXSk7XHJcbiAgICAgICAgICAgIHBhcmFtcy5hcHBlbmQoJ3R5cGUnLCAxKTtcclxuICAgICAgICAgICAgcGhvdG8ocGFyYW1zKTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBJbWFnZSwgc3RpY2tlciBvciBnaWYgc3VibWl0XHJcbiAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmltYWdlLXN1Ym1pdCwgLnN0aWNrZXItc3VibWl0LCAuZ2lmLXN1Ym1pdCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICBfdG9rZW46ICQoJyNjc3JmLXRva2VuJykudmFsKCksXHJcbiAgICAgICAgICAgIHJlY2VpdmVyX2lkOiAkKCcjcmVjZWl2ZXJfaWQnKS52YWwoKSxcclxuICAgICAgICAgICAgYm9keTogJCh0aGlzKS5hdHRyKCdzcmMnKSxcclxuICAgICAgICAgICAgdHlwZTogJCh0aGlzKS5kYXRhKCd0eXBlJylcclxuICAgICAgICB9O1xyXG4gICAgICAgIG1lc3NhZ2UocGFyYW1zKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIE1lc3NhZ2Ugc3VibWl0XHJcbiAgICAkKCcjbWVzc2FnZS1zdWJtaXQnKS5jbGljayhmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIHZhciBib2R5ID0gJCgnI2JvZHknKS52YWwoKTtcclxuICAgICAgICBpZiAoYm9keSA9PSAnJykge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgIF90b2tlbjogJCgnI2NzcmYtdG9rZW4nKS52YWwoKSxcclxuICAgICAgICAgICAgcmVjZWl2ZXJfaWQ6ICQoJyNyZWNlaXZlcl9pZCcpLnZhbCgpLFxyXG4gICAgICAgICAgICBib2R5OiBib2R5LFxyXG4gICAgICAgICAgICB0eXBlOiAwXHJcbiAgICAgICAgfTtcclxuICAgICAgICBtZXNzYWdlKHBhcmFtcyk7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gV2F2ZSBcclxuICAgICQoJyN3YXZlLWJ0bicpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICB2YXIgYnRuID0gJCh0aGlzKTtcclxuICAgICAgICB2YXIgcGFyYW1zID0ge1xyXG4gICAgICAgICAgICBfdG9rZW46ICQoJyNjc3JmLXRva2VuJykudmFsKCksXHJcbiAgICAgICAgICAgIHJlY2VpdmVyX2lkOiAkKCcjcmVjZWl2ZXJfaWQnKS52YWwoKSxcclxuICAgICAgICB9O1xyXG4gICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgIHVybDogJy93YXZlL3NlbmQnLFxyXG4gICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgZGF0YVR5cGU6ICdKU09OJyxcclxuICAgICAgICAgICAgZGF0YTogcGFyYW1zLFxyXG4gICAgICAgICAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBidG4ucHJvcCgnZGlzYWJsZWQnLCB0cnVlKS5hZGRDbGFzcygnZGlzYWJsZWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgaWYgKGRhdGEuc3VjY2VzcyA9PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcjd2F2ZS1idG4nKS5wcm9wKCdkaXNhYmxlZCcsIHRydWUpLmFkZENsYXNzKCd3YXZlZCcpLmh0bWwoJ1dhdmVkIDxpIGNsYXNzPVwiaWNvbi1jaGVja1wiPjwvaT4nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmZhaWwoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgLy8gZmFpbGVkLi5cclxuICAgICAgICB9KS5hbHdheXMoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgLy8gYWx3YXlzLi5cclxuICAgICAgICAgICAgYnRuLnByb3AoJ2Rpc2FibGVkJywgZmFsc2UpLnJlbW92ZUNsYXNzKCdkaXNhYmxlZCcpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gQWRkIEZhdm9yaXRlXHJcbiAgICAkKCcuYWN0aW9ucycpLm9uKCdjbGljaycsICcjYWRkLWZhdm9yaXRlJywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBidG4gPSAkKHRoaXMpO1xyXG4gICAgICAgIHZhciBwYXJhbXMgPSB7XHJcbiAgICAgICAgICAgIF90b2tlbjogJCgnI2NzcmYtdG9rZW4nKS52YWwoKSxcclxuICAgICAgICAgICAgdXNlcm5hbWU6ICQoJyN1c2VybmFtZScpLnZhbCgpLFxyXG4gICAgICAgIH07XHJcbiAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgdXJsOiAnL2FjY291bnQvZmF2b3JpdGUvYWRkJyxcclxuICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgIGRhdGFUeXBlOiAnSlNPTicsXHJcbiAgICAgICAgICAgIGRhdGE6IHBhcmFtcyxcclxuICAgICAgICAgICAgYmVmb3JlU2VuZDogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgYnRuLnByb3AoJ2Rpc2FibGVkJywgdHJ1ZSkuYWRkQ2xhc3MoJ2Rpc2FibGVkJykudGV4dCgnJykuYXBwZW5kKCc8aSBjbGFzcz1cImZhcyBmYS1zeW5jIGZhLXNwaW5cIj48L2k+Jyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhLnN1Y2Nlc3MgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICAgICAgYnRuLnJlbW92ZSgpO1xyXG4gICAgICAgICAgICAgICAgdmFyIHJlbW92ZV9idG4gPSAkKCc8YnV0dG9uPicsIHsgaWQ6ICdyZW1vdmUtZmF2b3JpdGUnLCBjbGFzczogJ2J0biBidG4tYmxvY2sgYnRuLWxpbmsnLCB0eXBlOiAnYnV0dG9uJywgdGV4dDogJ1JlbW92ZSBGYXZvcml0ZScgfSk7XHJcbiAgICAgICAgICAgICAgICAkKCcuYWN0aW9ucycpLmFwcGVuZChyZW1vdmVfYnRuKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmFsd2F5cyhmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICBidG4ucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSkucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBSZW1vdmUgRmF2b3JpdGVcclxuICAgICQoJy5hY3Rpb25zJykub24oJ2NsaWNrJywgJyNyZW1vdmUtZmF2b3JpdGUnLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGJ0biA9ICQodGhpcyk7XHJcbiAgICAgICAgdmFyIHBhcmFtcyA9IHtcclxuICAgICAgICAgICAgX3Rva2VuOiAkKCcjY3NyZi10b2tlbicpLnZhbCgpLFxyXG4gICAgICAgICAgICB1c2VybmFtZTogJCgnI3VzZXJuYW1lJykudmFsKCksXHJcbiAgICAgICAgfTtcclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICB1cmw6ICcvYWNjb3VudC9mYXZvcml0ZS9yZW1vdmUnLFxyXG4gICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgZGF0YVR5cGU6ICdKU09OJyxcclxuICAgICAgICAgICAgZGF0YTogcGFyYW1zLFxyXG4gICAgICAgICAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICBidG4ucHJvcCgnZGlzYWJsZWQnLCB0cnVlKS5hZGRDbGFzcygnZGlzYWJsZWQnKS50ZXh0KCcnKS5hcHBlbmQoJzxpIGNsYXNzPVwiZmFzIGZhLXN5bmMgZmEtc3BpblwiPjwvaT4nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgaWYgKGRhdGEuc3VjY2VzcyA9PSB0cnVlKSB7XHJcbiAgICAgICAgICAgICAgICBidG4ucmVtb3ZlKCk7XHJcbiAgICAgICAgICAgICAgICB2YXIgYWRkX2J0biA9ICQoJzxidXR0b24+JywgeyBpZDogJ2FkZC1mYXZvcml0ZScsIGNsYXNzOiAnYnRuIGJ0bi1ibG9jayBidG4tbGluaycsIHR5cGU6ICdidXR0b24nLCB0ZXh0OiAnQWRkIFRvIEZhdm9yaXRlcycgfSk7XHJcbiAgICAgICAgICAgICAgICAkKCcuYWN0aW9ucycpLmFwcGVuZChhZGRfYnRuKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pLmFsd2F5cyhmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICBidG4ucHJvcCgnZGlzYWJsZWQnLCBmYWxzZSkucmVtb3ZlQ2xhc3MoJ2Rpc2FibGVkJyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBBZGRpbmcgcGhvdG9zXHJcbiAgICAkKCcjdXNlci1waG90b3MtZm9ybScpLnN1Ym1pdChmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIHZhciBwaG90byA9ICQoJyNpbWFnZS1lZGl0b3InKS5jcm9waXQoJ2V4cG9ydCcsIHtcclxuICAgICAgICAgICAgdHlwZTogJ2ltYWdlL2pwZWcnLFxyXG4gICAgICAgICAgICBxdWFsaXR5OiAxLFxyXG4gICAgICAgICAgICBvcmlnaW5hbFNpemU6IHRydWUsXHJcbiAgICAgICAgICAgIGV4cG9ydFpvb206IHRydWVcclxuICAgICAgICB9KTtcclxuICAgICAgICAkKCcjcGhvdG8nKS52YWwocGhvdG8pO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLy8gRmF2b3JpdGVzIFByb2ZpbGUgUGhvdG9zXHJcbiAgICAkKCcuZmF2LXByb2ZpbGUtcGhvdG8nKS5lYWNoKGZ1bmN0aW9uIChpKSB7XHJcbiAgICAgICAgdmFyIHNyYyA9ICQodGhpcykuZGF0YSgnc3JjJyk7XHJcbiAgICAgICAgJCh0aGlzKS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCAndXJsKCcgKyBzcmMgKyAnKScpO1xyXG4gICAgfSk7XHJcblxyXG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2pzL3Byb2ZpbGUuanMiXSwic291cmNlUm9vdCI6IiJ9