<?php

return [

	'genders' => [
		'female',
		'male',
		'transgender',
	],

	'search' => [
		'distances' => [
			'normal' => [
				'30',
				'50',
				'75'
			],
			'premium' => [
				'100',
				'150',
				'200'
			]
		],
	],

	'social_media_urls' => [
		'instagram' => 'https://instagram.com',
		'facebook' => 'https://facebook.com',
		'twitter' => 'https://twitter.com'
	],

];