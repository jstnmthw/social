<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Giphy extends Model
{
   
    public static function curl($params, $path)
    {

        $data = array(
            'api_key' => 'IdKZGB88RubeWBCzp98jnCIhREs0yFlS',
        );
        $data = array_merge($data, $params);
        $geturl = http_build_query($data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.giphy.com'.$path.'?'.$geturl);  
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl);
        
        return $result;

    }

    public static function searchGifs($query)
    {

        $path = '/v1/gifs/search';
        $params = array(
            'q' => $query,
            'limit' => 15
        );

        $data = self::curl($params, $path);
        return \Response::json(json_decode($data));

    }

    public static function trending()
    {

        $path = '/v1/gifs/trending';
        $params = array(
            'limit' => 15
        );

        $data = self::curl($params, $path);
        return \Response::json(json_decode($data));

    }

    public static function searchStickers($query)
    {

        $path = '/v1/stickers/search';
        $params = array(
            'q' => $query,
            'limit' => 15
        );

        $data = self::curl($params, $path);
        return \Response::json(json_decode($data));

    }

    public static function stickers() 
    {

        $path = '/v1/stickers/trending';
        $params = array(
            'limit' => 20
        );

        $data = self::curl($params, $path);
        return \Response::json(json_decode($data));

    }

}
