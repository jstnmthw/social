<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'receiver_id',
        'body',
        'type',
        'read',
        'photo',
    ];

    /**
     * Belongs to User model
     *
     * @return object
     */
    public function User()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Check if user replied to message
     *
     * @param interger 
     * @return boolean
     */
    public static function is_replied($id)
    {
        $author = (int) $id;
        $user = Auth::user();

        $first = static::where('user_id', $author)->where('receiver_id', $user->id);
        $second = static::where('user_id', $user->id)->where('receiver_id', $author)->union($first)->orderBy('created_at', 'DESC')->first();

        if(is_null($second) || $second->user_id == $author) {
            return false;
        }
        return $second;
    }

    /**
     *  Throttle msg notifications
     *
     * @param integer
     * @return boolean
     */
    public static function throttleNotify($id)
    {
        $id = (int) $id;
        $last_msg_received = static::where('receiver_id', '=', $id)->orderBy('created_at', 'DESC')->first();
        if($last_msg_received) {
            $last_msg_date = $last_msg_received->created_at->addSeconds(3);
            $now = Carbon::now();
            return $now->gte($last_msg_date);
        } else {
            return true;
        }
    }

}
