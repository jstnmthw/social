<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use App\Notifications\CustomDbChannel;

class MessageIncoming extends Notification implements ShouldQueue
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {   
        return $notifiable->msg_notification ? [CustomDbChannel::class, 'broadcast', 'mail'] : [CustomDbChannel::class, 'broadcast'];
    }   

     /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'sender_id' => $this->data->sender_id,
            'sender' => $this->data->sender,
            'action' => 'has sent you a message.'
        ];
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'data' => $this->data
        ]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
        public function toMail($notifiable)
        {
            $url = 'https://www.thailovely.co/'.$this->data->sender;

            return (new MailMessage)
                ->subject('New Message')
                ->greeting('New Message!')
                ->line('You have received a new message from '.$this->data->sender.'!')
                ->action('View Message', $url)
                ->line('Go get em\' champ!');
        }

}
