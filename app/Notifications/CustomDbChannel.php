<?php
namespace App\Notifications;

use Illuminate\Notifications\Notification;

class CustomDbChannel 
{

    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toDatabase($notifiable);

        $obj = (object)[];
        $obj->action = $data['action'];
        $obj->sender = $data['sender'];

        return $notifiable->routeNotificationFor('database')->create([
            'id' => $notification->id,
            'sender_id'=> $data['sender_id'],
            'type' => get_class($notification),
            'data' => $obj,
            'read_at' => null,
        ]);
    }

}