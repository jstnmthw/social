<?php

namespace App\Http\ViewCreators;

use App\User;
use App\Message;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationCreator
{

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function create(View $view)
    {
        if(Auth::check()) {

            // Get user
            $user = Auth::user();

            // Set data
            $notifications_unread_count = $user->unreadNotifications->whereNotIn('type', ['App\\Notifications\\MessageIncoming'])->count();
            $notifications = $user->notifications()->whereNotIn('type', ['App\\Notifications\\MessageIncoming'])->limit(20)->orderBy('created_at')->get();

            $unread_msg_count = Message::where('receiver_id', $user->id)->where('read', 0)->get()->unique('user_id')->count();

            foreach ($notifications as $key => $value) {
                $sender = User::where('username', $value['data']['sender'])->first();

                if(!$sender->disabled){
                    $photo = (is_null($sender->profile->profile_photo)) ? false : $sender->profile->profile_photo;
                    $data = (object)array(
                        'type' => $value->type,
                        'username' => $sender->username,
                        'profile_photo' => $photo
                    );
                    $value->sender = $data;
                }else {
                    unset($notifications[$key]);
                }

            }

            // Attach data to view
            $view->with([
                'notifications' => $notifications,
                'notifications_unread_count' => $notifications_unread_count,
                'msg_unread_count' => $unread_msg_count,
            ]);

        }
    }

}
