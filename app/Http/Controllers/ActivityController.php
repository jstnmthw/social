<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Carbon\Carbon;
use App\Notifications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class ActivityController extends Controller
{
    
    public function index(Request $request)
    {

        $user = Auth::user();
        $activities = Notifications::where('notifiable_id', $user->id)
                                    ->Orwhere('sender_id', $user->id)
                                    ->orderBy('created_at', 'desc')->limit(90)->get();

        foreach ($activities as $activity) {

            $json       = json_decode($activity->data);
            $receiver   = User::findOrFail($activity->notifiable_id);
            $sender     = User::findOrFail($activity->sender_id);

            $activity->sender       = $json->sender;
            $activity->receiver     = $receiver->username;
            $activity->action       = $json->action;
            $activity->photo        = $sender->profile->profile_photo;
            $activity->when         = Carbon::parse($activity->created_at)->diffForHumans();

            if($user->id == $activity->sender_id) {
                $activity->photo  = $user->profile->profile_photo;
                $activity->sender = 'You';
                $activity->action = str_replace('has','have', $activity->action);
                $activity->action = str_replace('your', "<strong>".$receiver->username."</strong>'s", $activity->action);
                $activity->action = str_replace('you', "<strong>".$receiver->username."</strong>", $activity->action);
            }

            switch ($activity->type) {
                case 'App\Notifications\MessageIncoming':
                    $activity->icon = 'comment-alt';
                    break;                
                case 'App\Notifications\MessageSent':
                    $activity->icon = 'comment-alt';
                    break;
                case 'App\Notifications\ProfileViewed':
                    $activity->icon = 'eye';
                    break;
                case 'App\Notifications\WaveSent':
                    $activity->icon = 'hand-paper';
                    break;
            }


            unset($activity->data);
        }

        $page = $request->query('page') ? $request->query('page') : 1;
        $limit = 15;
        $offset = ($page * $limit) - $limit;
        $slice = array_slice($activities->toArray(), $offset, $limit);
        $paginate = new Paginator($slice, count($activities), $limit, $page, [
            'path'  => $request->url(),
            'query' => $request->query(),
        ]);

        return view('activity', [
            'activities' => (isset($paginate) ? $paginate : null)
        ]);

    }

}
