<?php

namespace App\Http\Controllers;

use App\Giphy;
use Illuminate\Http\Request;

class GiphyController extends Controller
{

    /**
     * Search Giphy gifs
     *
     * @return json
     */
    public function searchGifs($query)
    {
        $q = urlencode($query);
        return Giphy::searchGifs($q);
    }

    /**
     * Search Giphy stickers
     *
     * @return json
     */
    public function searchStickers($query)
    {
        $q = urlencode($query);
        return Giphy::searchStickers($q);
    }

    /**
     * Get Giphy trending gifs
     *
     * @return json
     */
    public function trending()
    {
        return Giphy::trending();
    }

    /**
     * Get Giphy sticker gifs
     *
     * @return json
     */
    public function stickers() 
    {
        return Giphy::stickers();
    }

}
