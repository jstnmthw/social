<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    
    /**
     *  Update notifications as read 
     *
     * @return json
     */
    public function markAllAsRead(Request $request)
    {
        if($request->ajax()) {
            Auth::user()->unreadNotifications->markAsRead();
            return json_encode(array('success'=>true));
        }else {
            return abort(404);
        }
    }

}
