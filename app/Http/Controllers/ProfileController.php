<?php

namespace App\Http\Controllers;

use Image;
use Cookie;
use Carbon\Carbon;
use App\User;
use App\Wave;
use App\Profile;
use App\Message;
use App\Rules\Text;
use App\Notifications\ProfileViewed;
use Igaster\LaravelCities\Geo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    /**
     * Validate incoming profile update request
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateProfileEdit(array $data)
    {

        return Validator::make($data, [
            'orientation' => 'required|integer',
            'height' => 'nullable|integer',
            'weight' => 'nullable|integer',
            'country' => 'required|integer',
            'region' => 'required|integer',
            'bio' => ['nullable', new Text],
            'instagram' => ['nullable', new Text],
            'facebook' => ['nullable', new Text],
            'twitter' => ['nullable', new Text],
        ]);

    }

    /**
     * Validate incoming profile search
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateSearch(array $data)
    {

        return Validator::make($data, [
            'country' => 'required|integer',
            'region' => 'required|integer',
            'age_from' => 'nullable|integer',
            'age_to' => 'nullable|integer',
            'distance' => 'required|integer'
        ]);

    }

    /**
     * Show user profile
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {

        // Check if user exists
        $user = User::where('username', $username)->firstOrFail();
        $profile = $user->profile;
        $auth = Auth::user();

        // Check owner's profile or anothers
        if($auth->id == $user->id){

            /**
             * User's own profile
             */

            // Set owner true
            $owner = true;

            // Get user's favorites
            $favorites = json_decode($user->favorites);

            // We need to grab each favorites user and set their data
            if($favorites){
                foreach ($favorites as $favorite) {
                    $fav_user           = User::findOrFail($favorite);
                    $fav_profile        = $fav_user->profile;
                    $fav_user->profile  = $fav_profile;

                    // Set favorite user data
                    $fav_user->profile->age = Carbon::parse($fav_user->profile->birthdate)->age;
                    $fav_user->last_active = Carbon::parse($fav_user->last_active)->diffForHumans();
                    $fav_user->profile->region = Geo::getName($fav_user->profile->region);
                    $favorite_users[] = $fav_user;
                }
                // Add to user's object
                $user->favorites = $favorite_users;
            }

        } else {

            /**
             * Viewing another user's profile
             */

            // Set owner false
            $owner = false;

            // If user is already blocked or has blocked you
            $you_blocked = json_decode(Auth::user()->blocked);
            if(!is_null($you_blocked)) {
                foreach ($you_blocked as $block) {
                    if($block == $user->id) {
                        return abort(404); // 404 the blocked users
                    }
                }
            }
            $blocked_you = json_decode($user->blocked);
            if(!is_null($blocked_you)) {
                foreach ($blocked_you as $block) {
                    if($block == Auth::user()->id) {
                        return abort(404); // 404 the blocked users
                    }
                }
            }

            // If user's account is disabled
            if($user->disabled == 1){
                return redirect()->route('browse');
            }

            // If user is not interested in auth's gender
            if($user->profile->sex != Auth::user()->profile->orientation){
                return redirect(route('browse'));
            }

            /**
             * Viewing another user's profile
             */

            // View profile notification
            if(Profile::throttleNotify($user->id))
            {
                $notify = (object)array(
                    'sender_id' => Auth::id(),
                    'sender' => Auth::user()->username,
                    'age' => Carbon::parse($profile->birthdate)->age,
                    'location' => Geo::getName($profile->region),
                    'photo' => Profile::getProfilePhoto(Auth::user()->username, true),
                    'date' => Carbon::now()->diffForHumans(),
                );
                $user->notify(new ProfileViewed($notify));
            }

            // Cannot use object in following Eloquent query
            $authID = $auth->id;
            $userID = $user->id;

            // Message Collections
            $msgs = Message::withTrashed()->where('user_id', $user->id)
                ->where('receiver_id', $authID)
                    ->orWhere(function ($query) use ($authID, $userID) {
                        $query->where('receiver_id', $userID)
                              ->where('user_id', $authID);
                })
                ->orderBy('created_at', 'DESC')
                ->paginate(15);

            foreach ($msgs as $msg) {
                if($msg->user_id == $authID){
                    $msg->owner = true;
                }else {
                    $msg->owner = false;
                }
            }

            // Mark msgs as read from user
            $user->messages()
                ->where('user_id', $userID)
                ->where('receiver_id', $authID)
                ->where('read', 0)
                ->update(['read' => 1]);

            // Mark notifications as read from user
            $notifications = Auth::user()->unreadNotifications;
            foreach ($notifications as $unread) {
                if($unread->data['sender'] == $user->username) {
                    $unread->update(['read_at' => Carbon::now()]);
                }
            }

            // Waves
            $user->profile->waved = Auth::user()->waves()->where('receiver_id', $userID)->first();

            // Favorited
            $user->favorited = (is_null(Auth::user()->favorites) ? false : in_array($user->id, json_decode(Auth::user()->favorites)));

        }

        /*
         * Set User's Data
         */

        // Set age
        $carbon = Carbon::parse($profile->birthdate);
        $profile->age = $carbon->age;

        // Set human readable last active
        $active = Carbon::parse($user->last_active);
        $user->last_active = $active->diffForHumans();

        // Get common name for country/region
        $profile->country = Geo::getCountryShortCode($profile->country);
        $profile->region = Geo::getName($profile->region);

        // Set gender/orientation
        $genders = Config::get('constants.genders');
        $profile->sex = $genders[$profile->sex];
        $profile->orientation = $genders[$profile->orientation].'s';

        // Set height
        if (isset($profile->height)){
            $inches = $profile->height/2.54; 
            $feet   = intval($inches/12); 
            $inches = $inches%12;
            $profile->height = $profile->height .'cm / '. $feet .'ft '. $inches .'in';
        }else {
            $profile->height = '--';
        }
        
        // Set weight
        if(isset($profile->weight)){
            $profile->weight = $profile->weight .'kgs / '. ceil($profile->weight / 0.45359237) .'lbs';
        }else {
            $profile->weight = '--';
        }

        // Return view
        return view('profile.index', [
            'messages' => (isset($msgs) ? $msgs : null),
            'owner' => $owner,
            'user' => $user,
            'profile' => $profile
        ]);

    }

    /**
     *  List user profiles
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        // Check for user prefs. or set users defaults
        $region     = (session('search.region')) ? session('search.region') : Auth::user()->profile->region;
        $age_from   = (session('search.age_from')) ? date('Y') - session('search.age_from').'-01-01' : null;
        $age_to     = (session('search.age_to')) ? date('Y') - session('search.age_to').'-01-01' : null;
        $distance   = (session('search.distance')) ? session('search.distance') : 0;

        // List profiles function
        $profiles = Profile::list(
            Auth::user()->profile->orientation,
            Auth::user()->profile->sex,
            $region,
            $distance,
            $age_from,
            $age_to
        );

        // Set age after pull
        foreach ($profiles as $profile) {
            $carbon = Carbon::parse($profile->birthdate);
            $profile->age = $carbon->age;
        }

        // List regions of selected country
        $regions = session('search.region') ? Geo::getRegions(session('search.country')) : Geo::getRegions(Auth::user()->profile->country);

        // Return view
        return view('browse', [
            'countries' => Geo::getCountries(),
            'regions'   => $regions,
            'distances' => Config::get('constants.search.distances.normal'),
            'profiles'  => $profiles,
        ]);

    }

    /**
     * Get profiles based on search user's criteria
     *
     * @param  object  $profile
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {

        // Validate request
        $this->validateSearch($request->all())->validate();

        // Change age to date
        $age_from = (isset($request->age_from) ? date('Y') - $request->age_from.'-01-01' : null);
        $age_to = (isset($request->age_to) ? date('Y') - $request->age_to.'-01-01' : null);

        // Save search criteria to session
        $request->session()->put('search.country', $request->country);
        $request->session()->put('search.region', $request->region);
        $request->session()->put('search.age_from', $request->age_from);
        $request->session()->put('search.age_to', $request->age_to);
        $request->session()->put('search.distance', $request->distance);

        // Return to browse with new session data
        return redirect('/browse');

    }

    /**
     * Waves Page
     */
    public function waves()
    {
        $user = Auth::user();
        $profiles = array();

        $waves = Wave::where('receiver_id', $user->id);

        if($waves){
            foreach ($waves as $wave){
                $wave_user = User::findOrFail($waves->receiver_id);

                $wave_user_bare = new \stdClass();
                $wave_user_bare->username = $wave_user->username;
                $wave_user_bare->last_active = Carbon::parse($wave_user->last_active)->diffForHumans();
                $wave_user_bare->age = Carbon::parse($wave_user->profile->birthdate)->age;
                $wave_user_bare->region = Geo::getRegion($wave_user->profile->region)->name;
                $wave_user_bare->country = Geo::getCountry($wave_user->profile->country)->country;
                $wave_user_bare->photo = Profile::getProfilePhoto($wave_user->username);

                $profiles[] = $wave_user_bare;
            }
        }else {
            $profiles = null;
        }

        // Return view
        return view('profile.waves', [
            'profiles'  => $profiles,
        ]);
    }

    /**
     * Show the form for editing the profile.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        return view('profile.edit', [
            'genders' => Config::get('constants.genders'),
            'countries' => Geo::getCountries(),
            'regions' => Geo::getRegions(Auth::user()->profile->country)
        ]);

    }

    /**
     * Update various profile fields
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $user = Auth::user();
        $profile = $user->profile;

        // Remove profile photo
        if($request->has('remove_profile_photo')) {
            $profile->profile_photo = null;
            $profile->save();

            return redirect(route('profile.edit'))->with('status', 'Your profile photo has been removed.');
        }

        // Update profile photo
        if($request->has('upload_profile_photo')) {
            $profile->profile_photo = $this->updateProfilePhoto($request);
            $profile->save();
            
            return redirect(route('profile.edit'))->with('status', 'Your profile photo has been updated.');
        }

        // Update photos to database
        if($request->has('upload_user_photo')) {
            $photos = json_decode($user->profile->photos);
            if(count($photos) < 5) { 
                $photos[] = $this->uploadUserPhoto($request);
                $photos = json_encode($photos);
                $profile->photos = $photos;
                $profile->save();
                return redirect('/'.$user->username);
            }
            return redirect('/'.$user->username)->with('photo_limit', 'You have reached the maximum photos allowed. Upgrade to Premium to unlock unlimited photos.'); 
        }

        // Update profile settings
        if($request->has('profile_settings')) {
            $this->validateProfileEdit($request->all())->validate();

            // $profile->sex           = $request->sex;
            $profile->orientation   = $request->orientation;
            $profile->height        = $request->height;
            $profile->weight        = $request->weight;
            $profile->country       = $request->country;
            $profile->region        = $request->region;
            $profile->bio           = $request->bio;
            $profile->instagram     = $request->instagram;
            $profile->facebook      = $request->facebook;
            $profile->twitter       = $request->twitter;

            $profile->save();

            return redirect(route('profile.edit'))->with('status', 'Your profile has been updated.');
        }

        // Delete user's uploaded photo
        if($request->has('delete_user_photo')) {
            
            $id     = $user->id;
            $photos = json_decode(Profile::getPhotos($id));
            $res    = array();

            foreach($photos as $photo) {
                if ($request->photo_id != $photo) {
                    $res[] = $photo;
                }
            }
            if(Profile::updatePhotos($id, $res)){
                return redirect('/'.$user->username)->with('status', 'Your photo has been removed.');
            }else {
                return redirect('/'.$user->username)->with('status', 'There was an error removing your photo.');
            }
        }

    }

    /**
     * Show complete profile form
     *
     * @return \Illuminate\Http\Response
     */
    public function showCompleteProfile()
    {

        if(Profile::profileCompleted(Auth::id())){
            return redirect('/browse');
        }else {
            return view('auth.register.profile', [
                'genders' => Config::get('constants.genders'),
                'countries' => Geo::getCountries()
            ]);
        }

    }

    /**
     * View first photo after registration
     *
     * @return \Illuminate\Http\Response
     */
    public function showCompletePhoto()
    {

        if(Profile::photoCompleted(Auth::id())){
            return redirect('/browse');
        }else {
            return view('auth.register.photo');
        }

    }

    /**
     * Update profile details after registration
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCompleteProfile(Request $request)
    {

        // Validate Request
        $this->validate($request, [
            'sex' => 'required|integer',
            'orientation' => 'required|integer',
            'dob_year' => 'required',
            'dob_month' => 'required',
            'dob_day' => 'required',
            'country' => 'required',
            'region' => 'required'
        ]);

        // Save profile data
        $user                   = User::find(Auth::id());

        $profile                = $user->profile()->first();
        $profile->sex           = request('sex');
        $profile->orientation   = request('orientation');
        $profile->birthdate     = request('dob_year').'-'.request('dob_month').'-'.request('dob_day');
        $profile->country       = request('country');
        $profile->region        = request('region');
        
        $profile->save();

        $user->registration_steps = 2;
        $user->save();

        // Redirect to app
        return redirect('/register/photo');

    }

    /**
     * Update first photo after registration
     *
     * @return \Illuminate\Http\Response
     */
    public function updateCompletePhoto(Request $request)
    {
        
        // Get user's details
        $user = User::find(Auth::id());
        $profile = $user->profile()->first();

        // Handle the user's upload of photo & avatar
        if($request->photo){
            $profile->profile_photo = $this->updateProfilePhoto($request);
            $profile->save();
            $user->registration_steps = 3;
            $user->save();
        }elseif($request->skip){        
            $user->registration_steps = 3;
            $user->save();
        }

        return redirect('/browse');

    }

    /**
     * Update user's profile photo
     *
     * @param request
     * @return string
     */
    private function updateProfilePhoto(Request $request)
    {

        $photo = $request->photo;
        // $avatar = $request->avatar;
        $time = time();
        $photo_fn = $time.'.jpg';
        $avatar_fn = $time.'_avatar.jpg';

        File::exists(public_path('uploads/'.$request->user()->username)) or 
        File::makeDirectory(public_path('uploads/'.$request->user()->username));

        Image::make($photo)->resize(null, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path('uploads/'.$request->user()->username.'/'.$photo_fn));
        Image::make($photo)->resize(null, 253, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path('uploads/'.$request->user()->username.'/'.$time.'_browse.jpg'));
        Image::make($photo)->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path('uploads/'.$request->user()->username.'/'.$time.'_avatar.jpg'));

        return $time;

    }

    /**
     * Upload user's photo
     *
     * @param request
     * @return string
     */
    public function uploadUserPhoto(Request $request) 
    {

        $photo = $request->photo;
        $time = time();
        $photo_fn = $time.'.jpg';

        File::exists(public_path('uploads/'.$request->user()->username)) or 
        File::makeDirectory(public_path('uploads/'.$request->user()->username));

        Image::make($photo)->resize(600, 1200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save(public_path('uploads/'.$request->user()->username.'/'.$photo_fn));

        return $time;

    }

    /**
     * Block user's profile
     *
     * @param request
     * @return \Illuminate\Http\Response
     */
    public function add_block(Request $request)
    {

        if(User::block($request->username)) {
            return redirect()->route('browse');
        }
        return abort(404);

    }

}
