<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use App\Rules\CurrentPassword;
use App\Notifications\UpdatePassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;

class UserController extends Controller
{    

    /**
     * Update user's account settings
     *
     * @return \Illuminate\Http\Response
     * @param request
     */
    public function update(Request $request)
    {

        // Auth'd User
        $user = Auth::user();


        // Unblock User
        if($request->has('unblock')) {
            User::unblock($request->unblock);
            return redirect()->route('user.settings');
        }

        // Temporarily disable account
        if($request->has('disable')) {
            if(User::disable()){
                Auth::logout();
                return redirect()->route('login')->with('status', 'You have temporarily disabled your account.');
            }
        }

        // Delete account
        if($request->has('delete')) {
            if(User::del_user()){
                Auth::logout();
                return redirect()->route('login')->with('success', 'You have permanently deleted your account.');
            }else {
                return redirect()->route('user.settings')->with('error', 'There was a problem deleting your account.');
            }
        }

        // Update user settings
        if($request->has('user_settings')) {

            // Validate Request
            // $this->validate($request, [
            //     'email' => 'required|string|email|max:255|unique:users'
            // ]);

            // if($user->email !== $request->email) {
            //     $user->email = $request->email;
            // }
            // $user->save();

            return redirect()->route('user.settings')->with('status', "You have updated your account settings.");
        }

        // Update notification settings
        if($request->has('notification_settings')) {

            // Notification checkboxes
            $request->has('msg_notification') ? $user->msg_notification = 1 : $user->msg_notification = 0;

            // Save to db
            $user->save();

            // Redirect with success msg
            return redirect()->route('user.notifications')->with('status', "You have updated your notification settings.");
        }

    }

    /**
     *  Get user's settings
     *
     * @return array
     */
    public function edit()
    {

        $user       = Auth::user();
        $blocked    = json_decode($user->blocked);
        
        if(!empty($blocked)) {
            foreach ($blocked as $key => $value) {
                $blocked_users[] = User::findOrFail($value,  $columns = array('username'));
            }
            $user->blocked = $blocked_users;
        }else {
            $user->blocked = array();
        }

        // Return view
        return view('user.edit', [
            'user' => $user,
        ]);

    }

    /**
     * Notification Settings
     */
    public function notifications()
    {
        $user = Auth::user();

        // Return view
        return view('user.notifications');
    }

    /**
     * Add favorite to account.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function favorite(Request $request)
    {

        if(User::favorite($request->username)) {
            return json_encode(array('success'=>true));
        }
        return json_encode(array('success'=>false));

    }

    /**
     * Remove favorite from account.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function unfavorite(Request $request)
    {

        if(User::unfavorite($request->username)) {
            return json_encode(array('success'=>true));
        }
        return json_encode(array('success'=>false));

    }

    /**
     * Change user password view
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        // Return view
        return view('user.password', [
            'user' => null,
        ]);
    }

    /**
     * Update users password
     *
     * @param Request $request
     * @return Response
     */
    public function update_password(Request $request)
    {

        // Validate the new password
        $request->validate([
            'current_password' => ['required', new CurrentPassword],
            'new_password' => 'required|string|min:6|confirmed'
        ]);

        // Update it
        $request->user()->fill([
            'password' => Hash::make($request->new_password)
        ])->save();

        // Send notification email
        $request->user()->notify(new UpdatePassword());

        // Redirect user
        return redirect()->route('user.password')->with('status', 'You have updated your password.');

    }

    /**
     *  Update user's activity
     *
     * @return boolean
     */
    public function active()
    {
        return User::active();
    }

    /**
     * Disabled account view
     *
     * @param  $Request
     * @return \Illuminate\Http\Response
     */
    public function disable()
    {
        // Return view
        return view('user.disabled');
    }

    /**
     * Enable user account
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function enable(Request $request)
    {
        if(User::enable()){
            return redirect()->route('browse')->with('status','Your account has been re-activated. Welcome back!');
        }else {
            return redirect()->route('user.disable')->with('error', 'There was a problem activating your account.');
        }
    }

}
