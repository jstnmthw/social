<?php

namespace App\Http\Controllers;

use App\Wave;
use App\User;
use App\Profile;
use Carbon\Carbon;
use Igaster\LaravelCities\Geo;
use App\Notifications\WaveSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WaveController extends Controller
{

    /**
     *  Insert user's wave to database & notify
     *
     *  @param \App\Profile  $profile
     *  @return \Illuminate\Http\Response
     */
    public function send(Request $request)
    {

        $user = $request->user();
        $receiver = User::find($request->receiver_id);
        $waves = $user->waves()->where('receiver_id', $request->receiver_id)->first();

        if(!$waves){

            $user->waves()->create(['receiver_id' => $request->receiver_id]);

            $notify = (object)array(
                'sender_id' => $user->id,
                'sender' => $user->username,
                'age' => Carbon::parse($user->profile->birthdate)->age,
                'location' => Geo::getName($user->profile->region),
                'photo' => Profile::getProfilePhoto($user->username, true)
            );
            $receiver->notify(new WaveSent($notify));

            return json_encode(array('success'=>true));
        }else {
            return json_encode(array('success'=>false));
        }

    }

}
