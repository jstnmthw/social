<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestController extends Controller
{

	/*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the front page along with login and 
    | registration links.
    |
    */
    public function index()
    {
    	return view('welcome');
    }

    /*
    |--------------------------------------------------------------------------
    | Terms Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles terms of service view.
    |
    */
    public function terms()
    {
    	return view('legal/terms');
    }

}
