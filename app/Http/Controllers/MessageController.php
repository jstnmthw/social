<?php

namespace App\Http\Controllers;

use Image;
use App\User;
use App\Profile;
use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Igaster\LaravelCities\Geo;
use App\Notifications\MessageSent;
use App\Notifications\MessageIncoming;
use App\Notifications\CustomDbChannel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class MessageController extends Controller
{
    
    /**
     *  User's message inbox
     *
     * @return Illuminate\Http\Request
     */
    public function inbox()
    {

        $user = Auth::user();

        // Chained Queries
        $msgs = Message::where('receiver_id', $user->id);
        if($user->blocked){
            $msgs = $msgs->whereNotIn('user_id', json_decode($user->blocked));
        }
        if($user->blocked_me){
            $msgs = $msgs->whereNotIn('user_id', json_decode($user->blocked_me));
        }
        $msgs = $msgs->where('trashed', 0)->orderBy('read', 'DESC')->get()->sortByDesc('created_at')->unique('user_id');

        foreach ($msgs as $msg) {
            $msg->sender = User::find($msg->user_id);
            $msg->sender->profile = $msg->sender->profile;
            $msg->replied = Message::is_replied($msg->user_id);
            if($replied = Message::is_replied($msg->user_id)) {
                $msg->body = $replied->body;
                $msg->created_at = $replied->created_at;
            }
        }

        return view('messages.inbox', [
            'msgs' => $msgs
        ]);
    }

    /**
     * Inbox Actions (trash, perm delete, mark unread)
     *
     * @param null
     * @return Illuminate\Http\Request
     */
    public function update(Request $request)
    {

        // Auth'd user
        $user = Auth::user();

        // Mark messages as read
        if($request->has('mark') && $request->has('msg_ids')){
            foreach ($request->msg_ids as $id) {
                $msg = Message::findOrFail($id);
                $msg->read = 1;
                $msg->save();
            }
        }

        // Send messages to trash
        if($request->has('trash') && $request->has('msg_ids'))
        {
            foreach ($request->msg_ids as $id) {
                $msg = Message::findOrFail($id);
                $sender = $msg->user_id;
                $delete = Message::where('receiver_id', $user->id)->where('user_id', $sender)->update(['trashed' => true]);
            }
        }

        // Untrash messages
        if($request->has('untrash') && $request->has('msg_ids'))
        {
            foreach ($request->msg_ids as $id) {
                Message::where('id', $id)->update(['trashed' => 0]);
            }
        }

        // Trash sent messages
        if($request->has('delete_sent') && $request->has('msg_ids'))
        {
            foreach ($request->msg_ids as $id) {
                $msg = Message::findOrFail($id);
                $receiver = $msg->receiver_id;
                $sent = Message::where('user_id', $user->id)->where('receiver_id', $receiver)->update(['sent_deleted' => 1]);
            }
        }

        // (Soft) Delete messages
        if($request->has('delete') && $request->has('msg_ids'))
        {
            foreach ($request->msg_ids as $id) {
                $msg = Message::findOrFail($id);
                $sender = $msg->user_id;
                $delete = Message::where('receiver_id', $user->id)->where('user_id', $sender)->delete();
            }
        }

        // Return
        return back();

    }

    /**
     *  User's message inbox
     *
     * @return Illuminate\Http\Response
     */
    public function sent()
    {
        $user = Auth::user();
        $msgs = $user->messages()->where('sent_deleted', 0);
        if($user->blocked){
            $msgs = $msgs->whereNotIn('receiver_id', json_decode($user->blocked));
        }
        if($user->blocked_me){
            $msgs = $msgs->whereNotIn('receiver_id', json_decode($user->blocked_me));
        }
        $msgs = $msgs->get();
        foreach ($msgs as $msg) {
            $msg->receiver = User::find($msg->receiver_id);
            $msg->receiver->profile = $msg->receiver->profile;
        }

        $msgs = $msgs->sortByDesc('created_at')->unique('receiver_id'); 

        return view('messages.sent', [
            'msgs' => $msgs
        ]);

    }

    /**
     * User's trashed messages
     *
     * @return Illuminate\Http\Response
     */
    public function trashed()
    {

        $user = Auth::user();
        $msgs = Message::where('receiver_id', $user->id)->where('trashed', true)->get();

        foreach ($msgs as $msg) {
            $msg->sender = User::find($msg->user_id);
            $msg->sender->profile = $msg->sender->profile;
        }

        $msgs = $msgs->sortByDesc('created_at')->unique('user_id'); 

        return view('messages.trash', [
            'msgs' => $msgs
        ]);
    }

    /**
     *  Send Message (AJAX)
     *
     * @return Illuminate\Httpd\Request
     */
    public function send(Request $request)
    {

        // Photo upload
        if($request->file('photo')){

            $photo = $request->file('photo')->getRealPath();
            $time = time();
            $photo_fn = $time.'_upload.jpg';

            File::exists(public_path('uploads/'.$request->user()->username)) or 
            File::makeDirectory(public_path('uploads/'.$request->user()->username));

            Image::make($photo)->resize(null, 900, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save(public_path('uploads/'.$request->user()->username.'/'.$photo_fn));

            $request->body = 'uploads/'.$request->user()->username.'/'.$photo_fn;

        }

        // Database
        $throttle = Message::throttleNotify($request->receiver_id);
        $user = $request->user();
        $profile = $user->profile;
        $msg = $user->messages()->create([
            'receiver_id' => (int)$request->receiver_id,
            'body' => $request->body,
            'type' => (int)$request->type,
            'read' => (int)0,
        ]);

        // Notification
        $notification = (object)array(
            'sender_id' => $user->id,
            'sender'    => $user->username,
            'type'      => (int) $request->type,
            'date'      => $msg->created_at->diffForHumans(),
            'photo'     => Profile::getProfilePhoto($user->username, true),
            'age'       => Carbon::parse($profile->birthdate)->age,
            'location'  => Geo::getName($profile->region),
            'body'      => $msg->body,
        );
        $receiver = User::find($request->receiver_id);
        if($throttle) {
            $receiver->notify(new MessageSent($notification));
        }
        $receiver->notify(new MessageIncoming($notification));

        // Return object
        return json_encode(array(
            'body' => $msg->body,
            'type' => $msg->type,
            'date' => $msg->created_at->diffForHumans(),
        ));

    }

}
