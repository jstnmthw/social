<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{    
	/*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the front page along with login and 
    | registration links.
    |
    */
    public function index()
    {
    	return view('welcome');
    }

}
