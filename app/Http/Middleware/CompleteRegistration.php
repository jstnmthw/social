<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CompleteRegistration
{
    /**
     * Redirect user if profile is not complete
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $path = $request->path();
        if(Auth::user()){
            $step = Auth::user()->registration_steps;
            switch ($step) {
                case 1:
                    if($path != 'register/profile'){
                        return redirect('register/profile');
                    }
                    break;
                case 2:
                    if($path != 'register/photo') {
                        return redirect('register/photo');
                    }
                    break;
            }
        }
        return $next($request);
    }
}
