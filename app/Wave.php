<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wave extends Model
{
	protected $fillable = [
        'receiver_id',
        'sender'
	];

    /**
     * Belongs to User model
     *
     * @return object
     */
    public function User()
    {
        return $this->belongsTo('App\User');
    }
    
}
