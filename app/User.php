<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'notifications.'.$this->username;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'registration_steps',
        'last_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Get the profile record associated with the user.
     */
    public function Profile()
    {
        return $this->hasOne('App\Profile');
    }

    /**
     * Get message records associated with the user.
     */
    public function Messages()
    {
        return $this->hasMany('App\Message');
    }

    /**
     * Get wave records associated with the user.
     */
    public function Waves()
    {
        return $this->hasMany('App\Wave');
    }

    /**
     * Update user active status
     *
     * @return boolean
     */
    public static function active()
    {
        $user = static::where('id', '=', Auth::id())->first();
        $user->last_active = date('Y-m-d G:i:s');
        $user->save();
    }

    /**
     * Count user's unread notifications
     *
     * @return integer
     */
    public static function unreadNotificationsCount($id)
    {
        $id = (int) $id;
        $user = static::findOrFail($id);
        $unread = $user->unreadNotifications->unique()->count();
        return $unread;
    }

    /**
     * Add to user's favorites
     *
     * @param string
     * @return json
     */
    public static function favorite($username)
    {

        $target     = static::where('username', $username)->firstOrFail();
        $user       = Auth::user();
        $favorites  = json_decode($user->favorites);

        if(!is_null($favorites) && in_array($target->id, $favorites)) {
            return false;
        }

        $favorites[] = $target->id;
        $user->favorites = json_encode($favorites);
        $user->save();

        return true;

    }

    /**
     * Remove from user's favorites
     *
     * @param request
     * @return boolean
     */
    public static function unfavorite($username)
    {

        $target     = static::where('username', $username)->firstOrFail();
        $user       = Auth::user();
        $favorites  = json_decode($user->favorites);

        if(!is_null($favorites) && !in_array($target->id, $favorites)) {
            return false;
        }

        $pos = array_search($target->id, $favorites);
        unset($favorites[$pos]);

        if(!empty($favorites)){
            $reset = array_values($favorites);
            $user->favorites = json_encode($reset);
        }else {
            $user->favorites = null;
        }

        $user->save();
        return true;

    }

    /**
     * Block User
     *
     * @param string
     * @return boolean
     */
    public static function block($username)
    {

        $user = Auth::user();
        $blocked_user = static::where('username', $username)->firstOrFail();

        // Add to `blocked` field
        $user_blocks = json_decode($user->blocked);
        $user_blocks[] = $blocked_user->id;
        $user->blocked = json_encode(array_unique($user_blocks));
        $user->save();

        // Add to their `blocked_me` field
        $their_blocked = json_decode($blocked_user->blocked_me);
        $their_blocked[] = $user->id;
        $blocked_user->blocked_me = json_encode(array_unique($their_blocked));
        $blocked_user->save();

        return true;

    }

    /**
     * Unblock User
     *
     * @param string
     * @return boolean
     */
    public static function unblock($username)
    {

        $user = Auth::user();
        $unblock_user = static::where('username', $username)->firstOrFail();

        $blocked_users = json_decode($user->blocked);
        $their_blocked = json_decode($unblock_user->blocked_me);

        // Unset in `blocked` field
        $blocked_pos = array_search($unblock_user->id, $blocked_users);
        unset($blocked_users[$blocked_pos]);

        // Unset in their `blocked_me` field
        $their_blocked_pos = array_search($user->id, $their_blocked);
        unset($their_blocked[$their_blocked_pos]);

        $user->blocked = (!empty($blocked_users)) ? json_encode($blocked_users) : null;
        $unblock_user->blocked_me = (!empty($their_blocked)) ? json_encode($their_blocked) : null;

        $user->save();
        $unblock_user->save();
        
        return true;

    }

    /**
     *  Enable user account
     */
    public static function enable()
    {
        $user = Auth::user();
        $user->disabled = 0;
        if($user->save()){
            return true;
        }
        return false;
    }

    /**
     * Disable user account
     */
    public static function disable()
    {
        $user = Auth::user();
        $user->disabled = 1;
        if($user->save()){
            return true;
        }
        return false;
    }

    /**
     * Delete user account
     * @param null
     * @return boolean
     */
    public static function del_user()
    {
        if(Auth::check()){
            $user = Auth::user();
            $user->email = $user->email.'_deleted';
            return $user->delete();
        }
    }

}
