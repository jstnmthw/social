<?php

namespace App;

use Carbon\Carbon;
use Igaster\LaravelCities\Geo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Profile extends Model
{

	protected $fillable = [
        'orientation',
        'country',
        'region',
        'dob_month',
        'dob_day',
        'dob_year',
        'birthdate',
        'bio',
        'instagram',
        'facebook',
        'twitter',
        'photo',
        'profile_photo',
        'avatar',
        'save',
        'skip',
        'age_from',
        'age_to',
        'distance',
        'receiver_id',
        'username'
    ];

    /**
     * Belongs to User model
     *
     * @return object
     */
    public function User()
    {
        return $this->belongsTo('App\User');
    }

    /**
     *  List user profiles
     *
     * @param $sex
     * @param $orientation
     * @param $region
     * @param $distance
     * @param $age_from
     * @param $age_to
     * @return array
     */
    public static function list($sex, $orientation, $region, $distance, $age_from = null, $age_to = null){

        // Get region's cords
        $region     = Geo::getRegion($region);
        $lat        = $region->lat;
        $long       = $region->long;
        $blocked    = json_decode(Auth::user()->blocked);
        $blocked_me = json_decode(Auth::user()->blocked_me);
        $distances  = Config::get('constants.search.distances.normal');

        // Our sexy optimized search query
        $profiles = DB::table(
            DB::raw('
                profiles
                JOIN (
                    SELECT geo.id,
                           geo.lat, geo.long,
                           p.distance_unit
                           * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                           * COS(RADIANS(geo.lat))
                           * COS(RADIANS(p.longpoint) - RADIANS(geo.long))
                           + SIN(RADIANS(p.latpoint))
                           * SIN(RADIANS(geo.lat)))) AS distance
                      FROM geo AS geo
                      JOIN (
                            SELECT 
                            '.$lat.' AS latpoint,  /* Latitude */
                            '.$long.' AS longpoint, /* Longitude */
                            '.$distances[$distance].' AS radius, /* Radius */
                            111.045 AS distance_unit /* Kilometers */
                          ) AS p
                      WHERE geo.lat
                         BETWEEN p.latpoint  - (p.radius / p.distance_unit)
                             AND p.latpoint  + (p.radius / p.distance_unit)
                        AND geo.long
                         BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
                             AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
                      ORDER BY distance
                ) AS geo
                JOIN users ON users.id = profiles.user_id
            ')
        )
        ->where(DB::raw('profiles.region'), '=', DB::raw('geo.id'))
        ->where('orientation', $orientation)
        ->where('sex', $sex)
        ->where('disabled', 0)
        ->when($age_from, function ($query) use ($age_from) {
            return $query->whereDate('birthdate', '<=', $age_from);
        })
        ->when($age_to, function ($query) use ($age_to) {
            return $query->whereDate('birthdate', '>=', $age_to);
        })
        ->when($blocked, function ($query) use ($blocked) {
            return $query->whereNotIn('profiles.user_id', $blocked);
        })
        ->when($blocked_me, function ($query) use ($blocked_me) {
            return $query->whereNotIn('profiles.user_id', $blocked_me);
        })
        ->whereNotIn('profiles.user_id', [Auth::id()])
        ->orderby('last_active', 'DESC')
        ->orderby('distance', 'ASC')
        ->paginate(20);

        foreach ($profiles as $profile) {
            // Human readable last active
            $active = new Carbon($profile->last_active);
            $profile->last_active = $active->diffForHumans();
        }

        return $profiles;

    }

    /**
     * Check if new profile is completed
     *
     * @param $id
     * @return boolean
     */
    public static function profileCompleted($id)
    {
        $step = User::find($id)->registration_steps;
        return ($step > 1) ? true : false;
    }

    /**
     * Check if profile photo is uploaded
     *
     * @param $id
     * @return boolean
     */
    public static function photoCompleted($id)
    {
        $step = User::find($id)->registration_steps;
        return ($step > 2) ? true : false;
    }

    /**
     * Grab user's profile photo and path
     *
     * @param username
     * @param avatar
     * @return string
     */ 
    public static function getProfilePhoto($username, $avatar = false) 
    {
        $user = User::where('username', $username)->firstOrFail();
        if(is_null($user->profile->profile_photo)) {
            return '/imgs/default_profile' . ($avatar == true ? '-40x40' : '');
        }else {
            return '/uploads/' . $user->username .'/'. $user->profile->profile_photo . ($avatar == true ? '_avatar' : '');
        }
    }

    /** 
     * Get photo list
     * @param id
     * @return json
     */
    public static function getPhotos($id) {
        $user = User::where('id', $id)->firstOrFail();
        return $user->profile->photos;
    }

    /**
     * Update photos
     * @param id 
     * @param arr
     * @return boolean
     */
    public static function updatePhotos($id, $arr) {
        if(Profile::where('user_id', $id)->update(['photos' => json_encode($arr)])) {
            return true;
        }
        return false;
    }

    /**
     * Throttle profile notifications
     *
     * @param integer
     * @return boolean
     */
    public static function throttleNotify($id)
    {
        $id = (int) $id;
        $user = User::findOrFail($id);
        $last_profile_view = $user->notifications->first();
        if($last_profile_view) {
            $delay = $last_profile_view->created_at->addMinutes(3);
            $now = Carbon::now();
            return $now->gte($delay);
        } else {
            return true;
        }
    }

}
